#!/bin/bash

function gentempfile {
  filepath=$2 
  lang=$1
  local tempdir=$(mktemp -d /tmp/test.XXX)
  local output="${tempdir}/${lang}.txt"
  #jq  -r '[paths | join(".")]' public/locales/en/translations.json
  jq  -r '[paths | join(".")]' "${filepath}" > "${output}"
  echo $output
}

x=$(gentempfile "es" "public/locales/es/translations.json")
y=$(gentempfile "en" "public/locales/en/translations.json")

echo $x
echo $y

diff $x $y
