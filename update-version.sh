#!/bin/bash
# This script will fill the package.json version:

# 1.0.[BUILD_NUMBER] for master branch
# @TODO 1.0.0-[branch].[BUILD_NUMBER] for feature branches

CI_COMMIT_REF_NAME="master" 
echo "CODEBUILD_BUILD_NUMBER=${CODEBUILD_BUILD_NUMBER}"
echo "CODEBUILD_RESOLVED_SOURCE_VERSION=${CODEBUILD_RESOLVED_SOURCE_VERSION}"

if [ `basename ${CI_COMMIT_REF_NAME}` == "master" ]; then
 PKG_BUILD_NUMBER=`jq -r '.version' ./package.json |cut -d. -f1-2`".$CODEBUILD_BUILD_NUMBER"
 PKG_NAME=`jq -r '.name' ./package.json`
else
 BRANCH_ID=`basename ${CI_COMMIT_REF_NAME//_/-}`
 PKG_BUILD_NUMBER=`jq -r '.version' ./package.json`-$BRANCH_ID.$CODEBUILD_BUILD_NUMBER
fi

jq --arg version "$PKG_BUILD_NUMBER" --arg branch_name "$CI_COMMIT_REF_NAME" --arg gitlab_build_num "$CODEBUILD_BUILD_NUMBER" --arg git_revision_sha1 "$CODEBUILD_RESOLVED_SOURCE_VERSION" '(.version)=$version | (.branch_name)=$branch_name | (.build_number)=$gitlab_build_num | (.git_revision_sha1)=$git_revision_sha1' package.json > package.json.tmp
rm -f ./package.json && mv package.json.tmp package.json