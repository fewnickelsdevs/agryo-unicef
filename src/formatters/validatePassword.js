const validatePassword = str => {
  // at least one number, one lowercase and one uppercase letter
  // at least 9 characters
  var re = /^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#=+_()/[\?!@$%^&*-]).{8,}$/
  return re.test(str)
}

export default validatePassword
