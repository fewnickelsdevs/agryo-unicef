const formatChartPayload = payload => [
  {
    label: 'Landsat Sentinel',
    data: payload.graphs_data.landsatsentinel[0]
  },
  {
    label: 'Mod13q1',
    data: payload.mod13q1.mod13q1[0]
  },
  {
    label: 'soil moisure',
    data: payload.soil_moisture.soil_moisture[0]
  },
  {
    label: "Carbon",
    data: payload.carbon || false
  }
]

export default formatChartPayload
