export const removeDecimals = value => {
  if (value === 0 || value === '0') {
    return '0'
  }
  const withoutDecimals = value.slice(0, -16)

  return withoutDecimals
}



