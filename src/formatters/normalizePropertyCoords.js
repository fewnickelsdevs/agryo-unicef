const normalizePropertyCoords = ([previousCoords]) => {
  const coords =
    previousCoords.length === 1 ? previousCoords[0] : previousCoords

  const withCorrectLatLng = coords.map(coords => coords.slice().reverse())

  return withCorrectLatLng
}

export default normalizePropertyCoords
