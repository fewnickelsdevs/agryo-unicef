import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  *,
  *::after,
  *::before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
    font-family: 'Montserrat', Arial, sans-serif !important;
  }
  
  html {
    box-sizing: border-box;
  }

  body {
    /* Material UI body2 variant */
    font-weight: 400;
    font-size: 0.875rem;
    line-height: 1.43;
    letter-spacing: 0.01071em;
  }
`

export default GlobalStyle
