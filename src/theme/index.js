const generateMediaQuery = breakpoint =>
  `@media only screen and (max-width: ${breakpoint})`

export const breakpoints = {
  xs: generateMediaQuery('600px'),
  sm: generateMediaQuery('960px'),
  md: generateMediaQuery('1280px'),
  lg: generateMediaQuery('1920px')
}

export const colors = {
  primary: '#3F8C48',
  primaryLight: '#49C16E',
  white: '#fff',
  background: '#fafafa'
}
