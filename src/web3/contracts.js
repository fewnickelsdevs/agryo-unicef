import CostingCropsABI from '../contracts/abi/CostingCropsABI.json'
import CostingLivestockABI from '../contracts/abi/CostingLivestockABI.json'
import GreenBondABI from '../contracts/abi/GreenBondABI.json'
import ERC20ABI from '../contracts/abi/TDRABI.json'
import AuthContractABI from '../contracts/abi/AuthContractABI.json'
import currency from 'currency.js'
import FaucetABI from '../contracts/abi/FaucetABI.json'

const generateContractInstance = (contractAbi, contractAddress) =>
  new Promise(async (resolve, reject) => {
    try {
      const { web3 } = window
      const contract = new web3.eth.Contract(contractAbi, contractAddress)
      resolve(contract)
    } catch (err) {
      console.log(err)
      reject(err)
    }
})

export const generateFaucetContract = () => {
    return generateContractInstance(FaucetABI, process.env.REACT_APP_FAUCET_CONTRACT_ADDRESS)
}

export const generateCostingLivestockContract = () => {
    return generateContractInstance(CostingLivestockABI, process.env.REACT_APP_LIVESTOCK_CONTRACT_ADDRESS)
}

export const generateGreenBondContract = () => {
    return generateContractInstance(GreenBondABI, process.env.REACT_APP_GREEN_BOND_CONTRACT_ADDRESS)
}

export const generateCostingCropsContract = () => {
    return generateContractInstance(CostingCropsABI, process.env.REACT_APP_CROPS_CONTRACT_ADDRESS)
}

export const generateAgryoUsdContract = () => {
    return generateContractInstance(ERC20ABI, process.env.REACT_APP_AGRYO_USD_CONTRACT_ADDRESS)
}


export const getErc20AddressBalance = async (userAddress, contractAddress) => {
    const agryoStableContract = await generateAgryoUsdContract(ERC20ABI, contractAddress)
    const rawBalance = await agryoStableContract.methods.balanceOf(userAddress).call()
    const balance = await removeDecimals(rawBalance)  
    return balance
}

export const getErc20AllowBalance = async (userAddress, spenderAddress, contractAddress) => {
    const agryoStableContract = await generateAgryoUsdContract(ERC20ABI, contractAddress)
    const rawBalance = await agryoStableContract.methods.allowance(userAddress, spenderAddress).call()
    console.log("rawBalance", rawBalance)
    const balance = await removeDecimals(rawBalance, userAddress, spenderAddress, contractAddress)  
    return balance
}

export const getEtherBalance = async(address) => {
    const { web3 } = window
    const weiBalance = await web3.eth.getBalance(address)
    const etherBalance = web3.utils.fromWei(weiBalance, 'ether')
    return etherBalance
}
    


export const etherFormatFromValue = value => {
    const intValue = currency(value).intValue
    console.log("INT AMOUNT", value, intValue)
    return `${intValue}0000000000000000`
  }
  
  export const removeDecimals = (value) => {
    if (value === 0 || value === '0') {
      return '0'
    }
    const withoutDecimals = value.slice(0, -16)
  
    return withoutDecimals
  }
  
  