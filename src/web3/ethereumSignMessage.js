import { fromUnixTime } from 'date-fns/esm';

const sigUtil = require('eth-sig-util')

  const signTypedDatav3Wallet = async (web3Instance, data) => new Promise(async (resolve, reject) => {
    try {
      const dataString = await JSON.stringify(data)
      console.log("data string", dataString)
      const accounts = await web3Instance.eth.getAccounts()
      web3Instance.currentProvider.sendAsync({
        method: 'eth_signTypedData_v3',
        params: [accounts[0], dataString],
        from: accounts[0]
      },
      async (error, res) => {
        if (error || !res.result) {
          reject({ err: 'Denied Sign', error })
        } else {
            const recovered = await sigUtil.recoverTypedSignature({
                data,
                sig: res.result
            })
          console.log("SIGNATURE: ", res.result)
          console.log("RECOVER ADDRESS: ", recovered)
          resolve(res.result.substring(2))
        }
      })
    } catch (err) {
      reject(err)
    }
  })


  const generateLoginObjectEip712Domain = challengeToken => {
    const domain = [
        { name: "name", type: "string" },
        { name: "version", type: "string" },
        { name: "chainId", type: "uint256" },
        // { name: "verifyingContract", type: "address" },
        { name: "salt", type: "string" }
    ];

  
    const message = [{ name: 'token', type: 'string' }, { name: 'action', type: 'string' }]
  
    const domainData = {
      name: "Agryo",
      version: "1",
      chainId: 4,
      salt: "saltDev0x"
    }
  
    const messageData = {
        token: challengeToken,
        action: "Login"
    }
  
    const challengeData = {
      types: {
        EIP712Domain: domain,
        LoginObject: message
      },
      primaryType: 'LoginObject',
      domain: domainData,
      message: messageData
    }
  
    return challengeData
  }
  

  const generateRegisterObjectEip712Domain = (name, email, address, userType) => {
    const domain = [
        { name: "name", type: "string" },
        { name: "version", type: "string" },
        { name: "chainId", type: "uint256" },
        // { name: "verifyingContract", type: "address" },
        { name: "salt", type: "string" }
    ];

  
    const message = [
      { name: 'name', type: 'string' }, 
      { name: 'email', type: 'string' },
      { name: 'address', type: 'address' },
      { name: 'userType', type: 'string' }
    ]
  
    const domainData = {
      name: "Agryo",
      version: "1",
      chainId: 4,
      salt: "saltDev0x"
    }
  
    const messageData = {
        name, email, address, userType
    }
  
    const challengeData = {
      types: {
        EIP712Domain: domain,
        RegisterObject: message
      },
      primaryType: 'RegisterObject',
      domain: domainData,
      message: messageData
    }
  
    return challengeData
  }
  

  export {
    signTypedDatav3Wallet,
    generateLoginObjectEip712Domain,
    generateRegisterObjectEip712Domain
  }