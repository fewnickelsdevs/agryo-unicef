import React from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import {
  TableContainer,
  TableHead,
  TableHeadCell,
  TableRow,
  TableBody,
  TableCell
} from '..'

const Container = styled.section`
  padding: 1rem;
`

const BottomGrid = styled.div`
  display: grid;
  grid-gap: 1rem;
  margin-top: 1rem;
  grid-template-columns: repeat(2, 1fr);
`

const DefaultTable = () => (
  <TableContainer>
    <TableHead>
      <TableRow>
        <TableHeadCell>Heading 1</TableHeadCell>
        <TableHeadCell>Heading 2</TableHeadCell>
      </TableRow>
    </TableHead>

    <TableBody>
      <TableRow>
        <TableCell>Item 1</TableCell>
        <TableCell>Item 2</TableCell>
      </TableRow>
    </TableBody>
  </TableContainer>
)

const FullTable = () => (
  <Container>
    <DefaultTable></DefaultTable>

    <BottomGrid>
      <DefaultTable></DefaultTable>
      <DefaultTable></DefaultTable>
    </BottomGrid>
  </Container>
)

storiesOf('Table', module).add('Full Table', () => <FullTable />)
