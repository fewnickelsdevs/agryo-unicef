import React from 'react'
import styled from 'styled-components'

const StyledTableCell = styled.td`
  ${({ small, align }) => `
    padding: 1rem;
    text-align: ${align};
    
    ${small && 'padding: .5rem 1rem;'}
  `}
`

const TableCell = ({ children, className, small, align }) => (
  <StyledTableCell className={className} small={small} align={align}>
    {children}
  </StyledTableCell>
)

export default TableCell
