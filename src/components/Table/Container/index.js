import React from 'react'
import styled from 'styled-components'

const StyledTable = styled.table.attrs({
  cellSpacing: 0,
  cellPadding: 0
})`
  width: 100%;
  border-radius: 4px;
  border: 1px solid #ccc;
`

const TableContainer = ({ children, className }) => (
  <StyledTable className={className}>{children}</StyledTable>
)

export default TableContainer
