import styled from 'styled-components'

export const TableDev = styled.table`
  width: 100%;
  padding: 1rem;
  border-collapse: collapse;
  background: #fff;
  border-radius: 5px;
  margin-top: 10px;
`

export const TableRowDev = styled.tr`
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  &:hover {
    background-color: #eaeeeb8f;
  }
`

export const TableRowTitleDev = styled.tr`
  border-bottom: 1px solid rgba(224, 224, 224, 1);
`

export const TableItemTitleDev = styled.td`
  padding: 16px;
  font-weight: bold;
  text-transform: uppercase;
  color: #808080d6;
  letter-spacing: 1.2px
    ${({ align }) => `
text-align: ${align}
`};
`

export const TableItemDev = styled.td`
  ${({ align }) => `
padding: 16px;
text-align: ${align}
`}
`

export const HeadingDev = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`
