import React from 'react'
import styled from 'styled-components'

const StyledTableBody = styled.tbody``

const TableBody = ({ children, className }) => (
  <StyledTableBody className={className}>{children}</StyledTableBody>
)

export default TableBody
