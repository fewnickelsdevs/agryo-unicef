import React from 'react'
import styled from 'styled-components'

const StyledTableRow = styled.tr`
  ${({ striped, theme }) =>
    striped &&
    `
    &:nth-of-type(odd) {
      background-color: ${theme.colors.background};
    }
  `}
`

const TableRow = ({ children, className, striped }) => (
  <StyledTableRow className={className} striped={striped}>
    {children}
  </StyledTableRow>
)

export default TableRow
