import React from 'react'
import styled from 'styled-components'

const StyledTableHead = styled.thead``

const StyledTableHeadCell = styled.td`
  ${({ theme }) => `
    &:first-of-type {
      border-top-left-radius: 4px;
    }

    &:last-of-type {
      border-top-right-radius: 4px;
    }

    padding: 1rem;
    text-transform: uppercase;
    color: ${theme.colors.white};
    background-color: ${theme.colors.primary};
  `}
`

const TableHead = ({ children, className }) => (
  <StyledTableHead className={className}>{children}</StyledTableHead>
)

const TableHeadCell = ({ children, className }) => (
  <StyledTableHeadCell className={className}>{children}</StyledTableHeadCell>
)

export { TableHead, TableHeadCell }
