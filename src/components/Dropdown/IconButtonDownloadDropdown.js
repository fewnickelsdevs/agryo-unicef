import React, { useState } from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons'

const Container = styled.div`
  color: #000;
  display: inline-block;
  position: relative;
  float: right;
`

const ALink = styled.a`
  text-decoration: none !important;
`

const OptionsContainer = styled.div(
  ({ visible }) => `
  display: ${visible ? 'block' : 'none'};

  right: 0;
  top: 2.5rem;
  background: #fff;
  border-radius: 3px;
  position: absolute;
  width: max-content;
  border: 1px solid #ccc;
  z-index: 1000000;
`
)

const OptionsList = styled.ul`
  list-style: none;
`

const Option = styled.li`
  cursor: pointer;
  transition: 0.1s all;
  padding: 0.5rem 0.8rem;

  &:hover {
    background: rgba(0, 0, 0, 0.07);
  }

  &:active {
    background: rgba(0, 0, 0, 0.1);
  }
`

const Hoverable = styled.div`
  width: 100%;
  height: 0.5rem;
  background: transparent;
  position: absolute;
`

const Icon = styled(FontAwesomeIcon)`
  color: grey;
  height: 2.65rem;
  padding: 0.6rem;
  border-radius: 50%;
  margin-left: 10px;
  width: 2.65rem !important;
  &:hover {
    background-color: #0080001a;
  }
`
const IconButtonDownloadDropdown = ({ options }) => {
  const [isVisible, setVisibility] = useState(false)

  const toggleVisibility = () => setVisibility(!isVisible)

  return (
    <Container onMouseEnter={toggleVisibility} onMouseLeave={toggleVisibility}>
      <Icon icon={faEllipsisV} />

      <Hoverable />

      <OptionsContainer visible={isVisible}>
        <OptionsList>
          {options.map(option => (
            <ALink href={option.url} target='_blank' rel='noopener noreferrer'>
              <Option key={option.label}>{option.label}</Option>
            </ALink>
          ))}
        </OptionsList>
      </OptionsContainer>
    </Container>
  )
}

export default IconButtonDownloadDropdown
