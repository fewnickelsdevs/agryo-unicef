import React, { useState } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Container = styled.div`
  color: #000;
  display: inline-block;
  position: relative;
`
const OptionsContainer = styled.div(
  ({ visible }) => `
  display: ${visible ? 'block' : 'none'};
  position: absolute;
  width: 120px;
  padding: 15px 0 0 0;
`
)

const OptionsList = styled.ul`
  list-style: none;
  background: #ffffff;
  border-radius: 10px;
  border: 1px solid #ccc;
`

const Option = styled.li.attrs({})`
  cursor: pointer;
  transition: 0.1s all;
  padding: 0.5rem 1rem;
  display: flex;
`
const OptionLabel = styled.p`
  text-align: left;
  padding-left: 10px;
  ${Option}:hover & {
    color: #2f8c4e;
  }
`

const FontIcon = styled(FontAwesomeIcon).attrs({})`
  margin-top: 3px;
  ${Option}:hover & {
    color: #2f8c4e;
  }
`

const Dropdown = ({ title, options }) => {
  const [isVisible, setVisibility] = useState(false)
  const toggleVisibility = () => setVisibility(!isVisible)

  return (
    <Container onMouseEnter={toggleVisibility} onMouseLeave={toggleVisibility}>
      <div style={{ display: 'inline-block' }}>{title}</div>
      <OptionsContainer visible={isVisible}>
        <OptionsList>
          {options.map(option => (
            <Option key={option.label} onClick={option.onClick}>
              {option.icon && <FontIcon icon={option.icon} />}
              <OptionLabel>{option.label}</OptionLabel>
            </Option>
          ))}
        </OptionsList>
      </OptionsContainer>
    </Container>
  )
}

Dropdown.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      onClick: PropTypes.func
    })
  ).isRequired
}

export default Dropdown
