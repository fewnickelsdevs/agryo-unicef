import React from 'react'
import { storiesOf } from '@storybook/react'
import DropDown from '..'

const dropdownOptions = [
  { label: 'Option 1', onClick: () => {} },
  { label: 'Option 2', onClick: () => {} },
  { label: 'Option 3', onClick: () => {} }
]

const BasicDropDown = () => (
  <DropDown color='#000' title='Dropdown example' options={dropdownOptions} />
)

storiesOf('Dropdown', module).add('Basic Dropdown', () => <BasicDropDown />)
