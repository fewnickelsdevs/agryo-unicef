import React, { Component, useEffect } from 'react';
import { createChart } from 'lightweight-charts';

// source https://www.tradingview.com/lightweight-charts/

class Graph extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      chart: null
    }
  }

  componentDidMount() {
    const { id, width, height, data } = this.props
    let chart = createChart(id, {
      width,
      height,
      rightPriceScale: {
        visible: false,
      },
      leftPriceScale: {
        visible: false,
      },
      timeScale: {
        visible: false,
      },
      crosshair: {
        horzLine: {
          visible: false,
        },
        vertLine: {
          visible: false,
        },
      },
      layout: {
        backgroundColor: '#f4f7f6',
      },
      grid: {
        vertLines: {
          visible: false,
        },
        horzLines: {
          visible: false,
        },
      },
      handleScroll: false,
      handleScale: false,
    });

    var areaSeries = chart.addAreaSeries({
      topColor: 'rgba(76, 175, 80, 0.5)',
      lineColor: 'rgba(76, 175, 80, 1)',
      bottomColor: 'rgba(76, 175, 80, 0)',
      lineWidth: 1,
    })

    areaSeries.setData(data)
    chart.timeScale().fitContent()
    this.setState({ chart })
  }

  render() {
    const { id, height } = this.props
    return (
      <div style={{ height }}>
        <div
          id={id}
          className={'LightweightChart'}
        />
      </div>
    )
  }
}

export default Graph