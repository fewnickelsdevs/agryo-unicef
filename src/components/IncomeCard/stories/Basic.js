import React from "react";
import { storiesOf } from '@storybook/react'
import IncomeCard from '../IncomeCard'

storiesOf('Income Card', module)
  .add('simple', () => (
    <IncomeCard
      id={'sample1'}
      title={`Average Request Value`}
      value={`U$ 18,850.43`}
      subTitle={`You have made 100 Today`}
    />
  ))
  .add('with graph', () => (
    <IncomeCard
      id={'sample2'}
      title={`Average Size Request`}
      value={`580`}
      subTitle={`Last month you have 380 hectares`}
      data={[]}
    />
  ))

