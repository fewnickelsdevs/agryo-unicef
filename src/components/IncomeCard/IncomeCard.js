import React from 'react'
import Graph from './Graph'
import styled from 'styled-components'

const Container = styled.div`
  padding: 16px;
  align-items: center;
  background: #f4f7f6;
  width: 280px;
  height: 270px;
  border: solid;
  border-radius: 15px;
  border-color: lightgrey;
  border-width: thin;
`

const IncomeCard = ({ id, title, value, subTitle
  //, seriesData = []
}) => {
  const seriesData = []
  for (var i = 1; i <= 12; i++) {
    seriesData.push({ time: `2019-${i}-01`, value: Math.random() * 2 + 50 })
  }

  return (
    <Container>
      <div>
        <h3>{title}</h3>
      </div>
      <div style={{ fontSize: `36px` }}>
        <strong>{value}</strong>
      </div>
      <div style={{ margin: `10px 0` }}>
        {subTitle}
      </div>

      {seriesData && (
        <div style={{ margin: `10px 0` }}>
          <Graph
            id={id}
            width={250}
            height={100}
            data={seriesData}
          />
        </div>
      )}

    </Container>
  )
}

export default IncomeCard
