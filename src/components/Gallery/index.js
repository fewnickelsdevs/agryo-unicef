import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { LazyImage } from 'react-lazy-images'
import imagePlaceHolder from '../../assets/1pixel-grey.png'

import { TableContainer, TableHead, TableRow, TableHeadCell } from '../Table'

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  min-width: 14rem;
  overflow: hidden;
  background: #fff;
  border-radius: 5px;
  padding-bottom: 1rem;
  border: 1px solid #ccc;
`

const ImageWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 115px;
  margin-bottom: 1rem;
`

const GalleryWrapper = styled.div`
  ${({ theme }) => `
    display: flex;
    align-items: center;
    width: 100%;
    height: auto;
    overflow-x: hidden;
    overflow-x: auto;
    text-transform: none;

    & > * {
      margin-right: 1rem;
    }

    ${theme.breakpoints.xs} {
      margin: 2rem 0 3rem;
    }
  `}
`
//<Background type={image.rgbs3path} />

const GalleryPropertyDetails = ({ galleryData, title }) => (
  <React.Fragment>
    <TableContainer>
      <TableHead>
        <TableRow>
          <TableHeadCell>{title}</TableHeadCell>
        </TableRow>
      </TableHead>
    </TableContainer>
    <GalleryWrapper>
      {galleryData.map(image => (
        <Container>
          <ImageWrapper>
            <LazyImage
              src={image.rgbs3path}
              alt={image.dates_s}
              height='115'
              width='230'
              placeholder={({ imageProps, ref }) => (
                <img
                  width={imageProps.width}
                  height={imageProps.height}
                  ref={ref}
                  src={imagePlaceHolder}
                  alt={imageProps.alt}
                />
              )}
              actual={({ imageProps }) => (
                <img
                  width={imageProps.width}
                  height={imageProps.height}
                  alt={imageProps.alt}
                  src={imageProps.src}
                />
              )}
            />
          </ImageWrapper>
          <span>{image.dates_s}</span>
        </Container>
      ))}
    </GalleryWrapper>
  </React.Fragment>
)

GalleryPropertyDetails.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
}

export default GalleryPropertyDetails
