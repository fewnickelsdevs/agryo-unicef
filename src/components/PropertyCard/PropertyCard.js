import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Map, Polygon, LayersControl, TileLayer } from 'react-leaflet'
import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'

const { BaseLayer } = LayersControl

const PropertyContainer = styled.div`
  min-width: 18rem;
  cursor: pointer;
  overflow: hidden;
  border-radius: 6px;
  border: 1px solid #ccc;

  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};
  color: ${({ active }) => (active ? '#fff' : '#000')};

  ${({ responsive, theme }) =>
    responsive &&
    `
    ${theme.breakpoints.xs} {
      width: 100%;
    }
  `}
`

const PropertyMap = styled(Map).attrs({
  zoom: 13,
  zoomControl: false,
  scrollWheelZoom: false
})`
  width: 100%;
  z-index: 2;
  height: 220px;
`

const PropertyData = styled.div`
  padding: 1rem;
`

export default function PropertyCard({
  t,
  active,
  locale,
  onClick,
  status,
  responsive,
  label,
  hectares
}) {
  return (
    <PropertyContainer active={active} responsive={responsive}>
      <PropertyMap bounds={locale} onClick={onClick}>
        <BingLayer bingkey={bingKey} type='Aerial' />
        <Polygon color='green' positions={locale} />
      </PropertyMap>

      <PropertyData onClick={onClick}>
        <p>
          <b>{t('properties.card.address')}</b> {label}
        </p>

        <p>
          <b>{t('properties.card.status')}</b>: {status}
        </p>

        <p>
          <b>{t(`properties.card.area`)}</b>: {hectares}
        </p>
      </PropertyData>
    </PropertyContainer>
  )
}

PropertyCard.propTypes = {
  active: PropTypes.bool.isRequired,
  locale: PropTypes.array.isRequired,
  status: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  hectares: PropTypes.number.isRequired,
  responsive: PropTypes.bool,
  onClick: PropTypes.func
}

PropertyCard.defaultProps = {
  responsive: true,
  useGutter: false
}
