import React from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import PropertyCard from '..'
import 'leaflet/dist/leaflet.css'

const Container = styled.section`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 1rem;
`

const coords = [
  [-15.6379150225973, -48.226203918457],
  [-15.6334516787792, -48.2219123840332],
  [-15.6400640052818, -48.2141876220703],
  [-15.6379150225973, -48.226203918457]
]

const BasicPropertyCard = () => (
  <Container>
    <PropertyCard
      hectares={5.2}
      locale={coords}
      status='Active'
      label='Brasilia'
    />
  </Container>
)

storiesOf('PropertyCard', module).add('Basic PropertyCard', () => (
  <BasicPropertyCard />
))
