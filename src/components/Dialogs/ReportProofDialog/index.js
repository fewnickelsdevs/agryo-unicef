import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import { ButtonPrimary, ButtonSecondary } from '../../Buttons'
import {
  Background,
  Container,
  Heading,
  Message,
  InputContainer,
  ButtonsWrapper,
  ButtonsConfirmation
} from '../styles'
import styled from 'styled-components'
import {
  TableContainer,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell
} from '../../../components/Table'

const FontHash = styled.p`
  font-size: 12px;
`

export default function ReportProofDialog({
  t,
  reportData,
  reportType,
  onCancel,
  propertyId
}) {
  const navigateToHederaExplorer = hash => {
    const txFormat = hash.replace(/[^\d]/g, '')
    window.open(
      `https://testnet.dragonglass.me/hedera/transactions/${txFormat}`,
      '__blank'
    )
  }

  const Dialog = () => {
    return (
      <Background>
        <Container border={'white'}>
          <TableContainer>
            <TableHead>
              <TableRow>
                <TableHeadCell>
                  Property #{propertyId} Report Proofs
                </TableHeadCell>
                <TableHeadCell></TableHeadCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow striped>
                <TableCell>Report Type</TableCell>
                <TableCell style={{ float: 'right' }}>
                  {t(reportType[reportData.title])}
                </TableCell>
              </TableRow>
            </TableBody>
          </TableContainer>

          <TableContainer>
            <TableHead>
              <TableRow>
                <TableHeadCell>Portuguese Version</TableHeadCell>
                <TableHeadCell></TableHeadCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow striped>
                <TableCell>Registered</TableCell>
                <TableCell style={{ float: 'right' }}>
                  {new Date(reportData.timestamppor * 1000).toUTCString()}
                </TableCell>
              </TableRow>
              <TableRow striped>
                <TableCell>Transaction</TableCell>
                <TableCell style={{ float: 'right' }}>
                  <a
                    onClick={() =>
                      navigateToHederaExplorer(reportData.transactionidpor)
                    }
                  >
                    {reportData.transactionidpor}
                  </a>
                </TableCell>
              </TableRow>

              <TableRow striped>
                <TableCell>SHA3-512</TableCell>
                <TableCell style={{ float: 'right' }}>
                  <FontHash>{reportData.filehashpor.slice(0, 32)}</FontHash>
                  <FontHash>{reportData.filehashpor.slice(32, 64)}</FontHash>
                  <FontHash>{reportData.filehashpor.slice(64, 96)}</FontHash>
                  <FontHash>{reportData.filehashpor.slice(96)}</FontHash>
                </TableCell>
              </TableRow>
            </TableBody>
          </TableContainer>

          <TableContainer>
            <TableHead>
              <TableRow>
                <TableHeadCell>English Version</TableHeadCell>
                <TableHeadCell></TableHeadCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow striped>
                <TableCell>Registered</TableCell>
                <TableCell style={{ float: 'right' }}>
                  {new Date(reportData.timestampeng * 1000).toUTCString()}
                </TableCell>
              </TableRow>
              <TableRow striped>
                <TableCell>Transaction</TableCell>
                <TableCell style={{ float: 'right' }}>
                  <a
                    onClick={() =>
                      navigateToHederaExplorer(reportData.transactionideng)
                    }
                  >
                    {reportData.transactionideng}
                  </a>
                </TableCell>
              </TableRow>
              <TableRow striped>
                <TableCell>SHA3-512</TableCell>
                <TableCell style={{ float: 'right' }}>
                  <FontHash>{reportData.filehasheng.slice(0, 32)}</FontHash>
                  <FontHash>{reportData.filehasheng.slice(32, 64)}</FontHash>
                  <FontHash>{reportData.filehasheng.slice(64, 96)}</FontHash>
                  <FontHash>{reportData.filehasheng.slice(96)}</FontHash>
                </TableCell>
              </TableRow>
            </TableBody>
          </TableContainer>
          <div
            style={{ marginLeft: 'auto', marginRight: 'auto', marginTop: 20 }}
          >
            <ButtonSecondary text={'Close'} onClick={() => onCancel()} />
          </div>
        </Container>
      </Background>
    )
  }

  if (!reportData) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}
