import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import Lottie from 'react-lottie'

import successAnimation from '../../../assets/animations/success.json'
import { ButtonPrimary } from '../../Buttons'
import { SuccessBackground, Container, Heading, Message } from '../styles'

export default function SuccessDialog({
  visible,
  title,
  message,
  buttonMessage,
  onClick
}) {
  const defaultLottieOptions = {
    loop: false,
    autoplay: true,
    animationData: successAnimation,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  const Dialog = () => (
    <SuccessBackground>
      <Container border='#6AC440'>
        <Lottie
          options={defaultLottieOptions}
          isPaused={false}
          isStopped={false}
          height={200}
          width={200}
        />
        <Heading gutterBottom={!message}>{title || 'Concluido!'}</Heading>

        {message && <Message>{message}</Message>}

        <ButtonPrimary text={buttonMessage} onClick={onClick} />
      </Container>
    </SuccessBackground>
  )

  if (!visible) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}

SuccessDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

SuccessDialog.defaultProps = {
  message: null
}
