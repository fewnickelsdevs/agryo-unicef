import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import Lottie from 'react-lottie'

import errorAnimation from '../../../assets/animations/error.json'
import { ButtonPrimary } from '../../Buttons'
import { Background, Container, Heading, Message } from '../styles'

export default function ErrorDialog({
  visible,
  title,
  message,
  buttonMessage,
  onClick
}) {
  const defaultLottieOptions = {
    loop: false,
    autoplay: true,
    animationData: errorAnimation,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  const Dialog = () => (
    <Background>
      <Container border='#F02323'>
        <Lottie
          options={defaultLottieOptions}
          isPaused={false}
          isStopped={false}
          height={150}
          width={150}
        />
        <Heading style={{ marginTop: 10 }} gutterBottom={!message}>
          {title || 'Concluido!'}
        </Heading>

        {message && <Message>{message}</Message>}

        <ButtonPrimary text={buttonMessage} onClick={onClick} />
      </Container>
    </Background>
  )

  if (!visible) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}

ErrorDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

ErrorDialog.defaultProps = {
  message: null
}
