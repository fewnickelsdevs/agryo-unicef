import LoadingContainer from '../../../containers/Loading'
import React from 'react'
import ReactDOM from 'react-dom'
import { Background, ContainerLoading } from '../styles'

export default function LoadingDialog({ visible }) {
  const Dialog = () => (
    <Background>
      <ContainerLoading border='#6AC440'>
        <LoadingContainer />
      </ContainerLoading>
    </Background>
  )

  if (!visible) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}
