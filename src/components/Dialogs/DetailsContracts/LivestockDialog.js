import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { ButtonPrimary } from '../../Buttons'
import { Background, Container, Heading, Message } from '../styles'
import currency from 'currency.js'

export default function LivestockDialog({ contractData, closeContractModal }) {
  const { contractGeneralID, fundedValue, numberAnimals } = contractData

  const Dialog = () => (
    <Background>
      <Container border='#27ae60'>
        <ButtonPrimary text={'X'} onClick={() => closeContractModal()} />
        <Heading style={{ marginTop: 10 }}>
          Contract {contractGeneralID}
        </Heading>

        <Message>
          Contract Value: $
          {currency(fundedValue)
            .divide(100)
            .format()}
        </Message>
        <Message>Animals: {numberAnimals}</Message>

        <ButtonPrimary text={'View Details'} />
      </Container>
    </Background>
  )

  if (!contractData) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}

LivestockDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}
