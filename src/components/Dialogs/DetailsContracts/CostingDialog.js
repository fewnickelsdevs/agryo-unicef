import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { ButtonPrimary } from '../../Buttons'
import {
  Background,
  ContainerDetailsContract,
  Heading,
  Message,
  ContainerHeader
} from '../styles'
import currency from 'currency.js'
import styled from 'styled-components'

const ButtonContainer = styled.button`
  border: 0;
  margin-top: 10px
  transition: 0.1s;
  background: #3F8C48;
  border-radius: 4px;
  cursor: pointer;
  width: 20px;

  &:hover {
    background: #2f8c4e;
  }

  &:active {
    background: #34ad5c;
  }
`

const ButtonText = styled.span`
  color: #fff;
  font-size: 1rem;
  font-weight: bold;
`

export default function CropsDialog({
  contractData,
  closeContractModal,
  goToContractDetails
}) {
  const { contractId, fundedValue, fieldHectares } = contractData

  const Dialog = () => (
    <Background>
      <ContainerDetailsContract border='#27ae60'>
        <ContainerHeader>
          <div style={{ float: 'right' }}>
            {/* <ButtonPrimary text={"X"} onClick={() => closeContractModal()} /> */}
            <ButtonContainer onClick={() => closeContractModal()}>
              <ButtonText>X</ButtonText>
            </ButtonContainer>
          </div>
          <div style={{ float: 'left' }}>
            <Heading style={{ marginTop: 10 }}>
              Contract {contractId}
            </Heading>
          </div>
        </ContainerHeader>

        <Message>
          Contract Value: $
          {currency(fundedValue)
            .divide(100)
            .format()}
        </Message>
        <Message>Hectares: {fieldHectares / 100}</Message>

        <ButtonPrimary
          onClick={() => goToContractDetails(contractData)}
          text={'View Details'}
        />
      </ContainerDetailsContract>
    </Background>
  )

  if (!contractData) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}

CropsDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}
