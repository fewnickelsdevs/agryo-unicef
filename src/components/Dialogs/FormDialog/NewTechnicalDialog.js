import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { ButtonPrimary, ButtonSecondary } from '../../Buttons'
import {
  Background,
  Heading,
  Message,
  InputContainer,
  ButtonsWrapper,
  ButtonsConfirmation
} from '../styles'
import TextInput from '../../Form/TextInput/index.js'
import styled from 'styled-components'
import SelectInput from '../../Form/SelectInput';
import { REGISTRED_ENTERPRISE_USER_TYPES } from '../../../utils/user-type';

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  background: #fff;
  border-radius: 6px;
  padding: 12px 24px 24px;
  border: 3px solid ${props => props.border};

  height: 20rem;
  min-width: 400px;
  max-height: 800px;

  top: 50%;
  left: 50%;
  position: fixed;
  transform: translate(-50%, -50%);
`

export default function NewTechnicalDialog({
  t,
  visible,
  loading,
  onConfirm,
  onCancel
}) {
  const Dialog = () => {
    const [email, setEmail] = useState('')
    const [typeLogin, setTypeLogin] = useState(2)
    const [region, setRegion] = useState("")

    return (
      <Background>
        <Container>
          <React.Fragment>
            <Heading>Register New Technical</Heading>
            <InputContainer>
              <TextInput
                value={email}
                onChange={e => setEmail(e)}
                placeholder={'Email'}
              />
            </InputContainer>
            <div style={{width: 200}}>
              <SelectInput
                onChange={setTypeLogin}
                value={typeLogin}
                options={REGISTRED_ENTERPRISE_USER_TYPES(t)}
              />
              <SelectInput
                onChange={setRegion}
                value={region}
                options={[{value: 1, name: "MA"}, {value: 2, name: "PA"}]}
              />
            </div>
            <ButtonsWrapper>
              <ButtonsConfirmation>
                <div style={{ margin: 5 }}>
                  <ButtonSecondary text={'Cancel'} onClick={() => onCancel()} />
                </div>
                <ButtonPrimary
                  loading={loading}
                  text={'Confirm'}
                  onClick={() => onConfirm({ typeLogin, email, document })}
                />
              </ButtonsConfirmation>
            </ButtonsWrapper>
          </React.Fragment>
        </Container>
      </Background>
    )
  }

  if (!visible) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}

NewTechnicalDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  codeValue: PropTypes.string.isRequired,
  onChangeCodeValue: PropTypes.func.isRequired
}

NewTechnicalDialog.defaultProps = {
  message: null
}
