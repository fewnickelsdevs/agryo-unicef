import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { ButtonPrimary, ButtonSecondary } from '../../Buttons'
import {
  Background,
  Heading,
  Message,
  InputContainer,
  ButtonsWrapper,
  ButtonsConfirmation
} from '../styles'
import TextInput from '../../Form/TextInput/index.js'
import styled from 'styled-components'
import { Subtitle } from '../../Typography'

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  background: #fff;
  border-radius: 6px;
  padding: 12px 24px 24px;
  border: 3px solid ${props => props.border};

  height: min-content;
  min-width: 400px;
  height: 200px;

  top: 50%;
  left: 50%;
  position: fixed;
  transform: translate(-50%, -50%);
`

export default function ConfirmOrCancelDialog({
  visible,
  loading,
  title,
  subtitle,
  onConfirm,
  onCancel
}) {
  const Dialog = () => {
    return (
      <Background>
        <Container>
          <React.Fragment>
            <Heading>{title}</Heading>
            <Subtitle>{subtitle}</Subtitle>
            <ButtonsWrapper>
              <ButtonsConfirmation>
                <div style={{ margin: 5 }}>
                  <ButtonSecondary text={'Cancel'} onClick={() => onCancel()} />
                </div>
                <ButtonPrimary
                  loading={loading}
                  text={'Confirm'}
                  onClick={() => onConfirm()}
                />
              </ButtonsConfirmation>
            </ButtonsWrapper>
          </React.Fragment>
        </Container>
      </Background>
    )
  }

  if (!visible) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}

ConfirmOrCancelDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  buttonMessage: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired
}
