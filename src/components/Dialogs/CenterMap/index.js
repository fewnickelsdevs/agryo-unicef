import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { ButtonPrimary, ButtonSecondary } from '../../Buttons'
import {
  Background,
  ContainerCenterMap,
  Heading,
  Message,
  ErrorMessage
} from '../styles'
import TextInput from '../../Form/TextInput/index.js'

export default function CenterMapDialog({ visible, centerMap, onCancel }) {
  const [lng, setLng] = useState('')
  const [lat, setLat] = useState('')
  const [invalidLng, setInvalidLng] = useState(false)
  const [invalidLat, setInvalidLat] = useState(false)

  const setLngValue = lng => {
    setLng(parseFloat(lng))
    if (invalidLat || invalidLng) {
      setInvalidLat(false)
      setInvalidLng(false)
    }
  }

  const setLatValue = lat => {
    setLat(parseFloat(lat))
    if (invalidLat || invalidLng) {
      setInvalidLat(false)
      setInvalidLng(false)
    }
  }

  const setCenterMap = () => {
    let invalidLat,
      invalidLng = false
    if (lat < -90 || lat > 90) {
      console.error('Invalid lat', lat)
      invalidLat = true
    }
    if (lng < -180 || lng > 180) {
      console.error('Invalid lng', lng)
      invalidLng = true
    }

    if (invalidLat || invalidLng) {
      setInvalidLng(invalidLng)
      setInvalidLat(invalidLat)
    } else {
      centerMap([lat, lng])
    }
  }

  return (
    <ContainerCenterMap border='#6AC440'>
      <Message style={{ fontSize: 20 }}>Center the map</Message>
      {invalidLng && invalidLat && (
        <ErrorMessage>Invalid Latitude and Logitude</ErrorMessage>
      )}
      {invalidLng && !invalidLat && (
        <ErrorMessage>Invalid Logitude</ErrorMessage>
      )}
      {!invalidLng && invalidLat && (
        <ErrorMessage>Invalid Latitude</ErrorMessage>
      )}
      <TextInput
        type='number'
        value={lat}
        onChange={setLatValue}
        placeholder={'Latitude'}
      />
      <TextInput
        type='number'
        value={lng}
        onChange={setLngValue}
        placeholder={'Longitude'}
      />

      <ButtonPrimary text={'Search'} onClick={() => setCenterMap()} />
    </ContainerCenterMap>
  )
}

CenterMapDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  centerMap: PropTypes.func.isRequired
}
