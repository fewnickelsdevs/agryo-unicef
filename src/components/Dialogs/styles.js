import styled from 'styled-components'

export const Background = styled.div`
  top: 0;
  bottom: 0;
  width: 100vw;
  height: 100vh;
  z-index: 50000000;
  position:fixed;
  background: rgba(0, 0, 0, 0.3);'
`

export const SuccessBackground = styled.div`
  top: 0;
  bottom: 0;
  width: 100vw;
  height: 100vh;
  z-index: 50000000000000;
  position:fixed;
  background: rgba(0, 0, 0, 0.3);'
`

export const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  background: #fff;
  border-radius: 6px;
  padding: 12px 24px 24px;
  border: 3px solid ${props => props.border};

  height: min-content;
  min-width: 300px;
  max-height: 800px;

  top: 50%;
  left: 50%;
  position: fixed;
  transform: translate(-50%, -50%);
`

export const ContainerLoading = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  background: #fff;
  border-radius: 6px;
  border: 3px solid ${props => props.border};

  height: min-content;
  min-width: 300px;
  max-height: 300px;

  top: 50%;
  left: 50%;
  position: fixed;
  transform: translate(-50%, -50%);
`

export const ContainerCenterMap = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  margin-right: 15px;
  background: #fff;
  border-radius: 6px;
  border: 3px solid ${props => props.border};

  height: min-content;
  min-width: 120px;
  height: 250px;
  width: 300px;
`
//  width: 45vw;

export const InputContainer = styled.div`
  width: '80%';
  margin-bottom: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

export const Heading = styled.h2`
  font-weight: bold;
  text-align: center;
  margin-bottom: 0.5rem;

  ${props => props.gutterBottom && 'margin-bottom: 1.5rem;'}
`

export const Message = styled.p`
  text-align: center;
  line-height: 1.3;
  margin-bottom: 1.2rem;
`

export const ErrorMessage = styled.p`
  text-align: center;
  color: red;
  line-height: 1.3;
  margin-bottom: 1.2rem;
`

export const ContainerHeader = styled.div`
  width: 100%;
  display: inline-block;
`

export const ContainerDetailsContract = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  background: #fff;
  border-radius: 6px;
  padding: 12px 24px 24px;
  border: 3px solid ${props => props.border};

  width: 45vw;
  height: min-content;
  min-width: 200px;
  max-width: 300px
  max-height: 300px;

  top: 50%;
  left: 50%;
  position: fixed;
  transform: translate(-50%, -50%);
`

export const ButtonsWrapper = styled.div`
  ${({ theme }) => `
    width: 80%;
    height: 50%;
    display: flex;
    align-items: flex-end;
    flex-direction: column;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      align-items: center;
    }
  `}
`

export const ButtonsConfirmation = styled.div`
  ${({ theme }) => `
  margin-top: 1rem;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${theme.breakpoints.xs} {
    flex-direction: column-reverse;
  }
  `}
`
