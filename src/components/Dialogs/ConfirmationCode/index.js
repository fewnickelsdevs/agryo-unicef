import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import Lottie from 'react-lottie'

import successAnimation from '../../../assets/animations/success.json'
import { ButtonPrimary, ButtonSecondary } from '../../Buttons'
import {
  Background,
  Container,
  Heading,
  Message,
  InputContainer,
  ButtonsWrapper,
  ButtonsConfirmation
} from '../styles'
import TextInput from '../../Form/TextInput/index.js'

export default function ConfirmationCode({
  visible,
  title,
  message,
  buttonMessage,
  onConfirmPassword,
  waitingNewApiConfirmation,
  onCodeConfirm,
  onCancel
}) {
  const defaultLottieOptions = {
    loop: false,
    autoplay: true,
    animationData: successAnimation,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  const Dialog = () => {
    const [confirmPassword, setConfirmPassword] = useState('')
    const [code, setCode] = useState('')

    return (
      <Background>
        <Container>
          {waitingNewApiConfirmation ? (
            <React.Fragment>
              <Heading gutterBottom={!message}>
                {'Confirm a New Api Key'}
              </Heading>
              <Message>
                To confirm the new APi creation, use the code that we send to your email
              </Message>
              <InputContainer>
                <TextInput
                  value={code}
                  onChange={e => setCode(e)}
                  placeholder={'Confirmation Code'}
                />
              </InputContainer>
              <ButtonPrimary
                text={'Confirm'}
                onClick={() => onCodeConfirm(code)}
              />
            </React.Fragment>
          ) : (
              <React.Fragment>
                <Heading gutterBottom={!message}>
                  {'Request a New Api Key'}
                </Heading>
                <Message>
                  To request a new API key, please confirm your password
              </Message>
                <InputContainer>
                  <TextInput
                    value={confirmPassword}
                    onChange={e => setConfirmPassword(e)}
                    placeholder={'Confirm your Password'}
                    type='password'
                  />
                </InputContainer>
                <ButtonsWrapper>
                  <ButtonsConfirmation>
                    <ButtonSecondary text={'Cancel'} onClick={() => onCancel()} />

                    <ButtonPrimary
                      text={'Confirm'}
                      onClick={() => onConfirmPassword(confirmPassword)}
                    />
                  </ButtonsConfirmation>
                </ButtonsWrapper>
              </React.Fragment>
            )}
        </Container>
      </Background>
    )
  }

  if (!visible) {
    return <div />
  }

  return ReactDOM.createPortal(<Dialog />, document.body)
}

ConfirmationCode.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  codeValue: PropTypes.string.isRequired,
  onChangeCodeValue: PropTypes.func.isRequired
}

ConfirmationCode.defaultProps = {
  message: null
}
