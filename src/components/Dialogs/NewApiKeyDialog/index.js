import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { ButtonPrimary } from '../../Buttons'
import { Background, Container, Heading, Message } from '../styles'

export default function NewApiKeyDialog({ onClick, newApiKey }) {
  const { accessKey, privateKey } = newApiKey
  const Dialog = () => (
    <Background>
      <Container border='#6AC440'>
        <Heading>{'New API Key'}</Heading>

        <Message>
          Here your new credentials to access the Agryo API, store the private
          key in a safe way, you won't be able to see her around here anymore
        </Message>
        <p style={{ fontWeight: 'bold' }}>Access Key</p>
        <p align='center' style={{ wordBreak: 'break-all', marginBottom: 10 }}>
          {accessKey}
        </p>
        <p style={{ fontWeight: 'bold' }}>Private Key</p>
        <p align='center' style={{ wordBreak: 'break-all', marginBottom: 10 }}>
          {privateKey}
        </p>
        <ButtonPrimary text={'OK'} onClick={onClick} />
      </Container>
    </Background>
  )

  if (!newApiKey) return <div />

  return ReactDOM.createPortal(<Dialog />, document.body)
}

NewApiKeyDialog.propTypes = {
  newApiKey: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
}
