import React from 'react'
import styled from 'styled-components'

const T = styled.section`
  height: 100%;
  padding: 1.5rem 2.5rem;
  background: #f1f2f8;

  ${({ theme }) => `
    
    ${theme.breakpoints.sm} {
      padding: 0.5rem 1rem;
    }
  `}
`

export default function Container({ children }) {
  return <T>{children}</T>
}
