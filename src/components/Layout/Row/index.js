import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

const Row = ({ children, style, className }) => (
  <Container style={style} className={className}>
    {children}
  </Container>
)

Row.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.object,
  className: PropTypes.string
}

Row.defaultProps = {
  style: {},
  className: ''
}

export default Row
