import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'

import { ButtonPrimary } from '../Buttons'
import Row from '../Layout/Row'

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
  min-width: 14rem;
  background: #fff;
  padding: 0.8rem;
  border-radius: 5px;
  border: 1px solid #ccc;
`

const Icon = styled(FontAwesomeIcon)`
  color: #fff;
  height: 2.25rem;
  padding: 0.6rem;
  background: #e6a91e;
  border-radius: 50%;
  width: 2.25rem !important;
  margin-bottom: 1rem;

  ${props => props.disabled && 'background: #928372'}
`

const Counter = styled.h4`
  margin: 1rem 0 0.75rem;
`

const HomeDescriptionCard = ({
  t,
  icon,
  label,
  description,
  disabled,
  onClick,
  url
}) => (
  <Container>
    <Icon icon={icon} disabled={disabled} />

    <h3>{label}</h3>

    <Counter>{description}</Counter>

    <Row>
      <Link to={url}>
        <ButtonPrimary disabled={disabled} text={'Enter >'} />
      </Link>
    </Row>
  </Container>
)

HomeDescriptionCard.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  disabled: PropTypes.bool
}

HomeDescriptionCard.defaultProps = {
  disabled: false
}

export default HomeDescriptionCard
