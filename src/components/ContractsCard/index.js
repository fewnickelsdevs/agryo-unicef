import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { ButtonPrimary } from '../Buttons'
import Row from '../Layout/Row'

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  min-width: 14rem;
  background: #fff;
  padding: 0.8rem;
  border-radius: 5px;
  border: 1px solid #ccc;
`

const Icon = styled(FontAwesomeIcon)`
  color: #fff;
  height: 2.25rem;
  padding: 0.6rem;
  background: #e6a91e;
  border-radius: 50%;
  width: 2.25rem !important;
  margin-bottom: 1rem;

  ${props => props.disabled && 'background: #928372'}
`

const Counter = styled.h4`
  margin: 1rem 0 0.75rem;
`

const CounterLabel = styled.span`
  font-weight: 400;
`

const ContractsCard = ({ t, icon, label, count, disabled, onClick }) => (
  <Container>
    <Icon icon={icon} disabled={disabled} />

    <h3>{label}</h3>

    <Counter>
      {count} <CounterLabel>{t('home.active-contracts')}</CounterLabel>
    </Counter>

    <Row>
      <ButtonPrimary
        onClick={() => {
          !disabled && onClick && onClick()
        }}
        disabled={disabled}
        text={disabled ? t('home.cards.soon') : t('home.cards.create-contract')}
      />
    </Row>
  </Container>
)

ContractsCard.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  disabled: PropTypes.bool
}

ContractsCard.defaultProps = {
  disabled: false
}

export default ContractsCard
