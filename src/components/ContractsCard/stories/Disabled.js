import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import ContractCard from '..'

library.add(faPlus)

const Container = styled.section`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(4, 1fr);
`

const BasicContractCard = () => (
  <Container>
    <ContractCard disabled icon='plus' />
    <ContractCard disabled icon='plus' />
    <ContractCard disabled icon='plus' />
  </Container>
)

storiesOf('Contract Card', module).add('Disabled Contract Card', () => (
  <BasicContractCard />
))
