import React from 'react'
import { storiesOf } from '@storybook/react'
import Subtitle from '..'

const BasicSubtitle = () => <Subtitle>A Basic Subtitle</Subtitle>

storiesOf('Typography/Subtitle', module).add('Basic Subtitle', () => (
  <BasicSubtitle />
))
