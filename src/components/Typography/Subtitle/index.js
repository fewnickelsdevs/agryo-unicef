import React from 'react'
import styled from 'styled-components'

const StyledSubtitle = styled.p`
  color: #919191;
  padding-top: 5px;
`

const Subtitle = ({ children, ...props }) => (
  <StyledSubtitle {...props}>{children}</StyledSubtitle>
)

export default Subtitle
