import React from 'react'
import styled from 'styled-components'

const StyledTitle = styled.h2`
  font-weight: 600;
  font-family: Montserrat;
  margin-top: 20px;
  font-size: 25px;
  color: #000;
`

const Title = ({ children }) => <StyledTitle>{children}</StyledTitle>

export default Title
