import React from 'react'
import { storiesOf } from '@storybook/react'
import Title from '..'

const BasicTitle = () => <Title>A Basic Title</Title>

storiesOf('Typography/Title', module).add('Basic Title', () => <BasicTitle />)
