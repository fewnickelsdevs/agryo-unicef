import React from 'react'
import { storiesOf } from '@storybook/react'
import Caption from '..'

const BasicCaption = () => <Caption>A Basic Caption</Caption>

storiesOf('Typography/Caption', module).add('Basic Caption', () => (
  <BasicCaption />
))
