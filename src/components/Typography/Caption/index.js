import React from 'react'
import styled from 'styled-components'

const StyledCaption = styled.p`
  font-size: 0.75rem;
  color: rgba(0, 0, 0, 0.54);
`

const Caption = ({ children }) => <StyledCaption>{children}</StyledCaption>

export default Caption
