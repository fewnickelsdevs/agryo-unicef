import Title from './Title'
import Subtitle from './Subtitle'
import Caption from './Caption'

export { Title, Subtitle, Caption }
