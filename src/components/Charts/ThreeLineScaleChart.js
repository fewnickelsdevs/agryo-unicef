import React, { useEffect } from 'react'
import { createChart } from 'lightweight-charts'

var darkTheme = {
  chart: {
    layout: {
      backgroundColor: '#f1f2f8',
      lineColor: '#2B2B43',
      textColor: 'black'
    },
    crosshair: {
      color: '#758696'
    },
    grid: {
      vertLines: {
        color: '#f1f2f8'
      },
      horzLines: {
        color: '#f1f2f8'
      }
    },
    rightPriceScale: {
      visible: false
    },
    leftPriceScale: {
      visible: false
    },
    timeScale: {
      visible: false
    },
    crosshair: {
      horzLine: {
        visible: false
      },
      vertLine: {
        visible: false
      }
    }
  },
  series: {
    topColor: 'rgba(76, 175, 80, 0.5)',
    lineColor: 'rgba(76, 175, 80, 1)',
    bottomColor: 'rgba(76, 175, 80, 0)',
    lineWidth: 2,
    priceLineVisible: false
  }
}

const ThreeAreaLineScaleChart = ({ width, height, data, containerId }) => {
  const [chart, setChart] = React.useState(false)
  // const [height, setHeight] = React.useState(300)

  // const updateWidth = (width) => {
  //     if(!chart) return
  //     const heightX = (width>750) ? 400 : 300
  // 	console.log(`WIDTH: ${width} - HEIGHT: ${heightX}`)
  // 	if(!(height == heightX)){
  // 		setHeight(heightX+30)
  // 	}
  // 	chart.resize(width, heightX);
  // 	chart.timeScale().fitContent();
  // }

  // useEffect(() => {
  //     updateWidth(width, height)
  // }, [width, height])

  const generateChart = () => {
    //const height= (width>750) ? 400 : 300
    let chart = createChart(containerId, { width: 850, height: 400 })

    let firstSeries = chart.addLineSeries({
      color: 'red',
      lineWidth: 3
    })
    firstSeries.setData(data[0])

    let secondSeries = chart.addLineSeries({
      color: 'green',
      lineWidth: 3
    })
    secondSeries.setData(data[1] || [{ value: 0, date: '2019-05-06' }])

    let thirdSeries = chart.addLineSeries({
      color: 'blue',
      lineWidth: 3
    })
    thirdSeries.setData(data[2] || [{ value: 0, date: '2019-05-06' }])

    chart.applyOptions(darkTheme.chart)
    chart.timeScale().fitContent()
    setChart(chart)
  }

  React.useEffect(() => {
    if (!data) {
      return
    }
    generateChart()
  }, [])

  return <div id={containerId} className={'LightweightChart'} />
}

export default ThreeAreaLineScaleChart
