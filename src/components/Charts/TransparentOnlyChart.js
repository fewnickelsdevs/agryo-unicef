import React, { useEffect } from 'react'
import { createChart, PriceScaleMode } from 'lightweight-charts'

var darkTheme = {
  chart: {
    layout: {
      backgroundColor: 'white',
      lineColor: '#2B2B43',
      textColor: 'black'
    },
    grid: {
      vertLines: {
        visible: false
      },
      horzLines: {
        visible: false
      }
    },
    rightPriceScale: {
      visible: false
    },
    leftPriceScale: {
      visible: false
    },
    timeScale: {
      visible: false
    },
    crosshair: {
      horzLine: {
        visible: false,
        labelVisible: false
      },
      vertLine: {
        visible: false,
        labelVisible: false
      }
    },
    priceScale: {
      drawTicks: false
    },
    handleScroll: false,
    handleScale: false
    // handleScroll: {
    // 	mouseWheel: false,
    // 	pressedMouseMove: false,
    // },
    // handleScale: {
    // 	axisPressedMouseMove: false,
    // 	mouseWheel: false,
    // 	pinch: false,
    // },
  },
  series: {
    topColor: 'rgba(76, 175, 80, 1)',
    lineColor: '#3F8C48',
    bottomColor: 'rgba(76, 175, 80, 0)',
    lineWidth: 1,
    priceLineVisible: false,
    lastValueVisible: false
  }
}

const TransparentOnlyChart = ({ width, height, data, containerId }) => {
  const [chart, setChart] = React.useState(false)

  const updateWidth = width => {
    if (!chart) {
      return
    }
    let heightX = height ? height : width > 750 ? 250 : 150
    console.log(`WIDTH: ${width} - HEIGHT: ${heightX}`)
    chart.resize(width - 30, heightX)
    chart.timeScale().fitContent()
  }

  useEffect(() => {
    updateWidth(width)
  }, [width])

  const generateChart = () => {
    //const height= (width>750) ? 400 : 300
    let chart = createChart(containerId, {
      width,
      height: height ? height : width > 750 ? 250 : 150
    })

    let firstSeries = chart.addAreaSeries()
    firstSeries.applyOptions(darkTheme.series)

    firstSeries.setData(data)

    chart.applyOptions(darkTheme.chart)
    chart.timeScale().fitContent()
    setChart(chart)
  }

  React.useEffect(() => {
    if (!data) return
    generateChart()
  }, [])

  return <div id={containerId} className={'LightweightChart'} />
}

export default TransparentOnlyChart
