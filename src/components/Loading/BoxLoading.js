import React from 'react'
import styled from 'styled-components'
import { useTranslation } from 'react-i18next'
import agryoLogo from '../../assets/agryo-sm.svg'

const Container = styled.div`
  width: 100%;
  height: 200px;
  background: #f1f2f8;
  display: flex;
  align-items: center;
  justify-content: center;
`

const LoadContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

const BrandLogo = styled.img.attrs({
  alt: 'Agryo logo'
})`
  height: 3rem;
  animation-name: bounce;
  animation-duration: 1s;
  animation-timing-function: linear;
  animation-iteration-count: infinite;

  @keyframes bounce {
    0%,
    100% {
      transform: translateY(0);
    }
    50% {
      transform: translateY(-5px);
    }
  }
`

const Message = styled.h3`
  ${({ theme }) => `
    font-size: 1.75rem;
    margin-left: 0.9rem;
    
    ${theme.breakpoints && theme.breakpoints.xs} {
      display: none;
      margin-left: 0rem;
    }

    ${!theme.breakpoints && '@media only screen and (max-width: 600px)'} {
      display: none;
      margin-left: 0rem;
    }
  `}
`

export default function BoxLoading() {
  const { t } = useTranslation()

  return (
    <Container>
      <LoadContainer>
        <BrandLogo src={agryoLogo} />
        <Message>{t('general.loading')}</Message>
      </LoadContainer>
    </Container>
  )
}
