import React, { Suspense } from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import Map from '../Map'
import normalizePropertyCoords from '../../../formatters/normalizePropertyCoords'
import PolygonTestData from '../test/data/polygon.json'
import 'leaflet/dist/leaflet.css'

const Container = styled.section`
  border: 1px solid;
`

storiesOf('Map', module)
  .add('simple', () => (
    <Container>
      <Map
        drawPolygon={false}
        polygonCoords={normalizePropertyCoords(PolygonTestData)}
      />
    </Container>
  ))
  .add('with polygon', () => (
    <Container>
      <Map
        drawPolygon={true}
        polygonCoords={normalizePropertyCoords(PolygonTestData)}
      />
    </Container>
  ))
  .add('with NDVI from external Data', () => {
    const ndviPath = `https://dev-ceos-data-storage.s3.us-east-2.amazonaws.com/NDVIimages/farmer_30_302a300506032b6570032100aabc9cef8f1b854496c79b00799b4c0fcf72242b8646f98decbdbcbee087a251_ldt7_ndvi_20000123.tif`
    return (
      <Container>
        <Map
          drawPolygon={true}
          polygonCoords={normalizePropertyCoords(PolygonTestData)}
          tiffURL={ndviPath}
        />
      </Container>
    )
  })
  .add('with RGB from external Data', () => {
    const rgbPath = `https://dev-ceos-data-storage.s3.us-east-2.amazonaws.com/RGBimages/farmer_30_302a300506032b6570032100aabc9cef8f1b854496c79b00799b4c0fcf72242b8646f98decbdbcbee087a251_ldt7_rgb_20000123.tif`

    return (
      <Container>
        <Map
          drawPolygon={true}
          polygonCoords={normalizePropertyCoords(PolygonTestData)}
          tiffURL={rgbPath}
          isRGB={true}
        />
      </Container>
    )
  })
