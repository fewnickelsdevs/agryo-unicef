import parse_georaster from 'georaster'

export function parseGeoRaster(url) {
  return fetch(url)
    .then(response => response.arrayBuffer())
    .then(arrayBuffer => parse_georaster(arrayBuffer))
}

export function parseGeoJSON(url) {
  return fetch(url)
    .then(response => response.json())
}