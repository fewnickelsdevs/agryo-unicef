import React from 'react'
import { parseGeoRaster, parseGeoJSON } from './utils'

const MODES = {
  NOT_INITIALIZED: 'NOT_INITIALIZED',
  FETCHING: 'FETCHING',
  FETCHED: 'FETCHED',
  FAILED: 'FAILED'
}

export const TYPES = {
  GEOTIFF: `GEOTIFF`,
  GEOJSON: `GEOJSON`
}

export default class Wrapper extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mode: MODES.NOT_INITIALIZED,
      fileContent: null
    }
    this.handleSuccess = this.handleSuccess.bind(this)
    this.handleError = this.handleError.bind(this)
    this.renderChildren = this.renderChildren.bind(this)
  }

  fetchData() {
    const { fileURL } = this.props
    if (this.state.mode === MODES.FETCHING) {
      return
    }

    this.setState({ mode: MODES.FETCHING })

    let parserFunction = parseGeoJSON
    if (this.props.type === TYPES.GEOTIFF) {
      parserFunction = parseGeoRaster
    }
    parserFunction(fileURL)
      .then(this.handleSuccess)
      .catch(this.handleError)
  }
  componentDidUpdate(prevProps) {
    if (this.props.fileURL !== prevProps.fileURL) {
      this.fetchData()
    }
  }

  handleSuccess(fileContent) {
    this.setState({ mode: MODES.FETCHED, fileContent })
  }

  handleError(error) {
    this.setState({ mode: MODES.FAILED })
  }
  componentDidMount() {
    this.fetchData()
  }

  renderChildren(child) {
    if (this.state.mode !== MODES.FETCHED) {
      return null
    }

    // checking isValidElement is the safe way and avoids a typescript error too
    const props = { [this.props.field]: this.state.fileContent }
    if (React.isValidElement(child)) {
      return React.cloneElement(child, props)
    }
    return child
  }

  render() {
    if (!this.state.fileContent) {
      return null
    }
    return React.Children.map(this.props.children, this.renderChildren)
  }
}
