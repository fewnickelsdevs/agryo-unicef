import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  Map as LeafLetMap,
  withLeaflet,
  MapLayer,
  Polygon,
  Polyline,
  LayersControl,
  TileLayer,
  GeoJSON
} from 'react-leaflet'
import hash from 'object-hash';
import L from 'leaflet';

import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'
import { default as Wrapper, TYPES } from './Wrapper'
import MapGeoRasterLayer from './MapGeoRasterLayer'

const { BaseLayer } = LayersControl

const style = function () {
  return {
    fillColor: "red",
    weight: 0.3,
    opacity: 1,
    color: "purple",
    dashArray: "3",
    fillOpacity: 0.5
  }
}

function pointToLayer(feature, latlng) {
  return L.circleMarker(latlng, null); // Change marker to circle
}

const onEachFeature = (feature, layer) => {
  const popupContent = `Confidence: ${feature.properties.confidence}`
  layer.bindPopup(popupContent)
}


const MapContainer = styled.div`
  width: 100%;
  height: 500px;
`

const StyledLeafLetMap = styled(LeafLetMap)`
  width: 100%;
  height: 100%;
  z-index: 2;
`

function renderPolygon(props) {
  const { drawPolygon, polygonCoords, fillPolygon } = props

  if (!drawPolygon) {
    return null
  }

  if (fillPolygon) {
    return <Polygon color='green' positions={polygonCoords} />
  }

  return <Polyline color='green' positions={polygonCoords} />
}

function renderLayerControls(props) {
  return (
    <LayersControl>
      <BaseLayer name='Satelite' checked>
        <BingLayer bingkey={bingKey} type='Aerial' />
      </BaseLayer>
      {renderTile()}
    </LayersControl>
  )
}

function renderTile(props) {
  return (
    <BaseLayer name='Map'>
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      />
    </BaseLayer>
  )
}

function renderGeoTif(props) {
  const { tiffURL, isRGB } = props

  if (!tiffURL) {
    return null
  }
  return (
    <Wrapper field={`georaster`} fileURL={tiffURL} type={TYPES.GEOTIFF}>
      <MapGeoRasterLayer isRGB={isRGB} />
    </Wrapper>
  )
}

function renderGeoJson(props) {
  const { geoJSONURL } = props

  if (!geoJSONURL) {
    return null
  }

  return (<Wrapper field={`data`} fileURL={geoJSONURL} type={TYPES.GEOJSON}>
    <GeoJSON key={hash(geoJSONURL)} pointToLayer={pointToLayer} style={style()} onEachFeature={onEachFeature} />
  </Wrapper>)
}

export default function Map(props) {
  const { polygonCoords } = props

  if (!polygonCoords) {
    return null
  }

  return (
    <MapContainer>
      <StyledLeafLetMap
        bounds={polygonCoords}
        onClick={props.onClick}
        maxZoom={20}
        zoom={13}
      >
        {renderLayerControls(props)}
        {renderPolygon(props)}
        {renderGeoTif(props)}
        {renderGeoJson(props)}
      </StyledLeafLetMap>
    </MapContainer>
  )
}

Map.propTypes = {
  polygonCoords: PropTypes.array,
  drawPolygon: PropTypes.bool,
  onClick: PropTypes.func
}

Map.defaultProps = {}
