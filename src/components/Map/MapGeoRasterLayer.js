import { withLeaflet, MapLayer } from 'react-leaflet'
import GeoRasterLayer from 'georaster-layer-for-leaflet'
import chroma from 'chroma-js'

function pixelValuesThreeBand(range, min, max) {
  return values => {
    const [RED, GREEN, BLUE] = values
    const r = Math.round(RED * 255)
    const g = Math.round(GREEN * 255)
    const b = Math.round(BLUE * 255)
    return `rgb(${r},${g},${b})`
  }
}

function pixelValuesSingleBand(range, min, max) {
  console.log(chroma.brewer)
  const scale = chroma.scale('Spectral')

  return pixelValues => {
    // there's just one band in this raster
    var pixelValue = pixelValues[0]

    // if there's zero, don't return a color
    //if (pixelValue === 0){
    //  return null
    //}

    // scale to 0 - 1 used by chroma
    var scaledPixelValue = range > 0 ? (pixelValue - min) / range : 0
    var color = scale(scaledPixelValue).hex()
    return color
  }
}

class MapGeoRasterLayer extends MapLayer {
  createLeafletElement(props) {
    const { georaster, isRGB } = props
    const min = georaster.mins[0]
    const max = georaster.maxs[0]
    const range = georaster.ranges[0]

    const pixelValuesToColorFn = isRGB
      ? pixelValuesThreeBand(range, min, max)
      : pixelValuesSingleBand(range, min, max)

    return new GeoRasterLayer({
      georaster,
      pixelValuesToColorFn,
      resolution: 256,
      opacity: 0.8,
      updateWhenIdle: false,
      updateWhenZooming: false,
      keepBuffer: 16,
      debugLevel: 1
    })
  }

  updateLeafletElement(fromProps, toProps) {
    if (toProps.georaster !== fromProps.georaster) {
      const { map } = this.props.leaflet
      map.removeLayer(this.leafletElement)
      this.leafletElement = this.createLeafletElement()
      this.leafletElement.addTo(map)
    }
  }

  componentDidMount() {
    super.componentDidMount()
    const { map } = this.props.leaflet

    // add the layer to the map
    // e.g layer.addTo(map)
    this.leafletElement.addTo(map)

    // move the view
    // e.g map.fitBounds(layer.getBounds())
    map.fitBounds(this.leafletElement.getBounds())
  }
}

export default withLeaflet(MapGeoRasterLayer)
