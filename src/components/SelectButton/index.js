import React, { useState } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { select } from '@redux-saga/core/effects'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Container = styled.div`
  color: #000;
  display: inline-block;
  position: relative;
`

const Button = styled.button(
  ({ color }) => `
  color: ${color || '#fff'};
  border: 0;
  cursor: pointer;
  font-size: .85rem;
  border-radius: 3px;
  transition: 0.2s all;
  padding: 0.5rem 1rem;
  background: transparent;

  &:hover {
    background: rgba(0, 0, 0, 0.1);
  }
`
)

const OptionsContainer = styled.div(
  ({ visible }) => `
  display: ${visible ? 'block' : 'none'};

  right: 0;
  top: 0.5rem;
  left: 2rem;
  background: #fff;
  border-radius: 3px;
  position: absolute;
  width: max-content;
  border: 1px solid #ccc;
  z-index: 4;
`
)

const OptionsList = styled.ul`
  list-style: none;
`

const Option = styled.li`
  cursor: pointer;
  transition: 0.1s all;
  padding: 0.5rem 0.8rem;

  &:hover {
    background: rgba(0, 0, 0, 0.07);
  }

  &:active {
    background: rgba(0, 0, 0, 0.1);
  }
`

const Hoverable = styled.div`
  width: 100%;
  height: 0.5rem;
  background: transparent;
  position: absolute;
`

const ActivityButton = styled.div`
  width: 8rem;
  height: 3.5rem;
  cursor: pointer;
  padding: 0.7rem 0;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-around;

  &:not(:last-of-type) {
    margin-right: 1rem;
  }

  & h4 {
    text-align: center;
    line-height: 100%;
  }
`

const Title = styled.h3``

const SelectButtonDropdown = ({
  selectedData,
  options,
  color,
  onSelectOption
}) => {
  const [isVisible, setVisibility] = useState(false)

  const toggleVisibility = () => setVisibility(!isVisible)

  const onSelect = data => {
    onSelectOption(data)
    setVisibility(false)
  }
  return (
    <Container onMouseEnter={toggleVisibility} onMouseLeave={toggleVisibility}>
      <ActivityButton color={color}>
        <FontAwesomeIcon icon={selectedData.icon} size='2x' />
        <h4>{selectedData.label}</h4>
      </ActivityButton>

      <Hoverable />

      <OptionsContainer visible={isVisible}>
        <OptionsList>
          {options.map(option => (
            <Option
              key={option.value || option.slug}
              onClick={() => onSelectOption(option)}
            >
              {option.label}
            </Option>
          ))}
        </OptionsList>
      </OptionsContainer>
    </Container>
  )
}

SelectButtonDropdown.propTypes = {
  title: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      onClick: PropTypes.func
    })
  ).isRequired,
  color: PropTypes.string
}

export default SelectButtonDropdown
