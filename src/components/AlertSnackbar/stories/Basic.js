import React from 'react'
import { storiesOf } from '@storybook/react'
import AlertSnackbar from '..'

const BasicSnackbar = () => <AlertSnackbar open message='Alert Snackbar' />

storiesOf('Alert Snackbar', module).add('Basic', () => <BasicSnackbar />)
