import React from 'react'
import styled, { keyframes } from 'styled-components'

const bottomUp = keyframes`
  0% {
    transform: translateY(5rem);
  }

  100% {
    transform: translateY(0);
  }
`

const Container = styled.div`
  right: 1rem;
  bottom: 1rem;
  color: #fff;
  position: fixed;
  padding: 0.8rem;
  border-radius: 3px;
  z-index: 999;
  background: #313131;
  animation: ${bottomUp} 0.5s ease-in-out;
`

const AlertSnackbar = ({ open, message }) => {
  if (!open) {
    return <div></div>
  }

  return <Container>{message}</Container>
}

export default AlertSnackbar
