import React from 'react'
import { storiesOf } from '@storybook/react'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import styled from 'styled-components'
import Help from '..'

library.add(faInfoCircle)

const Container = styled.div`
  padding: 1rem;
  display: flex;
  align-items: center;
`

const DefaultHelp = () => (
  <Container>
    <p style={{ marginRight: 10 }}>Hover to see the tip</p>

    <Help tip='A wild tip has appear!' />
  </Container>
)

storiesOf('Help', module).add('Default Help', () => <DefaultHelp />)
