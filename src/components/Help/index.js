import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const HelpContainer = styled.div`
  display: inline;
  position: relative;
`

const HelpTooltip = styled.div`
  display: none;
  visibility: hidden;

  z-index: 2;
  top: -100%;
  right: -11rem;
  position: absolute;

  display: flex;
  align-items: center;
  flex-direction: column;

  padding: 1rem;
  background: #fff;
  text-align: center;
  width: 10rem;
  border: 1px solid #eee;
  border-radius: 6px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.1);
`

const HelpIcon = styled(FontAwesomeIcon).attrs({
  icon: 'info-circle'
})`
  color: #5060b4;
  font-size: 1rem;
  cursor: help;

  &:hover + ${HelpTooltip} {
    display: block;
    visibility: visible;
  }
`

const HelpIconAsHeader = styled(HelpIcon)`
  margin: 0 0 0.5rem;
  font-size: 1.5rem;
`

const Help = ({ tip }) => (
  <HelpContainer>
    <HelpIcon />

    <HelpTooltip>
      <HelpIconAsHeader />
      <p>{tip}</p>
    </HelpTooltip>
  </HelpContainer>
)

Help.propTypes = {
  tip: PropTypes.string.isRequired
}

export default Help
