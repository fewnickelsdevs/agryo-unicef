import React from 'react'
import PropTypes from 'prop-types'
import styled, { keyframes } from 'styled-components'

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const Spinner = styled.div`
  margin: 3px;
  animation: ${rotate360} 1s linear infinite;
  transform: translateZ(0);

  border-top: 2px solid grey;
  border-right: 2px solid grey;
  border-bottom: 2px solid grey;
  border-left: 4px solid black;
  background: transparent;
  width: 14px;
  height: 14px;
  border-radius: 50%;
`

const ButtonContainer = styled.button`
  border: 0;
  transition: 0.1s;
  background: #3f8c48;
  border-radius: 4px;
  cursor: pointer;
  padding: 0.8rem 1.4rem;
  min-width: 95px;
  display: flex;
  flex-direction: row;

  ${props => props.disabled && 'opacity: 0.2'}

  &:hover {
    background: #2f8c4e;
  }

  &:active {
    background: #34ad5c;
  }

  @media only screen and (max-width: 425px) {
    width: ${props => props.smSize};
  }
`

const ButtonText = styled.span`
  color: #fff;
`

export default function ButtonPrimary({
  text,
  onClick,
  disabled,
  margin,
  smSize,
  type,
  className,
  loading = false
}) {
  return (
    <ButtonContainer
      className={className}
      type={type}
      style={{ margin }}
      smSize={smSize}
      onClick={onClick}
      disabled={disabled}
    >
      <ButtonText>{text}</ButtonText>
      {loading && <Spinner />}
    </ButtonContainer>
  )
}

ButtonPrimary.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  smSize: PropTypes.string,
  margin: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string
}

ButtonPrimary.defaultProps = {
  smSize: 'auto',
  margin: '',
  type: 'button',
  className: '',
  disabled: false
}
