import React from 'react'
import { storiesOf } from '@storybook/react'
import PrimaryButton from '..'

const BasicButton = () => (
  <PrimaryButton onClick={() => {}} text='Basic Primary Button' />
)

storiesOf('Button/Primary Button', module).add('Basic Button', () => (
  <BasicButton />
))
