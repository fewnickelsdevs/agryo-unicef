import React from 'react'
import { storiesOf } from '@storybook/react'
import PrimaryButton from '..'

const MarginButton = () => (
  <PrimaryButton margin='1rem' onClick={() => {}} text='Button with margins' />
)

storiesOf('Button/Primary Button', module).add('Button w/ Margin', () => (
  <MarginButton />
))
