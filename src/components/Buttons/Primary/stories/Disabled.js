import React from 'react'
import { storiesOf } from '@storybook/react'
import PrimaryButton from '..'

const DisabledButton = () => (
  <PrimaryButton disabled onClick={() => {}} text='Disabled Primary Button' />
)

storiesOf('Button/Primary Button', module).add('Disabled Button', () => (
  <DisabledButton />
))
