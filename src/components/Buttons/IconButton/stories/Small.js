import React from 'react'
import { storiesOf } from '@storybook/react'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import IconButton from '..'

library.add(faPlus)

const BasicIconButton = () => (
  <IconButton small onClick={() => {}}>
    <FontAwesomeIcon icon='plus' color='#3F8C48' size='lg' />
  </IconButton>
)

storiesOf('Button/Icon Button', module).add('Small Icon Button', () => (
  <BasicIconButton />
))
