import React from 'react'
import styled from 'styled-components'

const StyledIconButton = styled.button`
  ${({ small, color }) => `
    color: ${color || '#fff'};
    border: 0;
    width: 3rem;
    height: 3rem;
    cursor: pointer;
    border-radius: 50%;
    background: transparent;
    outline: none;

    &:hover {
      background: rgba(0, 0, 0, 0.1);
    }

    ${small &&
      `
      width: 2rem;
      height: 2rem;
    `}
  `}
`

const IconButton = ({ children, className, small, color, onClick }) => (
  <StyledIconButton
    className={className}
    small={small}
    color={color}
    onClick={onClick}
  >
    {children}
  </StyledIconButton>
)

export default IconButton
