import React from 'react'
import { storiesOf } from '@storybook/react'
import SecondaryButton from '..'

const DisabledButton = () => (
  <SecondaryButton
    disabled
    onClick={() => {}}
    text='Disabled Secondary Button'
  />
)

storiesOf('Button/Secondary Button', module).add('Disabled Button', () => (
  <DisabledButton />
))
