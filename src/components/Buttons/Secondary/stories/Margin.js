import React from 'react'
import { storiesOf } from '@storybook/react'
import SecondaryButton from '..'

const DisabledButton = () => (
  <SecondaryButton
    margin='1rem'
    onClick={() => {}}
    text='Secondary Button w/ Margins'
  />
)

storiesOf('Button/Secondary Button', module).add('Margin Button', () => (
  <DisabledButton />
))
