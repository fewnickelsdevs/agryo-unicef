import React from 'react'
import { storiesOf } from '@storybook/react'
import SecondaryButton from '..'

const BasicButton = () => (
  <SecondaryButton onClick={() => {}} text='Basic Secondary Button' />
)

storiesOf('Button/Secondary Button', module).add('Basic Button', () => (
  <BasicButton />
))
