import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ButtonContainer = styled.button`
  border: 0;
  transition: 0.1s;
  cursor: pointer;
  border-radius: 4px;
  background: #fff;
  padding: calc(0.8rem - 2px) 1.4rem;
  border: 2px solid #3f8c48;
  min-width: 95px;

  ${props => props.disabled && 'opacity: 0.2'}

  &:hover {
    background: #2f8c4e;
    border-color: #2f8c4e;

    & * {
      color: #fff !important;
    }
  }

  &:active {
    background: #34ad5c;
    border-color: #2f8c4e;
  }

  @media only screen and (max-width: 425px) {
    width: ${props => props.smSize};
  }
`

const ButtonText = styled.span`
  color: #3f8c48;
  font-size: 1rem;
  font-weight: bold;
`

export default function ButtonSecondary({
  text,
  onClick,
  disabled,
  margin,
  smSize
}) {
  return (
    <ButtonContainer
      style={{ margin }}
      smSize={smSize}
      onClick={onClick}
      disabled={disabled}
    >
      <ButtonText>{text}</ButtonText>
    </ButtonContainer>
  )
}

ButtonSecondary.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  smSize: PropTypes.string,
  margin: PropTypes.string
}

ButtonSecondary.defaultProps = {
  smSize: 'auto',
  margin: '',
  disabled: false
}
