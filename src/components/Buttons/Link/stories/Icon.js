import React from 'react'
import { storiesOf } from '@storybook/react'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import LinkButton from '..'

library.add(faPlus)

const BasicButton = () => (
  <LinkButton
    icon={<FontAwesomeIcon icon='plus' color='#3F8C48' />}
    onClick={() => {}}
    text='Icon Link'
  />
)

storiesOf('Button/Link', module).add('Icon Link', () => <BasicButton />)
