import React from 'react'
import { storiesOf } from '@storybook/react'
import LinkButton from '..'

const BasicButton = () => (
  <LinkButton light onClick={() => {}} text='Light Link' />
)

storiesOf('Button/Link', module).add('Light Link', () => <BasicButton />)
