import React from 'react'
import { storiesOf } from '@storybook/react'
import LinkButton from '..'

const BasicButton = () => <LinkButton onClick={() => {}} text='Basic Link' />

storiesOf('Button/Link', module).add('Basic Link', () => <BasicButton />)
