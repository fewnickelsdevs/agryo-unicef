import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const LinkContainer = styled.div`
  display: flex;
  cursor: pointer;
  position: relative;
  width: fit-content;
  align-items: center;

  &::before {
    content: '';
    left: 50%;
    bottom: -2px;
    width: 100%;
    height: 2px;
    position: absolute;
    background-color: #3f8c48;
    transform-origin: center;
    transform: translate(-50%, 0) scaleX(0);
    transition: transform 0.2s ease-in-out;
  }

  ${({ light }) =>
    !light &&
    `
    &:hover::before {
      transform: translate(-50%, 0) scaleX(1);
    }
    `}
`

const LinkHref = styled.a`
  color: #3f8c48;
  font-size: 1rem;
  font-weight: ${({ light }) => (light ? 'auto' : 'bold')};

  ${({ hasIcon }) => hasIcon && 'margin-right: 5px'}
`

export default function Link({ text, icon, light, onClick }) {
  return (
    <LinkContainer light={light} onClick={onClick}>
      <LinkHref light={light} hasIcon={!!icon}>
        {text}
      </LinkHref>

      {icon}
    </LinkContainer>
  )
}

Link.propTypes = {
  text: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  icon: PropTypes.element,
  light: PropTypes.bool,
  onClick: PropTypes.func.isRequired
}

Link.defaultProps = {
  icon: null,
  light: false
}
