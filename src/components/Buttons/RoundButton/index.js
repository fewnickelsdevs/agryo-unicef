import React from 'react'
import styled, { keyframes } from 'styled-components'

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const Spinner = styled.div`
  margin: 3px;
  animation: ${rotate360} 1s linear infinite;
  transform: translateZ(0);

  border-top: 2px solid grey;
  border-right: 2px solid grey;
  border-bottom: 2px solid grey;
  border-left: 4px solid white;
  background: transparent;
  width: 14px;
  height: 14px;
  border-radius: 50%;
  margin: auto;
`

const StyledIconButton = styled.button`
  ${({ small, color, disabled }) => `
    color: ${color || '#fff'};
    border: 0;
    width: 3rem;
    height: 3rem;
    cursor: pointer;
    border-radius: 50%;
    background: #3F8C48;
    outline: none;
    ${disabled && 'opacity: 0.2;'}

    &:hover {
      background: #2f8c4e;
    }
  
    &:active {
      background: #34ad5c;
    }

    ${small &&
      `
      width: 2rem;
      height: 2rem;
    `}
  `}
`

const RoundButton = ({
  children,
  className,
  small,
  onClick,
  loading = false,
  disabled
}) => (
  <StyledIconButton
    className={className}
    small={small}
    onClick={onClick}
    disabled={disabled}
  >
    {loading ? <Spinner /> : children}
  </StyledIconButton>
)

export default RoundButton
