import React, { useRef } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledSelectInputContainer = styled.div`
  position: relative;
  display: inline;
  margin-top: 15px;
`

const StyledSelectInput = styled.select`
  width: 80%;
  background: #ffffff;
  border-radius: 6px;
  padding: 0.6rem 0.7rem;
  border: 1px solid #ccc;
  font-size: 0.85rem;

  ${props => props.disabled && 'opacity: .6;'}

  &:focus {
    outline: 0;
  }
`

const StyledPlaceholder = styled.label.attrs({
  htmlFor: 'input'
})`
  left: 0.9rem;
  top: 50%;
  color: #000;
  font-size: 0.85rem;
  position: absolute;
  transform: translateY(-50%);

  display: flex;
  align-items: center;
`

export default function SelectInput({
  placeholder,
  options,
  value,
  onChange,
  className,
  disabled,
  ...rest
}) {
  const inputRef = useRef()

  return (
    <StyledSelectInputContainer className={className}>
      <StyledSelectInput
        {...rest}
        id='input'
        disabled={disabled}
        ref={inputRef}
        autoComplete='on'
        value={value}
        onChange={e => onChange(e.target.value)}
      >
        {options.map(({ value, name, id }) => (
          <option value={value || id}>{name}</option>
        ))}
      </StyledSelectInput>
    </StyledSelectInputContainer>
  )
}

SelectInput.propTypes = {
  disabled: PropTypes.bool,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

SelectInput.defaultProps = {
  disabled: false
}
