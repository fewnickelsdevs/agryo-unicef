import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'
import styled from 'styled-components'
import Slider from '..'

const Container = styled.section`
  padding: 2rem;
`

const BasicSlider = () => {
  const [value, setValue] = useState(1)

  return (
    <Container>
      <Slider
        min={1}
        max={100}
        step={1}
        value={value}
        onChange={newValue => setValue(newValue)}
      />
    </Container>
  )
}

storiesOf('Form/Slider', module).add('Basic Slider', () => <BasicSlider />)
