import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Container = styled.div`
  position: relative;
  padding-top: 1.25rem;
`

const Counter = styled.div`
  ${({ position, offset, theme }) => `
    position: absolute;
    top: -1rem;
    color: #777;
    width: 50px;
    height: 1.5rem;
    font-size: .8em;
    line-height: 1.5rem;
    text-align: center;
    border-radius: 3px;
    border: 1px solid #ccc;
    background-color: ${theme.colors.white};
    left: calc(${position}% - ${offset}px)
  `}
`

const StyledSlider = styled.input.attrs({
  type: 'range'
})`
  ${({ theme }) => `
    width: 100%;
    height: 15px;
    outline: none;
    opacity: 0.7;
    border-radius: 5px;
    background: #d3d3d3;
    -webkit-appearance: none;
    -webkit-transition: 0.2s;
    transition: opacity 0.2s;

    &:hover {
      opacity: 1;
    }

    &::-webkit-slider-thumb {
      width: 25px;
      height: 25px;
      cursor: pointer;
      appearance: none;
      background: #fff;
      border-radius: 50%;
      border: 3px solid ${theme.colors.primaryLight};
      -webkit-appearance: none;
    }

    &::-moz-range-thumb {
      width: 25px;
      height: 25px;
      cursor: pointer;
      background: #fff;
      border-radius: 50%;
      border: 1px solid ${theme.colors.primaryLight};
    }
  `}
`

export default function Slider({ min, max, step, value, onChange }) {
  const position = ((value - min) / (max - min)) * 100

  const calculateOffset = () => {
    const thumbWidth = 50
    const positionOffset = Math.round((thumbWidth * position) / 100)

    return positionOffset
  }

  return (
    <Container>
      <Counter position={position} offset={calculateOffset()}>
        {value}
      </Counter>

      <StyledSlider
        value={value}
        onChange={ev => onChange(ev.target.value)}
        step={step}
        min={min}
        max={max}
      />
    </Container>
  )
}

Slider.propTypes = {
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired
}

Slider.defaultProps = {
  min: 1,
  max: 100,
  step: 1,
  value: 50,
  onChange: () => {}
}
