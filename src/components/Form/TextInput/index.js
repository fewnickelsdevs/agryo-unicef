import React, { useRef } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledTextInputContainer = styled.div`
  display: inline;
  margin-top: 15px;
`

const StyledTextInput = styled.input`
  display: inline;
  background: #ffffff;
  color: #333333;
  border-radius: 6px;
  padding: 0.6rem 0.7rem;
  border: 1px solid #ccc;
  font-size: 0.85rem;
  margin-bottom: 5px;
  ${props => props.disabled && 'opacity: .6;'}
  &:focus {
    outline: 0;
  }
`

const StyledPlaceholder = styled.label.attrs({
  htmlFor: 'input'
})`
  left: 0.9rem;
  top: 50%;
  color: #333333;
  font-size: 0.85rem;
  position: absolute;
  transform: translateY(-50%);
  display: flex;
  align-items: center;
`

export default function TextInput({
  placeholder,
  value,
  onChange,
  className,
  disabled,
  ...rest
}) {
  const inputRef = useRef()

  return (
    <StyledTextInputContainer className={className}>
      <StyledTextInput
        {...rest}
        disabled={disabled}
        ref={inputRef}
        autoComplete='on'
        placeholder={placeholder}
        value={value}
        onChange={e => onChange(e.target.value)}
      />
      {/* // {placeholder && !value && (
      //   <StyledPlaceholder onClick={() => inputRef.current.focus()}>
      //     {placeholder}
      //   </StyledPlaceholder>
      // )} */}
    </StyledTextInputContainer>
  )
}

TextInput.propTypes = {
  disabled: PropTypes.bool,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
}

TextInput.defaultProps = {
  disabled: false
}
