import React from 'react'
import { storiesOf } from '@storybook/react'
import TextInput from '..'

const DisabledTextInput = () => (
  <TextInput
    placeholder='Placeholder Disabled'
    disabled
    onChange={value => {}}
  />
)

storiesOf('Form/TextInput', module).add('Disabled TextInput', () => (
  <DisabledTextInput />
))
