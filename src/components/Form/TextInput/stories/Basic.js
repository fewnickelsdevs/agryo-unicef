import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'
import TextInput from '..'

const BasicTextInput = () => {
  const [value, setValue] = useState('')

  return (
    <TextInput
      placeholder='Placeholder'
      value={value}
      onChange={value => setValue(value)}
    />
  )
}

storiesOf('Form/TextInput', module).add('Basic TextInput', () => (
  <BasicTextInput />
))
