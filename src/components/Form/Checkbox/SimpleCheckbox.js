import React, { useRef } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledTextInputContainer = styled.div`
  display: inline;
  margin-top: 15px;
`

const StyledTextInput = styled.input`
  display: inline;
  background: #eee;
  border-radius: 6px;
  padding: 0.6rem 0.7rem;
  border: 1px solid #ccc;
  font-size: 0.85rem;
  margin-bottom: 5px;
  ${props => props.disabled && 'opacity: .6;'}

  &:focus {
    outline: 0;
  }
`

export default function SimpleCheckbox({ message, value, setValue }) {
  return (
    <StyledTextInputContainer>
      <input
        onClick={() => setValue(!value)}
        type='checkbox'
        value={value}
        style={{ margin: 10 }}
      />
      <label style={{ margin: 10, color: value ? 'black' : 'grey' }}>
        {message}
      </label>
    </StyledTextInputContainer>
  )
}
