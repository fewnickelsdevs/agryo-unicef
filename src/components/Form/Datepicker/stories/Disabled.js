import React from 'react'
import { storiesOf } from '@storybook/react'
import Calendar from '..'

const DisabledCalendar = () => <Calendar disabled selected={new Date()} />

storiesOf('Form/Calendar', module).add('Disabled Calendar', () => (
  <DisabledCalendar />
))
