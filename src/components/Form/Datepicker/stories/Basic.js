import React from 'react'
import { storiesOf } from '@storybook/react'
import Calendar from '..'

const BasicCalendar = () => <Calendar selected={new Date()} />

storiesOf('Form/Calendar', module).add('Basic Calendar', () => (
  <BasicCalendar />
))
