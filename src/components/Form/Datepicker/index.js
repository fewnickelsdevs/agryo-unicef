import React from 'react'
import ReactDatePicker from 'react-datepicker'
import PropTypes from 'prop-types'
import 'react-datepicker/dist/react-datepicker.min.css'

import './styles.css'

export default function DatePicker({ selected, onChange, disabled }) {
  return (
    <ReactDatePicker
      disabled={disabled}
      className={`datepicker ${disabled && 'datepicker--disabled'}`}
      dateFormat='dd/MM/yyyy'
      onChange={onChange}
      selected={selected}
    />
  )
}

DatePicker.propTypes = {
  onChange: PropTypes.func.isRequired,
  selected: PropTypes.object,
  disabled: PropTypes.bool
}

DatePicker.defaultProps = {
  selected: new Date(),
  disabled: false
}
