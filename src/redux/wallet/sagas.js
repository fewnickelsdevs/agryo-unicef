import { takeLatest, call, put, select, all } from 'redux-saga/effects'

import {
  FETCH_WALLET,
  FETCH_WALLET_FETCHING,
  FETCH_WALLET_SUCCESS,
  FETCH_WALLET_ERROR,
  AUTORIZE_VALUE_WALLET,
  REQUEST_FAUCET
} from './constants'
import {
  autorizeValueWalletRunning,
  autorizeValueWalletThrowError,
  autorizeValueWalletSuccess
} from './actions'
import { createContractTransaction } from '../transaction/sagas'
import { getErc20AllowBalance, getErc20AddressBalance, getEtherBalance, etherFormatFromValue, generateAgryoUsdContract, generateFaucetContract } from '../../web3/contracts';

const getWalletFinanceInfoFromBlockchain = async userAddress =>
  new Promise(async (resolve, reject) => {
    try {
      const [
        crops,
        livestock,
        greenBond,
        agryoStableBalance,
        etherBalance
      ] = await Promise.all([
        getErc20AllowBalance(userAddress, process.env.REACT_APP_CROPS_CONTRACT_ADDRESS, process.env.REACT_APP_AGRYO_USD_CONTRACT_ADDRESS),
        getErc20AllowBalance(userAddress, process.env.REACT_APP_LIVESTOCK_CONTRACT_ADDRESS, process.env.REACT_APP_AGRYO_USD_CONTRACT_ADDRESS),
        getErc20AllowBalance(userAddress, process.env.REACT_APP_GREEN_BOND_CONTRACT_ADDRESS, process.env.REACT_APP_AGRYO_USD_CONTRACT_ADDRESS),
        getErc20AddressBalance(userAddress, process.env.REACT_APP_AGRYO_USD_CONTRACT_ADDRESS),
        getEtherBalance(userAddress)
      ])

      resolve({ crops, livestock, greenBond, agryoStableBalance, etherBalance })
    } catch (err) {
      console.log(err)
      reject(err)
    }
  })

function* getWalletInfo() {
  yield put({ type: FETCH_WALLET_FETCHING, payload: {} })
  try {
    const userAddress = yield select(state => state.user.user.web3Address)
    const {
      crops,
      livestock,
      greenBond,
      agryoStableBalance,
      etherBalance
    } = yield call(getWalletFinanceInfoFromBlockchain, userAddress)
    yield put({
      type: FETCH_WALLET_SUCCESS,
      payload: {
        aproveValues: { crops, livestock, greenBond },
        balance: { agryoStableBalance, etherBalance }
      }
    })
  } catch (err) {
    console.log(err)
    yield put({ type: FETCH_WALLET_ERROR, payload: err })
  }
}

function* aproveAllowValue(action) {
  function* onTransactionSend(channel) {
    yield put(autorizeValueWalletRunning(channel.tx))
  }

  function* onTransactionSuccess(channel) {
    yield put(autorizeValueWalletSuccess(channel.receipt))
    yield call(getWalletInfo)
  }

  function* onTransactionError(channel) {
    yield put(autorizeValueWalletThrowError(channel.error))
  }

  const contractAddressTypes = {
    Crops: process.env.REACT_APP_CROPS_CONTRACT_ADDRESS,
    'Green Bond': process.env.REACT_APP_GREEN_BOND_CONTRACT_ADDRESS,
    Livestock: process.env.REACT_APP_LIVESTOCK_CONTRACT_ADDRESS
  }

  const { contract: contractType, value } = action.payload
  const contractAddress = contractAddressTypes[contractType]

  try {
    const rawValue = etherFormatFromValue(value)
    const tokenContract = yield call(generateAgryoUsdContract)
    const transactionData = yield call(
      tokenContract.methods.approve,
      contractAddress,
      rawValue
    )

    yield call(
      createContractTransaction,
      transactionData,
      onTransactionSuccess,
      onTransactionError,
      onTransactionSend
    )
  } catch (err) {
    yield put(autorizeValueWalletThrowError(err))
    throw err
  }
}

function* requestFaucetSaga(){
  try{
    const faucetContract = yield call(generateFaucetContract)
    const result = yield call(faucetContract.methods.requestFaucetTokens)
    yield call(createContractTransaction, result)

  }catch (err){
    ;
  }
}

export function* walletSagas() {
  yield all([
    takeLatest(FETCH_WALLET, getWalletInfo),
    takeLatest(AUTORIZE_VALUE_WALLET, aproveAllowValue),
    takeLatest(REQUEST_FAUCET, requestFaucetSaga)
  ])
}
