import {
  FETCH_WALLET_FETCHING,
  FETCH_WALLET_SUCCESS,
  FETCH_WALLET_ERROR,
  AUTORIZE_VALUE_WALLET_WAITING,
  AUTORIZE_VALUE_WALLET_RUNNING,
  AUTORIZE_VALUE_WALLET_ERROR,
  AUTORIZE_VALUE_WALLET_SUCCESS,
  AUTORIZE_VALUE_WALLET_RESET
} from './constants'

const defaultWallet = {
  loading: false,
  error: false,
  valueAprove: { crops: 0, livestock: 0, greenBond: 0 },
  balance: { agryoStableBalance: 0, etherBalance: 0 }
}

export const walletReducer = (state = defaultWallet, { type, payload }) => {
  switch (type) {
    case FETCH_WALLET_FETCHING:
      return { ...defaultWallet, loading: true }
    case FETCH_WALLET_SUCCESS:
      return {
        loading: false,
        error: false,
        balance: payload.balance,
        valueAprove: payload.aproveValues
      }
    case FETCH_WALLET_ERROR:
      return { ...defaultWallet, error: true }
    default:
      return state
  }
}

const aproveValueDefault = {
  waitingSign: false,
  txRunning: false,
  deploy: false,
  confimed: false,
  errorAproveTx: false
}

export const aproveValueReducer = (
  state = aproveValueDefault,
  { type, payload }
) => {
  switch (type) {
    case AUTORIZE_VALUE_WALLET_WAITING:
      return {
        waitingSign: true,
        deploy: false,
        confimed: true,
        errorAproveTx: false
      }
    case AUTORIZE_VALUE_WALLET_RUNNING:
      return {
        waitingSign: false,
        txRunning: true,
        deploy: payload.transactionHash,
        confimed: false,
        ererrorAproveTxror: false
      }
    case AUTORIZE_VALUE_WALLET_ERROR:
      return {
        waitingSign: false,
        deploy: false,
        confimed: false,
        errorAproveTx: payload.error
      }
    case AUTORIZE_VALUE_WALLET_SUCCESS:
      return {
        waitingSign: false,
        txRunning: true,
        deploy: false,
        confimed: payload.txReceipt,
        errorAproveTx: false
      }
    case AUTORIZE_VALUE_WALLET_RESET:
      return aproveValueDefault
    default:
      return state
  }
}
