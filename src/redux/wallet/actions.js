import {
  AUTORIZE_VALUE_WALLET,
  AUTORIZE_VALUE_WALLET_WAITING,
  AUTORIZE_VALUE_WALLET_RUNNING,
  AUTORIZE_VALUE_WALLET_ERROR,
  AUTORIZE_VALUE_WALLET_SUCCESS,
  AUTORIZE_VALUE_WALLET_RESET,
  FETCH_WALLET,
  REQUEST_FAUCET
} from './constants'

export const fetchWalletData = () => ({
  type: FETCH_WALLET
})


export const requestFaucetAgryoUsd = () => ({
  type: REQUEST_FAUCET
})


export const autorizeValueWallet = (contract, value) => ({
  type: AUTORIZE_VALUE_WALLET,
  payload: { contract, value }
})

export const autorizeValueWalletWaiting = () => ({
  type: AUTORIZE_VALUE_WALLET_WAITING
})

export const autorizeValueWalletRunning = transactionHash => ({
  type: AUTORIZE_VALUE_WALLET_RUNNING,
  payload: { transactionHash }
})

export const autorizeValueWalletThrowError = error => ({
  type: AUTORIZE_VALUE_WALLET_ERROR,
  payload: { error }
})

export const autorizeValueWalletSuccess = txReceipt => ({
  type: AUTORIZE_VALUE_WALLET_SUCCESS,
  payload: { txReceipt }
})

export const autorizeValueWalletReset = () => ({
  type: AUTORIZE_VALUE_WALLET_RESET
})
