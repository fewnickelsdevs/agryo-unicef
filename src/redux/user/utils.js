import * as R from 'ramda'

import { removeDecimals } from '../../formatters/currencyDecimal'
import api from '../../services/config'

export const requestLogin = async (email, password, userType) => {
  const result = await api.post('/dapp/v1/user/auth', {
    email,
    password,
    userType: parseInt(userType)
  })
  return result
}

export const fetchUserInformation = async () => {
  const result = await api.get(`/api/v1/user`)
  return result.data
}

export const confirmLoginWithEmailCode = async (
  tokenConfirmation,
  identifyBrowserToken
) => {
  const result = await api.post('/v1/user/auth/login/confirm', {
    tokenConfirmation,
    identifyBrowserToken
  })
  return result.data
}

export const requestEmailCode = async identifyBrowserToken => {
  const result = await api.post('/v1/user/auth/login/confirm/request/email', {
    identifyBrowserToken
  })
  return result
}

export const registerNewUser = async ({ name, email, password }) => {
  const result = await api.post('/dapp/v1/user/signup', {
    name,
    email,
    password
  })

  return result.data
}

export const registerFarmerInvitate = async ({
  name,
  email,
  password,
  invitateFarmerToken
}) => {
  const result = await api.post('/dapp/v1/farmer/invitate/confirm', {
    name,
    email,
    password,
    token: invitateFarmerToken
  })
  return result.data
}

export const confirmNewUser = async token => {
  const result = await api.post('/dapp/v1/user/signup/confirm', {
    token
  })

  return result.data
}

export const logoutMainApi = async () => {
  await api.post("/api/v1/logout")
  window.localStorage.removeItem('agryo-jwt')
  document.location.reload()
  return true
}

export const requestResetPasswordApi = async (email, userType) => {
  const result = await api.post('/dapp/v1/user/reset/password', { email, userType: parseInt(userType) })
  return result
}

export const confirmResetPasswordApi = async (token, password) => {
  const result = await api.post('/dapp/v1/user/reset/password/confirm', {
    password,
    token
  })
  return result
}

export const confirmCompanyRegister = async (
  token,
  email,
  password,
  companyName
) => {
  const result = await api.post('/dapp/v1/company/signup', {
    token,
    email,
    password,
    companyName
  })
  return result
}

export const confirmAgroTechnicalRegister = async (
  token,
  email,
  password,
  name,
  city,
  state,
  country
) => {
  const result = await api.post('/dapp/v1/technical/signup', {
    token,
    email,
    password,
    name,
    city,
    state,
    country
  })
  return result
}

export const getAgryoStableBalanceByAddress = async address => {
  const balance = await removeDecimals(80000000000000000)
  return balance
}


export const mapObjectToReducerFormat = R.applySpec({
  publicAddress: R.path(['address']),
  ethereumNetwork: R.path(['network']),
  balanceAgryoStable: R.path(['agryoUsdBalance']),
  authContractVerified: !R.isNil(R.path(['token'])),
  name: R.path(['user', 'name']),
  jwtToken: R.path(['token']),
  weatherData: R.path(['weatherData'])
})




// Web3


export const getNounceLoginTokenWeb3 = async web3Address => {
  const response = await api.post('/dapp/v1/user/web3/auth/request', {
    web3Address
  })
  return response.data
}


export const requestLoginWeb3 = async (signature, token, web3Address, userType) => {
  const result = await api.post('/dapp/v1/user/web3/auth/confirm', {
    token, signature, web3Address, userType
  })
  return result.data
}


export const registerNewWeb3User = async (name, email, userType, signature, web3Address) => {
  const result = await api.post('/dapp/v1/user/web3/register', {
    name, email, userType, signature, web3Address
  })
  return result.data
}

export const confirmNewWeb3Investor = async (token) => {
  const result = await api.post('/dapp/v1/user/web3/register/invest/confirm', {
    token
  })
  return result.data
}

export const confirmNewWeb3Farmer = async (token) => {
  const result = await api.post('/dapp/v1/user/web3/register/farmer/confirm', {
    token
  })
  return result.data
}