import {
  UPDATE_USER,
  TOGGLE_LOADING,
  REGISTER_USER_ERROR,
  REGISTER_USER_SUCCESS,
  RESET_STATE,
  AUTH_USER_ERROR,
  CONFIRM_REGISTER_SUCCESS,
  CONFIRM_REGISTER_ERROR,
  TOO_MANY_LOGIN_ATTEMPS,
  AUTH_WAITING_EMAIL_CONFIRMATION,
  UNLOCK_AFTER_TIME_WAITING,
  AUTH_WAITING_EMAIL_CONFIRMATION_UNLOCK_RESEND,
  HIDE_AUTH_USER_ERROR,
  GO_TO_LOGIN_AND_LOCK,
  UPDATE_IDENTIFY_BROWSER_TOKEN,
  UNLOCK_SEND_EMAIL_AFTER_TIME_WAITING,
  REQUEST_RESET_PASSWORD_SUCCESS,
  REQUEST_RESET_PASSWORD_ERROR,
  CONFIRM_RESET_PASSWORD_SUCCESS,
  CONFIRM_RESET_PASSWORD_ERROR,
  SET_FARMER_WEATHER_INFO
} from './constants'

const defaultUserState = {
  balanceAgryoStable: 0,
  authContractVerified: '',
  user: {
    name: false,
    userType: 0, 
    companyId: 0,
    regionId: 0,
    id: 0,
    web3Address: false
  },
  farmerWeatherInfo: false,
  loading: false,
  success: false,
  error: null,
  requestEmailCodeConfirmation: false,
  tooManyRequest: false,
  requestEmailWaitPeriod: false,
  identifyBrowserToken: false,
  resetPasswordSuccess: false,
  resetPasswordError: false
}

const userReducer = (state = defaultUserState, action) => {
  switch (action.type) {
    case RESET_STATE:
      return defaultUserState
    case UPDATE_USER:
      return { ...state, user: action.payload }
    case SET_FARMER_WEATHER_INFO:
      return { ...state, farmerWeatherInfo: action.payload }
    case TOGGLE_LOADING:
      return { ...state, loading: true }
    case AUTH_USER_ERROR:
    case REGISTER_USER_ERROR:
      return { ...state, error: action.payload, loading: false }
    case REGISTER_USER_SUCCESS:
      return { ...state, success: true, loading: false }
    case CONFIRM_REGISTER_SUCCESS:
      return { ...state, success: true, loading: false }
    case CONFIRM_REGISTER_ERROR:
      return { ...state, success: false, loading: false, error: true }

    case AUTH_WAITING_EMAIL_CONFIRMATION:
      return {
        ...state,
        requestEmailCodeConfirmation: true,
        requestEmailWaitPeriod: action.retryAfter,
        identifyBrowserToken: action.identifyBrowserToken,
        loading: false
      }
    case AUTH_WAITING_EMAIL_CONFIRMATION_UNLOCK_RESEND:
      return { ...state, requestEmailWaitPeriod: false }
    case UPDATE_IDENTIFY_BROWSER_TOKEN:
      return { ...state, identifyBrowserToken: action.identifyBrowserToken }

    case HIDE_AUTH_USER_ERROR:
      return { ...state, error: false }

    case TOO_MANY_LOGIN_ATTEMPS:
      return {
        ...defaultUserState,
        tooManyRequest: action.retryAfter,
        loading: false,
        error: false
      }

    case UNLOCK_AFTER_TIME_WAITING:
      return { ...state, tooManyRequest: false }

    case UNLOCK_SEND_EMAIL_AFTER_TIME_WAITING:
      return { ...state, requestEmailWaitPeriod: false }

    case GO_TO_LOGIN_AND_LOCK:
      return { ...defaultUserState, tooManyRequest: action.retryAfter }

    case REQUEST_RESET_PASSWORD_SUCCESS:
      return { ...defaultUserState, resetPasswordSuccess: true }

    case REQUEST_RESET_PASSWORD_ERROR:
      return { ...defaultUserState, error: true }

    case CONFIRM_RESET_PASSWORD_SUCCESS:
      return { ...defaultUserState, resetPasswordSuccess: true }

    case CONFIRM_RESET_PASSWORD_ERROR:
      return { ...defaultUserState, error: true }

    default:
      return state
  }
}

export default userReducer
