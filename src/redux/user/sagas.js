import { put, call, all, takeLatest, delay, select } from 'redux-saga/effects'
import Web3 from 'web3'
import { push } from 'connected-react-router'
import { fetchFarmerWeatherDataFromAPI } from '../property/utils'
import {
  AUTH_USER_WEB3_REQUEST,
  AUTH_USER_REQUEST,
  REFRESH_USER,
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
  TOGGLE_LOADING,
  AUTH_USER_ERROR,
  REGISTER_AGROTECHNICAL_REQUEST,
  CONFIRM_REGISTER_SUCCESS,
  CONFIRM_REGISTER_ERROR,
  CONFIRM_REGISTER_REQUEST,
  AUTH_REQUEST_EMAIL_CONFIRMATION,
  TOO_MANY_LOGIN_ATTEMPS,
  AUTH_WAITING_EMAIL_CONFIRMATION,
  HIDE_AUTH_USER_ERROR,
  GO_TO_LOGIN_AND_LOCK,
  UPDATE_IDENTIFY_BROWSER_TOKEN,
  REQUEST_EMAIL_WITH_CODE,
  REQUEST_RESET_PASSWORD,
  REQUEST_RESET_PASSWORD_SUCCESS,
  REGISTER_COMPANY_REQUEST,
  CONFIRM_RESET_PASSWORD,
  CONFIRM_RESET_PASSWORD_SUCCESS,
  CONFIRM_RESET_PASSWORD_ERROR,
  REFRESH_USER_COMPANY,
  AUTH_COMPANY_REQUEST,
  LOGOUT_USER,
  UPDATE_USER,
  SET_FARMER_WEATHER_INFO,
  REGISTER_USER_WEB3_REQUEST,
  CONFIRM_REGISTER_WEB3_FARMER_REQUEST,
  CONFIRM_REGISTER_WEB3_INVESTOR_REQUEST
} from './constants'

import {
  requestLogin,
  fetchUserInformation,
  registerNewUser,
  confirmNewUser,
  confirmLoginWithEmailCode,
  requestEmailCode,
  requestResetPasswordApi,
  confirmResetPasswordApi,
  confirmCompanyRegister,
  confirmAgroTechnicalRegister,
  registerFarmerInvitate,
  logoutMainApi,
  getCompanyAnnualStats,
  getCompanyRegionStats,
  getNounceLoginTokenWeb3,
  requestLoginWeb3,
  registerNewWeb3User,
  confirmNewWeb3Investor,
  confirmNewWeb3Farmer
} from './utils'
import { generateLoginObjectEip712Domain, signTypedDatav3Wallet, generateRegisterObjectEip712Domain } from '../../web3/ethereumSignMessage';

async function getWeb3(){
    if (!window.ethereum && !window.web3) {
      return false
    } else if (window.ethereum) {
      try {
        let web3 = new Web3(window.ethereum)
        await window.ethereum.enable()
        const network = await web3.eth.getChainId()
        console.log("Web3 Network: ", network)
        const accounts = await web3.eth.getAccounts()
        console.log("Connected Address: ", accounts[0])
        if(accounts[0].length > 5){
          return { web3, address: accounts[0] }
        }
        return false
      } catch (err) {
        console.log(err)
        return false

      }
    } else if (window.web3) {
        let web3 = new Web3(window.web3.currentProvider)
        const network = await web3.eth.getChainId()
        console.log("Web3 Network: ", network)
        const accounts = await web3.eth.getAccounts()
        console.log("Connected Address: ", accounts[0])
        if(accounts[0].length > 5){
          return { web3, address: accounts[0] }
        }
        return false
    } else {
      return false
    }
}


function* fetchAndUpdateUserData() {
  try {
    const isLoading = yield select(state => state.user.loading)
    if (isLoading) {
      return
    }
    const user = yield call(fetchUserInformation)
    const data = yield call(getWeb3)
    if(!data || !(user.web3Address == data.address)){
      localStorage.removeItem('agryo-jwt')
      yield put(push('/'))
    }
    window.web3 = data.web3
    //@@Dev Dont call this on Investor side
    const investMode = process.env.REACT_APP_INVEST_MODE === 'true'
    let weatherData = {}
    if(!investMode){
      weatherData = yield call(fetchFarmerWeatherDataFromAPI, user.id)
    }
    yield put({ type: UPDATE_USER, payload: user })
    yield put({ type: SET_FARMER_WEATHER_INFO, payload: weatherData})
  } catch (err) {
    console.log(err)
    localStorage.removeItem('agryo-jwt')
    yield put(push('/'))
  }
}

function* logoutUser() {
  try {
    yield call(logoutMainApi)
  } catch (err) {
    console.log(err)
  }
}

function* dispatchErrorAndResetAfterDelay(error, ms) {
  yield put({ type: error.type, payload: error.message })
  yield delay(ms)
  yield put({ type: HIDE_AUTH_USER_ERROR })

  console.warn(error.message)
}


function* authenticateWeb3User({ payload }) {
  const { web3Instance, userWeb3Address, userType } = payload
  try {
    yield put({ type: TOGGLE_LOADING })

    // Get token login to user sign on login
    const challenge = yield call(getNounceLoginTokenWeb3, userWeb3Address)
    const loginWeb3Object = yield call(
      generateLoginObjectEip712Domain,
      challenge.token
    )
    const signature = yield call(
      signTypedDatav3Wallet,
      web3Instance,
      loginWeb3Object
    )
    const { token } = yield call(
      requestLoginWeb3,
      signature,
      challenge.token,
      userWeb3Address,
      userType
    )

    // if (!rest.status) {
    //   throw new Error('Usuário não registrado!')
    // }

    if(token){
      window.web3 = web3Instance
      localStorage.setItem('agryo-jwt', token)
      let userTypeString = "7"
      if(userType === "farmer"){
        userTypeString = 6
      }
      localStorage.setItem('agryo-user-type', userTypeString)
    }

    yield call(fetchAndUpdateUserData)
    yield put(push('/account'))
  } catch (err) {
    yield dispatchErrorAndResetAfterDelay(
      {
        type: AUTH_USER_ERROR,
        message: err.message
      },
      3000
    )
  }
}


function* authenticateUser({ payload }) {
  const { password, email } = payload
  try {
    yield put({ type: TOGGLE_LOADING })

    const result = yield call(requestLogin, email, password, 5)
    if (result.data.token) {
      localStorage.setItem('agryo-jwt', result.data.token)
      localStorage.setItem('agryo-user-type', '5')
    }
    yield put(push('/account'))
  } catch (err) {
    console.log(err)
    const { message, requestConfirmation } = err.response.data
    if (message === 'Too Many Requests') {
      const retryAfter = parseInt(err.response.headers['retry-after']) || 60
      yield put({ type: TOO_MANY_LOGIN_ATTEMPS, retryAfter })
      return
    }
    if (message === 'LOGIN_INVALID_PASS') {
      yield dispatchErrorAndResetAfterDelay(
        { type: AUTH_USER_ERROR, message: 'Invalid Password' },
        3000
      )
      return
    }
    if (message === 'LOGIN_ACCOUNT_NOT_FOUND') {
      yield dispatchErrorAndResetAfterDelay(
        { type: AUTH_USER_ERROR, message: 'User not registered' },
        3000
      )
      return
    }
    if (message === 'ACCOUNT_LOCKED') {
      yield dispatchErrorAndResetAfterDelay(
        { type: AUTH_USER_ERROR, message: 'Locked Account' },
        3000
      )
      return
    }
    if (requestConfirmation) {
      const identifyBrowserToken = err.response.data.identifyBrowserToken
      const retryAfter = parseInt(err.response.headers['retry-after']) || 60
      yield put({
        type: AUTH_WAITING_EMAIL_CONFIRMATION,
        retryAfter,
        identifyBrowserToken
      })
      return
    }
    yield dispatchErrorAndResetAfterDelay(
      { type: AUTH_USER_ERROR, message: 'Error' },
      3000
    )
    return
  }
}


function* confirmAuthenticateUserWithEmailToken({ payload }) {
  const { tokenConfirmation } = payload
  try {
    yield put({ type: TOGGLE_LOADING })
    const identifyBrowserToken = yield select(
      state => state.user.identifyBrowserToken
    )

    const { token } = yield call(
      confirmLoginWithEmailCode,
      tokenConfirmation,
      identifyBrowserToken
    )
    console.log(token)

    if (token) {
      localStorage.setItem('agryo-jwt', token)
    }

    yield call(fetchAndUpdateUserData)
    yield put(push('/account'))
  } catch (err) {
    console.log(err)
    const {
      message,
      requestConfirmation,
      identifyBrowserToken
    } = err.response.data

    if (message === 'Invalid Code' || 'Invalid Input') {
      //console.log('Invalid Code')
      yield put({ type: UPDATE_IDENTIFY_BROWSER_TOKEN, identifyBrowserToken })
    }
    if (message === 'Too Many Errors') {
      //console.log('Too Many Errors')
      const retryAfter = parseInt(err.response.headers['retry-after']) || 60
      yield put({ type: GO_TO_LOGIN_AND_LOCK, retryAfter })
    }
    yield dispatchErrorAndResetAfterDelay(
      {
        type: AUTH_USER_ERROR,
        message
      },
      3000
    )
  }
}

function* requestResendEmailWithCode() {
  try {
    const identifyBrowserToken = yield select(
      state => state.user.identifyBrowserToken
    )
    const result = yield call(requestEmailCode, identifyBrowserToken)
    const newIdentifyBrowserToken = result.data.identifyBrowserToken
    const retryAfter = parseInt(result.headers['retry-after'])
    yield put({
      type: AUTH_WAITING_EMAIL_CONFIRMATION,
      retryAfter,
      identifyBrowserToken: newIdentifyBrowserToken
    })
    yield dispatchErrorAndResetAfterDelay(
      {
        type: AUTH_USER_ERROR,
        message: 'Email Sent!'
      },
      3000
    )
  } catch (err) {
    console.log(err)
    const { identifyBrowserToken } = err.response.data
    if (identifyBrowserToken) {
      const retryAfter = parseInt(err.response.headers['retry-after'])
      yield put({
        type: AUTH_WAITING_EMAIL_CONFIRMATION,
        retryAfter,
        identifyBrowserToken
      })
    } else {
      yield dispatchErrorAndResetAfterDelay(
        {
          type: AUTH_USER_ERROR,
          message: 'Internal Error'
        },
        3000
      )
    }
  }
}

function* createUser({ payload }) {
  try {
    yield put({ type: TOGGLE_LOADING })
    if (payload.invitateFarmerToken) {
      yield call(registerFarmerInvitate, payload)
    } else {
      yield call(registerNewUser, payload)
    }
    yield put({ type: REGISTER_USER_SUCCESS })
  } catch (err) {
    console.log(err)
    let message = 'Error'
    if (err.response.data.message === 'REGISTER_EMAIL_ALREADY_USE') {
      message = 'Email already in use'
    } else if (
      err.response.data.message === 'REGISTER_WAITING_EMAIL_CONFIRMATION'
    ) {
      message = 'Check your mailbox, you already have a pending account'
    }
    if (err.response.data.message === 'REGISTER_EMAIL_NOT_AUTHORIZED') {
      message = 'Not authorized'
    }
    yield dispatchErrorAndResetAfterDelay(
      { type: REGISTER_USER_ERROR, message: message },
      3000
    )
  }
}

function* confirmUser({ payload }) {
  const { token } = payload
  try {
    yield put({ type: TOGGLE_LOADING })
    yield call(confirmNewUser, token)
    yield put({ type: CONFIRM_REGISTER_SUCCESS })
  } catch (err) {
    yield put({ type: CONFIRM_REGISTER_ERROR })
  }
}

function* confirmWeb3Farmer({ payload }) {
  const { token } = payload
  try {
    yield put({ type: TOGGLE_LOADING })
    yield call(confirmNewWeb3Farmer, token)
    yield put({ type: CONFIRM_REGISTER_SUCCESS })
  } catch (err) {
    yield put({ type: CONFIRM_REGISTER_ERROR })
  }
}

function* confirmWeb3Investor({ payload }) {
  const { token } = payload
  try {
    yield put({ type: TOGGLE_LOADING })
    yield call(confirmNewWeb3Investor, token)
    yield put({ type: CONFIRM_REGISTER_SUCCESS })
  } catch (err) {
    yield put({ type: CONFIRM_REGISTER_ERROR })
  }
}

function* requestResetUserPassword({ payload }) {
  const { email, userType } = payload
  try {
    yield put({ type: TOGGLE_LOADING })
    const result = yield call(requestResetPasswordApi, email, userType)
    yield put({ type: REQUEST_RESET_PASSWORD_SUCCESS })
  } catch (err) {
    if (err.response.status === 429) {
      yield put({ type: REQUEST_RESET_PASSWORD_SUCCESS })
    } else {
      let messageX = 'Error'
      const { message } = err.response.data
      if (message === 'RESET_PASS_NOT_FOUND_USER') {
        messageX = 'User Not Found'
      }
      yield dispatchErrorAndResetAfterDelay(
        { type: REGISTER_USER_ERROR, message: messageX },
        3000
      )
      //yield put({ type: REQUEST_RESET_PASSWORD_ERROR })
    }
  }
}

function* confirmResetUserPassword({ payload }) {
  const { token, password } = payload
  try {
    yield put({ type: TOGGLE_LOADING })
    const result = yield call(confirmResetPasswordApi, token, password)
    yield put({ type: CONFIRM_RESET_PASSWORD_SUCCESS })
  } catch (err) {
    yield put({ type: CONFIRM_RESET_PASSWORD_ERROR })
  }
}



function* createWeb3User({ payload }) {
  try {
    const { name, email, web3Address, web3Instance, typeUser } = payload
    yield put({ type: TOGGLE_LOADING })
    
    const registerWeb3Object = yield call(
      generateRegisterObjectEip712Domain,
      name, email, web3Address, typeUser
    )
    const signature = yield call(
      signTypedDatav3Wallet,
      web3Instance,
      registerWeb3Object
    )    
    yield call(registerNewWeb3User, name, email, typeUser, signature, web3Address)   
    yield put({ type: REGISTER_USER_SUCCESS })

  } catch (err) {
    console.log(err)
    let message = 'Error'
    if (err.response.data.message === 'REGISTER_EMAIL_ALREADY_USE') {
      message = 'Email already in use'
    } else if (
      err.response.data.message === 'REGISTER_WAITING_EMAIL_CONFIRMATION'
    ) {
      message = 'Check your mailbox, you already have a pending account'
    }
    if (err.response.data.message === 'REGISTER_EMAIL_NOT_AUTHORIZED') {
      message = 'Not authorized'
    }
    yield dispatchErrorAndResetAfterDelay(
      { type: REGISTER_USER_ERROR, message: message },
      3000
    )
  }
}


export function* userSagas() {
  yield all([
    takeLatest(AUTH_USER_REQUEST, authenticateUser),
    takeLatest(AUTH_USER_WEB3_REQUEST, authenticateWeb3User),
    takeLatest(LOGOUT_USER, logoutUser),
    takeLatest(REFRESH_USER, fetchAndUpdateUserData),
    takeLatest(REGISTER_USER_REQUEST, createUser),
    takeLatest(REGISTER_USER_WEB3_REQUEST, createWeb3User),
    takeLatest(CONFIRM_REGISTER_REQUEST, confirmUser),
    takeLatest(CONFIRM_REGISTER_WEB3_FARMER_REQUEST, confirmWeb3Farmer),
    takeLatest(CONFIRM_REGISTER_WEB3_INVESTOR_REQUEST, confirmWeb3Investor),
    takeLatest(
      AUTH_REQUEST_EMAIL_CONFIRMATION,
      confirmAuthenticateUserWithEmailToken
    ),
    takeLatest(REQUEST_EMAIL_WITH_CODE, requestResendEmailWithCode),
    takeLatest(REQUEST_RESET_PASSWORD, requestResetUserPassword),
    takeLatest(CONFIRM_RESET_PASSWORD, confirmResetUserPassword)
  ])
}
