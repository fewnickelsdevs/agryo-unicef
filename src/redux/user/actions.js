import { UPDATE_USER } from './constants'

export const updateUserInformations = information => ({
  type: UPDATE_USER,
  payload: information
})
