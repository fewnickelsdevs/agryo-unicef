import currency from 'currency.js'
import { all, call, put, takeLatest, select } from 'redux-saga/effects'
import { differenceInCalendarDays, format } from 'date-fns'
import {
  CROPS_CONTRACT_REQUEST,
  LIVESTOCK_CONTRACT_REQUEST,
  GREEN_BOND_REQUEST,
  CREATE_CONTRACT_LOADING,
  REQUEST_COMPANY_CONTRACTS,
  SET_PENDING_CONTRACTS,
  REQUEST_REGION_CONTRACTS
} from './constants.js'
import { throwTxError, createContractSuccess } from './actions.js'
import { createContractTransaction } from '../transaction/sagas'
import { crops } from '../../mocks/crops.json'
import { livestocks } from '../../mocks/livestocks.json'
import { etherFormatFromValue } from '../../contracts/api/utils.js'
import {
  registerNewWeb3CropsContractApi,
  registerNewWeb3GreenBondContractApi,
  registerNewWeb3LivestockContractApi,
  getRegionContractsFromApi,
  getCompanyContractsFromApi
} from './utils'
import { generateCostingCropsContract, generateCostingLivestockContract, generateGreenBondContract } from '../../web3/contracts.js';
import { finishTx, mineTx } from '../transaction/actions.js';

const differenceInDays = (laterDate, erlierDate) => {
  return differenceInCalendarDays(laterDate, erlierDate)
}

const removeSymbolsFromString = string => string.replace(/[^0-9]/g, '')



function* defaultOnSuccess(channel) {
  const userAddress = yield select(state => state.user.web3Address)
  yield put(finishTx())
}

function* defaultOnError(channel) {
  yield put(throwTxError(channel.error))
}

function* defaultOnSend(channel) {
  
  yield put(mineTx(channel.tx))
}

function* createCropsContract({ payload }) {
  const isLoading = yield select(state => state.contract.loading)
  if (isLoading) {
    return
  }
  let { contract, farmerId } = payload
  const {
    product,
    property,
    technologyLevel,
    contractStarts,
    contractEnds,
    interest,
    loanValue,
    minimumContractValue: percentage
  } = contract

   if(!farmerId){
      farmerId = yield select(state => state.user.user.id)
    }
  try {
    yield put({ type: CREATE_CONTRACT_LOADING })

    const selectedProduct = crops.find(c => c.id === product)
    const yieldInTons = {
      1: selectedProduct.prodOne,
      2: selectedProduct.prodTwo,
      3: selectedProduct.prodThree
    }[technologyLevel]

    const startDate = Math.floor(contractStarts / 1000)
    const endDate = Math.floor(contractEnds / 1000)

    const fundedValueInEtherFormat = etherFormatFromValue(loanValue)

    const cropsContract = yield call(generateCostingCropsContract)

    const result = yield call(
      cropsContract.methods.addCropsContractInfo,
      property.properties.f1,
      product,
      +technologyLevel,
      currency(parseInt(property.properties.f4 * 100) / 100).intValue,
      currency(yieldInTons).intValue,
      startDate,
      endDate,
      9,
      fundedValueInEtherFormat,
      fundedValueInEtherFormat
    )



    const x = yield call(createContractTransaction, result)
    if(x.type == "FINAL_TX"){      
    const result = yield call(
      registerNewWeb3CropsContractApi,
      x.receipt.transactionHash,
      property.properties.f1,
      product,
      +technologyLevel,
      currency(parseInt(property.properties.f4 * 100) / 100).intValue,
      currency(yieldInTons).intValue,
      startDate,
      endDate,
      currency(loanValue).intValue
    )
      yield put(createContractSuccess())
    } else {
      throw ("errors_create_contract_web3_crops")
    }
    // const result = yield call(
    //   deployNewCropsContractFromTechnicalApi,
    //   property.properties.f1,
    //   product,
    //   currency(parseInt(property.properties.f4 * 100) / 100).intValue,
    //   +technologyLevel,
    //   currency(yieldInTons).intValue,
    //   startDate,
    //   endDate,
    //   currency(loanValue).intValue,
    //   farmerId,
    //   contract.currency
    // )
  } catch (err) {
    yield put(throwTxError(err))
    throw err
  }
}

function* createLivestockContract({ payload }) {
  const isLoading = yield select(state => state.contract.loading)
  if (isLoading) {
    return
  }
  let { contract, farmerId } = payload
  const {
    product: animal,
    property,
    technologyLevel,
    contractStarts,
    contractEnds,
    interest,
    loanValue,
    numberOfAnimals,
    minimumContractValue: percentage
  } = contract

  if(!farmerId){
    farmerId = yield select(state => state.user.user.id)
  }

  try {
    yield put({ type: CREATE_CONTRACT_LOADING })

    const selectedProduct = livestocks.find(c => c.id === animal)
    const yieldInKgByAnimal = {
      1: selectedProduct.prodOne,
      2: selectedProduct.prodTwo,
      3: selectedProduct.prodThree
    }[technologyLevel]

    const startDate = Math.floor(contractStarts / 1000)
    const endDate = Math.floor(contractEnds / 1000)



    const livestocksContract = yield call(generateCostingLivestockContract)

    const result = yield call(
      livestocksContract.methods.addLivestockContractInfo,
      property.properties.f1,
      animal,
      +technologyLevel,
      parseInt(numberOfAnimals),
      currency(parseInt(property.properties.f4 * 100) / 100).intValue,
      currency(yieldInKgByAnimal).intValue,
      startDate,
      endDate,
      9,
      currency(loanValue).intValue
    )

    const x = yield call(createContractTransaction, result)
    if(x.type == "FINAL_TX"){      
      const result = yield call(
        registerNewWeb3LivestockContractApi,
        x.receipt.transactionHash,
        property.properties.f1,
        animal,
        parseInt(numberOfAnimals),
        +technologyLevel,
        currency(parseInt(property.properties.f4 * 100) / 100).intValue,
        currency(yieldInKgByAnimal).intValue,
        startDate,
        endDate,
        currency(loanValue).intValue
      )
        yield put(createContractSuccess())
      } else {
        throw ("errors_create_contract_web3_greenbond")
      }
    yield put(createContractSuccess())


    // const result = yield call(
    //   deployNewLivestockContractFromTechnicalApi,
    //   property.properties.f1,
    //   animal,
    //   numberOfAnimals,
    //   +technologyLevel,
    //   currency(parseInt(property.properties.f4 * 100) / 100).intValue,
    //   currency(yieldInKgByAnimal).intValue,
    //   startDate,
    //   endDate,
    //   currency(loanValue).intValue,
    //   farmerId,
    //   contract.currency
    // )
    yield put(createContractSuccess())
  } catch (err) {
    yield put(throwTxError(err))
    throw err
  }
}

function* createGreenBondContract({ payload }) {
  const isLoading = yield select(state => state.contract.loading)
  if (isLoading) {
    return
  }
  let { contract, farmerId } = payload

  const {
    property,
    contractStarts,
    contractEnds,
    interest,
    loanValue,
  } = contract

  if(!farmerId){
    farmerId = yield select(state => state.user.user.id)
  }

  try {
    yield put({ type: CREATE_CONTRACT_LOADING })

    const startDate = Math.floor(contractStarts / 1000)
    const endDate = Math.floor(contractEnds / 1000)

    const fundedValueInEtherFormat = etherFormatFromValue(loanValue)

    const greenBondContract = yield call(generateGreenBondContract)

    const result = yield call(
      greenBondContract.methods.newGreenBond,
      property.properties.f1,
      currency(parseInt(property.properties.f4 * 100) / 100).intValue,
      9,
      startDate,
      endDate,
      fundedValueInEtherFormat,
      fundedValueInEtherFormat
    )
    const x = yield call(createContractTransaction, result)
    if(x.type == "FINAL_TX"){      
      const result = yield call(
        registerNewWeb3GreenBondContractApi,
        x.receipt.transactionHash,
        property.properties.f1,
        currency(parseInt(property.properties.f4 * 100) / 100).intValue,
        startDate,
        endDate,
        currency(loanValue).intValue
      )
        yield put(createContractSuccess())
      } else {
        throw ("errors_create_contract_web3_greenbond")
      }
    yield put(createContractSuccess())
  } catch (err) {
    yield put(throwTxError(err))
    console.warn(err)
  }
}

function* getCompanyContracts() {
  try {
    yield put({ type: CREATE_CONTRACT_LOADING })
    const companyId = yield select(state => state.user.user.companyId)
    const contracts = yield call(getCompanyContractsFromApi, companyId)
    yield put({
      type: SET_PENDING_CONTRACTS,
      payload: { contracts, pendingContracts: [] }
    })
  } catch (err) {
    yield put(throwTxError(err))
  }
}

function* getRegionContracts() {
  try {
    yield put({ type: CREATE_CONTRACT_LOADING })
    const regionId = yield select(state => state.user.user.regionId)
    const contracts = yield call(getRegionContractsFromApi, regionId)
    yield put({
      type: SET_PENDING_CONTRACTS,
      payload: { contracts, pendingContracts: [] }
    })
  } catch (err) {
    yield put(throwTxError(err))
  }
}
export function* contractSagas() {
  yield all([
    takeLatest(CROPS_CONTRACT_REQUEST, createCropsContract),
    takeLatest(LIVESTOCK_CONTRACT_REQUEST, createLivestockContract),
    takeLatest(GREEN_BOND_REQUEST, createGreenBondContract),
    takeLatest(REQUEST_COMPANY_CONTRACTS, getCompanyContracts),
    takeLatest(REQUEST_REGION_CONTRACTS, getRegionContracts)
  ])
}
