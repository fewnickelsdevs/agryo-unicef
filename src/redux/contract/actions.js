import {
  UPDATE_CONTRACT,
  CREATE_CONTRACT_ERROR,
  RESET_CONTRACT,
  CREATE_CONTRACT_SUCCEEDED
} from './constants'

export const updateContract = information => ({
  type: UPDATE_CONTRACT,
  payload: { ...information }
})

export const throwTxError = error => ({
  type: CREATE_CONTRACT_ERROR,
  payload: { error }
})

export const resetContract = () => ({
  type: RESET_CONTRACT
})

export const createContractSuccess = () => ({
  type: CREATE_CONTRACT_SUCCEEDED
})
