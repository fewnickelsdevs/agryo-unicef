import api from '../../services/config'

export const deployNewCropsContractApi = async (
  propertyId,
  cropsType,
  fieldHectares,
  technologyLevel,
  yieldInTonsByHectare,
  startDate,
  endDate,
  fundedValue,
  currency
) => {
  try {
    const result = await api.post('/api/v1/farmer/contract/crops/new', {
      propertyId,
      cropsType,
      fieldHectares,
      technologyLevel,
      yieldInTonsByHectare,
      startDate,
      endDate,
      fundedValue,
      currency
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const deployNewLivestockContractApi = async (
  propertyId,
  livestockType,
  numberAnimals,
  technologyLevel,
  fieldHectares,
  yieldInKgByAnimal,
  startDate,
  endDate,
  fundedValue,
  currency
) => {
  try {
    const result = await api.post('/api/v1/farmer/contract/livestock/new', {
      propertyId,
      livestockType,
      numberAnimals,
      technologyLevel,
      fieldHectares,
      yieldInKgByAnimal,
      startDate,
      endDate,
      fundedValue,
      currency
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const deployNewGreenBondContractApi = async (
  propertyId,
  fieldHectares,
  startDate,
  endDate,
  fundedValue,
  currency
) => {
  try {
    const result = await api.post('/api/v1/farmer/contract/greenbond/new', {
      propertyId,
      fieldHectares,
      startDate,
      endDate,
      fundedValue,
      currency
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const deployNewCropsContractFromTechnicalApi = async (
  propertyId,
  cropsType,
  fieldHectares,
  technologyLevel,
  yieldInTonsByHectare,
  startDate,
  endDate,
  fundedValue,
  farmerId,
  currency
) => {
  try {
    const result = await api.post('/api/v1/technical/contract/crops/new', {
      contract: {
        propertyId,
        cropsType,
        fieldHectares,
        technologyLevel,
        yieldInTonsByHectare,
        startDate,
        endDate,
        fundedValue,
        currency
      },
      farmerId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const deployNewLivestockContractFromTechnicalApi = async (
  propertyId,
  livestockType,
  numberAnimals,
  technologyLevel,
  fieldHectares,
  yieldInKgByAnimal,
  startDate,
  endDate,
  fundedValue,
  farmerId,
  currency
) => {
  try {
    const result = await api.post(
      '/api/v1/technical/contract/livestock/new',
      {
        contract: {
          propertyId,
          livestockType,
          numberAnimals,
          technologyLevel,
          fieldHectares,
          yieldInKgByAnimal,
          startDate,
          endDate,
          fundedValue,
          currency
        },
        farmerId
      }
    )
    return result.data
  } catch (err) {
    throw err
  }
}

export const deployNewGreenBondContractFromTechnicalApi = async (
  propertyId,
  fieldHectares,
  startDate,
  endDate,
  fundedValue,
  farmerId,
  currency
) => {
  try {
    const result = await api.post(
      '/api/v1/technical/contract/greenbond/new',
      {
        contract: {
          propertyId,
          fieldHectares,
          startDate,
          endDate,
          fundedValue,
          currency
        },
        farmerId
      }
    )
    return result.data
  } catch (err) {
    throw err
  }
}

export const getCompanyPendingContractsFromApi = async () => {
  try {
    const result = await api.get('/company/v1/contracts/pending')
    return result.data
  } catch (err) {
    throw err
  }
}

export const getCompanyContractsFromApi = async (companyId) => {
  try {
    const result = await api.post('/api/v1/contract/company', { companyId })
    return result.data
  } catch (err) {
    throw err
  }
}


export const getRegionContractsFromApi = async (regionId) => {
  try {
    const result = await api.post('/api/v1/contract/region', { regionId })
    return result.data
  } catch (err) {
    throw err
  }
}



// web3


export const registerNewWeb3LivestockContractApi = async (
  transactionHash,
  propertyId,
  livestockType,
  numberAnimals,
  technologyLevel,
  fieldHectares,
  yieldInKgByAnimal,
  startDate,
  endDate,
  fundedValue
) => {
  try {
    const result = await api.post(
      '/api/v1/contract/livestock/register',
      {
        contract: {
          propertyId,
          livestockType,
          numberAnimals,
          technologyLevel,
          fieldHectares,
          yieldInKgByAnimal,
          startDate,
          endDate,
          fundedValue
        },
        transactionHash
      }
    )
    return result.data
  } catch (err) {
    throw err
  }
}

export const registerNewWeb3GreenBondContractApi = async (
  transactionHash,
  propertyId,
  fieldHectares,
  startDate,
  endDate,
  fundedValue
) => {
  try {
    const result = await api.post(
      '/api/v1/contract/greenbond/register',
      {
        contract: {
          propertyId,
          fieldHectares,
          startDate,
          endDate,
          fundedValue
        },
        transactionHash
      }
    )
    return result.data
  } catch (err) {
    throw err
  }
}

export const registerNewWeb3CropsContractApi = async (
  transactionHash,
  propertyId,
  cropsType,
  technologyLevel,
  fieldHectares,
  yieldInTonsByHectare,
  startDate,
  endDate,
  fundedValue
) => {
  try {
    const result = await api.post('/api/v1/contract/crops/register', {
      contract: {
        propertyId,
        cropsType,
        fieldHectares,
        technologyLevel,
        yieldInTonsByHectare,
        startDate,
        endDate,
        fundedValue
      },
      transactionHash
    })
    return result.data
  } catch (err) {
    throw err
  }
}
