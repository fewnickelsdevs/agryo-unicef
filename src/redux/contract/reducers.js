import {
  UPDATE_CONTRACT,
  CREATE_CONTRACT_ERROR,
  RESET_CONTRACT,
  SET_PENDING_CONTRACTS
} from './constants'
import { addDays } from 'date-fns'

import {
  CREATE_CONTRACT_SUCCEEDED,
  MINE_TX,
  WAIT_TX_APPROVAL
} from '../transaction/constants'
import { CREATE_CONTRACT_LOADING } from './constants'

const curDate = new Date()

const defaultContractState = {
  product: 1,
  property: null,
  technologyLevel: 2,
  contractStarts: curDate,
  contractEnds: addDays(curDate, 1),
  interest: 9,
  numberOfAnimals: '1',
  loanValue: 60000,
  minimumContractValue: 100,
  currency: "USD",
  error: false,
  success: false,
  loading: false,
  isGreenBond: false,
  pendingContracts: { crops: [], livestock: [], greenBond: [] },
  contracts: []
}

const contractReducer = (state = defaultContractState, { type, payload }) => {
  switch (type) {
    case UPDATE_CONTRACT:
      return { ...state, ...payload }
    case WAIT_TX_APPROVAL:
    case MINE_TX:
      return { ...state, loading: true }
    case CREATE_CONTRACT_LOADING:
      return { ...state, loading: true, error: false, success: false }
    case CREATE_CONTRACT_ERROR:
      return { ...state, loading: false, error: payload.error }
    case CREATE_CONTRACT_SUCCEEDED:
      return { ...state, loading: false, success: true }
    case SET_PENDING_CONTRACTS:
      return {
        ...state,
        loading: false,
        pendingContracts: payload.pendingContracts,
        contracts: payload.contracts
      }
    case RESET_CONTRACT:
      return defaultContractState
    default:
      return state
  }
}

export default contractReducer
