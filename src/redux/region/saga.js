import { all, takeLatest, call, put, select } from 'redux-saga/effects'
import {
    fetchCompanyRegionsFromAPI,
    fetchRegionDetailsFromAPI,
    newRegionFromAPI,
    fetchUserCompanyDetailsFromAPI
} from './utils'
import {
    REQUEST_COMPANY_REGION,
    GET_REGION_DETAILS,
    CREATE_NEW_REGION,
    REQUEST_COMPANY_REGION_COMPLETE,
    REQUEST_REGION_LOADING,
    REQUEST_REGION_DETAILS_COMPLETE,
    REQUEST_REGION_ACTION_SUCCESS,
    REQUEST_REGION_ACTION_ERROR,
    REQUEST_REGION_ACTION_LOADING
} from './actions'



function* listCompanyRegions({ payload }) {
    try{
        yield put({ type: REQUEST_REGION_LOADING })
        const companyId = yield select(state => state.user.user.companyId)
        const userType = yield select(state => state.user.user.userType)
        console.log("search" ,userType)
        let regionData = []
        if (userType == 1 || userType == 2) {
            regionData = yield call(fetchCompanyRegionsFromAPI, companyId)
        } else {
            const companyData = yield call(fetchUserCompanyDetailsFromAPI)
            console.log(companyData)
            regionData = [ {id: companyData.regionId, name: companyData.regionName }]
        }

        console.log(regionData)

        yield put({ type: REQUEST_COMPANY_REGION_COMPLETE, payload: regionData || [] })
    } catch(err){

    }
}
  

function* getRegionDetails({ payload }) {
    const { regionId } = payload
    try{
        yield put({ type: REQUEST_REGION_LOADING })
        const regionData = yield call(fetchRegionDetailsFromAPI, regionId)
        yield put({ type: REQUEST_REGION_DETAILS_COMPLETE, payload: regionData })
    } catch(err){

    }
}
  

function* createNewRegion({ payload }) {
    const { regionName } = payload
    try{
        const loading = yield select(state => state.region.loadingAction)
        if(loading){
            return
        }
        yield put({ type: REQUEST_REGION_ACTION_LOADING })
        const companyId = yield select(state => state.user.user.companyId)
        yield call(newRegionFromAPI, regionName, companyId)
        yield put({ type: REQUEST_REGION_ACTION_SUCCESS })
    } catch(err){
        console.log(err)
        yield put({ type: REQUEST_REGION_ACTION_ERROR })

    }
}
  

export function* regionSagas() {
    yield all([
      takeLatest(CREATE_NEW_REGION, createNewRegion),
      takeLatest(GET_REGION_DETAILS, getRegionDetails),
      takeLatest(REQUEST_COMPANY_REGION, listCompanyRegions)
    ])
}