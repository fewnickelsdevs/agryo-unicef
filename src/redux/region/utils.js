import api from '../../services/config'


export const fetchCompanyRegionsFromAPI = async (companyId) => {
    const response = await api.post('/api/v1/region/company/list', { companyId })
    return response.data
}

export const fetchRegionDetailsFromAPI = async (regionId) => {
    const response = await api.post('/api/v1/region', { regionId })
    return response.data
}

export const fetchUserCompanyDetailsFromAPI = async () => {
    const response = await api.get('/api/v1/user/enterprise')
    return response.data
}

export const newRegionFromAPI = async (name, companyId) => {
    const response = await api.post('/api/v1/region/new', { name, companyId })
    return response.data
}
