import {
    REQUEST_REGION_LOADING,
    REQUEST_REGION_ERROR,
    REQUEST_COMPANY_REGION_COMPLETE,
    REQUEST_REGION_ACTION_LOADING,
    REQUEST_REGION_ACTION_ERROR,
    REQUEST_REGION_ACTION_SUCCESS,
    REQUEST_REGION_DETAILS_COMPLETE
  } from './actions'
  
  const defaultState = {
    loading: false,
    error: false,
    regionsData: [],
    regionDetails: false,
    loadingAction: false,
    errorAction: false,
    successAction: false
  }
  
  const regionReducers = (state = defaultState, action) => {
    switch (action.type) {
      case REQUEST_REGION_LOADING:
        return { ...defaultState, loading: true }
      case REQUEST_REGION_ERROR:
        return { ...defaultState, error: true }

  
      case REQUEST_COMPANY_REGION_COMPLETE:
        return { ...defaultState, regionsData: action.payload }

        case REQUEST_REGION_DETAILS_COMPLETE:
        return { ...state, regionDetails: action.payload }

      // Access or Revoke Technical
      case REQUEST_REGION_ACTION_LOADING:
        return {
          ...state,
          loadingAction: true,
          errorAction: false,
          successAction: false
        }
      case REQUEST_REGION_ACTION_ERROR:
        return {
          ...state,
          errorAction: true,
          loadingAction: false,
          successAction: false
        }
      case REQUEST_REGION_ACTION_SUCCESS:
        return {
          ...state,
          errorAction: false,
          loadingAction: false,
          successAction: true
        }
      default:
        return state
    }
  }
  
  export default regionReducers
  