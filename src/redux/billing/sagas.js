import { takeLatest, call, put, all, select } from 'redux-saga/effects'

import {
  UPDATE_BILLING,
  FETCH_BILLING_ANNUAL_SUCCESS,
  FETCH_BILLING_ANNUAL_ERROR,
  FETCH_BILLING_ANNUAL_REQUEST
} from './actions'

import {
  fetchCompanyStatsAnnualFromAPI
} from './utils'

function* fetchCompanyStatsAnnual() {
  yield put({ type: UPDATE_BILLING })

  try {
    const companyId = yield select(state => state.user.user.companyId)
    const data = yield call(fetchCompanyStatsAnnualFromAPI, companyId)
    yield put({ type: FETCH_BILLING_ANNUAL_SUCCESS, payload: data })
  } catch (err) {
    console.log(err)
    yield put({ type: FETCH_BILLING_ANNUAL_ERROR })
  }
}

export function* billingSagas() {
  yield all([
    takeLatest(FETCH_BILLING_ANNUAL_REQUEST, fetchCompanyStatsAnnual)
  ])
}
