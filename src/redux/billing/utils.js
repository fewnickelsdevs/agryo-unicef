import api from '../../services/config'
/*
  CompanyId uint64 
  StartDate float64  
  EndDate float64  
  Country string  
  Currency uint32  
  MonthRef uint8  
  YearRef  uint32  
  NRequest        uint64  
  QntCropFields      uint64   
  MeanSizeCropFields float64  
  SumCropFields 	   float64  
  MonthConsumption float64  
  ContractsConsumption float64 
  PreviousBalance float64  
  ActualBalance float64  
  PaymentId string
  PaymentStatus bool  
*/

export const fetchCompanyStatsAnnualFromAPI = async (companyId) => {
  try {
    const result = await api.post('/api/v1/company/stats/annual', { companyId })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchCompanyStatsByRegionFromAPI = async (companyId) => {
  try {
    const result = await api.post('/api/v1/company/stats/region', { companyId })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchCompanyStatsMonthlyFromAPI = async (companyId) => {
  try {
    const result = await api.post('/api/v1/company/stats/invoices', { companyId })
    return result.data
  } catch (err) {
    throw err
  }
}
