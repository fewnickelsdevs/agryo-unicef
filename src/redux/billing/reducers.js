import {
  RESET_BILLING,
  UPDATE_BILLING,
  FETCH_BILLING_ANNUAL_ERROR,
  FETCH_BILLING_ANNUAL_SUCCESS
} from './actions'

const defaultBilling = {
  data: {},
  success: false,
  error: null,
  loading: false,
}

const propertyReducer = (state = defaultBilling, action) => {
  switch (action.type) {
    case RESET_BILLING:
      return { ...defaultBilling }
    case UPDATE_BILLING:
      return { ...defaultBilling, loading: true }
    case FETCH_BILLING_ANNUAL_ERROR:
      return { error: action.payload, success: false, loading: false }
    case FETCH_BILLING_ANNUAL_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        data: action.payload
      }
    default:
      return state
  }
}

export default propertyReducer
