import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from 'history'

import userReducer from './user/reducers'
import contractReducer from './contract/reducers'
import propertyReducer from './property/reducers'
import billingReducer from './billing/reducers'
import investContractsReducer from './investContracts/reducers'
import { walletReducer, aproveValueReducer } from './wallet/reducers'
import referralReducer from './referral/reducers'
import contractsReducers from './contracts-list/reducers'
import contractDetailsReducer from './contractDetails/reducers'
import apiAdmReducer from './apiAdmin/reducers'

import { userSagas } from './user/sagas'
import { contractSagas } from './contract/sagas'
import { propertySagas } from './property/sagas'
import { billingSagas } from './billing/sagas'
import { contractsForInvestSagas } from './investContracts/sagas'
import { walletSagas } from './wallet/sagas'
import { referralSagas } from './referral/sagas'
import { contractListSagas } from './contracts-list/sagas'
import { contractDetailsSaga } from './contractDetails/sagas'
import { apiAdmSagas } from './apiAdmin/sagas'

import enterpriseInfoReducers from './enterprise/reducers'
import { enterpriseInfoSagas } from './enterprise/saga'
import regionReducers from './region/reducer';
import { regionSagas } from './region/saga';

const history = createBrowserHistory()
const sagaMiddleware = createSagaMiddleware()
const rootReducer = combineReducers({
  contracts: contractsReducers,
  user: userReducer,
  contract: contractReducer,
  contractDetails: contractDetailsReducer,
  property: propertyReducer,
  billing: billingReducer,
  investContracts: investContractsReducer,
  wallet: walletReducer,
  aproveValue: aproveValueReducer,
  referral: referralReducer,
  apiAdm: apiAdmReducer,
  enterpriseInfo: enterpriseInfoReducers,
  region: regionReducers,
  router: connectRouter(history)
})

const configureStore = () => {
  const store = createStore(
    rootReducer,
    composeWithDevTools(
      applyMiddleware(sagaMiddleware),
      applyMiddleware(routerMiddleware(history))
    )
  )

  sagaMiddleware.run(userSagas)
  sagaMiddleware.run(contractSagas)
  sagaMiddleware.run(propertySagas)
  sagaMiddleware.run(billingSagas)
  sagaMiddleware.run(contractsForInvestSagas)
  sagaMiddleware.run(walletSagas)
  sagaMiddleware.run(referralSagas)
  sagaMiddleware.run(contractListSagas)
  sagaMiddleware.run(contractDetailsSaga)
  sagaMiddleware.run(apiAdmSagas)
  sagaMiddleware.run(enterpriseInfoSagas)
  sagaMiddleware.run(regionSagas)

  return store
}

export { history }
export default configureStore
