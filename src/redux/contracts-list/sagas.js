import { all, takeLatest, call, put, select } from 'redux-saga/effects'
import {
  fetchWeb3InvestimentsContractsFromApi,
  fetchFarmerContractsFromApi,
  fetchAgroTechnicalContractsFromApi
} from './utils'
import {
  FETCH_CONTRACTS_SUCCESS,
  FETCH_CONTRACTS_LOADING,
  FETCH_FARMER_CONTRACTS_REQUEST,
  FETCH_INVESTOR_CONTRACTS_REQUEST,
  FETCH_TECHNICAL_CONTRACTS_SUCCESS,
  FETCH_TECHNICAL_CONTRACTS_REQUEST
} from './actions'

function* findFarmerContracts() {
  yield put({
    type: FETCH_CONTRACTS_LOADING
  })
  const userId = yield select(state => state.user.user.id)
  const contracts = yield call(fetchFarmerContractsFromApi, userId)
  yield put({
    type: FETCH_CONTRACTS_SUCCESS,
    payload: contracts
  })
}

function* findInvestorContracts() {
  yield put({
    type: FETCH_CONTRACTS_LOADING
  })
  const contracts = yield call(fetchWeb3InvestimentsContractsFromApi)
  yield put({
    type: FETCH_CONTRACTS_SUCCESS,
    payload: contracts
  })
}

function* findAgroTechnicalContracts() {
  try {
    yield put({
      type: FETCH_CONTRACTS_LOADING
    })
    const userId = yield select(state => state.user.user.id)
    const contracts = yield call(fetchAgroTechnicalContractsFromApi, userId)
    yield put({
      type: FETCH_TECHNICAL_CONTRACTS_SUCCESS,
      payload: contracts
    })
  } catch (err) {}
}

export function* contractListSagas() {
  yield all([
    takeLatest(FETCH_FARMER_CONTRACTS_REQUEST, findFarmerContracts),
    takeLatest(FETCH_INVESTOR_CONTRACTS_REQUEST, findInvestorContracts),
    takeLatest(FETCH_TECHNICAL_CONTRACTS_REQUEST, findAgroTechnicalContracts)
  ])
}
