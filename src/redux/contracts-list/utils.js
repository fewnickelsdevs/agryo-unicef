import api from '../../services/config'

export const fetchWeb3InvestimentsContractsFromApi = async () => {
  const response = await api.get('/api/v1/invest/user/info')
  return response.data
}

export const fetchFarmerContractsFromApi = async (farmerId) => {
  const contracts = await api.post('/api/v1/contract/web3/farmer', { farmerId })
  // const { crops, livestock, greenBond } = contracts.data

  // return [...crops, ...livestock, ...greenBond]
  return contracts.data
}

export const fetchAgroTechnicalContractsFromApi = async (technicalId) => {
  const result = await api.post('/api/v1/contract/technical', { technicalId })
  return result.data
}
