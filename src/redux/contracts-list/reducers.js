import {
  FETCH_CONTRACTS_SUCCESS,
  FETCH_CONTRACTS_LOADING,
  FETCH_TECHNICAL_CONTRACTS_SUCCESS
} from './actions'

const defaultContracts = {
  loading: false,
  error: false,
  contracts: [],
  enterprise: {
    pendingContracts: { crops: [], livestock: [], greenBond: [] },
    contracts: { crops: [], livestock: [], greenBond: [] }
  }
}

const contractsReducers = (state = defaultContracts, action) => {
  switch (action.type) {
    case FETCH_CONTRACTS_LOADING:
      return { ...defaultContracts, loading: true }
    case FETCH_CONTRACTS_SUCCESS:
      return { loading: false, error: false, contracts: action.payload }
    case FETCH_TECHNICAL_CONTRACTS_SUCCESS:
      return {
        loading: false,
        error: false,
        contracts: action.payload
      }
    default:
      return state
  }
}

export default contractsReducers
