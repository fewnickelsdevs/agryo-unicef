const defaultState = {
  refId: '',
  referralList: [],
  referralContracts: []
}

const referralReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'FETCH_REFERRAL_DATA_SUCCESS':
      return { ...state, ...action.payload }
    default:
      return defaultState
  }
}

export default referralReducer
