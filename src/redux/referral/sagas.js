import { all, takeLatest, call, put } from 'redux-saga/effects'
import api from '../../services/config'

const referralEndpoint = '/farmer/v1/user/referral'

const fetchReferralData = async () => {
  try {
    const response = await api.get(referralEndpoint)
    return response.data
  } catch (err) {
    throw err
  }
}

function* fetchAndSaveReferralData() {
  const referralData = yield call(fetchReferralData)
  yield put({ type: 'FETCH_REFERRAL_DATA_SUCCESS', payload: referralData })
}

export function* referralSagas() {
  yield all([
    takeLatest('FETCH_REFERRAL_DATA_REQUEST', fetchAndSaveReferralData)
  ])
}
