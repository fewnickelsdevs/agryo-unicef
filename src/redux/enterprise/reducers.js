import {
  REQUEST_COMPANY_LOADING,
  REQUEST_COMPANY_ERROR,
  REQUEST_COMPANY_ACTION_LOADING,
  REQUEST_COMPANY_ACTION_ERROR,
  REQUEST_COMPANY_ACTION_SUCCESS,
  REQUEST_COMPANY_TECHNICAL_COMPLETE,
  REQUEST_COMPANY_FARMERS_COMPLETE,
  REQUEST_COMPANY_NEW_TECHNICAL_COMPLETE
} from './actions'

const defaultEntepriseState = {
  loading: false,
  error: false,
  farmers: [],
  company: { technicals: [], pendingTechnicals: [] },
  loadingAction: false,
  errorAction: false,
  successAction: false
}

const enterpriseInfoReducers = (state = defaultEntepriseState, action) => {
  switch (action.type) {
    case REQUEST_COMPANY_LOADING:
      return { ...defaultEntepriseState, loading: true }
    case REQUEST_COMPANY_ERROR:
      return { ...defaultEntepriseState, error: true }
    case REQUEST_COMPANY_NEW_TECHNICAL_COMPLETE:
      return {
        ...state,
        loadingAction: false,
        errorAction: false,
        successAction: true
      }

    case REQUEST_COMPANY_TECHNICAL_COMPLETE:
      return { ...defaultEntepriseState, company: action.payload.data }
    case REQUEST_COMPANY_FARMERS_COMPLETE:
      return { ...defaultEntepriseState, farmers: action.payload.data }

    // Access or Revoke Technical
    case REQUEST_COMPANY_ACTION_LOADING:
      return {
        ...state,
        loadingAction: true,
        errorAction: false,
        successAction: false
      }
    case REQUEST_COMPANY_ACTION_ERROR:
      return {
        ...state,
        errorAction: true,
        loadingAction: false,
        successAction: false
      }
    case REQUEST_COMPANY_ACTION_SUCCESS:
      return {
        ...state,
        errorAction: false,
        loadingAction: false,
        successAction: true
      }
    default:
      return state
  }
}

export default enterpriseInfoReducers
