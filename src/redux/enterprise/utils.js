import api from '../../services/config'

export const getCompanyFarmers = async (companyId) => {
  try {
    const result = await api.post('/api/v1/company/farmers', { companyId })
    return result.data
  } catch (err) {
    throw err
  }
}


export const getRegionFarmers = async (regionId) => {
  try {
    const result = await api.post('/api/v1/region/farmers', { regionId })
    return result.data
  } catch (err) {
    throw err
  }
}


export const getRegionAgroTechnicals = async (regionId) => {
  try {
    const result = await api.post('/api/v1/region/technicals', { regionId })
    return result.data
  } catch (err) {
    throw err
  }
}

export const getAgroTechnicalRegisteredFarmers = async (technicalId) => {
  try {
    const result = await api.post('/api/v1/technical/farmers', { technicalId })
    return result.data
  } catch (err) {
    throw err
  }
}


export const getCompanyAgroTechnicals = async (companyId) => {
  try {
    const result = await api.post('/api/v1/company/technicals', { companyId })
    return result.data
  } catch (err) {
    throw err
  }
}


export const registerFarmerWithAgroTechnical = async (
  name,
  email,
  document,
  regionId,
  invitateFarmer
) => {
  try {
    const result = await api.post('/api/v1/company/farmer/invitate', {
      name,
      email,
      document,
      regionId: parseInt(regionId),
      invitateFarmer: invitateFarmer || false
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const registerAgroTechnical = async (email, userType, regionId) => {
  try {
    const result = await api.post('/api/v1/company/technicals/invitate', {
      email,
      userType: parseInt(userType),
      regionId: parseInt(regionId)
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const cancelAgroTechnicalInvitateApi = async email => {
  try {
    const result = await api.post('/api/v1/company/technicals/invitate/cancel', { email })
    return result.data
  } catch (err) {
    throw err
  }
}

export const revokeAccessAgroTechnicalApi = async technicalId => {
  try {
    const result = await api.post('/company/v1/technical/revoke', {
      technicalId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const authorizeAccessAgroTechnicalApi = async technicalId => {
  try {
    const result = await api.post('/company/v1/technical/authorize', {
      technicalId
    })
    return result.data
  } catch (err) {
    throw err
  }
}
