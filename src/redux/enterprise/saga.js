import { all, takeLatest, call, put, select } from 'redux-saga/effects'
import {
  REQUEST_COMPANY_LOADING,
  REQUEST_CANCEL_TECHNICAL_INVITATE,
  REQUEST_COMPANY_NEW_FARMER,
  REQUEST_COMPANY_TECHNICAL_ERROR,
  REQUEST_COMPANY_TECHNICAL_COMPLETE,
  REQUEST_COMPANY_FARMERS_COMPLETE,
  REQUEST_COMPANY_TECHNICALS,
  REQUEST_COMPANY_FARMERS,
  REQUEST_COMPANY_NEW_TECHNICAL,
  REQUEST_TECHNICAL_FARMERS,
  REQUEST_COMPANY_ACTION_LOADING,
  REQUEST_COMPANY_ACTION_ERROR,
  REQUEST_COMPANY_ACTION_SUCCESS,
  REQUEST_REVOKE_ACCESS_TECHNICAL,
  REQUEST_AUTHORIZE_ACCESS_TECHNICAL,
  REQUEST_REGION_FARMERS,
  REQUEST_REGION_TECHNICALS
} from './actions'
import {
  getAgroTechnicalRegisteredFarmers,
  getCompanyAgroTechnicals,
  getCompanyFarmers,
  registerAgroTechnical,
  registerFarmerWithAgroTechnical,
  cancelAgroTechnicalInvitateApi,
  revokeAccessAgroTechnicalApi,
  authorizeAccessAgroTechnicalApi,
  getRegionFarmers,
  getRegionAgroTechnicals
} from './utils'

function* findCompanyAgroTechnicals() {
  try {
    yield put({
      type: REQUEST_COMPANY_LOADING
    })
    const companyId = yield select(state => state.user.user.companyId)
    const technicals = yield call(getCompanyAgroTechnicals, companyId)
    yield put({
      type: REQUEST_COMPANY_TECHNICAL_COMPLETE,
      payload: { data: technicals }
    })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_TECHNICAL_ERROR })
  }
}

function* findRegionAgroTechnicals() {
  try {
    yield put({
      type: REQUEST_COMPANY_LOADING
    })
    const regionId = yield select(state => state.user.user.regionId)
    const technicals = yield call(getRegionAgroTechnicals, regionId)
    yield put({
      type: REQUEST_COMPANY_TECHNICAL_COMPLETE,
      payload: { data: technicals }
    })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_TECHNICAL_ERROR })
  }
}


function* newCompanyAgrotechnical({ payload }) {
  const { email, userType, regionId } = payload
  const isLoading = yield select(state => state.enterpriseInfo.loadingAction)
  if (isLoading) {
    return
  }
  try {
    yield put({
      type: REQUEST_COMPANY_ACTION_LOADING
    })
    yield call(registerAgroTechnical, email, userType, regionId)
    yield put({ type: REQUEST_COMPANY_ACTION_SUCCESS })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_ACTION_ERROR })
  }
}

function* cancelAgroTechnicalInvitate({ payload }) {
  const { email } = payload
  const isLoading = yield select(state => state.enterpriseInfo.loadingAction)
  if (isLoading) {
    return
  }
  try {
    yield put({
      type: REQUEST_COMPANY_ACTION_LOADING
    })
    yield call(cancelAgroTechnicalInvitateApi, email)
    yield put({ type: REQUEST_COMPANY_ACTION_SUCCESS })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_ACTION_ERROR })
  }
}

function* revokeAgroTechnicalAccess({ payload }) {
  const { technicalId } = payload
  const isLoading = yield select(state => state.enterpriseInfo.loadingAction)
  if (isLoading) {
    return
  }
  try {
    yield put({
      type: REQUEST_COMPANY_ACTION_LOADING
    })
    yield call(revokeAccessAgroTechnicalApi, technicalId)
    yield put({ type: REQUEST_COMPANY_ACTION_SUCCESS })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_ACTION_ERROR })
  }
}

function* authorizeAccessAgroTechnicalAccess({ payload }) {
  const { technicalId } = payload
  const isLoading = yield select(state => state.enterpriseInfo.loadingAction)
  if (isLoading) {
    return
  }
  try {
    yield put({
      type: REQUEST_COMPANY_ACTION_LOADING
    })
    yield call(authorizeAccessAgroTechnicalApi, technicalId)
    yield put({ type: REQUEST_COMPANY_ACTION_SUCCESS })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_ACTION_ERROR })
  }
}

function* newFarmerByAgroTechnical({ payload }) {
  const {
    user: { email, name, document, regionId, invitateFarmer }
  } = payload
  try {
    yield put({
      type: REQUEST_COMPANY_ACTION_LOADING
    })
    yield call(
      registerFarmerWithAgroTechnical,
      name,
      email,
      document,
      regionId,
      invitateFarmer
    )
    yield put({ type: REQUEST_COMPANY_ACTION_SUCCESS })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_ACTION_ERROR })
  }
}

function* findCompanyFarmers() {
  try {
    yield put({
      type: REQUEST_COMPANY_LOADING
    })
    const companyId = yield select(state => state.user.user.id)
    const farmers = yield call(getCompanyFarmers, companyId)
    yield put({
      type: REQUEST_COMPANY_FARMERS_COMPLETE,
      payload: { data: farmers }
    })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_TECHNICAL_ERROR })
  }
}

function* findRegionFarmers() {
  try {
    yield put({
      type: REQUEST_COMPANY_LOADING
    })
    const regionId = yield select(state => state.user.user.regionId)
    const farmers = yield call(getRegionFarmers, regionId)
    yield put({
      type: REQUEST_COMPANY_FARMERS_COMPLETE,
      payload: { data: farmers }
    })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_TECHNICAL_ERROR })
  }
}

function* findAgroTechnicalRegisteredFarmers() {
  try {
    yield put({
      type: REQUEST_COMPANY_LOADING
    })
    const technicalId = yield select(state => state.user.user.id)

    const farmers = yield call(getAgroTechnicalRegisteredFarmers, technicalId)
    yield put({
      type: REQUEST_COMPANY_FARMERS_COMPLETE,
      payload: { data: [...farmers.farmers, ...farmers.pendingFarmers] }
    })
  } catch (err) {
    yield put({ type: REQUEST_COMPANY_TECHNICAL_ERROR })
  }
}

export function* enterpriseInfoSagas() {
  yield all([
    takeLatest(REQUEST_COMPANY_TECHNICALS, findCompanyAgroTechnicals),
    takeLatest(REQUEST_REGION_TECHNICALS, findRegionAgroTechnicals),
    takeLatest(REQUEST_COMPANY_FARMERS, findCompanyFarmers),
    takeLatest(REQUEST_REGION_FARMERS, findRegionFarmers),
    takeLatest(REQUEST_COMPANY_NEW_TECHNICAL, newCompanyAgrotechnical),
    takeLatest(REQUEST_TECHNICAL_FARMERS, findAgroTechnicalRegisteredFarmers),
    takeLatest(REQUEST_COMPANY_NEW_FARMER, newFarmerByAgroTechnical),
    takeLatest(REQUEST_CANCEL_TECHNICAL_INVITATE, cancelAgroTechnicalInvitate),
    takeLatest(REQUEST_REVOKE_ACCESS_TECHNICAL, revokeAgroTechnicalAccess),
    takeLatest(
      REQUEST_AUTHORIZE_ACCESS_TECHNICAL,
      authorizeAccessAgroTechnicalAccess
    )
  ])
}
