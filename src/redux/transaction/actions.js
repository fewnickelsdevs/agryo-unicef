import {
  WAIT_TX_APPROVAL,
  MINE_TX,
  CREATE_CONTRACT_FAILED,
  CREATE_CONTRACT_SUCCEEDED
} from './constants'

export const waitTxApproval = () => ({
  type: WAIT_TX_APPROVAL
})

export const mineTx = () => ({
  type: MINE_TX
})

export const throwTxError = error => ({
  type: CREATE_CONTRACT_FAILED,
  payload: { error }
})

export const finishTx = () => ({
  type: CREATE_CONTRACT_SUCCEEDED
})
