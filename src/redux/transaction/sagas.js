import { put, select, take, call } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'

import { waitTxApproval, mineTx, throwTxError, finishTx } from './actions'
import { updateUserInformations } from '../user/actions'
import { getUserAgryoTokens } from '../../contracts/api/user'

function* defaultOnSuccess(channel) {
  const userAddress = yield select(state => state.user.web3Address)
  // const userBalance = yield call(getUserAgryoTokens, userAddress)

  // yield put(updateUserInformations({ balanceAgryoStable: userBalance }))
  yield put(finishTx())
}

function* defaultOnError(channel) {
  yield put(throwTxError(channel.error))
}

function* defaultOnSend(channel) {
  yield put(mineTx(channel.tx))
}

export function* createContractTransaction(
  transactionData,

  onSuccess = defaultOnSuccess,
  onError = defaultOnError,
  onSend = defaultOnSend
) {
  try {
    yield put(waitTxApproval())
    const userAddress = yield select(state => state.user.user.web3Address)
    const tx = transactionData.send({ from: userAddress })

    const channels = new eventChannel(emit => {
      tx.once('transactionHash', tx => {
        console.log("send", tx)
        emit({
          tx,
          type: 'TX_SEND'
        })
      })
      tx.once('receipt', receipt => {
        console.log("receipt", receipt)
        emit({
          receipt,
          type: 'FINAL_TX'
        })
      })
      tx.on('error', error => {
        emit({
          error,
          type: 'ERROR_TX'
        })
      })

      return () => {}
    })

    while (true) {
      const channel = yield take(channels)

      if (channel.type === 'TX_SEND') {
        onSend(channel)
      }

      if (channel.type === 'ERROR_TX') {
        onError(channel)

        return channel
      }

      if (channel.type === 'FINAL_TX') {
        onSuccess(channel)

        return channel
      }
    }
  } catch (err) {
    yield put(throwTxError(err))
    throw err
  }
}
