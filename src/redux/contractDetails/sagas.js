import { all, takeLatest, call, put } from 'redux-saga/effects'
import {
  INIT_CONTRACT_REQUEST,
  LOADING_CONTRACT_REQUEST,
  FETCH_CONTRACT_SUCCEEDED,
  FETCH_CONTRACT_ERROR,
  CONTRACT_INVESTMENT_REQUEST,
  CONTRACT_INVESTMENT_SUCCEEDED,
  CONTRACT_INVESTMENT_FAILED,
  CONTRACT_INVESTMENT_LOADING,
  REQUEST_CONTRACT_PENDING_FARMER_DATA,
  REQUEST_AUTHORIZE_CONTRACT,
  REQUEST_REPROVE_CONTRACT,
  INIT_WEB3CONTRACT_FOR_INVEST_REQUEST,
  INIT_WEB3CONTRACT_PENDING_FARMER
} from './constants'
import {
  fetchPropertyContractDetailsFromAPI,
  investOnContractApi,
  formatPropertyData,
  authorizeCropsContract,
  authorizeLivestockContract,
  authorizeGreenBondContract,
  reproveCropsContract,
  reproveLivestockContract,
  reproveGreenBondContract,
  fetchPropertyContractEnvironmentDataFromAPI,
  getContractBlockchainHistory,
  fetchPropertyWeb3ContractDetailsFromAPI,
  getContractWeb3BlockchainHistory,
  fetchPropertyWeb3ContractEnvironmentDataFromAPI,
  fetchInvestorPropertyWeb3ContractDetailsFromAPI,
  fetchInvestorPropertyWeb3ContractEnvironmentDataFromAPI,
  fetchPropertyPendingWeb3ContractDetailsFromAPI,
  fetchPropertyWeb3PendingContractEnvironmentDataFromAPI
} from './utils'
import { getContractByFullId } from '../../contracts/api/contracts'
import { etherFormatFromValue } from '../../contracts/api/utils'
import { getUserTypeFromLocalStorage } from '../../services/storage_utils'
import { extractNumbersFromString } from '../../utils/extractNumberString';

function* getContractInfo({ payload }) {
  try {
    yield put({ type: LOADING_CONTRACT_REQUEST })
    const { contractId, contractData } = payload
    let contractInfo = contractData
    if (!contractInfo) {
      contractInfo = yield call(getContractByFullId, contractId)
    }
    const propertyInfo = yield call(
          fetchPropertyWeb3ContractDetailsFromAPI,
          contractId
    )

    const contractBlockchainHistory = yield call(getContractWeb3BlockchainHistory, contractId)

    const env = yield call(fetchPropertyWeb3ContractEnvironmentDataFromAPI, contractId)
    const formattedPayload = yield call(formatPropertyData, {
      ...propertyInfo, ...env
    })
    yield put({
      type: FETCH_CONTRACT_SUCCEEDED,
      payload: { contract: { ...contractInfo, blockchainHistory: contractBlockchainHistory || [] }, property: formattedPayload }
    })
  } catch (err) {
    console.log(err)
    yield put({ type: FETCH_CONTRACT_ERROR, payload: {} })
  }
}

function* getWeb3PendingContractInfo({ payload }) {
  try {
    yield put({ type: LOADING_CONTRACT_REQUEST })
    const { contractId, contractType, propertyId } = payload

    //const formatContractId = yield call(extractNumbersFromString, contractId)


    const propertyInfo = yield call(
      fetchPropertyPendingWeb3ContractDetailsFromAPI,
      contractType,
      contractId
    )
    const env = yield call(fetchPropertyWeb3PendingContractEnvironmentDataFromAPI, contractType, contractId)
    const formattedPayload = yield call(formatPropertyData, {
      ...propertyInfo, ...env
    })
    yield put({
      type: FETCH_CONTRACT_SUCCEEDED,
      payload: { contract: { blockchainHistory: false }, property: formattedPayload }
    })
  } catch (err) {
    console.log(err)
    yield put({ type: FETCH_CONTRACT_ERROR, payload: {} })
  }
}

function* getWeb3InvestContractInfo({ payload }) {
  try {
    yield put({ type: LOADING_CONTRACT_REQUEST })
    const { contractId, contractData } = payload
    console.log("get web3 invest", contractId, contractData)
    let contractInfo = contractData
    if (!contractInfo) {
      contractInfo = yield call(getContractByFullId, contractId)
    }
    const propertyInfo = yield call(
      fetchInvestorPropertyWeb3ContractDetailsFromAPI,
      contractId
    )

    const contractBlockchainHistory = yield call(getContractWeb3BlockchainHistory, contractId)

    const env = yield call(fetchInvestorPropertyWeb3ContractEnvironmentDataFromAPI, contractId)
    const formattedPayload = yield call(formatPropertyData, {
      ...propertyInfo, ...env
    })
    yield put({
      type: FETCH_CONTRACT_SUCCEEDED,
      payload: { contract: { ...contractInfo, blockchainHistory: contractBlockchainHistory || [] }, property: formattedPayload }
    })
  } catch (err) {
    console.log(err)
    yield put({ type: FETCH_CONTRACT_ERROR, payload: {} })
  }
}

function* getTechnicalContractPendingInfo({ payload }) {
  try {
    yield put({ type: LOADING_CONTRACT_REQUEST })
    const { contractId, contractType, propertyId } = payload
    const userType = yield call(getUserTypeFromLocalStorage)
    const propertyInfo = yield call(
      fetchPropertyContractDetailsFromAPI,
      contractId
    )
    const blockchainHistory = yield call(getContractBlockchainHistory, contractId)

    const env = yield call(fetchPropertyContractEnvironmentDataFromAPI, contractId)
    const formattedPayload = yield call(formatPropertyData, {
      ...propertyInfo, ...env
    })
    yield put({
      type: FETCH_CONTRACT_SUCCEEDED,
      payload: { contract: { blockchainHistory: blockchainHistory || [] }, property: formattedPayload }
    })
  } catch (err) {
    console.log(err)
    yield put({ type: FETCH_CONTRACT_ERROR, payload: {} })
  }
}

function* investOnContract({ payload }) {
  const { contractId, amountToInvest } = payload
  try {
    yield put({ type: CONTRACT_INVESTMENT_LOADING, payload: {} })

    const amountToInvestWithDecimals = yield call(
      etherFormatFromValue,
      amountToInvest
    )
    yield call(investOnContractApi, contractId, amountToInvestWithDecimals)
    yield put({ type: CONTRACT_INVESTMENT_SUCCEEDED, payload: {} })
  } catch (err) {
    console.log(err)
    yield put({ type: CONTRACT_INVESTMENT_FAILED, payload: {} })
  }
}

function* technicalAuthorizeContract({ payload }) {
  const { contractId, contractType } = payload
  try {
    yield put({ type: CONTRACT_INVESTMENT_LOADING, payload: {} })
    console.log(contractType)
    switch (contractType) {
      case 'crops':
        yield call(authorizeCropsContract, contractId)
        break
      case 'livestock':
        yield call(authorizeLivestockContract, contractId)
        break
      case 'greenBond':
        yield call(authorizeGreenBondContract, contractId)
        break
      default:
        throw 'Invalid Type'
    }
    yield put({ type: CONTRACT_INVESTMENT_SUCCEEDED, payload: {} })
  } catch (err) {
    console.log(err)
    yield put({ type: CONTRACT_INVESTMENT_FAILED, payload: {} })
  }
}

function* technicalReproveContract({ payload }) {
  const { contractId, contractType } = payload
  try {
    yield put({ type: CONTRACT_INVESTMENT_LOADING, payload: {} })
    console.log("reprove type", contractType)

    switch (contractType) {
      case 'crops':
        yield call(reproveCropsContract, contractId)
        break
      case 'livestock':
        yield call(reproveLivestockContract, contractId)
        break
      case 'greenBond':
        yield call(reproveGreenBondContract, contractId)
        break
      default:
        throw 'Invalid Type'
    }
  } catch (err) {
    console.log(err)
    yield put({ type: CONTRACT_INVESTMENT_FAILED, payload: {} })
  }
}

export function* contractDetailsSaga() {
  yield all([
    takeLatest(INIT_CONTRACT_REQUEST, getContractInfo),
    takeLatest(INIT_WEB3CONTRACT_FOR_INVEST_REQUEST, getWeb3InvestContractInfo),
    takeLatest(INIT_WEB3CONTRACT_PENDING_FARMER, getWeb3PendingContractInfo),
    takeLatest(CONTRACT_INVESTMENT_REQUEST, investOnContract),
    takeLatest(
      REQUEST_CONTRACT_PENDING_FARMER_DATA,
      getTechnicalContractPendingInfo
    ),
    takeLatest(REQUEST_AUTHORIZE_CONTRACT, technicalAuthorizeContract),
    takeLatest(REQUEST_REPROVE_CONTRACT, technicalReproveContract)
  ])
}
