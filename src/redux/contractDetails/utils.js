import api from '../../services/config'

export const fetchPropertyContractDetailsFromAPI = async contractId => {
  try {
    const result = await api.post('/api/v1/contract/property', {
      contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}


export const fetchInvestorPropertyWeb3ContractDetailsFromAPI = async contractId => {
  try {
    const result = await api.post('/api/v1/invest/contract/web3/property', {
      contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}


export const fetchInvestorPropertyWeb3ContractEnvironmentDataFromAPI = async contractId => {
  try {
    const result = await api.post('/api/v1/invest/contract/web3/property/environment', {
      contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}



export const fetchPropertyWeb3ContractDetailsFromAPI = async contractId => {
  try {
    const result = await api.post('/api/v1/contract/web3/property', {
      contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}


export const fetchPropertyPendingWeb3ContractDetailsFromAPI = async (contractType, contractId) => {
  try {
    const result = await api.post('/api/v1/contract/web3/pending/property', {
      contractType, contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}



export const fetchPropertyContractEnvironmentDataFromAPI = async contractId => {
  try {
    const result = await api.post('/api/v1/contract/property/environment', {
      contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchPropertyWeb3ContractEnvironmentDataFromAPI = async contractId => {
  try {
    const result = await api.post('/api/v1/contract/web3/property/environment', {
      contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}
export const fetchPropertyWeb3PendingContractEnvironmentDataFromAPI = async (contractType, contractId) => {
  try {
    const result = await api.post('/api/v1/contract/web3/pending/property/environment', {
      contractType, contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const investOnContractApi = async (contractGeneralId, amount) => {
  const response = await api.post('/farmer/v1/investor/contract/invest', {
    contractGeneralId,
    amount
  })
  return response.data.data
}

export const formatPropertyData = propertyInfo => {
  const galleryIsDefined =
    propertyInfo.property.f4.rgbimages &&
    propertyInfo.property.f4.rgbimages[0].graph &&
    propertyInfo.property.f4.rgbimages[0].graph.s3links
  let gallery = null
  if (galleryIsDefined) {
    gallery = propertyInfo.property.f4.rgbimages[0].graph.s3links.sort(
      (a, b) => parseFloat(a.dates_m) - parseFloat(b.dates_m)
    )
  }
  const basicInfo = propertyInfo.property.f3.features[0]
  const informations = propertyInfo.property.f2.array_to_json
    ? propertyInfo.property.f2.array_to_json[0]
    : false
  const charts = propertyInfo.property.f1
  const formattedPayload = {
    reports: {
      initial: propertyInfo.reports.initial || propertyInfo.reports,
      fraud: propertyInfo.reports.fraud,
      monit: propertyInfo.reports.monit
    },
    informations,
    charts,
    gallery: galleryIsDefined ? gallery : null,
    basicInfo,
    heatmap: propertyInfo.heatMap,
    carbon: propertyInfo.carbon || [],
    deforestation: propertyInfo.deforestation || [],
    fireData: propertyInfo.fire || [],
    polygon: propertyInfo.polygon || false
  }
  return formattedPayload
}

export const authorizeCropsContract = async(contractId) => {
  const response = await api.post('/api/v1/contract/crops/status/authorize',{
    contractId
  })
  return response.data
}


export const authorizeLivestockContract = async(contractId) => {
  const response = await api.post('/api/v1/contract/livestock/status/authorize',{
    contractId
  })
  return response.data
}

export const authorizeGreenBondContract = async(contractId) => {
  const response = await api.post('/api/v1/contract/greenbond/status/authorize',{
    contractId
  })
  return response.data
}

export const reproveCropsContract = async(contractId) => {
  const response = await api.post('/api/v1/contract/crops/status/reprove',{
    contractId
  })
  return response.data
}

export const reproveLivestockContract = async(contractId) => {
  const response = await api.post('/api/v1/contract/livestock/status/reprove',{
    contractId
  })
  return response.data
}

export const reproveGreenBondContract = async(contractId) => {
  const response = await api.post('/api/v1/contract/greenbond/status/reprove',{
    contractId
  })
  return response.data
}

export const getContractHeatMap = async(contractId) => {
  const response = await api.post('/company/v1/contract/geodata',{
    contractId
  })
  return response.data
}


export const getContractBlockchainHistory = async(contractId) => {
  const response = await api.post('/api/v1/contract/history/blockchain',{
    contractId
  })
  return response.data
}

export const getContractWeb3BlockchainHistory = async(contractId) => {
  const response = await api.post('/api/v1/contract/history/web3',{
    contractId
  })
  return response.data
}
