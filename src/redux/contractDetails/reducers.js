import {
  LOADING_CONTRACT_REQUEST,
  FETCH_CONTRACT_ERROR,
  FETCH_CONTRACT_SUCCEEDED,
  RESET_VIEW_CONTRACT,
  CONTRACT_INVESTMENT_SUCCEEDED,
  CONTRACT_INVESTMENT_FAILED,
  CONTRACT_INVESTMENT_LOADING,
  INVESTMENT_RESET
} from './constants'

const defaultContractState = {
  loading: true,
  error: false,
  contract: false,
  property: { gallery: false, charts: false, reports: false },
  investLoading: false,
  investComplete: false,
  investError: false
}

const contractDetailsReducer = (
  state = defaultContractState,
  { type, payload }
) => {
  switch (type) {
    case LOADING_CONTRACT_REQUEST:
      return { property: false, contract: false, loading: true, error: false }
    case FETCH_CONTRACT_ERROR:
      return { ...defaultContractState, error: payload.error }
    case FETCH_CONTRACT_SUCCEEDED:
      return {
        ...defaultContractState,
        property: payload.property,
        contract: payload.contract,
        loading: false
      }
    case RESET_VIEW_CONTRACT:
      return { ...defaultContractState }

    case CONTRACT_INVESTMENT_LOADING:
      return {
        ...state,
        investLoading: true,
        investComplete: false,
        investError: false
      }
    case CONTRACT_INVESTMENT_SUCCEEDED:
      return {
        ...state,
        investComplete: true,
        investError: false,
        investLoading: false
      }
    case CONTRACT_INVESTMENT_FAILED:
      return {
        ...state,
        investError: true,
        investComplete: false,
        investLoading: false
      }

    case INVESTMENT_RESET:
      return {
        ...state,
        investError: false,
        investComplete: false,
        investLoading: false
      }

    default:
      return state
  }
}

export default contractDetailsReducer
