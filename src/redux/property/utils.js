import api from '../../services/config'
import { equals } from 'ramda'
import GeoJSON from 'geojson'
import { getUserTypeFromLocalStorage } from '../../services/storage_utils'

export const fetchFarmerWeatherDataFromAPI = async (farmerId) => {
  try {
    const result = await api.post('/api/v1/farmer/weather', { farmerId })
    return result.data
  } catch (err) {
    throw err
  }
}

export const sendPropertyPolygonToAPI = async geoData => {
  const response = await api.post('/api/v1/farmer/ceos/polygon/new', { ...geoData })

  // if (equals(response.data.status, false)) {
  //   throw new Error('Error creating a new property')
  // }

  return response.data
}

export const sendPropertyPolygonsToTechnicalAPI = async (geoData, farmerid, pastProduction) => {
  const response = await api.post('/api/v1/technical/ceos/polygon/new', {
    geoData,
    farmerid,
    pastProduction
  })

  // if (equals(response.data.status, false)) {
  //   throw new Error('Error creating a new property')
  // }

  return response.data
}



export const uploadPolygonShape = async (shape, farmerId, pastProduction) => {
  const response = await api.post(
    '/api/v1/technical/ceos/polygon/upload/shape/new',
    { polygon: shape, farmerId, pastProduction }
  )
  return response.data
}
  

export const uploadPolygonKml = async (kml, farmerId, pastProduction) => {
  const response = await api.post(
    '/api/v1/technical/ceos/polygon/upload/kml/new',
    { kml, farmerId, pastProduction }
  )
  return response.data
}
  


export const fetchFarmerPropertiesFromAPI = async (farmerId) => {
  const response = await api.post('/api/v1/farmer/properties', { farmerId: parseInt(farmerId) })
  return response.data
}

export const fetchPropertyDetailsFromAPI = async propertyId => {
  try {
    const result = await api.post('/api/v1/property', {
      propertyId: parseInt(propertyId)
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchPropertyDetailsFromTechnicalAPI = async (
  propertyId,
  farmerId
) => {
  try {
    const result = await api.post('/technical/v1/farmer/property/info', {
      propertyId: parseInt(propertyId),
      farmerId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchPropertyDetailsFromCompanylAPI = async (
  propertyId,
  farmerId
) => {
  try {
    const result = await api.post('/company/v1/farmer/property/info', {
      propertyId: parseInt(propertyId),
      farmerId
    })
    return result.data
  } catch (err) {
    throw err
  }
}


export const fetchPropertyFireDataFromAPI = async propertyId => {
  try {
    const result = await api.post('/farmer/v1/property/fire', {
      propertyId: parseInt(propertyId)
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchPropertyFireDataFromTechnicalAPI = async (
  propertyId,
  farmerId
) => {
  try {
    const result = await api.post('/technical/v1/property/fire', {
      propertyId: parseInt(propertyId),
      farmerId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchPropertyFireDataFromCompanylAPI = async (
  propertyId,
  farmerId
) => {
  try {
    const result = await api.post('/company/v1/property/fire', {
      propertyId: parseInt(propertyId),
      farmerId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchPropertyContractsFromAPI = async propertyId => {
  try {
    const result = await api.post('/farmer/v1/contract/property/get', {
      propertyId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchAgroTechnicalFarmerPropertiesFromAPI = async farmerId => {
  try {
    const result = await api.post('/technical/v1/farmer/properties', {
      farmerId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchCompanyFarmerPropertiesFromAPI = async farmerId => {
  try {
    const result = await api.post('/company/v1/farmer/properties', { farmerId })
    return result.data
  } catch (err) {
    throw err
  }
}

export const fetchPropertyHeatMapFromAPI = async propertyId => {
  try {
    const result = await api.post('/api/v1/property/heatmap', {
      propertyId
    })
    return result.data
  } catch (err) {
    throw err
  }
}


export const fetchPropertyEnvironmentDataFromAPI = async propertyId => {
  try {
    const result = await api.post('/api/v1/property/environment', {
      propertyId
    })
    return result.data
  } catch (err) {
    throw err
  }
}

export const generateGeoJson = (polygon, typeOfUse, farmerSerialId = false) => {
  polygon.push(polygon[0])
  const slot = {
    polygon: [polygon],
    typeofuse: typeOfUse
  }
  Object.assign(slot, farmerSerialId && { farmerserial: farmerSerialId })

  const data = [slot]

  const geoData = GeoJSON.parse(data, {
    Polygon: 'polygon'
  })

  return geoData
}

export const fetchPropertyPolygonApi = async contractId => {
  try {
    const result = await api.post("/api/v1/contract/property/polygon", {
      contractId
    })
    return result.data
  } catch (err) {
    throw err
  }
}
