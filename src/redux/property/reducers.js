import {
  CREATE_PROPERTY_ERROR,
  CREATE_PROPERTY_SUCCESS,
  UPDATE_PROPERTY,
  FETCH_PROPERTY_SUCCESS,
  FETCH_PROPERTY_ERROR,
  FETCH_PROPERTY_DETAILS_SUCCESS,
  RESET_PROPERTY
} from './actions'

const defaultProperty = {
  data: [],
  success: false,
  error: null,
  loading: false,
  loadingAction: false,
  details: {
    informations: null,
    gallery: null,
    charts: null,
    contracts: null
  }
}

const propertyReducer = (state = defaultProperty, action) => {
  switch (action.type) {
    case RESET_PROPERTY:
      return { ...defaultProperty }
    case UPDATE_PROPERTY:
      return { ...defaultProperty, loading: true }
    case CREATE_PROPERTY_SUCCESS:
      return { success: true, loading: false, error: null }
    case CREATE_PROPERTY_ERROR:
      return { error: action.payload, success: false, loading: false }
    case FETCH_PROPERTY_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload
      }
    case FETCH_PROPERTY_ERROR:
      return { error: action.payload, success: false, loading: false }
    case FETCH_PROPERTY_DETAILS_SUCCESS:
      return { ...state, details: action.payload, loading: false, error: null }
    default:
      return state
  }
}

export default propertyReducer
