import { takeLatest, call, put, all, select } from 'redux-saga/effects'
import { isNil } from 'ramda'

import {
  CREATE_PROPERTY_REQUEST,
  CREATE_PROPERTY_ERROR,
  CREATE_PROPERTY_SUCCESS,
  UPDATE_PROPERTY,
  FETCH_PROPERTY_SUCCESS,
  FETCH_PROPERTY_ERROR,
  FETCH_PROPERTY_REQUEST,
  FETCH_PROPERTY_DETAILS_SUCCESS,
  FETCH_PROPERTY_DETAILS_REQUEST,
  FETCH_PROPERTY_DETAILS_ERROR,
  AGROTECHNICAL_FETCH_FARMER_PROPERTIES,
  AGROTECHNICAL_CREATE_PROPERTY_REQUEST,
  UPLOAD_POLYGON_SHAPE_REQUEST,
  UPLOAD_POLYGON_KML_REQUEST
} from './actions'
import {
  sendPropertyPolygonToAPI,
  generateGeoJson,
  fetchPropertyDetailsFromAPI,
  sendPropertyPolygonsToTechnicalAPI,
  fetchPropertyEnvironmentDataFromAPI,
  fetchFarmerPropertiesFromAPI,
  uploadPolygonShape,
  uploadPolygonKml
} from './utils'
import { formatPropertyData } from '../contractDetails/utils'

function* createProperty({ payload }) {
  const isLoading = yield select(state => state.property.loading)
  if (isLoading) {
    return
  }
  yield put({ type: UPDATE_PROPERTY })
  const { property, typeProperty } = payload
  try {
    const geoData = yield call(generateGeoJson, property, typeProperty)
    yield call(sendPropertyPolygonToAPI, geoData)
    yield put({ type: CREATE_PROPERTY_SUCCESS })
  } catch (err) {
    console.warn(err)

    yield put({ type: CREATE_PROPERTY_ERROR, payload: err })
  }
}

function* createPropertyEnterprise({ payload }) {
  const isLoading = yield select(state => state.property.loading)
  if (isLoading) {
    return
  }
  yield put({ type: UPDATE_PROPERTY })
  const {
    propertyData: { property, typeProperty },
    farmerId,
    pastProduction
  } = payload
  try {
    const geoData = yield call(generateGeoJson, property, typeProperty)
    yield call(sendPropertyPolygonsToTechnicalAPI, geoData, farmerId, pastProduction)
    yield put({ type: CREATE_PROPERTY_SUCCESS })
  } catch (err) {
    console.warn(err)

    yield put({ type: CREATE_PROPERTY_ERROR, payload: err })
  }
}

function* fetchUsersProperties() {
  yield put({ type: UPDATE_PROPERTY })

  try {
    const farmerId = yield select(state => state.user.user.id)
    const data = yield call(fetchFarmerPropertiesFromAPI, farmerId)
    if (isNil(data.features)) {
      return yield put({ type: FETCH_PROPERTY_SUCCESS, payload: [] })
    }

    yield put({ type: FETCH_PROPERTY_SUCCESS, payload: data.features })
  } catch (err) {
    console.log(err)

    yield put({ type: FETCH_PROPERTY_ERROR })
  }
}

function* fetchPropertyDetails({ payload }) {
  const { propertyId } = payload
  const isLoading = yield select(state => state.property.loading)
  if (isLoading) {
    return
  }
  yield put({ type: UPDATE_PROPERTY })
  try {
    const data = yield call(fetchPropertyDetailsFromAPI, propertyId)
    const env = yield call(fetchPropertyEnvironmentDataFromAPI, propertyId)
    const formattedPayload = yield call(formatPropertyData, {
      ...data, ...env
    })
    yield put({
      type: FETCH_PROPERTY_DETAILS_SUCCESS,
      payload: formattedPayload
    })
  } catch (err) {
    yield put({ type: FETCH_PROPERTY_DETAILS_ERROR })
  }
}



function* fetchFarmerPropertyListByAgroTechnical({ payload }) {
  const { farmerId } = payload
  try {
    yield put({ type: UPDATE_PROPERTY })
    const data = yield call(fetchFarmerPropertiesFromAPI, farmerId)
    if (isNil(data.features)) {
      return yield put({ type: FETCH_PROPERTY_SUCCESS, payload: [] })
    } else {
      yield put({ type: FETCH_PROPERTY_SUCCESS, payload: data.features })
    }
  } catch (err) {
    console.log(err)
    yield put({ type: FETCH_PROPERTY_ERROR })
  }
}


function* uploadPolygonShapeSaga({ payload }) {
  yield put({ type: 'UPDATE_PROPERTY', payload: { loading: true } })
  const { data, farmerId, pastProduction } = payload
  try {
    yield call(uploadPolygonShape, data, farmerId, pastProduction)    
    yield put({ type: CREATE_PROPERTY_SUCCESS })
  } catch (err) {
    console.warn(err)
    yield put({ type: CREATE_PROPERTY_ERROR, payload: err })
  }
}


function* uploadPolygonKmlSaga({ payload }) {
  yield put({ type: 'UPDATE_PROPERTY', payload: { loading: true } })
  const { data, farmerId, pastProduction } = payload
  try {
    yield call(uploadPolygonKml, data, farmerId, pastProduction)    
    yield put({ type: CREATE_PROPERTY_SUCCESS })
  } catch (err) {
    console.warn(err)
    yield put({ type: CREATE_PROPERTY_ERROR, payload: err })
  }
}



export function* propertySagas() {
  yield all([
    takeLatest(CREATE_PROPERTY_REQUEST, createProperty),
    takeLatest(UPLOAD_POLYGON_SHAPE_REQUEST, uploadPolygonShapeSaga),
    takeLatest(UPLOAD_POLYGON_KML_REQUEST, uploadPolygonKmlSaga),
    takeLatest(FETCH_PROPERTY_REQUEST, fetchUsersProperties),
    takeLatest(FETCH_PROPERTY_DETAILS_REQUEST, fetchPropertyDetails),
    takeLatest(
      AGROTECHNICAL_FETCH_FARMER_PROPERTIES,
      fetchFarmerPropertyListByAgroTechnical
    ),
    takeLatest(AGROTECHNICAL_CREATE_PROPERTY_REQUEST, createPropertyEnterprise)
  ])
}
