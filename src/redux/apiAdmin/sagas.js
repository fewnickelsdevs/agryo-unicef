import { all, takeLatest, call, put, delay } from 'redux-saga/effects'
import {
  SET_API_INFO_COMPLETE,
  GET_API_INFO_REQUEST,
  REQUEST_NEW_API,
  REQUEST_NEW_API_COMPLETE,
  CONFIRM_NEW_API,
  CONFIRM_NEW_API_COMPLETE,
  SET_APIADM_ERROR,
  REMOVE_APIADM_ERROR
} from './constants'
import {
  requestConfirmationCodeToCreateNewApiKeys,
  confirmCodeAndCreateNewApiKeys,
  getApiAdmUserInfo
} from './utils'

function* dispatchErrorAndResetAfterDelay(error, ms) {
  yield put({ type: error.type, payload: error.message })
  yield delay(ms)
  yield put({ type: REMOVE_APIADM_ERROR })

  console.warn(error.message)
}

function* getApiKeysUserInfo() {
  try {
    const { apiData } = yield call(getApiAdmUserInfo)
    yield put({ type: SET_API_INFO_COMPLETE, payload: { apiData } })
  } catch (err) {
    console.log(err)
    //yield put({  })
  }
}

function* requestNewApiKey({ payload }) {
  const { name, password } = payload
  try {
    const result = yield call(
      requestConfirmationCodeToCreateNewApiKeys,
      name,
      password
    )
    yield put({ type: REQUEST_NEW_API_COMPLETE })
  } catch (err) {
    console.log(err)

    // if('err' === invalid pass)
    const dispatchData = { type: SET_APIADM_ERROR, message: 'Invalid Password' }
    yield call(dispatchErrorAndResetAfterDelay, dispatchData, 3000)
  }
}

function* requestConfirmApiKey({ payload }) {
  const { token, apiLabel } = payload
  try {
    const result = yield call(
      confirmCodeAndCreateNewApiKeys,
      token,
      'api-label'
    )
    const newApiKey = result.data.newApiKey
    yield put({ type: CONFIRM_NEW_API_COMPLETE, payload: { newApiKey } })
  } catch (err) {
    console.log(err)
    // if('err' === invalid pass)
    const dispatchData = { type: SET_APIADM_ERROR, message: 'Invalid Code' }
    yield call(dispatchErrorAndResetAfterDelay, dispatchData, 3000)
  }
}

export function* apiAdmSagas() {
  yield all([
    takeLatest(GET_API_INFO_REQUEST, getApiKeysUserInfo),
    takeLatest(REQUEST_NEW_API, requestNewApiKey),
    takeLatest(CONFIRM_NEW_API, requestConfirmApiKey)
  ])
}
