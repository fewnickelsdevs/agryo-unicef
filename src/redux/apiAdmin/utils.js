import api from '../../services/config'

export const requestConfirmationCodeToCreateNewApiKeys = async (
  name,
  password
) => {
  try {
    const result = await api.post('/farmer/v1/user/api/request', {
      name,
      password
    })
    return result
  } catch (err) {
    throw err
  }
}

export const confirmCodeAndCreateNewApiKeys = async (token, name) => {
  const response = await api.post('/farmer/v1/user/api/confirm', {
    token,
    name
  })
  return response
}

export const getApiAdmUserInfo = async () => {
  const response = await api.get('/farmer/v1/user/api')
  return response.data
}
