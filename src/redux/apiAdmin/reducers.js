import {
  SET_API_INFO_COMPLETE,
  OPEN_MODAL_NEW_API,
  REQUEST_NEW_API_COMPLETE,
  CONFIRM_NEW_API_COMPLETE,
  SET_APIADM_ERROR,
  REMOVE_APIADM_ERROR,
  CANCEL_REQUEST_NEW_API,
  CLOSE_NEW_API
} from './constants'

const defaultApiAdm = {
  loading: false,
  error: false,
  apiData: [],
  requestNewApi: false,
  waitingNewApiConfirmation: false,
  newApiData: false
}

const apiAdmReducer = (state = defaultApiAdm, { type, payload }) => {
  switch (type) {
    case SET_API_INFO_COMPLETE:
      return { ...defaultApiAdm, apiData: payload.apiData }
    case OPEN_MODAL_NEW_API:
      return { ...state, requestNewApi: true }
    case REQUEST_NEW_API_COMPLETE:
      return { ...state, waitingNewApiConfirmation: true }

    case CONFIRM_NEW_API_COMPLETE:
      return {
        ...state,
        newApiData: payload.newApiKey,
        waitingNewApiConfirmation: false,
        requestNewApi: false
      }
    case CLOSE_NEW_API:
      return { ...state, newApiData: false }

    case SET_APIADM_ERROR:
      return { ...state, error: payload }
    case REMOVE_APIADM_ERROR:
      return { ...state, error: false }

    case CANCEL_REQUEST_NEW_API:
      return {
        ...state,
        requestNewApi: false,
        waitingNewApiConfirmation: false,
        newApiData: false
      }

    default:
      return state
  }
}

export default apiAdmReducer
