import api from '../../services/config'

export const getGreenBondContractList = async () => {
  const response = await api.get('/api/v1/contract/web3/greenbond/list')
  return response.data
}

export const getLivestockContractList = async () => {
  const response = await api.get('/api/v1/contract/web3/livestock/list')
  return response.data
}

export const getCropsContractList = async () => {
  const response = await api.get('/api/v1/contract/web3/crops/list')
  return response.data
}
