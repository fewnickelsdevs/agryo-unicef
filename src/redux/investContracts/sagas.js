import { takeLatest, call, put, select, all } from 'redux-saga/effects'

import {
  FETCH_GREENBOND_CONTRACTS,
  FETCH_GREENBOND_CONTRACTS_FETCHING,
  FETCH_GREENBOND_CONTRACTS_SUCCESS,
  FETCH_GREENBOND_CONTRACTS_ERROR,
  FETCH_CROPS_CONTRACTS,
  FETCH_CROPS_CONTRACTS_FETCHING,
  FETCH_CROPS_CONTRACTS_SUCCESS,
  FETCH_CROPS_CONTRACTS_ERROR,
  FETCH_LIVESTOCK_CONTRACTS_FETCHING,
  FETCH_LIVESTOCK_CONTRACTS,
  FETCH_LIVESTOCK_CONTRACTS_SUCCESS,
  FETCH_LIVESTOCK_CONTRACTS_ERROR,
  REQUEST_WEB3_INVEST,
  CONTRACT_INVESTMENT_SUCCEEDED,
  CONTRACT_INVESTMENT_FAILED
} from './constants'
import {
  getGreenBondContractList,
  getLivestockContractList,
  getCropsContractList
} from './utils'
import { generateGreenBondContract, generateCostingCropsContract, generateCostingLivestockContract, etherFormatFromValue } from '../../web3/contracts';
import { extractNumbersFromString } from '../../utils/extractNumberString';
import { createContractTransaction } from '../transaction/sagas';

function* getCropsContractsForInvest() {
  yield put({ type: FETCH_CROPS_CONTRACTS_FETCHING, payload: {} })

  try {
    const cropsContracts = yield call(getCropsContractList)

    yield put({ type: FETCH_CROPS_CONTRACTS_SUCCESS, payload: cropsContracts })
  } catch (err) {
    yield put({ type: FETCH_CROPS_CONTRACTS_ERROR, payload: {} })
  }
}

function* getLivestockContractsForInvest() {
  yield put({ type: FETCH_LIVESTOCK_CONTRACTS_FETCHING, payload: {} })

  try {
    const livestockContracts = yield call(getLivestockContractList)

    yield put({
      type: FETCH_LIVESTOCK_CONTRACTS_SUCCESS,
      payload: livestockContracts
    })
  } catch (err) {
    yield put({ type: FETCH_LIVESTOCK_CONTRACTS_ERROR, payload: err })
  }
}

function* getGreenBondContractsForInvest() {
  yield put({ type: FETCH_GREENBOND_CONTRACTS_FETCHING, payload: {} })

  try {
    const greenBondContracts = yield call(getGreenBondContractList)

    yield put({
      type: FETCH_GREENBOND_CONTRACTS_SUCCESS,
      payload: greenBondContracts
    })
  } catch (err) {
    yield put({ type: FETCH_GREENBOND_CONTRACTS_ERROR, payload: err })
  }
}


function* investOnContract({ payload }) {
  const { contractId, contractType, amountToInvest } = payload

  const costingContracts = {
    crops: generateCostingCropsContract,
    greenBond: generateGreenBondContract,
    livestock: generateCostingLivestockContract
  }

  try {
    const contract = yield call(costingContracts[contractType])
    const amountToInvestWithDecimals = yield call(
      etherFormatFromValue,
      amountToInvest
    )

    const formatContractId = yield call(extractNumbersFromString, contractId)
    console.log("--- invest contract ---")
    console.log(formatContractId[0])
    console.log(amountToInvestWithDecimals)
    console.log("------")

    const contractData = yield call(
      contract.methods.depositToken,
      formatContractId[0],
      amountToInvestWithDecimals
    )
    const transactionReceipt = yield call(
      createContractTransaction,
      contractData
    )

    if (transactionReceipt.error) throw new Error(transactionReceipt.error)

    //console.log(transactionReceipt)

    yield put({ type: CONTRACT_INVESTMENT_SUCCEEDED })
  } catch (err) {
    yield put({ type: CONTRACT_INVESTMENT_FAILED })

    console.warn(err)
  }
}


export function* contractsForInvestSagas() {
  yield all([
    takeLatest(FETCH_GREENBOND_CONTRACTS, getGreenBondContractsForInvest),
    takeLatest(FETCH_LIVESTOCK_CONTRACTS, getLivestockContractsForInvest),
    takeLatest(FETCH_CROPS_CONTRACTS, getCropsContractsForInvest),
    takeLatest(REQUEST_WEB3_INVEST, investOnContract)
  ])
}
