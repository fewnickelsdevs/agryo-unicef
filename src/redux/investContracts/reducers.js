import {
  FETCH_GREENBOND_CONTRACTS_FETCHING,
  FETCH_GREENBOND_CONTRACTS_SUCCESS,
  FETCH_GREENBOND_CONTRACTS_ERROR,
  FETCH_CROPS_CONTRACTS_FETCHING,
  FETCH_CROPS_CONTRACTS_SUCCESS,
  FETCH_CROPS_CONTRACTS_ERROR,
  FETCH_LIVESTOCK_CONTRACTS_FETCHING,
  FETCH_LIVESTOCK_CONTRACTS_SUCCESS,
  FETCH_LIVESTOCK_CONTRACTS_ERROR
} from './constants'

const defaultState = {
  loading: false,
  error: false,
  contracts: []
}

const investContractReducer = (
  state = { ...defaultState },
  { type, payload }
) => {
  switch (type) {
    case FETCH_GREENBOND_CONTRACTS_FETCHING:
      return { ...defaultState, loading: true }
    case FETCH_GREENBOND_CONTRACTS_SUCCESS:
      return { ...defaultState, contracts: payload }
    case FETCH_GREENBOND_CONTRACTS_ERROR:
      return { ...defaultState, error: true }

    case FETCH_CROPS_CONTRACTS_FETCHING:
      return { loading: true, error: false, contracts: [] }
    case FETCH_CROPS_CONTRACTS_SUCCESS:
      return { loading: false, error: false, contracts: payload }
    case FETCH_CROPS_CONTRACTS_ERROR:
      return { loading: false, error: true, contracts: [] }

    case FETCH_LIVESTOCK_CONTRACTS_FETCHING:
      return { loading: true, error: false, contracts: [] }
    case FETCH_LIVESTOCK_CONTRACTS_SUCCESS:
      return { loading: false, error: false, contracts: payload }
    case FETCH_LIVESTOCK_CONTRACTS_ERROR:
      return { loading: false, error: true, contracts: [] }

    default:
      return state
  }
}

export default investContractReducer
