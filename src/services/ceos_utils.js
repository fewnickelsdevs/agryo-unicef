export const getPropertyLocation = property => {
  return ` ${property.f7} - ${property.f5}`
}

export const mappedStatus = t => ({
  0: t('properties.status.processing'),
  1: t('properties.status.confirmed'),
  2: t('properties.status.invalid'),
  3: t('properties.status.error')
})
