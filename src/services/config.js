import axios from 'axios'

const MAX_REQUESTS_COUNT = 1
const INTERVAL_MS = 10
let PENDING_REQUESTS = 0

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  headers: {
    'x-access-token': {
      toString() {
        return localStorage.getItem('agryo-jwt')
      }
    }
  }
})

// api.interceptors.response.use((response) => {
//   // Do something with response data
//   const token = response.headers['update-x-access-token']
//   if(token) localStorage.setItem('agryo-jwt', token)
//   return response
// }, (error) => {
//   if(error.response.status === 401){
//     window.localStorage.removeItem('agryo-jwt')
//     document.location.reload()
//   }
//   return Promise.reject(error);
// })

api.interceptors.request.use(function(config) {
  return new Promise((resolve, reject) => {
    let interval = setInterval(() => {
      if (PENDING_REQUESTS < MAX_REQUESTS_COUNT) {
        PENDING_REQUESTS++
        clearInterval(interval)
        resolve(config)
      }
    }, INTERVAL_MS)
  })
})
/**
 * Axios Response Interceptor
 */
api.interceptors.response.use(
  function(response) {
    PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1)
    const token = response.headers['update-x-access-token']
    if (token) {
      localStorage.setItem('agryo-jwt', token)
    }
    return Promise.resolve(response)
  },
  function(error) {
    PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1)
    if (error.response.status === 401) {
      window.localStorage.removeItem('agryo-jwt')
      document.location.reload()
    }
    const token = error.response.headers['update-x-access-token']
    if (token) {
      localStorage.setItem('agryo-jwt', token)
    }
    return Promise.reject(error)
  }
)

export default api
