import { crops } from '../mocks/crops.json'
import { livestocks } from '../mocks/livestocks.json'

export const generateContractTypeAndId = (contractType, contractId) => {
    const product = [...crops, ...livestocks].find(product => {
      const productId = contractType
      return product.id === productId
    }) || { name: 'Green Bond', value: 'greenBond' }
  
    let data = {
      "crops": `C${contractId}`,
      "livestock": `L${contractId}`,
      "greenBond": `GB${contractId}`
    }
    return { contractId: data[product.value] || "INVALID", name: product.name || "INVALID", value: product.value || "INVALID" }
  }