import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'
import './assets/leaflet-sidebar.css'

import './initializeI18n'
import { withTranslation } from "react-i18next";

const packageJSON = require('../package.json')

/* Hot reloading */
if (module.hot) {
  module.hot.accept()
}

window.APP_VERSION = packageJSON.version || `0.0.0`
window.GIT_SHA1 = packageJSON.git_revision_sha1 || `SHA1`

const AppWrapper = withTranslation()(props => {
  return <App t={props.t} />
})

ReactDOM.render(
  (
    <React.Suspense fallback="">
      <AppWrapper />
    </React.Suspense>
  ), document.getElementById('root'))
  serviceWorker.unregister()
  
// if (process.env.REACT_APP_PRODUCTION === 'true') {
//   serviceWorker.register()
// } else {
//   serviceWorker.unregister()
// }
