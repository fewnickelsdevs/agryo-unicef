import React, { useRef, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import MenuList from './MenuList'
import DevListItem from './DevListItem'

const Container = styled.nav`
  ${({ theme, open }) => `
    top: 0;
    left: 0;
    height: 100%;
    position: fixed;
    background: #fff;
    border-right: 1px solid #ccc;
    transition: transform 0.3s ease-in-out;
    z-index: 10;

    ${theme.breakpoints.sm} {
      z-index: 1202;
      outline: ${open ? '100rem solid rgba(0,0,0,.3)' : 'none'};
      transform: ${open ? 'translateX(0)' : 'translateX(-100%)'};
    }

  `}
`

const useOnClickOutside = (ref, handler) => {
  useEffect(() => {
    const listener = event => {
      if (!ref.current || ref.current.contains(event.target)) {
        return
      }

      handler(event)
    }
    document.addEventListener('mousedown', listener)

    return () => {
      document.removeEventListener('mousedown', listener)
    }
  }, [ref, handler])
}

function renderItems(items, onClose) {
  const location = useLocation()
  const [selectedIndex, setSelectedIndex] = React.useState(null)

  return items.map((item, index) => {
    let isSelected = selectedIndex === index
    let onClickHandler = () => setSelectedIndex(index)

    if (item.to) {
      isSelected = location.pathname.indexOf(item.to) !== -1 && !selectedIndex
      onClickHandler = () => {
        setSelectedIndex(null) && onClose()
      }
    }
    return (
      <DevListItem
        index={index}
        onClick={onClickHandler}
        isSelected={isSelected}
        location={location.pathname}
        item={item}
      />
    )
  })
}

const SidebarContainer = ({ items, open, onClose }) => {
  const sidebarRef = useRef()
  useOnClickOutside(sidebarRef, onClose)

  return (
    <Container ref={sidebarRef} open={open}>
      <MenuList>{renderItems(items, onClose)}</MenuList>
    </Container>
  )
}

SidebarContainer.propTypes = {
  items: PropTypes.array.isRequired
}

export default SidebarContainer
