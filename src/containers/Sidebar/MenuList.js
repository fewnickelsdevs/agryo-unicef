import React from 'react'
import styled from 'styled-components'

const StyledMenuList = styled.ul`
  ${({ theme }) => `
    padding: 0;
    width: 14rem;
    margin-top: 5rem;
    list-style: none;
  
    ${theme.breakpoints.sm} {
      margin-top: 1rem;
    }
  `}
`

const MenuList = ({ children }) => <StyledMenuList>{children}</StyledMenuList>

export default MenuList
