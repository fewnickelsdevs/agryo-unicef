import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Content = styled(Link)`
  ${props => `
    color: #555;
    font-size: 14px;
    padding: 11px 16px;
    text-decoration: none;

    display: grid;
    align-items: center;
    justify-items: center;
    grid-template-columns: 0.2fr 1fr;

    & > svg {
      font-size: 19px;
    }

    &:hover {
      background: #F9F9F9;
    }

    ${props.isSelected &&
      `
      background: #eee;
      border-right: 4px solid #3F8C48;

      & > svg {
        color: #3F8C48
      }

      &:hover {
        background: #EEEEEE;
      }
    `}
  `}
`

const SubContent = styled(Link)`
  ${props => `
    color: #555;
    font-size: 12px;
    padding: 11px 16px 11px 30px;
    
    display: grid;
    align-items: center;
    justify-items: left;
    grid-template-columns: 0.2fr 0.5fr;

    & > svg {
      font-size: 14px;
    }

    &:hover {
      background: #F9F9F9;
    }

    ${props.isSelected &&
      `
      background: #80808012;

      & > svg {
        color: #3F8C48
      }

      &:hover {
        background: #EEEEEE;
      }
    `}
  `}
`

const MenuListItemLabel = styled.span`
  justify-self: start;
  margin-left: 0.4rem;
`

const DevListItem = ({ item, isSelected, onClick, location }) => {
  const contentProps = {}
  if (item.to) {
    contentProps.to = item.to
  }
  if (onClick) {
    contentProps.onClick = onClick
  }

  if (item.nested) {
    contentProps.to = null
  }


  return (
    <li>
      <Content isSelected={isSelected} {...contentProps}>
        <FontAwesomeIcon icon={item.icon} />
        <MenuListItemLabel>{item.label}</MenuListItemLabel>
      </Content>
      {item.nested && isSelected && (
        <ul>
          {item.nested.map(subItem => {
            const subContentProps = {
              isSelected: location === subItem.to,
              to: subItem.to
            }

            if (onClick) {
              subContentProps.onClick = onClick
            }

            return (
              <li>
                <SubContent {...subContentProps}>
                  <FontAwesomeIcon size='xs' icon={subItem.icon} />
                  <span>{subItem.label}</span>
                </SubContent>
              </li>
            )
          })}
        </ul>
      )}
    </li>
  )
}

export default DevListItem
