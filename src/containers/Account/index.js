import React from 'react'
import { ContainerBackground, AgryoLogo } from './styles'

import agryoLogo from '../../assets/logo-white.png'

export default function AccountContainer({ primaryContent }) {
  return (
    <ContainerBackground>
      <AgryoLogo src={agryoLogo} alt='Logo Agryo' />
      {primaryContent}
    </ContainerBackground>
  )
}
