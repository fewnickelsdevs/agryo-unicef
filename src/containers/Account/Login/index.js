import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {
  Link,
  ButtonPrimary,
  ButtonSecondary
} from '../../../components/Buttons'
import { LoginForm, AgryoOverlayLogo, FormWrapper } from '../styles'
import agryoOverlayIcon from '../../../assets/logo-overlay.png'
import AlertSnackbar from '../../../components/AlertSnackbar'
import { TextInput } from '../../../components/Form'
import Timer from './timer'
import SelectInput from '../../../components/Form/SelectInput'
import { USER_TYPES } from '../../../mocks/userTypes';
import { USER_TYPES_ENTERPRISE } from '../../../utils/user-type';

const Heading = styled.h2`
  margin-bottom: 1rem;
`

const SubHeading = styled.p`
  margin-bottom: 2rem;
  color: grey;
`

const Buttons = styled.div`
  ${({ theme }) => `
    margin-top: 2rem;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      margin-bottom: 2rem;
      flex-direction: column-reverse;
      height: 5rem;
      justify-content: space-between;
    }
  `}
`

const ButtonsConfirmation = styled.div`
  ${({ theme }) => `
  margin-top: 1rem;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${theme.breakpoints.xs} {
    flex-direction: column-reverse;
  }
  `}
`

const ButtonsWrapper = styled.div`
  ${({ theme }) => `
    width: 80%;
    height: 50%;
    display: flex;
    padding-bottom: 15px;
    align-items: flex-end;
    flex-direction: column;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      align-items: center;
    }
  `}
`

const ButtonAndTimerContainer = styled.div`
  ${({ theme }) => `
    flex-direction: column;

  ${theme.breakpoints.xs} {
      margin-bottom: 25px;
  }
`}
`

const LoginInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 80%;
    }

    ${theme.breakpoints.xs} {
      width: 100%;
      & > * {
        width: 100%;
      }
    }
  `}
`

export default function LoginContainer({
  t,
  title,
  onCreateAccount,
  onLogin,
  password,
  setPassword,
  email,
  setEmail,
  loading,
  tooManyRequest,
  error,
  requestConfirmationCode,
  onChangeConfirmationCode,
  onSubmitConfirmationCode,
  tokenConfirmation,
  timeIsOver,
  resendEmailConfirmation,
  requestEmailWaitPeriod,
  handleResendEmailConfirmationUnlockAfterTime,
  handleSendEmailToken,
  handleResetPassword,
  showTypeLogin = false,
  showCreateAccount = false,
  setTypeLogin,
  typeLogin
}) {
  const [alertSnackbar, setAlertSnackbar] = useState({
    open: false,
    message: ''
  })

  useEffect(() => {
    setAlertSnackbar({
      message: error,
      open: Boolean(error)
    })
  }, [error])

  return (
    <React.Fragment>
      <LoginForm>
        <AgryoOverlayLogo src={agryoOverlayIcon} alt='Agryo Logo Overlay' />
        {requestConfirmationCode ? (
          <FormWrapper>
            <Heading>{t('login.confirmation-code')}</Heading>
            <SubHeading>{t('login.code-message')}</SubHeading>
            <LoginInput
              value={tokenConfirmation}
              onChange={e => onChangeConfirmationCode(e)}
              placeholder={t('login.confirmation-code')}
            />
            <ButtonsWrapper>
              <ButtonsConfirmation>
                <ButtonAndTimerContainer>
                  <p
                    align='center'
                    style={{
                      color: requestEmailWaitPeriod ? 'black' : 'white'
                    }}
                  >
                    {requestEmailWaitPeriod ? (
                      <Timer
                        start={requestEmailWaitPeriod}
                        reset={handleResendEmailConfirmationUnlockAfterTime}
                      />
                    ) : (
                      '0'
                    )}
                  </p>
                  <ButtonSecondary
                    disabled={requestEmailWaitPeriod}
                    onClick={handleSendEmailToken}
                    smSize='100%'
                    text={t('login.send-code')}
                  />
                </ButtonAndTimerContainer>
                <ButtonAndTimerContainer>
                  <p align='center' style={{ color: 'white' }}>
                    {' '}
                    0
                  </p>
                  <ButtonPrimary
                    disabled={!tokenConfirmation}
                    onClick={onSubmitConfirmationCode}
                    loading={loading}
                    smSize='100%'
                    text={'Confirm'}
                  />
                </ButtonAndTimerContainer>
              </ButtonsConfirmation>
            </ButtonsWrapper>
          </FormWrapper>
        ) : (
          <FormWrapper>
            <Heading>{t(title)}</Heading>

            <LoginInput
              value={email}
              onChange={e => setEmail(e)}
              placeholder={t('login.input-email')}
              />
            <LoginInput
              value={password}
              onChange={e => setPassword(e)}
              placeholder={t('login.input-password')}
              type='password'
            />
            {showTypeLogin && (
              <SelectInput
                onChange={setTypeLogin}
                value={typeLogin}
                options={USER_TYPES_ENTERPRISE(t)}
              />
            )}

            {tooManyRequest &&
              t('login.to-many-requests', {
                timer: <Timer start={tooManyRequest} reset={timeIsOver} />
              })}

            <ButtonsWrapper>
              <Buttons>
                <Link
                  onClick={handleResetPassword}
                  text={t('login.forgot-data')}
                  light
                />

                <ButtonPrimary
                  disabled={!email || !password || tooManyRequest}
                  onClick={onLogin}
                  loading={loading}
                  smSize='125%'
                  text={t('login.button-login')}
                  />
              </Buttons>
              {showCreateAccount && (
                <Link
                  onClick={onCreateAccount}
                  text={t('login.link-create-account')}
                  />
              )}
              <div style={{ fontSize: 10 }}>
                <span title={window.GIT_SHA1}>v:{window.APP_VERSION}</span>
              </div>
            </ButtonsWrapper>
          </FormWrapper>
        )}
      </LoginForm>
      <AlertSnackbar {...alertSnackbar} />
    </React.Fragment>
  )
}

LoginContainer.propTypes = {
  onRecovery: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  onCreateAccount: PropTypes.func.isRequired,
  address: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
}

LoginContainer.defaultProps = {
  error: null
}
