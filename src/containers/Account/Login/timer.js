import React, { useState, useEffect, useRef } from 'react'

const Timer = ({ start, reset }) => {
  const [time, setTime] = useState(false)
  const [seconds, setSeconds] = useState(60)
  const fmtMSS = s => (s - (s %= 60)) / 60 + (9 < s ? ':' : ':0') + s

  useEffect(() => {
    if (start) {
      setSeconds(start)
      setTime(new Date().toLocaleTimeString())
    }
  }, [start])

  useEffect(() => {
    if (!time) return
    const timeout = setTimeout(() => {
      if (seconds === 0) {
        return reset()
      }
      const date = new Date()
      const actual = seconds - 1
      setSeconds(actual)
      setTime(date.toLocaleTimeString())
    }, 1000)
    return () => {
      clearTimeout(timeout)
    }
  }, [time, start])

  return seconds
}

export default Timer
