import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {
  Link,
  ButtonPrimary,
  ButtonSecondary
} from '../../../components/Buttons'
import { LoginForm, AgryoOverlayLogo, FormWrapper } from '../styles'
import agryoOverlayIcon from '../../../assets/logo-overlay.png'
import AlertSnackbar from '../../../components/AlertSnackbar'
import { TextInput } from '../../../components/Form'
import ErrorDialog from '../../../components/Dialogs/Error'
import SuccessDialog from '../../../components/Dialogs/Success'
import validatePassword from '../../../formatters/validatePassword';

const Heading = styled.h2`
  margin-bottom: 1rem;
`

const SubHeading = styled.p`
  margin-bottom: 2rem;
  color: grey;
`

const Buttons = styled.div`
  ${({ theme }) => `
    margin-top: 2rem;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      margin-bottom: 2rem;
      flex-direction: column-reverse;
      height: 5rem;
      justify-content: space-between;
    }
  `}
`

const ButtonsConfirmation = styled.div`
  ${({ theme }) => `
  margin-top: 1rem;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${theme.breakpoints.xs} {
    flex-direction: column-reverse;
  }
  `}
`

const ButtonsWrapper = styled.div`
  ${({ theme }) => `
    width: 80%;
    height: 50%;
    display: flex;
    align-items: flex-end;
    flex-direction: column;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      align-items: center;
    }
  `}
`

const ButtonAndTimerContainer = styled.div`
  ${({ theme }) => `
    flex-direction: column;

  ${theme.breakpoints.xs} {
      margin-bottom: 25px;
  }
`}
`

const LoginInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 80%;
    }

    ${theme.breakpoints.xs} {
      width: 100%;
      & > * {
        width: 100%;
      }
    }
  `}
`

export default function ConfirmResetPassword({
  t,
  password,
  onChangePassword,
  confirmPassword,
  onChangeConfirmationPassword,
  loading,
  tooManyRequest,
  error,
  success,
  onSubmitConfirmationPassword,
  redirectToLogin
}) {
  const [invalidPass, setInvalidPass] = useState(false)
  const [alertSnackbar, setAlertSnackbar] = useState({
    open: false,
    message: ''
  })

  const confirmResetPassword = () => {
    const validPass = validatePassword(password)
    if (validPass) {
      onSubmitConfirmationPassword()
    } else {
      setInvalidPass(true)
    }
  }

  useEffect(() => {
    setAlertSnackbar({
      message: error,
      open: Boolean(error)
    })
  }, [error])

  return (
    <React.Fragment>
      <LoginForm>
        <AgryoOverlayLogo src={agryoOverlayIcon} alt='Agryo Logo Overlay' />

        <FormWrapper>
          <Heading>{t("reset-password.heading")}</Heading>
          <SubHeading>{t("reset-password.confirm-message")}</SubHeading>
          <LoginInput
            value={password}
            onChange={e => onChangePassword(e)}
            placeholder={t("account-password.input-new-password")}
            type='password'
          />
          <LoginInput
            value={confirmPassword}
            onChange={e => onChangeConfirmationPassword(e)}
            placeholder={t("account-password.input-confim-password")}
            type='password'
          />

          <p style={{ fontSize: 12, color: invalidPass ? 'red' : 'grey', width: '90%' }}>
            {t('signup.password-message')}
          </p>

          <ButtonsWrapper>
            <ButtonsConfirmation>
              <ButtonSecondary
                onClick={redirectToLogin}
                smSize='100%'
                text={'Cancel'}
              />

              <ButtonPrimary
                onClick={confirmResetPassword}
                loading={loading}
                smSize='100%'
                text={t("contracts.confirm")}
              />
            </ButtonsConfirmation>
          </ButtonsWrapper>
        </FormWrapper>
      </LoginForm>

      <AlertSnackbar {...alertSnackbar} />
      <ErrorDialog
        visible={error}
        title={t('reset-password.heading')}
        message={t('reset-password.error-message')}
        buttonMessage={'OK'}
        onClick={redirectToLogin}
      />
      <SuccessDialog
        visible={success}
        title={t('reset-password.heading')}
        message={t('reset-password.success-confirm-message')}
        buttonMessage={'Go To Login'}
        onClick={redirectToLogin}
      />
    </React.Fragment>
  )
}

ConfirmResetPassword.propTypes = {
  onRecovery: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  onCreateAccount: PropTypes.func.isRequired,
  address: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
}

ConfirmResetPassword.defaultProps = {
  error: null
}
