import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import {
  Link,
  ButtonPrimary,
  ButtonSecondary
} from '../../../components/Buttons'
import { LoginForm, AgryoOverlayLogo, FormWrapper } from '../styles'
import agryoOverlayIcon from '../../../assets/logo-overlay.png'
import AlertSnackbar from '../../../components/AlertSnackbar'
import { TextInput } from '../../../components/Form'
import { USER_TYPES_ENTERPRISE } from '../../../utils/user-type';
import SelectInput from '../../../components/Form/SelectInput';

const Heading = styled.h2`
  margin-bottom: 1rem;
`

const SubHeading = styled.p`
  margin-bottom: 2rem;
  color: grey;
`

const Buttons = styled.div`
  ${({ theme }) => `
    margin-top: 2rem;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      margin-bottom: 2rem;
      flex-direction: column-reverse;
      height: 5rem;
      justify-content: space-between;
    }
  `}
`

const ButtonsWrapper = styled.div`
  ${({ theme }) => `
    width: 80%;
    height: 50%;
    display: flex;
    align-items: flex-end;
    flex-direction: column;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      align-items: center;
    }
  `}
`
const LoginInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 80%;
    }

    ${theme.breakpoints.xs} {
      width: 100%;
      & > * {
        width: 100%;
      }
    }
  `}
`

export default function RequestResetPasswordContainer({
  t,
  onResetPassword,
  email,
  setEmail,
  userType,
  setUserType,
  loading,
  error,
  success,
  onRedirectToRegister
}) {
  const [alertSnackbar, setAlertSnackbar] = useState({
    open: false,
    message: ''
  })

  useEffect(() => {
    setAlertSnackbar({
      message: error,
      open: Boolean(error)
    })
  }, [error])

  return (
    <React.Fragment>
      <LoginForm>
        <AgryoOverlayLogo src={agryoOverlayIcon} alt='Agryo' />
        {success ? (
          <FormWrapper>
            <Heading>{t('reset-password.heading')}</Heading>
            <SubHeading>
              {t('reset-password.success-message')}
            </SubHeading>
          </FormWrapper>
        ) : (
            <FormWrapper>
              <Heading>{t('reset-password.heading')}</Heading>

              <LoginInput
                value={email}
                onChange={e => setEmail(e)}
                placeholder={t('login.input-email')}
              />

              <SelectInput
                onChange={setUserType}
                value={userType}
                options={USER_TYPES_ENTERPRISE(t)}
              />

              <ButtonsWrapper>
                <Buttons>
                  <ButtonPrimary
                    disabled={!email}
                    onClick={onResetPassword}
                    loading={loading}
                    smSize='125%'
                    text={t('contracts.confirm')}
                  />
                </Buttons>

                <Link
                  onClick={onRedirectToRegister}
                  text={t('login.link-create-account')}
                />
              </ButtonsWrapper>
            </FormWrapper>
          )}
      </LoginForm>

      <AlertSnackbar {...alertSnackbar} />
    </React.Fragment>
  )
}

RequestResetPasswordContainer.propTypes = {
  onRecovery: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  onResetPassword: PropTypes.func.isRequired,
  address: PropTypes.string.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
}

RequestResetPasswordContainer.defaultProps = {
  error: null
}
