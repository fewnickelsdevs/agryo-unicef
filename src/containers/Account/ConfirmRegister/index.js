import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { not } from 'ramda'

import { LoginForm, AgryoOverlayLogo, FormWrapper } from '../styles'
import agryoOverlayIcon from '../../../assets/logo-overlay.png'
import { TextInput } from '../../../components/Form'
import { ButtonPrimary, Link } from '../../../components/Buttons'
import SuccessDialog from '../../../components/Dialogs/Success'
import ErrorDialog from '../../../components/Dialogs/Error'
import Loading from '../../Loading'
const Title = styled.h2`
  margin-bottom: 2rem;
`

const StyledTextInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 80%;
      margin-bottom: 0.4rem;

      ${theme.breakpoints.xs} {
        width: 100%;
      }
    }

  `}
`

const Buttons = styled.div`
  ${({ theme }) => `
    width: 80%;
    display: flex;
    margin-top: 2rem;
    align-items: center;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      width: 92%;
    }
  `}
`

const ConfirmRegisterContainer = ({
  t,
  onSuccess,
  error,
  loading,
  success
}) => {
  const [alertSnackbar, setAlertSnackbar] = useState({
    message: '',
    open: false
  })

  useEffect(() => {
    setAlertSnackbar({
      message: 'Invalid Confirmation Token',
      open: Boolean(error)
    })
  }, [error])

  if (loading) {
    return <Loading />
  }

  return (
    <React.Fragment>
      <ErrorDialog
        visible={error}
        title={'Account Confirmation'}
        message={'Invalid Token'}
        buttonMessage={'Go To Login'}
        onClick={onSuccess}
      />
      <SuccessDialog
        visible={success}
        title={'Account Confirmation'}
        message={'Your new account has been successfully verified'}
        buttonMessage={'Go To Login'}
        onClick={onSuccess}
      />
    </React.Fragment>
  )
}

ConfirmRegisterContainer.propTypes = {
  t: PropTypes.func.isRequired,
  user: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.string
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onGoBack: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  error: PropTypes.string
}

ConfirmRegisterContainer.defaultProps = {
  error: ''
}

export default ConfirmRegisterContainer
