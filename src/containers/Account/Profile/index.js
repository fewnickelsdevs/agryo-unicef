import React from 'react'
import styled from 'styled-components'

import { ProfileForm } from '../styles'

import { TextInput } from '../../../components/Form'

const Heading = styled.h2`
  margin-bottom: 10px;
  color: #333;
`

const ButtonChange = styled.div`
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  padding: 16px;
  width: 180px;
  height: 40px;
  border-radius: 5px;
  background-color: #2f8c4e;
  margin-top: 20px;
`
const ButtonText = styled.p`
  color: #fff;
`

const LoginInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 100%;
    }

    ${theme.breakpoints.xs} {
      width: 100%;
      & > * {
        width: 100%;
      }
    }
  `}
`
const Title = styled.h2`
  font-weight: 400;
  margin-top: 20px;
  font-size: 16px;
  color: #333333;
  margin-bottom: 14px;
`
const MessageValidation = styled.p`
  margin-top: 3px;
  font-size: 12px;
  color: #ff0000;
  margin-bottom: 3px;
`

export default function ProfileContainer({
  t,
  currentPassword,
  setCurrentPassword,
  newPassword,
  setNewPassword,
  confirmPassword,
  setConfirmPassword,
  OnChangePassword
}) {
  const showMessageValidation = false
  return (
    <ProfileForm>
      <Heading>{t('account-password.title')}</Heading>
      <Title>{t('account-password.input-current-password')}</Title>
      <LoginInput
        value={currentPassword}
        onChange={e => setCurrentPassword(e)}
        placeholder={t('account-password.input-current-password')}
      />
      { showMessageValidation && <MessageValidation>Entre com a senha atual</MessageValidation>}
      <Title>{t('account-password.input-new-password')}</Title>
      <LoginInput
        value={newPassword}
        onChange={e => setNewPassword(e)}
        placeholder={t('account-password.input-new-password')}
      />
      { showMessageValidation && <MessageValidation>Entre com a nova senha</MessageValidation>}
      <Title>{t('account-password.input-confim-password')}</Title>
      <LoginInput
        value={confirmPassword}
        onChange={e => setConfirmPassword(e)}
        placeholder={t('account-password.input-confim-password')}
      />
      {showMessageValidation && <MessageValidation>Entre com a confirmação da senha</MessageValidation>}
      <ButtonChange onClick={OnChangePassword}>
        <ButtonText>{t('account-password.button')}</ButtonText>
      </ButtonChange>
    </ProfileForm>
  )
}
