import React from 'react'
import styled from 'styled-components'
import { EditProfileForm } from '../styles'
import { TextInput } from '../../../components/Form'
import { getUserType } from '../../../utils/user-type'

const Heading = styled.h2`
  color: #333;
`

const ButtonChange = styled.div`
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  padding: 16px;
  width: 180px;
  height: 40px;
  border-radius: 5px;
  background-color: #2f8c4e;
  margin-top: 10px;
`
const ButtonText = styled.p`
  color: #fff;
`

const LoginInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 100%;
    }

    ${theme.breakpoints.xs} {
      width: 100%;
      & > * {
        width: 100%;
      }
    }
  `}
`
const Title = styled.h2`
  font-weight: 400;
  margin-top: 20px;
  font-size: 16px;
  color: #333333;
  margin-bottom: 4px;
`
const UploadImage = styled.input.attrs(props => ({
  type: 'file',
  size: props.small ? 5 : undefined
}))`
  border-radius: 3px;
  display: block;
  margin: 0 0 1em;
  padding: ${props => props.padding};
`
const PhotoContainer = styled.div`
  display: flex;
  justify-content: space-between;
`
const PhotoCard = styled.div`
  display: flex;
  justify-content: space-between;
`
const UploadCard = styled.div`
  display: flex;
  flex-direction: column;
  background-color: color;
`
const MessageValidation = styled.p`
  margin-top: 3px;
  font-size: 12px;
  color: #ff0000;
  margin-bottom: 3px;
`

export default function EditProfileContainer({
  t,
  name,
  setName,
  email,
  setEmail,
  role,
  setRole,
  image,
  setImage,
  OnChangeProfile
}) {

  const userTypeValueString = getUserType()
  const showMessageValidation = false
  return (
    <EditProfileForm>
      <Heading>{t('account-profile.title')}</Heading>
      <Title>{t('account-profile.input-name')}</Title>
      <LoginInput
        value={name}
        onChange={e => setName(e)}
        placeholder={t('account-profile.input-name')}
      />
      {showMessageValidation && <MessageValidation>Este campo é necessário</MessageValidation>}
      <Title>{t('account-profile.input-email')}</Title>
      <LoginInput
        value={email}
        onChange={e => setEmail(e)}
        placeholder={t('account-profile.input-email')}
      />
      <Title>{t('account-profile.input-role')}</Title>
      <LoginInput
        readOnly={true}
        value={userTypeValueString}
      />
      {showMessageValidation && <MessageValidation>Este campo é necessário</MessageValidation>}
      <Heading>{t('account-profile.sub-title-photo')}</Heading>
      <PhotoContainer>
        <PhotoCard>
          <Title>{t('account-profile.no-photo-message')}</Title>
        </PhotoCard>
        <UploadCard>
          <Title>{t('account-profile.upload-title')}</Title>
          <UploadImage />
        </UploadCard>
      </PhotoContainer>
      <ButtonChange onClick={OnChangeProfile}>
        <ButtonText>{t('account-profile.button')}</ButtonText>
      </ButtonChange>
    </EditProfileForm>
  )
}
