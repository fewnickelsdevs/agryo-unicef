import styled from 'styled-components'

const backgroundImage = require('../../assets/account-bg-opt1.jpg')

export const LoginForm = styled.div`
  width: 45rem;
  height: 25rem;
  background: #fff;
  border-radius: 5px;

  display: grid;
  grid-template-columns: 40% 1fr;
  grid-template-rows: 1fr;

  @media only screen and (max-width: 425px) {
    width: 90%;
    display: block;
    height: min-content;
  }
`
export const ProfileForm = styled.div`
  background: #fff;
  border-radius: 8px;
  justify-content: center;
  width: 90%;
  height: 100vh;
  padding: 30px 25px 30px 25px;
  margin: 40px;
`
export const EditProfileForm = styled.div`
  background: #fff;
  border-radius: 8px;
  justify-content: center;
  width: 90%;
  height: 100vh;
  padding: 30px 25px 30px 25px;
  margin: 40px;
`

export const RegisterAgroTechnicalForm = styled.div`
  width: 45rem;
  background: #fff;
  border-radius: 5px;

  display: grid;
  grid-template-columns: 40% 1fr;
  grid-template-rows: 1fr;

  @media only screen and (max-width: 425px) {
    width: 90%;
    display: block;
    height: min-content;
  }
`

export const AgryoOverlayLogo = styled.img.attrs({
  alt: 'Agryo logo with transparency'
})`
  height: 100%;

  @media only screen and (max-width: 425px) {
    display: none;
  }
`

export const FormWrapper = styled.form.attrs({
  autoComplete: 'on'
})`
  padding: 2.5rem 0 0;

  @media only screen and (max-width: 425px) {
    display: flex;
    padding: 20px;
    align-items: center;
    flex-direction: column;
  }
`

export const ContainerBackground = styled.div`
  width: 100%;
  height: 100%;
  background-size: cover;
  background-position: center;
  background-image: linear-gradient(
      to bottom,
      rgba(31, 91, 51, 0.8),
      rgba(73, 193, 110, 0.7)
    ),
    url(${backgroundImage});

  display: flex;
  position: absolute;
  align-items: center;
  flex-direction: column;
`

export const AgryoLogo = styled.img.attrs({
  alt: 'Agryo logo'
})`
  margin: 45px;
`
