import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { not } from 'ramda'

import { LoginForm, AgryoOverlayLogo, FormWrapper } from '../styles'
import agryoOverlayIcon from '../../../assets/logo-overlay.png'
import { TextInput } from '../../../components/Form'
import { ButtonPrimary, Link } from '../../../components/Buttons'
import SuccessDialog from '../../../components/Dialogs/Success'
import AlertSnackbar from '../../../components/AlertSnackbar'
import SelectInput from '../../../components/Form/SelectInput';
import Web3 from 'web3'
import { P2P_USER_TYPES } from '../../../utils/user-type';

const Title = styled.h2`
  margin-bottom: 2rem;
`

const StyledTextInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 80%;
      margin-bottom: 0.4rem;

      ${theme.breakpoints.xs} {
        width: 100%;
      }
    }

  `}
`

const Buttons = styled.div`
  ${({ theme }) => `
    width: 80%;
    display: flex;
    margin-top: 2rem;
    align-items: center;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      width: 92%;
    }
  `}
`

const RegisterUserContainer = ({
  t,
  title,
  user,
  onChange,
  onSubmit,
  onSuccess,
  onGoBack,
  error,
  loading,
  success,
  invalidPass
}) => {
  const [alertSnackbar, setAlertSnackbar] = useState({
    message: '',
    open: false
  })
  const [userWeb3Address, setUserWeb3Address] = useState("")
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [web3Instance, setWeb3Instance] = useState(false)
  const [userType, setUserType] = useState(6)


const getWeb3Account = async() => {
    if (!window.ethereum && !window.web3) {
      alert("No Ethereum Wallet")
    } else if (window.ethereum) {
      try {
        let web3 = new Web3(window.ethereum)
          console.log("aqui")
        await window.ethereum.enable()
        console.log("foi")
        const network = await web3.eth.getChainId()
        console.log("Web3 Network: ", network)
        const accounts = await web3.eth.getAccounts()
        console.log("eth account", accounts)
        if(accounts[0].length > 5){
            setUserWeb3Address(accounts[0])
            setWeb3Instance(web3)
        }
      } catch (err) {
        console.log(err)
      }
    } else if (window.web3) {
        let web3 = new Web3(window.web3.currentProvider)
        const network = await web3.eth.getChainId()
        console.log("Web3 Network: ", network)
        const accounts = await web3.eth.getAccounts()
        console.log("eth account", accounts)
        if(accounts[0].length > 5){
            setUserWeb3Address(accounts[0])
            setWeb3Instance(web3)
        }
    } else {
        alert("Not Authorized")
    }
}

  useEffect(() => {
    setAlertSnackbar({
      message: error,
      open: Boolean(error)
    })
  }, [error])

  return (
    <React.Fragment>
      <LoginForm>
        <AgryoOverlayLogo src={agryoOverlayIcon} alt='Agryo' />

        <FormWrapper>
          <Title>{title}</Title>

          <StyledTextInput
            value={name}
            placeholder={t('signup.inputs.name')}
            name='name'
            type='text'
            onChange={setName}
          />
          <StyledTextInput
            value={email}
            type='email'
            name='email'
            placeholder={t('signup.inputs.email')}
            onChange={setEmail}
          />
          <StyledTextInput
            value={userWeb3Address}
            placeholder={"Ethereum Address"}
          />
{/* 
            <SelectInput
                onChange={setUserType}
                value={userType}
                options={P2P_USER_TYPES(t)}
            /> */}


          <Buttons>
            <Link text={"Connect Ethereum Wallet"} onClick={getWeb3Account} />

            <ButtonPrimary
              onClick={() => onSubmit(name, email, userWeb3Address, web3Instance, userType)}
              type='submit'
              text={t('signup.buttons.submit')}
              loading={loading}
              disabled={
                not(name) ||
                not(email) ||
                not(userWeb3Address) ||
                loading
              }
            />
          </Buttons>
          <div style={{ fontSize: 10 }}>
            <span title={window.GIT_SHA1}>v:{window.APP_VERSION}</span>
          </div>
        </FormWrapper>
      </LoginForm>
      <AlertSnackbar {...alertSnackbar} />
      <SuccessDialog
        visible={success}
        title={t('signup.success.title')}
        message={t('signup.success.message')}
        buttonMessage={t('signup.success.button')}
        onClick={onSuccess}
      />
    </React.Fragment>
  )
}

RegisterUserContainer.propTypes = {
  t: PropTypes.func.isRequired,
  user: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.string
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onGoBack: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  error: PropTypes.string
}

RegisterUserContainer.defaultProps = {
  error: ''
}

export default RegisterUserContainer
