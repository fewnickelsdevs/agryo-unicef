import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { not } from 'ramda'

import { LoginForm, AgryoOverlayLogo, FormWrapper } from '../styles'
import agryoOverlayIcon from '../../../assets/logo-overlay.png'
import { TextInput } from '../../../components/Form'
import { ButtonPrimary, Link } from '../../../components/Buttons'
import SuccessDialog from '../../../components/Dialogs/Success'
import AlertSnackbar from '../../../components/AlertSnackbar'

const Title = styled.h2`
  margin-bottom: 2rem;
`

const StyledTextInput = styled(TextInput)`
  ${({ theme }) => `
    & > * {
      width: 80%;
      margin-bottom: 0.4rem;

      ${theme.breakpoints.xs} {
        width: 100%;
      }
    }

  `}
`

const Buttons = styled.div`
  ${({ theme }) => `
    width: 80%;
    display: flex;
    margin-top: 2rem;
    align-items: center;
    justify-content: space-between;

    ${theme.breakpoints.xs} {
      width: 92%;
    }
  `}
`

const RegisterContainer = ({
  t,
  user,
  onChange,
  onSubmit,
  onSuccess,
  onGoBack,
  error,
  loading,
  success,
  invalidPass
}) => {
  const [alertSnackbar, setAlertSnackbar] = useState({
    message: '',
    open: false
  })

  useEffect(() => {
    setAlertSnackbar({
      message: error,
      open: Boolean(error)
    })
  }, [error])

  return (
    <React.Fragment>
      <LoginForm>
        <AgryoOverlayLogo src={agryoOverlayIcon} alt='Agryo' />

        <FormWrapper onSubmit={e => onSubmit(e)}>
          <Title>{t('signup.title')}</Title>

          <StyledTextInput
            value={user.name}
            placeholder={t('signup.inputs.name')}
            name='name'
            type='text'
            onChange={onChange('name')}
          />
          <StyledTextInput
            value={user.email}
            type='email'
            name='email'
            placeholder={t('signup.inputs.email')}
            onChange={onChange('email')}
          />
          <StyledTextInput
            value={user.password}
            placeholder={t('login.input-password')}
            onChange={onChange('password')}
            type='password'
          />
          <p style={{ fontSize: 12, color: invalidPass ? 'red' : 'grey', width: '90%' }}>
            {t('signup.password-message')}
          </p>

          <Buttons>
            <Link text={t('signup.buttons.back')} onClick={onGoBack} />

            <ButtonPrimary
              onClick={() => { }}
              type='submit'
              text={t('signup.buttons.submit')}
              loading={loading}
              disabled={
                not(user.name) ||
                not(user.email) ||
                not(user.password) ||
                loading
              }
            />
          </Buttons>
          <div style={{ fontSize: 10 }}>
            <span title={window.GIT_SHA1}>v:{window.APP_VERSION}</span>
          </div>
        </FormWrapper>
      </LoginForm>
      <AlertSnackbar {...alertSnackbar} />
      <SuccessDialog
        visible={success}
        title={t('signup.success.title')}
        message={t('signup.success.message')}
        buttonMessage={t('signup.success.button')}
        onClick={onSuccess}
      />
    </React.Fragment>
  )
}

RegisterContainer.propTypes = {
  t: PropTypes.func.isRequired,
  user: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    address: PropTypes.string
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onGoBack: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  error: PropTypes.string
}

RegisterContainer.defaultProps = {
  error: ''
}

export default RegisterContainer
