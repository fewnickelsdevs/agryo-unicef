import React from 'react'
import styled from 'styled-components'
import { not, isNil } from 'ramda'
import PropTypes from 'prop-types'

import { FormContainer, Form, Description } from '../styled'
import { Slider, TextInput } from '../../../components/Form'

const SliderContainer = styled.div`
  ${({ theme }) => `
    margin: 2rem 0 4rem;
    width: 60%;

    ${theme.breakpoints.xs} {
      width: 100%;
    }
  `}
`

const AreaInformationRenderer = ({
  t,
  isGreenBond,
  technologyLevel,
  numberOfAnimals,
  currentLivestock,
  onChangeTechnologyLevel,
  onChangeNumberOfAnimals
}) => {
  return (
    <FormContainer visible={not(isGreenBond)}>
      <Form>
        <h2>{t('contracts.form.area-information')}</h2>
        <Description>{t('contracts.form.choose-tech-level')}:</Description>

        <SliderContainer>
          <Slider
            step={1}
            min={1}
            max={3}
            onChange={onChangeTechnologyLevel}
            value={technologyLevel}
          />
        </SliderContainer>

        {!isNil(currentLivestock) && (
          <React.Fragment>
            <Description>
              Quantos animais deste tipo vivem na sua propriedade?
            </Description>

            <TextInput
              type='number'
              min={1}
              max={currentLivestock.max}
              onChange={value =>
                onChangeNumberOfAnimals({ numberOfAnimals: value })
              }
              value={numberOfAnimals}
            />
          </React.Fragment>
        )}
      </Form>
    </FormContainer>
  )
}

AreaInformationRenderer.propTypes = {
  t: PropTypes.func.isRequired,
  isGreenBond: PropTypes.bool.isRequired,
  technologyLevel: PropTypes.number.isRequired,
  numberOfAnimals: PropTypes.number.isRequired,
  currentLivestock: PropTypes.shape({
    max: PropTypes.number
  }).isRequired,
  onChangeTechnologyLevel: PropTypes.func.isRequired,
  onChangeNumberOfAnimals: PropTypes.func.isRequired
}

export default AreaInformationRenderer
