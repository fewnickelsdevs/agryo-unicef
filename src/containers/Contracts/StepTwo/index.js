import React, { useEffect, useCallback, useState } from 'react'
import PropTypes from 'prop-types'
import { isNil, not, or } from 'ramda'
import currency from 'currency.js'
import { addDays, addMonths, addYears, toDate } from 'date-fns'

import { livestocks } from '../../../mocks/livestocks.json'
import { crops } from '../../../mocks/crops.json'
import AreaInformationRenderer from './AreaInformation'
import FinanceRenderer from './Finance'
import { CURRENCY_CALCULATE_AMOUNT, USD_EUR, USD_BRL } from '../../../mocks/currency.js';

export default function StepTwo({ t, contract, onUpdate }) {
  const [loanValues, setLoanValues] = useState([])
  const currentLivestock = livestocks.find(l => l.id === contract.product)

  const addPeriodToContractStartDate = useCallback(
    (timeToAdd, unit) => {
      const units = {
        days: addDays,
        months: addMonths,
        years: addYears
      }
      const contractStarts = contract.contractStarts
      const endDatePlusMonths = units[unit](contractStarts, timeToAdd)

      return toDate(endDatePlusMonths)
    },
    [contract.contractStarts]
  )

  const updateSliderValue = property => value => {
    const propertyValue = contract[property]

    if (value === undefined) return
    if (propertyValue === value) return

    onUpdate({ [property]: value })
  }

  useEffect(() => {
    const isGreenBond = contract.isGreenBond
    const isCropsContract = isNil(currentLivestock)

    if (isGreenBond) {
      const endDate = addPeriodToContractStartDate(10, 'years')
      onUpdate({ contractEnds: endDate })
    }

    if (not(isCropsContract)) {
      const endDate = addPeriodToContractStartDate(
        currentLivestock.months,
        'months'
      )
      onUpdate({ contractEnds: endDate })
    }
  }, [
    contract.isGreenBond,
    currentLivestock,
    addPeriodToContractStartDate,
    onUpdate
  ])

  useEffect(() => {
    if (or(not(contract.property), contract.isGreenBond)) {
      return
    }

    const propertyHectares = contract.property.properties.f4
    const levels = {
      1: 'levelOne',
      2: 'levelTwo',
      3: 'levelThree'
    }

    const currentProduct = [...crops, ...livestocks].find(
      product => product.id === contract.product
    )

    const currentProductLevelValue =
      currentProduct[levels[contract.technologyLevel]]

    const levelValueByHectare = currency(currentProductLevelValue).multiply(
      propertyHectares
    ).intValue

    const levelValueByHectarePercentage = currency(levelValueByHectare).divide(
      10
    )

    const formatCurrencyLevelValueByHectares = CURRENCY_CALCULATE_AMOUNT(levelValueByHectare, contract.currency, USD_EUR, USD_BRL)

    const higher = currency(formatCurrencyLevelValueByHectares)
      .add(levelValueByHectarePercentage)
      .divide(100).value

    const lower = currency(formatCurrencyLevelValueByHectares)
      .subtract(levelValueByHectarePercentage)
      .divide(100).value
      
    const currentValue = currency(formatCurrencyLevelValueByHectares).divide(100).value

    setLoanValues([lower, currentValue, higher])
    onUpdate({ loanValue: currentValue })
  }, [
    onUpdate,
    contract.technologyLevel,
    contract.product,
    contract.property,
    contract.isGreenBond,
    contract.currency
  ])

  return (
    <React.Fragment>
      <AreaInformationRenderer
        t={t}
        isGreenBond={contract.isGreenBond}
        technologyLevel={contract.technologyLevel}
        numberOfAnimals={contract.numberOfAnimals}
        onChangeNumberOfAnimals={onUpdate}
        onChangeTechnologyLevel={updateSliderValue('technologyLevel')}
        currentLivestock={currentLivestock}
      />

      <FinanceRenderer
        t={t}
        contract={contract}
        onContractChange={onUpdate}
        currentLivestock={currentLivestock}
        loanValueOptions={loanValues}
        onChangeMinimumContractValue={updateSliderValue('minimumContractValue')}
      />
    </React.Fragment>
  )
}

StepTwo.propTypes = {
  t: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  contract: PropTypes.shape({
    product: PropTypes.number,
    property: PropTypes.object,
    technologyLevel: PropTypes.number,
    contractStarts: PropTypes.instanceOf(Date),
    contractEnds: PropTypes.instanceOf(Date),
    interest: PropTypes.number,
    numberOfAnimals: PropTypes.string,
    loanValue: PropTypes.number,
    minimumContractValue: PropTypes.number,
    error: PropTypes.bool,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired
}
