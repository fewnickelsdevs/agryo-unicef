import React from 'react'
import styled from 'styled-components'
import { equals, not, or, isNil } from 'ramda'
import currency from 'currency.js'
import { addDays, addYears, addMonths, toDate } from 'date-fns'
import PropTypes from 'prop-types'

import { FormContainer, Form, Description } from '../styled'
import { Datepicker, Slider } from '../../../components/Form'
import Help from '../../../components/Help'
import { CURRENCY_TYPES, CURRENCY_SYMBOL } from '../../../mocks/currency';

const InputLabel = styled.label`
  ${({ theme }) => `
    margin-right: 0.5rem;

    &:last-of-type {
      margin-left: 1rem;
    }

    ${theme.breakpoints.xs} {
      display: block;

      &:last-of-type {
        margin-top: 1rem;
        margin-left: 0;
      }
    }
  `}
`

const TotalLoanValueContainer = styled.div`
  ${({ visible }) => `
    margin: 4rem 0;
    display: ${equals(visible, false) ? 'none' : 'auto'}
  `}
`

const HorizontalButtonsContainer = styled.div`
  display: flex;
  align-items: center;
`

const LoanValue = styled.button`
  width: 5.2rem;
  height: 5.5rem;
  cursor: pointer;
  padding: 1.4rem 0;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;

  &:not(:last-of-type) {
    margin-right: 1rem;
  }
`

const SliderContainer = styled.div`
  ${({ theme }) => `
    margin: 2rem 0 4rem;
    width: 60%;

    ${theme.breakpoints.xs} {
      width: 100%;
    }
  `}
`

const FinanceRenderer = ({
  t,
  contract,
  onContractChange,
  currentLivestock,
  loanValueOptions,
  onChangeMinimumContractValue
}) => {
  const addPeriodToContractStartDate = (time, unit) => {
    const units = {
      months: addMonths,
      years: addYears,
      days: addDays
    }
    const contractStarts = contract.contractStarts
    const endDatePlusUnit = units[unit](contractStarts, time)

    return toDate(endDatePlusUnit)
  }

  const getContractEndDate = () => {
    const { isGreenBond } = contract
    const isCropsContract = isNil(currentLivestock)

    if (isGreenBond) {
      return addPeriodToContractStartDate(10, 'years')
    }

    if (isCropsContract) {
      return contract.contractEnds
    }

    return addPeriodToContractStartDate(currentLivestock.months, 'months')
  }

  const changeGreenBondContractStartDate = date => {
    const endDate = addYears(date, 10)

    return onContractChange({
      contractStarts: date,
      contractEnds: toDate(endDate)
    })
  }

  const changeCropsContractStartDate = value =>
    onContractChange({ contractStarts: value })

  const changeLivestockStartDate = value => {
    const { contractEnds } = contract
    const endDate = addMonths(contractEnds, currentLivestock.months)

    onContractChange({ contractStarts: value, contractEnds: toDate(endDate) })
  }

  const handleStartDateChange = value => {
    const { isGreenBond } = contract
    const isCropsContract = isNil(currentLivestock)

    if (isGreenBond){
      return changeGreenBondContractStartDate(value)
    }
    if (isCropsContract) return changeCropsContractStartDate(value)

    changeLivestockStartDate(value)
  }

  return (
    <FormContainer>
      <Form>
        <h2>{t('contracts.form.finance')}</h2>
        <Description>{t('contracts.form.contract-duration')}</Description>

        <InputLabel htmlFor='from'>{t('contracts.form.from')}:</InputLabel>
        <Datepicker
          id='from'
          onChange={handleStartDateChange}
          selected={contract.contractStarts}
        />

        <InputLabel htmlFor='to'>{t('contracts.form.to')}:</InputLabel>
        <Datepicker
          id='to'
          disabled={or(!!currentLivestock, contract.isGreenBond)}
          onChange={value => onContractChange({ contractEnds: value })}
          selected={getContractEndDate()}
        />

        <TotalLoanValueContainer>
          <Description>{t('contracts.form.total-loan-value')}</Description>

          <HorizontalButtonsContainer>
            {loanValueOptions.map(value => (
              <LoanValue
                active={contract.loanValue === value}
                onClick={() => onContractChange({ loanValue: value })}
              >
                {CURRENCY_SYMBOL[contract.currency]} <b>{currency(value).format()}</b>
              </LoanValue>
            ))}
          </HorizontalButtonsContainer>
        </TotalLoanValueContainer>
      </Form>
    </FormContainer>
  )
}

FinanceRenderer.propTypes = {
  contract: PropTypes.shape({
    contractStarts: PropTypes.instanceOf(Date),
    contractEnds: PropTypes.instanceOf(Date),
    interest: PropTypes.number,
    loanValue: PropTypes.number,
    minimumContractValue: PropTypes.number
  }).isRequired,
  t: PropTypes.func.isRequired,
  onContractChange: PropTypes.func.isRequired,
  currentLivestock: PropTypes.shape({
    months: PropTypes.number
  }),
  loanValueOptions: PropTypes.arrayOf(PropTypes.number),
  onChangeMinimumContractValue: PropTypes.func.isRequired
}

export default FinanceRenderer
