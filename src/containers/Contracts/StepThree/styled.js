import styled from 'styled-components'

export const SplitPaneTitle = styled.h2`
  color: ${({ dark }) => (dark ? '#fff' : '#000')};
  font-weight: 600;
  line-height: 1.6;
`

export const ListHeading = styled.h3`
  display: flex;
  align-items: center;
  font-size: 1.2rem;
  margin: 2rem 0 1.5rem;
  color: ${({ dark }) => (dark ? '#fff' : '#000')};

  & > * {
    font-size: 1.6rem;
    margin-right: 1rem !important;
  }
`

export const List = styled.ul`
  list-style: none;
  color: #333;
  line-height: 2;
`
