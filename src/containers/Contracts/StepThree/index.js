import React, { useState, useEffect, useCallback } from 'react'
import PropTypes from 'prop-types'
import { find, propEq, compose, prop } from 'ramda'
import styled from 'styled-components'

import { crops } from '../../../mocks/crops.json'
import { livestocks } from '../../../mocks/livestocks.json'
import LeftPaneRenderer from './LeftPane'
import RightPaneRenderer from './RightPane'
import { FormContainer } from '../styled'

const FormSplitPane = styled.section`
  ${({ theme }) => `
    display: flex;
    align-items: flex-start;
    border-radius: 6px;
    margin-top: 2rem;
    border: 1px solid #ccc;
    overflow: hidden;
    background: #3F8C48;

    ${theme.breakpoints.xs} {
      flex-direction: column;
    }
  `}
`

export default function StepThree({ t, properties, contract }) {
  const [currentProperty, setCurrentProperty] = useState()
  const [productName, setProductName] = useState()

  const getChosenProductName = useCallback(() => {
    if (contract.isGreenBond) return t('contracts.form.green-bond')

    const isLivestockContract = contract.product >= 12
    const currentContract = isLivestockContract ? 'livestocks' : 'crops'
    const contractTypes = { crops, livestocks }
    const currentContractType = contractTypes[currentContract]
    const product = currentContractType.find(c => c.id === contract.product)
    const productName = t(product.name)

    return productName
  }, [contract.isGreenBond, contract.product, t])

  useEffect(() => {
    const productName = getChosenProductName()

    setProductName(productName)
  }, [contract.product, t, getChosenProductName])

  useEffect(() => {
    if (!contract.property) return

    const currentProperty = find(
      compose(propEq('f1', contract.property.properties.f1), prop('properties'))
    )(properties.data)

    setCurrentProperty(currentProperty)
  }, [properties.data, contract.property])

  return (
    <FormContainer>
      <FormSplitPane>
        <LeftPaneRenderer
          t={t}
          contract={contract}
          currentProperty={currentProperty}
          productName={productName}
        />

        <RightPaneRenderer
          t={t}
          startDate={contract.contractStarts}
          endDate={contract.contractEnds}
        />
      </FormSplitPane>
    </FormContainer>
  )
}

StepThree.propTypes = {
  t: PropTypes.func.isRequired,
  properties: PropTypes.shape({
    data: PropTypes.array
  }).isRequired,
  contract: PropTypes.object.isRequired
}
