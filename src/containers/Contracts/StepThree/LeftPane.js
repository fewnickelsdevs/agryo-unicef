import React from 'react'
import { not } from 'ramda'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { SplitPaneTitle, ListHeading, List } from './styled'
import { getPropertyLocation } from '../../../services/ceos_utils'

const FormSplitPaneLeft = styled.div`
  flex: 0.6;
  padding: 2rem;
  background: #fff;
`

const LeftPaneRenderer = ({ t, productName, currentProperty, contract }) => {
  const techLevels = {
    1: t('contracts.form.low-tech-level'),
    2: t('contracts.form.medium-tech-level'),
    3: t('contracts.form.high-tech-level')
  }

  const currencyFromPercentage = (value, percentage) => {
    const result = (value / 100) * percentage

    return currency(result)
  }

  const currency = num => num.toLocaleString()

  return (
    <FormSplitPaneLeft>
      <SplitPaneTitle>{t('contracts.form.check-contract-info')}</SplitPaneTitle>

      <ListHeading>
        <FontAwesomeIcon icon='clipboard-list' color='#3F8C48' />
        {t('contracts.costing.information')}
      </ListHeading>

      <List>
        <li>
          <b>{t('contracts.form.product-type')}: </b>
          {productName}
        </li>

        <li>
          <b>{t('contracts.form.tech-level')}: </b>
          {techLevels[contract.technologyLevel]}
        </li>

        <li>
          <b>{t('contracts.form.field-location')}:</b>
          {currentProperty && getPropertyLocation(currentProperty.properties)}
        </li>
      </List>

      <ListHeading>
        <FontAwesomeIcon icon='coins' color='#3F8C48' />
        {t('contracts.form.financial-info')}
      </ListHeading>

      <List>
        {not(contract.isGreenBond) && (
          <React.Fragment>
            <li>
              <b>{t('contracts.form.financed-value')}: </b>R$&nbsp;
              {currency(contract.loanValue)}
            </li>

            <li>
              <b>{t('contracts.form.minimum-contract')}: </b>R$&nbsp;
              {currencyFromPercentage(
                contract.loanValue,
                contract.minimumContractValue
              )}
            </li>
          </React.Fragment>
        )}

        <li>
          <b>{t('contracts.form.interest-contract')}: </b>
          {contract.interest}% {t('contracts.form.by-year')}
        </li>
      </List>
    </FormSplitPaneLeft>
  )
}

export default LeftPaneRenderer
