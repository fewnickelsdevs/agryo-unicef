import React from 'react'
import styled from 'styled-components'
import {} from 'ramda'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  format,
  differenceInCalendarDays,
  differenceInCalendarMonths
} from 'date-fns'

import { ListHeading, SplitPaneTitle, List } from './styled'

const FormSplitPaneRight = styled.div`
  flex: 0.4;
  height: 100%;
  padding: 0 2rem 2rem;
  background: #3f8c48;
`

const RightSplitPaneItem = styled.li`
  color: #fff;
  line-height: 1.8;
  margin: 1rem 0;
`

const RightPaneRenderer = ({ t, startDate, endDate }) => {
  const differenceBetweenDates = (date1, date2) => {
    const inMonths = differenceInCalendarMonths(date1, date2)

    if (inMonths === 0) {
      const inDays = differenceInCalendarDays(date1, date2)
      return `${inDays} ${t('contracts.form.days')}`
    }

    return `${inMonths} ${t('contracts.form.months')}`
  }

  return (
    <FormSplitPaneRight>
      <ListHeading dark>
        <FontAwesomeIcon icon='calendar' color='#fff' />
        {t('contracts.form.dates')}
      </ListHeading>

      <SplitPaneTitle dark>
        {t('contracts.form.dates-subtitle')}:
      </SplitPaneTitle>

      <List>
        <RightSplitPaneItem>
          {t('contracts.form.contract-starts')}: <br />
          <b>{format(startDate, 'dd/MM/yyyy')}</b>
        </RightSplitPaneItem>

        <RightSplitPaneItem>
          {t('contracts.form.contract-period')}: <br />
          <b>{differenceBetweenDates(endDate, startDate)}</b>
        </RightSplitPaneItem>

        <RightSplitPaneItem>
          {t('contracts.form.contract-ends')}: <br />
          <b>{format(endDate, 'dd/MM/yyyy')}</b>
        </RightSplitPaneItem>
      </List>
    </FormSplitPaneRight>
  )
}

export default RightPaneRenderer
