import styled from 'styled-components'
import { equals } from 'ramda'

export const FormContainer = styled.section(
  props => `
  ${equals(props.visible, false) && 'display: none'}
`
)

export const Form = styled.div`
  padding: 2rem;
  margin-top: 2rem;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ccc;
`

export const Description = styled.p`
  font-size: 1rem;
  margin: 2rem 0 1.25rem;
`
