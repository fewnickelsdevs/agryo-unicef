import React from 'react'
import { and, not, isEmpty, isNil, equals } from 'ramda'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { FormContainer, Form, Description } from '../styled'
import { ButtonPrimary } from '../../../components/Buttons'
import PropertyCard from '../../../components/PropertyCard'
import normalizePropertyCoords from '../../../formatters/normalizePropertyCoords'

const PropertyCards = styled.div`
  ${({ theme }) => `
    display: flex;
    align-items: center;

    height: auto;
    overflow-x: hidden;
    overflow-x: auto;

    ${theme.breakpoints.sm} {
      flex-direction: column;
    }
  `}
`

const getPropertyLocation = property => {
  return `${property.f7} - ${property.f5}`
}

const propertyStatusName = t => ({
  0: t('properties.status.processing'),
  1: t('properties.status.confirmed'),
  2: t('properties.status.invalid'),
  3: t('properties.status.error')
})

const PropertiesRenderer = ({
  t,
  property: currentProperty,
  properties,
  onChangeProperty,
  onCreateProperty
}) => {
  const renderPropertyCard = property => {
    const { geometry, properties } = property
    const identifier = properties.f1
    const hectares = properties.f4
    const coords = normalizePropertyCoords(geometry.coordinates)

    return (
      <PropertyCard
        t={t}
        onClick={() => onChangeProperty({ property })}
        key={identifier}
        active={
          isNil(currentProperty)
            ? false
            : equals(currentProperty.properties.f1, identifier)
        }
        status={propertyStatusName(t)[properties.f3]}
        label={getPropertyLocation(properties)}
        locale={coords}
        hectares={hectares}
        responsive
      />
    )
  }

  return (
    <FormContainer>
      <Form>
        <h2>{t('contracts.form.properties')}</h2>

        {not(isEmpty(properties.data)) && (
          <Description>{t('contracts.form.pick-property')}</Description>
        )}

        <PropertyCards>
          {not(properties.loading) && properties.data.map(renderPropertyCard)}

          {and(isEmpty(properties.data), not(properties.loading)) && (
            <ButtonPrimary
              margin='1rem 0 0 0'
              text={t('contracts.form.create-property')}
              onClick={onCreateProperty}
            />
          )}
        </PropertyCards>
      </Form>
    </FormContainer>
  )
}

PropertiesRenderer.propTypes = {
  t: PropTypes.func.isRequired,
  property: PropTypes.object,
  properties: PropTypes.shape({
    data: PropTypes.array,
    loading: PropTypes.bool
  }).isRequired,
  onChangeProperty: PropTypes.func.isRequired,
  onCreateProperty: PropTypes.func.isRequired
}

export default PropertiesRenderer
