import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { isEmpty, not, equals } from 'ramda'
import ActivitiesRenderer from './Activities'
import PropertiesRenderer from './Properties'

export default function StepOne({
  t,
  properties,
  contract,
  onUpdate,
  onCreateProperty
}) {

  const [activity, setActivity] = useState('crops')

  useEffect(() => {
    const hasProperties = not(isEmpty(properties.data))

    if (hasProperties) {
      const defaultProperty = properties.data[0]
      onUpdate({ property: defaultProperty })
    } else {
      onUpdate({ property: null })
    }
  }, [onUpdate, properties])

  useEffect(() => {
    const defaultProducts = {
      livestock: 12,
      crops: 1
    }

    onUpdate({
      product: defaultProducts[activity],
      interest: equals(activity, 'green-bond') ? 3.5 : 9,
      isGreenBond: equals(activity, 'green-bond')
    })
  }, [activity, onUpdate])

  return (
    <React.Fragment>
      <ActivitiesRenderer
        t={t}
        selectedProduct={contract.product}
        selectedActivity={activity}
        onChangeActivity={setActivity}
        onSelectProduct={onUpdate}
      />

      <PropertiesRenderer
        t={t}
        property={contract.property}
        properties={properties}
        onChangeProperty={onUpdate}
        onCreateProperty={onCreateProperty}
      />
    </React.Fragment>
  )
}

StepOne.propTypes = {
  t: PropTypes.func.isRequired,
  properties: PropTypes.shape({
    data: PropTypes.array,
    loading: PropTypes.bool
  })
}
