import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PropTypes from 'prop-types'

import { FormContainer, Form, Description } from '../styled'
import Row from '../../../components/Layout/Row'

import { crops } from '../../../mocks/crops.json'
import { livestocks } from '../../../mocks/livestocks.json'

const ActivityButton = styled.div`
  width: 5rem;
  height: 5.5rem;
  cursor: pointer;
  padding: 0.7rem 0;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;

  &:not(:last-of-type) {
    margin-right: 1rem;
  }

  & h4 {
    text-align: center;
    line-height: 100%;
  }
`

const ProductOption = styled.button`
  cursor: pointer;
  font-size: 1rem;
  font-weight: 600;
  border-radius: 6px;
  padding: 0.6rem 1rem;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  margin-right: 1rem;
  margin-bottom: 0.6rem;
`

const ProductsContainer = styled(Row)`
  width: 90%;
  flex-wrap: wrap;
`

export const ActivitiesRenderer = ({
  t,
  selectedProduct,
  selectedActivity,
  onChangeActivity,
  onSelectProduct
}) => {
  const activities = [
    {
      activity: 'livestock',
      label: t('contracts.form.animals'),
      icon: 'horse-head'
    },
    { activity: 'crops', label: t('contracts.form.grains'), icon: 'seedling' },
    {
      activity: 'green-bond',
      label: t('contracts.form.green-bond'),
      icon: 'globe'
    }
  ]

  const renderActivity = ({ activity, icon, label }) => (
    <ActivityButton
      key={activity}
      active={selectedActivity === activity}
      onClick={() => onChangeActivity(activity)}
    >
      <FontAwesomeIcon icon={icon} size='2x' />
      <h4>{label}</h4>
    </ActivityButton>
  )

  const renderProduct = ({ id, name }) => (
    <ProductOption
      key={id}
      active={selectedProduct === id}
      onClick={() => onSelectProduct({ product: id })}
    >
      {t(name)}
    </ProductOption>
  )

  const renderProductsByActivity = selectedActivity => {
    console.log(`selectedActivity: ${selectedActivity}`)
    if (selectedActivity === 'crops') {
      return crops.map(renderProduct)
    }

    if (selectedActivity === 'livestock') {
      return livestocks.map(renderProduct)
    }

    return <React.Fragment></React.Fragment>
  }

  const renderProductsHeading = () => {
    if (selectedActivity === `green-bond`) {
      return <React.Fragment></React.Fragment>
    }

    return (
      <React.Fragment>
        <h2>{t('contracts.form.product')}</h2>
        <Description>{t('contracts.form.pick-product')}</Description>
      </React.Fragment>
    )
  }

  return (
    <FormContainer>
      <Form>
        <h2>{t('contracts.form.activities')}</h2>
        <Description>{t('contracts.form.pick-activities')}</Description>

        <Row style={{ marginBottom: '3rem' }}>
          {activities.map(renderActivity)}
        </Row>

        {renderProductsHeading()}

        <ProductsContainer>
          {renderProductsByActivity(selectedActivity)}
        </ProductsContainer>
      </Form>
    </FormContainer>
  )
}

ActivitiesRenderer.propTypes = {
  t: PropTypes.func.isRequired,
  selectedProduct: PropTypes.number,
  selectedActivity: PropTypes.string.isRequired,
  onChangeActivity: PropTypes.func.isRequired,
  onSelectProduct: PropTypes.func.isRequired
}

export default ActivitiesRenderer
