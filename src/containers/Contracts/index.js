import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { not, isNil } from 'ramda'
import styled from 'styled-components'

import Container from '../../components/Layout/Container'
import { Title } from '../../components/Typography'
import { ButtonPrimary, ButtonSecondary } from '../../components/Buttons'
import StepOne from './StepOne'
import StepTwo from './StepTwo'
import StepThree from './StepThree'
import FormHeading from './FormHeading'
import SuccessDialog from '../../components/Dialogs/Success'
import ErrorDialog from '../../components/Dialogs/Error'
import LoadingContainer from '../Loading'
import LoadingDialog from '../../components/Dialogs/LoadingDialog'

const FooterWithButtons = styled.footer`
  width: 100%;
  display: flex;
  margin-top: 2rem;
  align-items: center;
  justify-content: center;
`

export default function ContractContainer({
  t,
  properties,
  contract,
  onUpdate,
  onSuccess,
  onError,
  onCreateContract,
  onCheckContractsList,
  pageHistory,
  showCheckContracts = true
}) {
  const [currentFormStep, setStep] = useState(1)
  const [isContinueButtonDisabled, setIsContinueButtonDisabled] = useState(true)

  const steps = {
    1: () => (
      <StepOne
        t={t}
        properties={properties}
        currentStep={currentFormStep}
        contract={contract}
        onUpdate={onUpdate}
        onCreateProperty={() => pageHistory.push('/account/properties')}
      />
    ),
    2: () => (
      <StepTwo
        t={t}
        currentStep={currentFormStep}
        contract={contract}
        onUpdate={onUpdate}
      />
    ),
    3: () => (
      <StepThree
        t={t}
        properties={properties}
        currentStep={currentFormStep}
        contract={contract}
      />
    )
  }

  const nextStep = () => {
    if (currentFormStep < 3) {
      setStep(currentFormStep + 1)
      return window.scrollTo(0, 0)
    }

    onCreateContract()
  }

  const handleCancelButtonClick = () => {
    currentFormStep > 1 ? setStep(currentFormStep - 1) : pageHistory.goBack()
  }

  useEffect(() => {
    const {
      product,
      property,
      technologyLevel,
      contractStarts,
      contractEnds,
      numberOfAnimals,
      loanValue,
      minimumContractValue,
      isGreenBond
    } = contract

    const stepsConditions = {
      1: not(isGreenBond) && (not(product) || isNil(property)),
      2:
        not(technologyLevel) ||
        isNil(contractStarts) ||
        isNil(contractEnds) ||
        not(numberOfAnimals) ||
        not(loanValue) ||
        not(minimumContractValue)
    }

    setIsContinueButtonDisabled(stepsConditions[currentFormStep])
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    currentFormStep,
    contract.product,
    contract.property,
    contract.isGreenBond
  ])

  if (properties.loading) {
    return <LoadingContainer />
  }

  return (
    <React.Fragment>
      <Container>
        <Title>{t('contracts.title')}</Title>
        {showCheckContracts && (
          <ButtonPrimary
            text='Check my contracts'
            onClick={() => pageHistory.push('/account/contracts')}
          />
        )}
        <FormHeading t={t} step={currentFormStep} />

        {steps[currentFormStep]()}

        <FooterWithButtons>
          <ButtonSecondary
            onClick={handleCancelButtonClick}
            text={
              currentFormStep === 1
                ? t('contracts.cancel')
                : t('contracts.back')
            }
          />
          <ButtonPrimary
            disabled={isContinueButtonDisabled}
            onClick={nextStep}
            margin='0 0 0 1rem'
            text={
              currentFormStep === 3
                ? t('contracts.confirm')
                : t('contracts.next')
            }
          />
        </FooterWithButtons>
      </Container>

      <SuccessDialog
        visible={contract.success}
        title={t('contracts.dialogs.success')}
        buttonMessage={t('contracts.dialogs.successButton')}
        onClick={onSuccess}
      />

      <ErrorDialog
        visible={!!contract.error}
        title={t('contracts.dialogs.error')}
        message={t('contracts.dialogs.errorMessage')}
        buttonMessage={t('contracts.dialogs.errorButton')}
        onClick={onError}
      />

      <LoadingDialog visible={contract.loading} />
    </React.Fragment>
  )
}

ContractContainer.propTypes = {
  t: PropTypes.func.isRequired,
  properties: PropTypes.shape({
    data: PropTypes.array,
    loading: PropTypes.bool
  }).isRequired,
  contract: PropTypes.shape({
    product: PropTypes.number,
    property: PropTypes.object,
    technologyLevel: PropTypes.number,
    contractStarts: PropTypes.instanceOf(Date),
    contractEnds: PropTypes.instanceOf(Date),
    interest: PropTypes.number,
    numberOfAnimals: PropTypes.string,
    loanValue: PropTypes.number,
    minimumContractValue: PropTypes.number,
    error: PropTypes.bool,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired,
  onUpdate: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onCreateContract: PropTypes.func.isRequired,
  pageHistory: PropTypes.shape({
    goBack: PropTypes.func
  }).isRequired
}
