import React from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PropTypes from 'prop-types'

import Row from '../../components/Layout/Row'

const Container = styled.header`
  ${({ theme }) => `
    display: flex;
    margin-top: 4rem;
    align-items: center;
    padding-bottom: 1.5rem;
    justify-content: space-between;
    border-bottom: 2px solid #ecddba;

    ${theme.breakpoints.xs} {
      margin-top: 2.75rem;
      flex-direction: column;
      align-items: flex-start;
    }
  `}
`

const Title = styled.h2`
  ${({ theme }) => `
    ${theme.breakpoints.xs} {
      font-size: 1.2rem;
    }
  `}
`

const StepsContainer = styled.div`
  ${({ theme }) => `
    display: flex;
    align-items: center;

    ${theme.breakpoints.xs} {
      width: 100%;
      margin-top: 1rem;
      justify-content: space-between;
    }
  `}
`

const Step = styled.div`
  ${({ theme, active }) => `
    color: #fff;
    width: 1.5rem;
    height: 1.5rem;
    border-radius: 50%;
    font-weight: bold;
    background: ${active ? '#3F8C48' : '#ccc'};
    margin: 0 0.25rem;

    display: flex;
    align-items: center;
    justify-content: center;

    ${theme.breakpoints.xs} {
      &:first-of-type {
        margin-left: auto;
      }
    }
  `}
`

const CircleIcon = styled(FontAwesomeIcon)`
  color: #fff;
  height: 2.25rem;
  padding: 0.6rem;
  background: #e6a91e;
  border-radius: 50%;
  margin-right: 1rem;
  width: 2.25rem !important;
`

const Heading4 = styled.h4`
  ${({ theme }) => `
    text-transform: uppercase;
    margin-right: 1rem;

    ${theme.breakpoints.sm} {
      margin-right: 0;
    }
  `}
`

export default function FormHeading({ t, step }) {
  return (
    <Container>
      <Row>
        <CircleIcon icon='calculator' />
        <Title>{t('contracts.costing.title')}</Title>
      </Row>

      <StepsContainer>
        <Heading4>{t('contracts.costing.information')}</Heading4>

        <Step active={step === 1}>1</Step>
        <Step active={step === 2}>2</Step>
        <Step active={step === 3}>3</Step>
      </StepsContainer>
    </Container>
  )
}

FormHeading.propTypes = {
  t: PropTypes.func.isRequired,
  step: PropTypes.number.isRequired
}
