import React, { useState } from 'react'
import styled from 'styled-components'
import Leaflet from 'leaflet'

import Container from '../../components/Layout/Container'
import { Title, Subtitle } from '../../components/Typography'
import { ButtonPrimary } from '../../components/Buttons'
import DrawableMap from './DevDrawableMap'
import BoxFarmerInfo from '../Farmer/FarmerInfo/BoxFarmerInfo'
import ActivitiesRenderer from '../Contracts/StepOne/Activities'
import UploadPolygon from './PolygonUpload';
import SuccessDialog from '../../components/Dialogs/Success';

//@TODO it should not be duplicated
const defaultProducts = {
  livestock: 12,
  crops: 1
}

const Heading = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const SaveButtonContainer = styled.div`
  ${({ theme }) => `
    ${theme.breakpoints.xs} {
      display: none
    }
  `}
`

const Content = styled.section`
  margin-top: 2.75rem;
`

const MobileButtonContainer = styled.div`
  ${({ theme }) => `
    display: none;
    
    & > * {
      width: 100%;
      margin-top: 1rem;
    }

    ${theme.breakpoints.xs} {
      display: block;
    }
  `}
`

//Select Property
const HorizontalButtonsContainer = styled.div`
  display: flex;
  align-items: center;
`

const TypePropertyOption = styled.button`
  width: 6.2rem;
  height: 3.5rem;
  cursor: pointer;
  padding: 1.4rem 0;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;

  &:not(:last-of-type) {
    margin-right: 1rem;
  }
`

const SendTypePropertyOption = styled.button`
  width: 8.2rem;
  height: 2.5rem;
  cursor: pointer;
  padding: 1rem 0;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#1f5b33')};
  background: ${({ active }) => (active ? '#1f5b33' : '#fff')};

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;

  &:not(:last-of-type) {
    margin-right: 1rem;
  }
`


const CreatePropertyWithPolygonUploadContainer = ({
  onCreateProperty,
  createPropertyLoading,
  t,
  farmerData = false,
  newPropertySuccess,
  closeSuccessDialog,
  onUploadPolygonFiles
}) => {

  const defaultProperty = {
    geoData: null,
    rawGeo: null,
    hectares: null
  }

  const [coords, setCoords] = useState([-15.7801, -47.9292])

  const [sendTypeProperty, setTypeSendProperty] = useState(1)
  const [uploadPolygonModal, setUploadPolygonModal] = useState(false)


  const [property, setProperty] = useState(defaultProperty)
  const [typeProperty, setTypeProperty] = useState(1)

  const [activity, setActivity] = useState(`livestock`)
  const [product, setProduct] = useState(defaultProducts[activity])

  const [modalCenterHelpMap, setModalCenterHelpMap] = useState(false)
  const [centerHelpMap, setCenterHelpMap] = useState(false)

  const handleMapPolygonsUpdate = polygon => {
    const area =
      Leaflet.GeometryUtil.geodesicArea(polygon.layer.getLatLngs()[0]) / 10000
    const hectares = parseInt(area, 10)
    const center = polygon.layer.getBounds().getCenter()
    const {
      latlngs: [[latlngs]]
    } = polygon.layer.editing

    const extra = latlngs.map(coord => [coord.lng, coord.lat])
    setCenterHelpMap(false)
    setProperty({
      geoData: extra,
      rawGeo: latlngs,
      hectares,
      centerHectares: center
    })
    console.log('Hectares: ', hectares)
  }

  const handleCenterMapHelper = coords => {
    setCenterHelpMap(coords)
    setCoords(coords)
    setModalCenterHelpMap(false)
  }

  const resetAfterSuccess = () => {
    setProperty(defaultProperty)
    closeSuccessDialog()
  }

  const goToUploadMode = () => {
    setTypeSendProperty(2)
    setProperty(defaultProperty)
  }

  const uploadPolygonFiles = (typeUpload, data) => {
    onUploadPolygonFiles(typeUpload, data, product)
  }

  return (
    <Container>
      <Heading>
        <div>
          <Title>{t('properties.create.title')}</Title>
          <Subtitle>{t('properties.create.message')}</Subtitle>
        </div>
      </Heading>
      {farmerData && (
        <Content>
          <BoxFarmerInfo farmerData={farmerData} />
        </Content>
      )}

      <ActivitiesRenderer
        t={t}
        selectedProduct={product}
        selectedActivity={activity}
        onChangeActivity={(newActivity) => {
          setProduct(defaultProducts[newActivity])
          setActivity(newActivity)
        }}
        onSelectProduct={({ product }) => setProduct(product)}
      />
      <Content>

        <HorizontalButtonsContainer>
              <SendTypePropertyOption
                active={sendTypeProperty === 1}
                onClick={() => setTypeSendProperty(1)}
              >
                Draw Property
              </SendTypePropertyOption>
              <SendTypePropertyOption
                active={sendTypeProperty === 2}
                onClick={() => goToUploadMode()}
              >
                Upload Property
              </SendTypePropertyOption>
      </HorizontalButtonsContainer>

      </Content>

      <Content>
      {(sendTypeProperty === 1) &&
        <DrawableMap
          t={t}
          shouldDraw
          coords={coords}
          setCoords={setCoords}
          onPolygonsUpdate={handleMapPolygonsUpdate}
          onDeletePolygons={() => setProperty(defaultProperty)}
          hectares={property.hectares}
          centerHectaresPopup={property.centerHectares}
          centerHelpMap={centerHelpMap}
          modalCenterHelpMap={modalCenterHelpMap}
          setCenterHelpMap={handleCenterMapHelper}
          setModalCenterHelpMap={setModalCenterHelpMap}
          onCreateProperty={() =>
            onCreateProperty({ property: property.geoData, typeProperty, product })
          }
          closeSuccessDialog={resetAfterSuccess}
          loadingCreateProperty={createPropertyLoading}
          typeProperty={typeProperty}
          setTypeProperty={setTypeProperty}
          newPropertySuccess={newPropertySuccess}
        />
      }

      {(sendTypeProperty === 2) &&
      <>
          <UploadPolygon
              visible={uploadPolygonModal}
              closeModal={setUploadPolygonModal}
              uploadPolygonFiles={uploadPolygonFiles}
              uploadPolygonFilesId={"upPolygonFileIdEnterprise"}
              uploadGeoJsonId={"upPolygonJsonIdEnterprise"}
              uploadKmlId={"upPolygonKmlIdEnterprise"}
              loading={createPropertyLoading}
          />
      

      <SuccessDialog
        visible={newPropertySuccess}
        title={t('properties.create.success.title')}
        message={t('properties.create.success.message')}
        buttonMessage={t('properties.create.success.button')}
        onClick={closeSuccessDialog}
      /> 

      </>
      }

      </Content>
      {/* 
      <MobileButtonContainer>
        <ButtonPrimary
          text={t('properties.create.submit')}
          disabled={!property.geoData}
          onClick={() => onCreateProperty(property)}
        />
      </MobileButtonContainer> */}
    </Container>
  )
}

export default CreatePropertyWithPolygonUploadContainer
