import React from 'react'
import PropTypes from 'prop-types'
import { ButtonPrimary, ButtonSecondary } from '../../components/Buttons'
import LoadingDialog from '../../components/Dialogs/LoadingDialog'
import { kml } from "@tmcw/togeojson";

export default function UploadPolygon({
  uploadPolygonFiles,
  uploadPolygonFilesId,
  uploadGeoJsonId,
  uploadKmlId,
  loading
}) {
  const [fileList, setFileList] = React.useState(false)
  const [shape, setShape] = React.useState(false)
  const [kml, setKml] = React.useState(false)
  const [typeUpload, setTypeUpload] = React.useState(false)
  const [geojson, setGeoJson] = React.useState(false)
  const openUploadGeoJson = () =>
    document.getElementById(uploadGeoJsonId).click()
  const openUploadPolygonFiles = () =>
    document.getElementById(uploadPolygonFilesId).click()

  const openUploadKml = () => 
    document.getElementById(uploadKmlId).click()

  const clearState = () => {
    setFileList(false)
    setShape(false)
    setGeoJson(false)
    setTypeUpload(false)
    setKml(false)
  }

  const readAsDataURL = data => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onerror = () => {
        reader.abort()
        reject()
      }
      reader.onload = () => resolve(reader.result)
      reader.readAsDataURL(data)
    })
  }

  const readAsText = data => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onerror = () => {
        reader.abort()
        reject()
      }
      reader.onload = () => resolve(reader.result)
      reader.readAsText(data)
    })
  }

  const initUploadPolygonJson = async e => {
    const name = e.target.files[0].name
    const data = await readAsText(e.target.files[0])
    setGeoJson(data)
    setFileList([name])
  }

  const initUploadPolygonFiles = async e => {
    try {
      let shp = false
      let shx = false
      let dbf = false
      let prj = false
      let fileNameList = []

      for (let file of e.target.files) {
        if (!shp) {
          const isShp = file.name.includes('.shp')
          if (isShp) {
            shp = await readAsDataURL(file)
            fileNameList.push(file.name)
          }
        }
        if (!shx) {
          const isShx = file.name.includes('.shx')
          if (isShx) {
            shx = await readAsDataURL(file)
            fileNameList.push(file.name)
          }
        }
        if (!dbf) {
          const isDbf = file.name.includes('.dbf')
          if (isDbf) {
            dbf = await readAsDataURL(file)
            fileNameList.push(file.name)
          }
        }
        if (!prj) {
          const isPrj = file.name.includes('.prj')
          if (isPrj) {
            prj = await readAsDataURL(file)
            fileNameList.push(file.name)
          }
        }
      }
      if (!shp || !shx || !dbf || !prj) {
        alert('Please, upload a SHP, SHX, DBF and PRJ files')
        return
      }

      setFileList(fileNameList)
      setShape({ shp, shx, dbf, prj })
      setTypeUpload(1)
    } catch (err) {
      alert('Error')
    }
  }

  const initUploadPolygonKml = async e => {
    const name = e.target.files[0].name

    const isXml = name.includes('.kml')
    if (isXml) {
      const data = await readAsText(e.target.files[0])
    // var kmlXmlData = (new DOMParser()).parseFromString(data, 'text/xml');
    // console.log(JSON.stringify(kml(kmlXmlData)))

    setFileList([name])
    setShape({ kml: data })
    setTypeUpload(2) 
    }else {
      alert('Please, upload a KML file')
    }
    
  }


  const truncateString = (string, maxLength = 20) => {
    if (!string) return null
    if (string.length <= maxLength) return string
    return `${string.substring(0, maxLength)}...  ${string.slice(
      string.length - 4
    )}`
  }

  return (
    <div style={{ padding: 15 }}>
      <LoadingDialog visible={loading} />
      {fileList ? (
        <React.Fragment>
          {fileList.map(item => (
            <p>{truncateString(item)}</p>
          ))}
          <ButtonSecondary
            margin={5}
            smSize='125%'
            onClick={() => clearState()}
            text={'Clear'}
          />
          <ButtonPrimary
            margin={5}
            smSize='125%'
            onClick={() => uploadPolygonFiles(typeUpload, shape)}
            text={'Confirm Upload'}
          />
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div style={{ width: 400 }}>
            <p align='center'>Select the type of the Upload</p>
          </div>

          {/* <div style={{width: 200}}>
          <ButtonPrimary
            margin={5}
            onClick={() => openUploadGeoJson()}
            text={' GeoJson / Json '}
          />
          <input
            type='file'
            accept='.json, .geojson'
            onChange={e => initUploadPolygonJson(e)}
            id={uploadGeoJsonId}
            style={{ display: 'none' }}
          />
          </div> */}

          <div style={{width: 200}}>
          <ButtonPrimary
            smSize={"200%"}
            margin={5}
            onClick={() => openUploadKml()}
            text={'KML'}
          />
          <input
            type='file'
            accept='.kml'
            onChange={e => initUploadPolygonKml(e)}
            id={uploadKmlId}
            style={{ display: 'none' }}
          />
          </div>

          <div style={{width: 200}}>
          <ButtonPrimary
            onClick={() => openUploadPolygonFiles()}
            text={'shp, shx, dbf, prj'}
          />
          <input
            type='file'
            multiple
            accept='.shp,.shx,.dbf,.prj'
            onChange={e => initUploadPolygonFiles(e)}
            id={uploadPolygonFilesId}
            style={{ display: 'none' }}
          />
          </div>
        </React.Fragment>
      )}
    </div>
  )
}

UploadPolygon.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}
