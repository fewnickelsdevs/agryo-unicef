/* eslint-disable no-return-assign */
import React, { useState, useEffect } from 'react'
import {
  Map,
  TileLayer,
  FeatureGroup,
  LayersControl,
  Popup,
  Marker,
  ZoomControl
} from 'react-leaflet'
import { EditControl } from 'react-leaflet-draw'
import PropTypes from 'prop-types'
import 'leaflet-draw/dist/leaflet.draw.css'
import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Control from 'react-leaflet-control'
import CenterMapDialog from '../../components/Dialogs/CenterMap'
import FullscreenControl from 'react-leaflet-fullscreen'
import 'react-leaflet-fullscreen-control'

const { BaseLayer } = LayersControl

const DrawableMap = ({
  coords,
  setCoords,
  onPolygonsUpdate,
  onDeletePolygons,
  shouldDraw,
  hectares,
  centerHectaresPopup,
  modalCenterHelpMap,
  centerHelpMap,
  setCenterHelpMap,
  setModalCenterHelpMap
}) => {
  //const [coords, setCoords] = useState([-15.7801, -47.9292])
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(({ coords }) => {
      const { latitude, longitude } = coords

      setCoords([latitude, longitude])
    })
  }, [])

  if (!coords) return <div />

  return (
    <Map
      zoom={8}
      zoomControl={false}
      center={coords}
      style={{ width: '100%', height: 400, zIndex: 2 }}
    >
      <LayersControl>
        <BaseLayer name='Satelite' checked>
          <BingLayer bingkey={bingKey} type='Aerial' />
        </BaseLayer>
        <BaseLayer name='Map'>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          />
        </BaseLayer>
      </LayersControl>
      {hectares && (
        <Popup position={centerHectaresPopup}>
          <div>
            <p>{hectares} Hectares.</p>
          </div>
        </Popup>
      )}

      {centerHelpMap && <Marker position={centerHelpMap} />}
      <FeatureGroup>
        <EditControl
          position='topright'
          onCreated={onPolygonsUpdate}
          onDeleted={onDeletePolygons}
          draw={{
            rectangle: false,
            circle: false,
            polyline: false,
            marker: false,
            circlemarker: false,
            polygon: shouldDraw
          }}
          edit={{ edit: false }}
        />
      </FeatureGroup>

      <Control position='topright'>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          {modalCenterHelpMap && (
            <CenterMapDialog
              visible={modalCenterHelpMap}
              centerMap={setCenterHelpMap}
              onCancel={() => setModalCenterHelpMap(false)}
            />
          )}
          <div
            onClick={() => setModalCenterHelpMap(!modalCenterHelpMap)}
            style={{
              width: 25,
              height: 25,
              borderRadius: 5,
              backgroundColor: modalCenterHelpMap ? 'grey' : 'white',
              color: 'black',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <FontAwesomeIcon icon={'search'} />
          </div>
        </div>
      </Control>

      <ZoomControl position='bottomright' />
      <FullscreenControl position='bottomright' />
    </Map>
  )
}

DrawableMap.propTypes = {
  onPolygonsUpdate: PropTypes.func.isRequired,
  onDeletePolygons: PropTypes.func.isRequired,
  shouldDraw: PropTypes.bool
}

DrawableMap.defaultProps = {
  shouldDraw: true
}

export default DrawableMap
