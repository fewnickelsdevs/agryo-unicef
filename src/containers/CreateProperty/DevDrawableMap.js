/* eslint-disable no-return-assign */
import React, { useState, useEffect, useRef } from 'react'
import {
  Map,
  TileLayer,
  FeatureGroup,
  LayersControl,
  Popup,
  Marker,
  ZoomControl
} from 'react-leaflet'
import { EditControl } from 'react-leaflet-draw'
import PropTypes from 'prop-types'
import 'leaflet-draw/dist/leaflet.draw.css'
import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Control from 'react-leaflet-control'
import CenterMapDialog from '../../components/Dialogs/CenterMap'
import FullscreenControl from 'react-leaflet-fullscreen'
import 'react-leaflet-fullscreen-control'
import { Sidebar, Tab } from 'react-leaflet-sidebarv2'
import agryoLogo from '../../assets/agryo-sm-white.svg'
import styled from 'styled-components'
import { ButtonPrimary, IconButton } from '../../components/Buttons'
import { colors } from '../../theme'
import RoundButton from '../../components/Buttons/RoundButton'

const BrandLogo = styled.img.attrs({
  alt: 'Agryo logo'
})`
  height: 2rem;
  margin-top: 7px;
`

const HorizontalButtonsContainer = styled.div`
  ${({ theme }) => `
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
  padding-left: 10px;
  padding-right: 10px;
  ${theme.breakpoints.xs} {
    flex-direction: column;
  }
  `}
`

const TypePropertyOption = styled.button`
  cursor: pointer;
  padding: 1rem 0.2rem;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : 'grey')};
  background: ${({ active }) => (active ? '#3F8C48' : '#dedede')};
  text-align: center;
  margin: 4px;
  width: 95px;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;
`

const ConfirmMobile = styled.button`
  cursor: pointer;
  padding: 20px;
  border-radius: 50%;
  margin-left: auto;
  margin-right: auto;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : 'grey')};
  background: ${({ active }) => (active ? '#3F8C48' : '#dedede')};
  display: inline-block;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;
`

const BoxHectaresInfo = styled.div`
  border: 1px solid #3c823b38;
  height: 100px; 
  width: 95%; 
  display: table
  margin-top: 20px;
`
const Text = styled.p`
  text-align: center;
  vertical-align: middle;
  display: table-cell;
`

const MobileButtonContainer = styled.div`
  ${({ theme }) => `
    display: none;
    ${theme.breakpoints.xs} {
      display: flex;
      justify-content: center;
    }
  `}
`

const ButtonContainer = styled.div`
  ${({ theme }) => `
  display: flex;
  justify-content: center;
    ${theme.breakpoints.xs} {
      display: none;
    }
  `}
`
const { BaseLayer } = LayersControl

const DevDrawableMap = ({
  t,
  setTypeProperty,
  typeProperty,
  onCreateProperty,
  loadingCreateProperty,
  coords,
  setCoords,
  onPolygonsUpdate,
  onDeletePolygons,
  shouldDraw,
  hectares,
  centerHectaresPopup,
  closeSuccessDialog,
  newPropertySuccess,
  modalCenterHelpMap,
  centerHelpMap,
  setCenterHelpMap,
  setModalCenterHelpMap
}) => {
  const [collapsed, setCollapsed] = useState(false)
  const [selected, setSelected] = useState('home')
  const drawRefControl = useRef(null)

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(({ coords }) => {
      const { latitude, longitude } = coords

      setCoords([latitude, longitude])
    })
  }, [])

  const removeAllEditControlLayers = () => {
    closeSuccessDialog()
    var layerContainer =
        drawRefControl.current.leafletElement.options.edit.featureGroup,
      layers = layerContainer._layers,
      layer_ids = Object.keys(layers),
      layer

    layer_ids.forEach(id => {
      layer = layers[id]
      layerContainer.removeLayer(layer)
    })
  }

  if (!coords) {
    return <div />
  }

  return (
    <Map
      zoom={8}
      zoomControl={false}
      center={coords}
      style={{ width: '100%', height: 400, margin: 10, padding: 10, zIndex: 2 }}
    >
      <Sidebar
        id='sidebar-create'
        collapsed={collapsed}
        selected={selected}
        autopan={true}
      >
        <Tab
          id='home'
          header={'Polygon Details'}
          icon={
            <div>
              <BrandLogo src={agryoLogo} />
            </div>
          }
        >
          {newPropertySuccess ? (
            <div
              style={{
                marginTop: 30,
                display: 'flex',
                justifyContent: 'center',
                flexDirection: 'column'
              }}
            >
              <p style={{ textAlign: 'center', fontSize: 15, fontWeight: 600 }}>
                {t('properties.create.success.title')}
              </p>
              <div
                style={{
                  marginTop: 10,
                  marginLeft: 'auto',
                  marginRight: 'auto'
                }}
              >
                <RoundButton
                  disabled={!newPropertySuccess}
                  onClick={removeAllEditControlLayers}
                >
                  <FontAwesomeIcon icon='check' color='white' />
                </RoundButton>
              </div>
            </div>
          ) : (
            <React.Fragment>
              <BoxHectaresInfo>
                {hectares ? (
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      flexDirection: 'column',
                      alignContent: 'center'
                    }}
                  >
                    <p
                      style={{
                        textAlign: 'center',
                        marginTop: 'auto',
                        marginBottom: 'auto'
                      }}
                    >
                      Aprox Hectares:
                    </p>
                    <p
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        marginTop: 'auto',
                        marginBottom: 'auto'
                      }}
                    >
                      {hectares}
                    </p>
                  </div>
                ) : (
                  <p
                    style={{
                      textAlign: 'center',
                      verticalAlign: 'middle',
                      display: 'table-cell'
                    }}
                  >
                    Draw a Polygon on the Map
                  </p>
                )}
              </BoxHectaresInfo>
              <div style={{ marginTop: 30 }}>
                <p>Property Type</p>
                <HorizontalButtonsContainer>
                  <TypePropertyOption
                    active={typeProperty === 1}
                    onClick={() => setTypeProperty(1)}
                  >
                    {t('properties.create.area.production')}
                  </TypePropertyOption>
                  <TypePropertyOption
                    active={typeProperty === 2}
                    onClick={() => setTypeProperty(2)}
                  >
                    {t('properties.create.area.preservation')}
                  </TypePropertyOption>
                </HorizontalButtonsContainer>
                <MobileButtonContainer>
                  <RoundButton
                    loading={loadingCreateProperty}
                    disabled={!hectares}
                    onClick={onCreateProperty}
                  >
                    <FontAwesomeIcon icon='check' color='white' />
                  </RoundButton>
                </MobileButtonContainer>
                <ButtonContainer>
                  <ButtonPrimary
                    text='Confirm Property'
                    loading={loadingCreateProperty}
                    disabled={!hectares}
                    onClick={onCreateProperty}
                  />
                </ButtonContainer>
              </div>
            </React.Fragment>
          )}
        </Tab>
      </Sidebar>

      <LayersControl>
        <BaseLayer name='Satelite' checked>
          <BingLayer bingkey={bingKey} type='Aerial' />
        </BaseLayer>
        <BaseLayer name='Map'>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          />
        </BaseLayer>
      </LayersControl>
      {/* {hectares
          && (
          <Popup
            position={centerHectaresPopup}
          >
            <div>
              <p>{hectares} Hectares.</p>
            </div>
          </Popup>
          )
        } */}

      {centerHelpMap && <Marker position={centerHelpMap} />}
      <FeatureGroup>
        <EditControl
          ref={drawRefControl}
          position='topright'
          onCreated={onPolygonsUpdate}
          onDeleted={onDeletePolygons}
          draw={{
            rectangle: false,
            circle: false,
            polyline: false,
            marker: false,
            circlemarker: false,
            polygon: shouldDraw
          }}
          edit={{ edit: false }}
        />
      </FeatureGroup>

      <Control position='topright'>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          {modalCenterHelpMap && (
            <CenterMapDialog
              visible={modalCenterHelpMap}
              centerMap={setCenterHelpMap}
              onCancel={() => setModalCenterHelpMap(false)}
            />
          )}
          <div
            onClick={() => setModalCenterHelpMap(!modalCenterHelpMap)}
            style={{
              width: 25,
              height: 25,
              borderRadius: 5,
              backgroundColor: modalCenterHelpMap ? 'grey' : 'white',
              color: 'black',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <FontAwesomeIcon icon={'search'} />
          </div>
        </div>
      </Control>

      <ZoomControl position='bottomright' />
      <FullscreenControl position='bottomright' />
    </Map>
  )
}

DevDrawableMap.propTypes = {
  onPolygonsUpdate: PropTypes.func.isRequired,
  onDeletePolygons: PropTypes.func.isRequired,
  shouldDraw: PropTypes.bool
}

DevDrawableMap.defaultProps = {
  shouldDraw: true
}

export default DevDrawableMap
