import React, { useState } from 'react'
import styled from 'styled-components'

import Container from '../../components/Layout/Container'
import { Title, Subtitle } from '../../components/Typography'
import { ButtonPrimary } from '../../components/Buttons'

const Heading = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const SaveButtonContainer = styled.div`
  ${({ theme }) => `
    ${theme.breakpoints.xs} {
      display: none
    }
  `}
`

const Content = styled.section`
  margin-top: 2.75rem;
`

const MobileButtonContainer = styled.div`
  ${({ theme }) => `
    display: none;
    
    & > * {
      width: 100%;
      margin-top: 1rem;
    }

    ${theme.breakpoints.xs} {
      display: block;
    }
  `}
`

const Table = styled.table`
  width: 100%;
  padding: 1rem;
  background: #fff;
  border-radius: 5px;
`

const TableRow = styled.tr`
  & > * {
    padding: 1px;
  }

  font-size: 1.25rem;
`

const HrefLink = styled.td`
  ${({ theme }) => `
    cursor: pointer;
    text-decoration: underline;
    color: ${theme.colors.primary};
  `}
`

const ApiAdmContainer = ({ apiData, onCreateKeys, t }) => {
  return (
    <Container>
      <Heading>
        <div>
          <Title>API</Title>
          <Subtitle>Administre suas chaves de acesso a API</Subtitle>
        </div>

        <ButtonPrimary text={'Create Keys'} onClick={() => onCreateKeys()} />
      </Heading>

      <Container>
        <Title>Your Keys</Title>

        <Table>
          <tr>
            <th align='left'>Access Key</th>
            <th align='left'>Private Key</th>
          </tr>

          {apiData.map(apiKeyData => (
            <TableRow>
              <td>{apiKeyData.accessKey}</td>
              <td>***********</td>
            </TableRow>
          ))}
        </Table>
      </Container>

      {/* <MobileButtonContainer>
        <ButtonPrimary
          text={t('properties.create.submit')}
          disabled={!property.geoData}
          onClick={() => onCreateKeys()}
        />
      </MobileButtonContainer> */}
    </Container>
  )
}

export default ApiAdmContainer
