import styled from 'styled-components'

export const ScrollBarHidden = styled.div`
  overflow: hidden;
`

export const Properties = styled.div`
  ${({ theme }) => `
    display: grid;
    grid-gap: 1rem;
    grid-template-columns: 1fr 1fr 1fr;

    & > * {
      flex: 1;
    }

    ${theme.breakpoints.sm} {
      grid-template-columns: 1fr 1fr;
    }

    ${theme.breakpoints.xs} {
      grid-template-columns: 1fr;
    }
  `}
`

export const CreatePropertyButtonContainer = styled.div`
  height: 254px;
  min-width: 18rem;
  background: #eee;
  cursor: pointer;
  overflow: hidden;
  border-radius: 6px;
  border: 1px dashed #ccc;
  transition: 0.2s all;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  &:hover {
    background: #e1e1e1;
  }
`

export const Label = styled.h4`
  color: #a6a6a6;
  text-align: center;
`
