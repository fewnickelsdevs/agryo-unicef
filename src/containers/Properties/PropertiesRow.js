import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PropTypes from 'prop-types'

import { Properties, CreatePropertyButtonContainer, Label } from './styles'
import PropertyCard from '../../components/PropertyCard'
import normalizePropertyCoords from '../../formatters/normalizePropertyCoords'
import { getPropertyLocation, mappedStatus } from '../../services/ceos_utils'

const PropertiesRow = ({
  t,
  properties,
  onClickProperty,
  redirectNewProperty
}) => {
  return (
    <Properties>
      {redirectNewProperty && (
        <CreatePropertyButtonContainer onClick={redirectNewProperty}>
          <FontAwesomeIcon icon='plus' size='2x' color='#a6a6a6' />
          <Label>{t('properties.add')}</Label>
        </CreatePropertyButtonContainer>
      )}
      {properties.map(({ geometry, properties }) => {
        const propertyId = properties.f1
        const hectares = properties.f4
        const coords = normalizePropertyCoords(geometry.coordinates)

        return (
          <PropertyCard
            t={t}
            onClick={() => onClickProperty(propertyId)}
            key={propertyId}
            active={false}
            status={mappedStatus(t)[properties.f3]}
            label={getPropertyLocation(properties)}
            hectares={hectares}
            locale={coords}
          />
        )
      })}
    </Properties>
  )
}

PropertiesRow.propTypes = {
  t: PropTypes.func.isRequired,
  properties: PropTypes.array.isRequired
}

export default PropertiesRow
