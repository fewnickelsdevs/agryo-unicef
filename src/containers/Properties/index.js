import React from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

import Container from '../../components/Layout/Container'
import { Title } from '../../components/Typography'
import PropertiesRow from './PropertiesRow'

const PropertiesContainer = ({ t, properties, onClickProperty }) => {
  const history = useHistory()
  const redirectNewProperty = () => history.push('/account/properties/create')

  return (
    <Container>
      <Title>{t('properties.title')}</Title>
      <p>{t('properties.message')}</p>
      <PropertiesRow
        t={t}
        redirectNewProperty={redirectNewProperty}
        properties={properties}
        onClickProperty={onClickProperty}
      />
    </Container>
  )
}

PropertiesContainer.propTypes = {
  t: PropTypes.func.isRequired,
  properties: PropTypes.array.isRequired
}

export default PropertiesContainer
