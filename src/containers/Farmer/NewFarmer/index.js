import React, { useState, useEffect } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import { ButtonPrimary, ButtonSecondary } from '../../../components/Buttons'
import TextInput from '../../../components/Form/TextInput/index.js'
import styled from 'styled-components'
import Container from '../../../components/Layout/Container'
import { Title, Subtitle } from '../../../components/Typography'
import SimpleCheckbox from '../../../components/Form/Checkbox/SimpleCheckbox'
import SelectInput from '../../../components/Form/SelectInput';

const InputContainer = styled.div`
  width: '80%';
  margin-bottom: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const ButtonsConfirmation = styled.div`
  ${({ theme }) => `
  margin-top: 1rem;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;


  ${theme.breakpoints.xs} {
  }
  `}
`

export default function NewFarmerContainer({ loading, onConfirm, onCancel, regionData }) {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [document, setDocument] = useState('')
  const [regionId, setRegionId] = useState(regionData[0] ? regionData[0].id : null)
  const [invitateFarmer, setInvitateFarmer] = useState(false)

  return (
    <Container>
      <div>
        <Title>New Farmer</Title>
        <Subtitle>
          Register a new Farmer on the company, you can invitate the farmer to Agryo
        </Subtitle>
      </div>
      <InputContainer>
        <TextInput
          style={{ width: 300 }}
          value={name}
          onChange={e => setName(e)}
          placeholder={'Name'}
        />

        <TextInput
          style={{ width: 300 }}
          value={document}
          onChange={e => setDocument(e)}
          placeholder={'Document'}
        />
        <TextInput
          style={{ width: 300 }}
          value={email}
          onChange={e => setEmail(e)}
          placeholder={'Email'}
        />

        <SelectInput
          style={{ width: 300 }}
          onChange={setRegionId}
          value={regionId}
          options={regionData}
          placeholder={"Region"}
        />



        <SimpleCheckbox
          message={'Invitate Farmer to Agryo'}
          value={invitateFarmer}
          setValue={setInvitateFarmer}
        />
      </InputContainer>
      <ButtonsConfirmation>
        <ButtonSecondary
          margin={10}
          text={'Cancel'}
          onClick={() => onCancel()}
        />
        <ButtonPrimary
          margin={10}
          loading={loading}
          text={'Confirm'}
          onClick={() => onConfirm({ name, email, document, regionId, invitateFarmer })}
        />
      </ButtonsConfirmation>
    </Container>
  )
}

NewFarmerContainer.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string,
  buttonMessage: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  codeValue: PropTypes.string.isRequired,
  onChangeCodeValue: PropTypes.func.isRequired
}

NewFarmerContainer.defaultProps = {
  message: null
}
