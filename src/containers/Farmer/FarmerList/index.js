import React, { useState } from 'react'
import styled from 'styled-components'
import { ButtonPrimary } from '../../../components/Buttons'
import { Title, Subtitle } from '../../../components/Typography'
import Container from '../../../components/Layout/Container'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Table = styled.table`
  width: 100%;
  padding: 1rem;
  border-collapse: collapse;
  background: #fff;
  border-radius: 5px;
  margin-top: 10px;
`

const TableRow = styled.tr`
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  &:hover {
    background-color: #eaeeeb8f;
  }
`

const TableRowTitle = styled.tr`
  border-bottom: 1px solid rgba(224, 224, 224, 1);
`

const TableItemTitle = styled.td`
  padding: 16px;
  font-weight: bold;
  text-transform: uppercase;
  color: #808080d6;
  letter-spacing: 1.2px
    ${({ align }) => `
  text-align: ${align}
`};
`

const TableItem = styled.td`
  ${({ align }) => `
  padding: 16px;
  text-align: ${align}
`}
`

const Heading = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const HrefLink = styled.td`
  ${({ theme }) => `
    cursor: pointer;
    text-decoration: underline;
    color: ${theme.colors.primary};
  `}
`

// const dataTable = [
//   {
//     value: 'name',
//     label: 'Name'
//   },
//   {
//     value: '',
//     label: ''
//   }
// ]

const FarmerListContainer = ({
  farmers,
  loading,
  onNewFarmer,
  navigateToFarmerDetails,
  showNewFarmer = false
}) => {
  const [newFarmerModal, setNewFarmerModal] = useState(false)
  const farmersData = farmers || []
  return (
    <Container>
      <Heading>
        <div>
          <Title>Farmers</Title>
          <Subtitle>Admin your company farmers</Subtitle>
        </div>
        {showNewFarmer && (
          <Link to={'/account/farmers/new'}>
            <ButtonPrimary
              text={'New Farmer'}
            //onClick={() => setNewFarmerModal(true)}
            />
          </Link>
        )}
      </Heading>
      <Table>
        <TableRowTitle>
          <TableItemTitle align='left'>Name</TableItemTitle>
          <TableItemTitle align='left'>Status</TableItemTitle>
          <TableItemTitle align='center'>Details</TableItemTitle>
        </TableRowTitle>

        {farmersData.map(farmer => (
          <TableRow>
            <TableItem>{farmer.name}</TableItem>
            <TableItem>
              {farmer.active === 'true' && farmer.registered === 'true' && (
                <p>Active Login</p>
              )}
              {farmer.active === 'true' && farmer.registered === 'false' && (
                <p>Registered</p>
              )}
              {!farmer.active && <p>Pending</p>}
            </TableItem>

            <TableItem align='center'>
              {farmer.active && (
                  <FontAwesomeIcon
                    size='lg'
                    icon='external-link-alt'
                    className='details-icon'
                    color='#3F8C48'
                  onClick={() => navigateToFarmerDetails(farmer)}
                  />
              )

              // <p onClick={() => navigateToFarmerDetails(farmer)}>Details</p>
              }
            </TableItem>
          </TableRow>
        ))}
      </Table>
    </Container>
  )
}

export default FarmerListContainer
