import React from 'react'
import {
  TableContainer,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell
} from '../../../components/Table'

const BoxFarmerInfo = ({ t, farmerData: { name, id, active } }) => (
  <TableContainer>
    <TableHead>
      <TableRow>
        <TableHeadCell>Info</TableHeadCell>
        <TableHeadCell></TableHeadCell>
      </TableRow>
    </TableHead>

    <TableBody>
      <TableRow striped>
        <TableCell>Name</TableCell>
        <TableCell>{name}</TableCell>
      </TableRow>
      <TableRow striped>
        <TableCell>Status</TableCell>
        <TableCell>{active ? 'Active' : 'Locked'}</TableCell>
      </TableRow>
    </TableBody>
  </TableContainer>
)

export default BoxFarmerInfo
