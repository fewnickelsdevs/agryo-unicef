import React from 'react'
import Container from '../../../components/Layout/Container'
import BoxFarmerInfo from './BoxFarmerInfo'
import styled from 'styled-components'
import { Title, Subtitle } from '../../../components/Typography'
import PropertiesRow from '../../Properties/PropertiesRow'
import { ButtonPrimary } from '../../../components/Buttons'

const Heading = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const FarmerInfoContainer = ({
  farmerData,
  propertyData,
  onCreateProperty = false,
  onClickProperty,
  redirectNewContract,
  t
}) => {
  return (
    <Container>
      <Heading>
        <div>
          <Title>Farmer Details</Title>
          <Subtitle></Subtitle>
        </div>
        {redirectNewContract && (
          <ButtonPrimary
            text={'New Contract'}
            onClick={() => redirectNewContract()}
          />
        )}
      </Heading>
      <BoxFarmerInfo farmerData={farmerData} />
      <div style={{ marginTop: 20 }}>
        <Title>Properties</Title>
        <PropertiesRow
          t={t}
          properties={propertyData}
          redirectNewProperty={onCreateProperty}
          onClickProperty={onClickProperty}
        />
      </div>
    </Container>
  )
}

export default FarmerInfoContainer
