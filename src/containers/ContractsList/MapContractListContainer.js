import React, { useState } from 'react'
import styled from 'styled-components'
import DevInvestmentMap from '../Investments/DevInvestmentMap'

const Table = styled.table`
  width: 100%;
  padding: 1rem;
  background: #fff;
  border-radius: 5px;
`

const TableRow = styled.tr`
  & > * {
    padding: 1px;
  }

  font-size: 1.25rem;
`

const ActivityButton = styled.div`
  width: 5rem;
  height: 5.5rem;
  cursor: pointer;
  padding: 0.7rem 0;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;

  &:not(:last-of-type) {
    margin-right: 1rem;
  }

  & h4 {
    text-align: center;
    line-height: 100%;
  }
`

export const Form = styled.div`
  margin-bottom: 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const MapContractListContainer = ({ t, contracts, goToDetails, showSideBar }) => {
  const activities = [
    {
      slug: 'livestock',
      label: t('contracts.form.animals'),
      icon: 'horse-head'
    },
    {
      slug: 'crops',
      label: t('contracts.form.grains'),
      icon: 'seedling'
    },
    {
      slug: 'greenBond',
      label: t('contracts.form.green-bond'),
      icon: 'globe'
    }
  ]

  const [visibleModal, setVisibleModal] = useState(false)
  const [mapActivity, setMapActivity] = useState({
    crops: true,
    livestock: true,
    greenBond: true,
    pending: true,
    confirmed: true
  })
  const [activity, setActivity] = useState(activities[1])

  const [contractDataModal, setContractDataModal] = useState(false)

  const setContractModal = contractData => {
    setContractDataModal(contractData)
    setVisibleModal(true)
  }

  // let data = [
  //   ...(mapActivity.pending && mapActivity.crops
  //     ? contracts.pendingContracts.crops
  //     : []),
  //   ...(mapActivity.confirmed && mapActivity.crops
  //     ? contracts.contracts.crops
  //     : []),

  //   ...(mapActivity.pending && mapActivity.greenBond
  //     ? contracts.pendingContracts.greenBond
  //     : []),
  //   ...(mapActivity.confirmed && mapActivity.greenBond
  //     ? contracts.contracts.greenBond
  //     : []),

  //   ...(mapActivity.pending && mapActivity.livestock
  //     ? contracts.pendingContracts.livestock
  //     : []),
  //   ...(mapActivity.confirmed && mapActivity.livestock
  //     ? contracts.contracts.livestock
  //     : [])
  // ]

  const setContractsData = data => {
    const x = { [data]: !activity[data] }
    let auxState = { ...activity, ...x }
    setActivity(auxState)
  }

  return (
    <DevInvestmentMap
      t={t}
      contracts={contracts}
      showSideBar={showSideBar}
      onClickContract={setContractModal}
      hideContractModal={setVisibleModal}
      contractData={contractDataModal}
      visibleModal={visibleModal}
      contractTypes={activities}
      selectTypeContract={setMapActivity}
      onSelectContract={setContractsData}
      goToContractDetails={goToDetails}
    />
  )
}

export default MapContractListContainer
