import React from 'react'
import styled from 'styled-components'
import currency from 'currency.js'
import { StatusContract } from '../../mocks/contractTypes'
import { crops } from '../../mocks/crops.json'
import { livestocks } from '../../mocks/livestocks.json'

const Table = styled.table`
  width: 100%;
  padding: 1rem;
  background: #fff;
  border-radius: 5px;
  margin-top: 10px;
`

const TableRow = styled.tr`
  & > * {
    padding: 1px;
  }

  font-size: 1.25rem;
`

export const Form = styled.div`
  padding: 1rem;
  padding-left: 7rem;
  padding-right: 7rem;
  margin-top: 2rem;
  margin-bottom: 2rem;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ccc;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const TableCropsContract = ({ t, contracts, goToDetails }) => {
  return (
    <Table>
      <tr>
        <th align='left'>Status</th>
        <th align='left'>Property</th>
        <th align='left'>Type</th>
        <th align='left'>Date Start</th>
        <th align='left'>Date End</th>
        <th align='left'>Funded Value</th>
        <th></th>
      </tr>

      {contracts.map(contract => {
        const product = [...crops, ...livestocks].find(product => {
          const productId = contract.cropsType || contract.livestockType
          return product.id === productId
        }) || { name: 'Green Bond', value: 'greenBond' }

        return (
          <TableRow>
            <td>{t(StatusContract[contract.status])}</td>
            <td>{contract.propertyId}</td>
            <td>{t(product.name)}</td>
            <td>
              {new Date(contract.startDate * 1000).toLocaleDateString('en-US')}
            </td>
            <td>
              {new Date(contract.endDate * 1000).toLocaleDateString('en-US')}
            </td>
            <td>
              $
              {currency(contract.fundedValue)
                .divide(100)
                .format()}
            </td>
            <td
              onClick={() => goToDetails(product.value, contract.id, contract)}
            >
              Details
            </td>
          </TableRow>
        )
      })}
    </Table>
  )
}

export default TableCropsContract
