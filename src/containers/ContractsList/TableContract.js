import React from 'react'
import styled from 'styled-components'
import currency from 'currency.js'
import { StatusContract } from '../../mocks/contractTypes'
import { crops } from '../../mocks/crops.json'
import { livestocks } from '../../mocks/livestocks.json'
import {
  TableDev,
  TableRowTitleDev,
  TableItemTitleDev,
  TableRowDev,
  TableItemDev
} from '../../components/Table/DevTable'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { CURRENCY_SYMBOL } from '../../mocks/currency';

export const Form = styled.div`
  padding: 1rem;
  padding-left: 7rem;
  padding-right: 7rem;
  margin-top: 2rem;
  margin-bottom: 2rem;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ccc;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const generateContractTypeAndId = (contractType) => {
  const product = [...crops, ...livestocks].find(product => {
    const productId = contractType
    return product.id === productId
  }) || { name: 'Green Bond', value: 'greenBond' }

  return { name: product.name || "INVALID", value: product.value || "INVALID" }
}

const TableContract = ({ t, contracts, goToDetails }) => {
  return (
    <TableDev>
      <TableRowTitleDev>
        <TableItemTitleDev align='left'>Status</TableItemTitleDev>
        <TableItemTitleDev align='left'>Property</TableItemTitleDev>
        <TableItemTitleDev align='left'>Type</TableItemTitleDev>
        <TableItemTitleDev align='center'>Date</TableItemTitleDev>
        <TableItemTitleDev align='left'>Funded Value</TableItemTitleDev>
        <TableItemTitleDev></TableItemTitleDev>
      </TableRowTitleDev>

      {contracts.map(contract => {
       const contractInfo = generateContractTypeAndId(contract.contractType)

        return (
          <TableRowDev>
            <TableItemDev>{t(StatusContract[contract.status])}</TableItemDev>
            <TableItemDev>{contract.propertyId}</TableItemDev>
            <TableItemDev>{t(contractInfo.name)}</TableItemDev>
            <TableItemDev>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <p align='center'>
                  {new Date(contract.startDate * 1000).toLocaleDateString(
                    'en-US'
                  )}
                </p>
                <p align='center'>~</p>
                <p align='center'>
                  {new Date(contract.endDate * 1000).toLocaleDateString(
                    'en-US'
                  )}
                </p>
              </div>
            </TableItemDev>
            <TableItemDev>
              {CURRENCY_SYMBOL[contract.currency]}
              {currency(contract.fundedValue)
                .divide(100)
                .format()}
            </TableItemDev>
            <TableItemDev>
              <FontAwesomeIcon
                size='lg'
                icon='external-link-alt'
                className='details-icon'
                color='#3F8C48'
                onClick={() => goToDetails({...contract}, contractInfo.value)}
              />
            </TableItemDev>
          </TableRowDev>
        )
      })}
    </TableDev>
  )
}

export default TableContract
