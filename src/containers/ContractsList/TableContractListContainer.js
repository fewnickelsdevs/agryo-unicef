import React from 'react'
import styled from 'styled-components'
import Container from '../../components/Layout/Container'
import TableContract from './TableContract'

export const Form = styled.div`
  margin-bottom: 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const TableContractListContainer = ({ t, contracts, goToDetails }) => {
  const activities = [
    {
      slug: 'livestock',
      label: t('contracts.form.animals'),
      icon: 'horse-head'
    },
    {
      slug: 'crops',
      label: t('contracts.form.grains'),
      icon: 'seedling'
    },
    {
      slug: 'greenBond',
      label: t('contracts.form.green-bond'),
      icon: 'globe'
    }
  ]

  return (
    <Container>
      <TableContract
        t={t}
        // contracts={[
        //   ...contracts.pendingContracts.livestock,
        //   ...contracts.contracts.livestock,
        //   ...contracts.contracts.greenBond,
        //   ...contracts.pendingContracts.greenBond,
        //   ...contracts.contracts.crops,
        //   ...contracts.pendingContracts.crops
        // ]}
        contracts={contracts}
        goToDetails={goToDetails}
      />
    </Container>
  )
}

export default TableContractListContainer
