import React from 'react'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import { Title } from '../../components/Typography'
import Container from '../../components/Layout/Container'
import TableContract from './TableContract'
import { ButtonPrimary } from '../../components/Buttons';


const Heading = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const ContractsListContainer = ({ t, contracts, navigateToNewContract }) => {
  const history = useHistory()
  const formatedTransactionHash = hash => {
    const reducedHash = hash
      .split('')
      .splice(0, 8)
      .join('')
    return `${reducedHash}...`
  }

  const navigateToHederaExplorer = hash => {
    const txFormat = hash.replace(/[^\d]/g, '')
    window.open(
      `https://testnet.dragonglass.me/hedera/transactions/${txFormat}`,
      '__blank'
    )
  }

  const goToContractDetails = (contractData, contractType) => {
    console.log("go to detailss", contractData)
    const url =
      contractData.status === 'PENDING'
        ? '/account/contracts/pending/details'
        : '/account/contracts/details'

    history.push({
      pathname: url,
      state: {
        contractType,
        contractId: contractData.id,
        contractData,
        noInvest: true,
        farmerMode: true,
        hideAprove: true
      }
    })
  }

  return (
    <Container>
      <Heading>
        <div>
          <Title>Contracts</Title>
        </div>

        <ButtonPrimary
          text={'New Contract'}
          onClick={() => navigateToNewContract()}
        />
      </Heading>

      <TableContract
        t={t}
        contracts={contracts}
        goToDetails={goToContractDetails}
      />
    </Container>
  )
}

export default ContractsListContainer
