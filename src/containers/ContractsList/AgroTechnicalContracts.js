import React, { useState } from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Title } from '../../components/Typography'
import Container from '../../components/Layout/Container'
import Row from '../../components/Layout/Row'
import TableCropsContract from './TableCropsContract'
import TableLivestockContract from './TableLivestockContract'
import TableGreenBondContract from './TableGreenBondContract'
import InvestmentMap from '../Investments/InvestmentMap'
import ContractBasicDialog from '../../components/Dialogs/DetailsContracts/ContractBasicDialog'

const ActivityButton = styled.div`
  width: 5rem;
  height: 5.5rem;
  cursor: pointer;
  padding: 0.7rem 0;
  border-radius: 7px;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-around;

  &:not(:last-of-type) {
    margin-right: 1rem;
  }

  & h4 {
    text-align: center;
    line-height: 100%;
  }
`

export const Form = styled.div`
  padding: 1rem;
  padding-left: 1rem;
  padding-right: 1rem;
  margin-top: 2rem;
  margin-bottom: 2rem;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ccc;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

const TechnicalContractListContainer = ({ t, contracts, goToDetails }) => {
  const [activity, setActivity] = useState('crops')
  const [contractDataModal, setContractDataModal] = useState(false)
  const activities = [
    {
      slug: 'livestock',
      label: t('contracts.form.animals'),
      icon: 'horse-head'
    },
    { slug: 'crops', label: t('contracts.form.grains'), icon: 'seedling' },
    {
      slug: 'green-bond',
      label: t('contracts.form.green-bond'),
      icon: 'globe'
    }
  ]

  const activities2 = [
    {
      slug: 'xx',
      label: 'Settings',
      icon: 'horse-head'
    }
  ]

  const renderActivity = ({ slug, icon, label }) => (
    <ActivityButton
      key={slug}
      active={activity === slug}
      onClick={() => setActivity(slug)}
    >
      <FontAwesomeIcon icon={icon} size='2x' />
      <h4>{label}</h4>
    </ActivityButton>
  )

  const handleGoToDetails = contractData => {
    goToDetails(activity, contractData.id, contractData)
  }

  return (
    <Container>
      <Title>Contracts</Title>
      <Form>
        <Row>{activities.map(renderActivity)}</Row>
        <div>{activities2.map(renderActivity)}</div>
      </Form>
      <ContractBasicDialog
        t={t}
        contractData={contractDataModal}
        closeContractModal={() => setContractDataModal(false)}
        goToContractDetails={handleGoToDetails}
      />
      {activity === 'crops' && (
        <React.Fragment>
          <TableCropsContract
            t={t}
            contracts={[
              ...contracts.pending.crops,
              ...contracts.contracts.crops
            ]}
            goToDetails={goToDetails}
          />
          <Form>
            <InvestmentMap
              contracts={[
                ...contracts.pending.crops,
                ...contracts.contracts.crops
              ]}
              onClickContract={setContractDataModal}
            />
          </Form>
        </React.Fragment>
      )}
      {activity === 'livestock' && (
        <React.Fragment>
          <TableLivestockContract
            t={t}
            contracts={[
              ...contracts.pending.livestock,
              ...contracts.contracts.livestock
            ]}
            goToDetails={goToDetails}
          />
          <Form>
            <InvestmentMap
              contracts={[
                ...contracts.pending.livestock,
                ...contracts.contracts.livestock
              ]}
              onClickContract={setContractDataModal}
            />
          </Form>
        </React.Fragment>
      )}
      {activity === 'green-bond' && (
        <React.Fragment>
          <TableGreenBondContract
            t={t}
            contracts={[
              ...contracts.pending.greenBond,
              ...contracts.contracts.greenBond
            ]}
            goToDetails={goToDetails}
          />
          <Form>
            <InvestmentMap
              contracts={[
                ...contracts.pending.greenBond,
                ...contracts.contracts.greenBond
              ]}
              onClickContract={setContractDataModal}
            />
          </Form>
        </React.Fragment>
      )}
    </Container>
  )
}

export default TechnicalContractListContainer
