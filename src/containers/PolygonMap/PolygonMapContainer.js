import React from 'react'
import styled from 'styled-components'
import Map from '../../components/Map/Map'
import normalizePropertyCoords from '../../formatters/normalizePropertyCoords'
import SelectInput from '../../components/Form/SelectInput'
import moment from 'moment'

const SectionContainer = styled.section`
  margin-bottom: 2rem;
`


const DivWithPadding = styled.div`
  padding: 10px 0;
  max-width: 33%;
  min-width: 320px;
`

const FlexDiv = styled.div`
display:flex;
`

const DivSubTitle = styled.div`
  padding: 5px 0;
  font-weight: bold;
  font-size: 12px;
`

const MAP_SOURCE = {
  NONE: `NONE`,
  MODIS: `MODIS`,
  LANDSAT: `LANDSAT`,
  FIREDATA: `FIREDATA`,
  DEFORESTATION: `DEFORESTATION`
}

const MAP_RASTER_LAYER = {
  NONE: `NONE`,
  RGB: `RGB`,
  NDVI: `NDVI`,
  TIF: `TIF`,
  GEOJSON: `GEOJSON`
}

const MAP_SOURCE_TO_MAP_RASTER_LAYER = {
  [MAP_SOURCE.NONE]: [MAP_RASTER_LAYER.NONE],
  [MAP_SOURCE.MODIS]: [MAP_RASTER_LAYER.NONE, MAP_RASTER_LAYER.NDVI],
  [MAP_SOURCE.LANDSAT]: [MAP_RASTER_LAYER.NONE, MAP_RASTER_LAYER.RGB, MAP_RASTER_LAYER.NDVI],
  [MAP_SOURCE.FIREDATA]: [MAP_RASTER_LAYER.NONE, MAP_RASTER_LAYER.GEOJSON],
  [MAP_SOURCE.DEFORESTATION]: [MAP_RASTER_LAYER.NONE, MAP_RASTER_LAYER.TIF],

}

class PolygonMapContainer extends React.Component {
  constructor(props) {
    super(props)
    const defaultMapSource = MAP_SOURCE.NONE
    const defaultRasterLayer = MAP_SOURCE_TO_MAP_RASTER_LAYER[defaultMapSource][0]
    this.state = {
      rasterLayer: defaultRasterLayer,
      mapSource: defaultMapSource,
      mapDate: null
    }
  }

  getGeoJSONURL() {
    const { property } = this.props
    const { rasterLayer, mapSource, mapDate } = this.state
    if (rasterLayer !== MAP_RASTER_LAYER.GEOJSON) {
      return
    }

    if (mapSource !== MAP_SOURCE.FIREDATA) {
      return null
    }

    const { fireData = {} } = property
    const selectedSatellite = fireData

    if (!selectedSatellite) {
      return null
    }

    const foundItem = selectedSatellite.find(item => {
      return item.date.toString() === mapDate
    })

    if (!foundItem) {
      return null
    }

    return foundItem.fireDataS3Path
  }

  getTiffURL() {
    const { property } = this.props
    const { rasterLayer, mapSource, mapDate } = this.state
    if (!mapDate) {
      return null
    }

    if (mapSource !== MAP_SOURCE.MODIS && mapSource !== MAP_SOURCE.LANDSAT && mapSource !== MAP_SOURCE.DEFORESTATION) {
      return
    }

    if (rasterLayer === MAP_RASTER_LAYER.NONE) {
      return
    }

    const { heatmap = {}, deforestation = [] } = property
    const satelliteKey = mapSource.toLowerCase()
    const selectedSatellite = mapSource === MAP_SOURCE.DEFORESTATION ? deforestation : heatmap[satelliteKey]

    if (!selectedSatellite) {
      return null
    }

    //each entry has .ndviTif, rgbTif and .date
    const foundItem = selectedSatellite.find(item => {
      return item.date.toString() === mapDate
    })

    if (!foundItem) {
      return null
    }
    let key
    if (mapSource === MAP_SOURCE.DEFORESTATION) {
      key = `deforestationS3Path`
    } else {
      //ndviTif or rgbTif
      key = rasterLayer === MAP_RASTER_LAYER.RGB ? `rgbTif` : `ndviTif`
    }

    return foundItem[key]// || foundItem.tif
  }

  getMapDates() {
    const { property } = this.props
    const { mapSource } = this.state
    if (mapSource === MAP_SOURCE.NONE) {
      return []
    }

    //@TODO wsoares + henrique
    //the name heatmap should be renamed
    const { heatmap = {}, fireData = [], deforestation = [] } = property
    const response = []
    if (mapSource === MAP_SOURCE.FIREDATA) {
      const selectedSatellite = fireData

      selectedSatellite && selectedSatellite.forEach(({ date, firesAround, firesFocus }) => {
        //@henrique the date should be string
        const formattedDate = moment(date.toString()).format(`DD/MM/YYYY`)
        response.push({
          value: date,
          name: `${formattedDate} - FA ${firesAround} - FF ${firesFocus}`
        })
      })
    } else if (mapSource === MAP_SOURCE.DEFORESTATION) {

      const selectedSatellite = deforestation
      selectedSatellite && selectedSatellite.forEach(({ date, firesAround, firesIn }) => {
        //@henrique the date should be string
        const formattedDate = moment.unix(date / 1000).format(`DD/MM/YYYY`)
        response.push({
          value: date,
          name: `${formattedDate} - FA ${firesAround} - FI ${firesIn}`
        })
      })
    } else if (mapSource === MAP_SOURCE.MODIS || mapSource === MAP_SOURCE.LANDSAT) {

      const satelliteKey = mapSource.toLowerCase()
      const selectedSatellite = heatmap[satelliteKey]

      selectedSatellite && selectedSatellite.forEach(({ date }) => {
        const formattedDate = moment(date).format(`DD/MM/YYYY`)
        response.push({
          value: date,
          name: formattedDate
        })
      })
    }

    //Sort Desc
    response.sort((a, b) => a.value - b.value)

    response.unshift({ value: null, name: '-' })
    return response
  }

  handleRasterLayerChange = value => {
    this.setState({ rasterLayer: value })
  }

  handleMapSourceChange = value => {
    const defaultRasterLayer = MAP_SOURCE_TO_MAP_RASTER_LAYER[value][0]
    this.setState({
      mapSource: value,
      mapDate: null,
      rasterLayer: defaultRasterLayer
    })
  }

  handleHeatMapDateChange = value => {
    this.setState({ mapDate: value })
  }

  renderMapSourceSection() {
    const { mapSource } = this.state
    const mapSources = Object.keys(MAP_SOURCE).map(key => ({
      value: key,
      name: MAP_SOURCE[key]
    }))

    return (
      <React.Fragment>
        <DivSubTitle>Mode</DivSubTitle>
        <div>
          <SelectInput
            onChange={this.handleMapSourceChange}
            value={mapSource}
            options={mapSources}
            style={{ width: '250px' }}
          />
        </div>
      </React.Fragment>
    )
  }

  renderRasterLayerSection() {
    const { rasterLayer, mapSource } = this.state
    if (mapSource === MAP_SOURCE.NONE) {
      return []
    }
    const rasterLayersByMapSource = MAP_SOURCE_TO_MAP_RASTER_LAYER[mapSource]
    let rasterLayers = rasterLayersByMapSource.map(key => ({
      value: key,
      name: MAP_RASTER_LAYER[key]
    }))

    return (
      <React.Fragment>
        <DivSubTitle>Layer</DivSubTitle>
        <div>
          <SelectInput
            onChange={this.handleRasterLayerChange}
            value={rasterLayer}
            options={rasterLayers}
            style={{ width: '250px' }}
          />
        </div>
      </React.Fragment>
    )
  }

  renderMapDateSection() {
    const { mapDate } = this.state

    return (
      <React.Fragment>
        <DivSubTitle>Date</DivSubTitle>
        <div>
          <SelectInput
            onChange={this.handleHeatMapDateChange}
            value={mapDate}
            options={this.getMapDates()}
            style={{ width: '250px' }}
          />
        </div>
      </React.Fragment>
    )
  }

  renderMapSelector() {
    return (
      <FlexDiv>
        <DivWithPadding>{this.renderMapSourceSection()}</DivWithPadding>
        <DivWithPadding>{this.renderMapDateSection()}</DivWithPadding>
        <DivWithPadding>{this.renderRasterLayerSection()}</DivWithPadding>
      </FlexDiv>
    )
  }

  renderMap() {
    const { rasterLayer } = this.state
    const { property } = this.props
    const { polygon, basicInfo } = property

    let coordinates
    if (polygon) {
      coordinates = polygon.coordinates
    } else if (basicInfo) {
      coordinates = basicInfo.geometry.coordinates
    } else {
      return null
    }

    const mapProps = {
      data: {},
      drawPolygon: true,
      fillPolygon: true,
      polygonCoords: normalizePropertyCoords(coordinates)
    }

    const tiffURL = this.getTiffURL()
    const geoJSONURL = this.getGeoJSONURL()
    if (tiffURL) {
      mapProps.fillPolygon = false
      mapProps.tiffURL = tiffURL
    }

    if (geoJSONURL) {
      mapProps.geoJSONURL = geoJSONURL
    }

    if (rasterLayer === MAP_RASTER_LAYER.RGB) {
      mapProps.isRGB = true
    }
    return <Map {...mapProps} />
  }


  render() {
    return (
      <SectionContainer>
        {this.renderMapSelector()}
        {this.renderMap()}
      </SectionContainer>
    )
  }
}

export default PolygonMapContainer
