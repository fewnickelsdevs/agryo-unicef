import React from 'react'
import styled from 'styled-components'
import makeEthBlockie from 'ethereum-blockies-base64'
import Dropdown from '../../components/Dropdown'

const Container = styled.div`
  ${({ theme }) => `
    display: flex;
    margin-right: 1rem;
    align-items: center;
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);

    ${theme.breakpoints.xs} {
      margin-left: auto;
      margin-right: 1rem;
    }
  `}
`
const VerticalPipe = styled.div`
  width: 1px;
  margin: 0 0.5rem 0 0.6rem;
  background: #35a758;
  height: 70px;
`

const ProfilePicture = styled.img.attrs({
  alt: 'Ethereum blockie as a rounded picture'
})`
  width: 34px;
  height: 34px;
  border-radius: 50%;
  border: 1px solid #fff;
`

const WhiteText = styled.div`
  color: #ffffff;
  font-family: 'Montserrat', Arial, sans-serif !important;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 36px;
`
const TitleUserName = styled(WhiteText)`
  font-weight: 600;
  font-size: 14px;
  line-height: 17px;
  width: 85px;
  margin: 0px 12px;
`
const TitleLanguage = styled(WhiteText)`
  font-weight: 600;
  font-size: 14px;
  line-height: 17px;
  width: 70px;
  margin: 0px 12px;
`

const ProfileBox = ({ items, langs, user, titleLanguage }) => {
  const firstName = user ? user.name.split(' ')[0] : false
  const dropDownTitle = (
    <TitleUserName>{firstName || user.companyName}</TitleUserName>
  )
  const dropDownLanguages = <TitleLanguage>{titleLanguage}</TitleLanguage>
  return (
    <Container>
      <Dropdown title={dropDownLanguages} options={langs} />
      <VerticalPipe />
      <ProfilePicture
        src={makeEthBlockie(`${user.publicAddress}` || user.companyName || "devteste")}
      />
      <Dropdown title={dropDownTitle} image={true} options={items} />
    </Container>
  )
}

export default ProfileBox
