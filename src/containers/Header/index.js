import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'

import ProfileBox from './ProfileBox'
import agryoLogo from '../../assets/logo-white.png'
import { IconButton } from '../../components/Buttons'
import { logoutUserApi } from '../../redux/user/utils'

const Container = styled.header`
  ${({ theme }) => `
    top: 0;
    height: 70px;
    background: linear-gradient(90deg, #3F8C48 0%, #49C16E 97.44%);
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
    width: 100%;
    position: fixed;
    z-index: 1201 !important;
    display: flex;
    align-items: center;
  `}
`

const Content = styled.div`
  margin: 0;
  padding: 0;
  display: flex;
  align-items: center;
`

const MenuButton = styled(IconButton)`
  ${({ theme }) => `
    display: none;
    margin-left: 1rem;

    ${theme.breakpoints.sm} {
      display: block;
    }
  `}
`

const Brand = styled.div`
  ${({ theme }) => `
    display: flex;
    margin-left: 1.5rem;
    align-items: center;

    ${theme.breakpoints.xs} {
      display: none;
      visibility: hidden;
    }
  `}
`

const BrandLogo = styled.img.attrs({
  alt: 'Agryo logo'
})`
  height: 2rem;
`

const VerticalPipe = styled.div`
  width: 1px;
  min-width: 1px;
  height: 2rem;
  background: rgba(255, 255, 255, 0.5);
  margin: 0 0.5rem 0 0.6rem;
`

const DashboardTitle = styled.h2`
  ${({ theme }) => `
    margin: 0;
    padding: 0;
    font-size: 1.6rem;
    font-weight: 300;
    color: ${theme.colors.white};
  `}
`

export default function HeaderContainer({
  t,
  user,
  title,
  onToggleSidebar,
  onLogout
}) {
  const boxItems = [
    {
      label: t('header.logout'),
      onClick: () => onLogout && onLogout(),
      icon: 'sign-out-alt'
    }
  ]
  const boxLanguages = [
    {
      label: t('header.en'),
      onClick: () => (window.location = `${window.location.pathname}?lang=en`)
    },
    {
      label: t('header.pt'),
      onClick: () => (window.location = `${window.location.pathname}?lang=pt`)
    },
    {
      label: t('header.es'),
      onClick: () => (window.location = `${window.location.pathname}?lang=es`)
    }
  ]

  return (
    <Container>
      <Content>
        <MenuButton onClick={onToggleSidebar}>
          <FontAwesomeIcon icon='bars' size='2x' />
        </MenuButton>

        <Brand>
          <BrandLogo src={agryoLogo} />
          <VerticalPipe />
          <DashboardTitle>{title}</DashboardTitle>
        </Brand>
      </Content>

      <ProfileBox
        titleLanguage={t('header.language')}
        items={boxItems}
        langs={boxLanguages}
        user={user}
      />
    </Container>
  )
}
