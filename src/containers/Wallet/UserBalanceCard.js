import React from 'react'
import PropTypes from 'prop-types'
import NumberFormat from 'react-number-format'
import currency from 'currency.js'

import {
  TableContainer,
  TableHead,
  TableHeadCell,
  TableBody,
  TableRow,
  TableCell
} from '../../components/Table'

const UserBalanceCard = ({ title, ethereumBalance, agryoBalance }) => (
  <TableContainer>
    <TableHead>
      <TableRow>
        <TableHeadCell>{title}</TableHeadCell>
        <TableHeadCell />
      </TableRow>
    </TableHead>

    <TableBody>
      <TableRow striped>
        <TableCell>Ethereum</TableCell>

        <TableCell align='right'>{ethereumBalance}</TableCell>
      </TableRow>

      <TableRow striped>
        <TableCell>Agryo Stable USD</TableCell>

        <TableCell align='right'>
          <NumberFormat
            value={currency(agryoBalance).divide(100).value}
            displayType={'text'}
            thousandSeparator={true}
            decimalScale={2}
            fixedDecimalScale={true}
            prefix='$'
          />
        </TableCell>
      </TableRow>
    </TableBody>
  </TableContainer>
)

UserBalanceCard.propTypes = {
  title: PropTypes.string.isRequired,
  ethereumBalance: PropTypes.string.isRequired,
  agryoBalance: PropTypes.string.isRequired
}

export default UserBalanceCard
