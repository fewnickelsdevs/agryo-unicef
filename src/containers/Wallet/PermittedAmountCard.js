import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { not, and, equals } from 'ramda'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import CurrencyInput from 'react-currency-input'

import { TextInput } from '../../components/Form'
import { IconButton } from '../../components/Buttons'
import {
  TableContainer,
  TableRow,
  TableHead,
  TableHeadCell,
  TableCell,
  TableBody
} from '../../components/Table'
import { colors } from '../../theme'

const AmountInput = styled(TextInput)`
  margin: 0 1rem 0 0.5rem;
`

const EditButton = styled(IconButton).attrs({
  small: true,
  color: colors.primary
})`
  vertical-align: middle;
`

const PermittedAmountCard = ({ title, contractTypes, onSave }) => {
  const [contractTypesFormatted, setContractTypes] = useState(
    contractTypes.map(c => ({ ...c, disabled: true }))
  )

  const authorizeAmountIfNeeded = contractType => {
    const rawContractType = contractTypes.find(
      c => c.label === contractType.label
    )
    const shouldSaveNewAmount = and(
      not(contractType.disabled),
      not(equals(contractType.value, rawContractType.value))
    )

    if (not(shouldSaveNewAmount)) return

    onSave(contractType.label, contractType.value)
  }

  const toggleContractTypeDisabledInput = contractType => {
    authorizeAmountIfNeeded(contractType)

    const updatedContractTypes = contractTypesFormatted.map(c => {
      if (c.label !== contractType.label) return c

      return {
        ...c,
        disabled: !contractType.disabled
      }
    })

    setContractTypes(updatedContractTypes)
  }

  const changeContractAmountToInvest = (contractType, amount) => {
    const updatedContractTypes = contractTypesFormatted.map(c => {
      if (c.label !== contractType.label) return c

      return {
        ...c,
        value: amount
      }
    })

    setContractTypes(updatedContractTypes)
  }

  return (
    <TableContainer>
      <TableHead>
        <TableRow>
          <TableHeadCell>{title}</TableHeadCell>
          <TableHeadCell />
        </TableRow>
      </TableHead>
      <TableBody>
        {contractTypesFormatted.map(contractType => (
          <TableRow striped>
            <TableCell>{contractType.label}</TableCell>

            <TableCell align='right'>
              <CurrencyInput
                disabled={contractType.disabled}
                style={{ width: '170px', padding: 10 }}
                prefix='$'
                value={contractType.value}
                onChange={(event, value) => changeContractAmountToInvest(contractType, value)}
              />
              <EditButton
                onClick={() => toggleContractTypeDisabledInput(contractType)}
              >
                {contractType.disabled ? (
                  <FontAwesomeIcon icon='pen' size='lg' />
                ) : (
                  <FontAwesomeIcon icon='save' size='lg' />
                )}
              </EditButton>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </TableContainer>
  )
}

PermittedAmountCard.propTypes = {
  title: PropTypes.string.isRequired
}

export default PermittedAmountCard
