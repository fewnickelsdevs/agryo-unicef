import React, { useEffect } from 'react'
import styled from 'styled-components'

import Container from '../../components/Layout/Container'
import { Title } from '../../components/Typography'
import UserBalanceCard from './UserBalanceCard'
import PermittedAmountCard from './PermittedAmountCard'
import LoadingContainer from '../Loading'
import { ButtonPrimary } from '../../components/Buttons';

const CardsContainer = styled.section`
  margin-top: 2rem;
  display: grid;
  grid-gap: 2rem;
  grid-template-rows: repeat(auto-fit, 2);
`

const WalletContainer = ({
  t,
  wallet,
  aproveValueTx,
  fetchWalletData,
  autorizeValueWallet,
  requestFaucet
}) => {
  useEffect(() => {
    fetchWalletData()
  }, [fetchWalletData])

  const { txRunning } = aproveValueTx
  const { loading, valueAprove, balance } = wallet
  const { crops, livestock, greenBond } = valueAprove
  const { etherBalance, agryoStableBalance } = balance

  const contractTypes = [
    {
      label: 'Green Bond',
      value: greenBond / 100
    },
    {
      label: 'Crops',
      value: crops / 100
    },
    {
      label: 'Livestock',
      value: livestock / 100
    }
  ]

  if (loading || txRunning) {
    return <LoadingContainer />
  }

  return (
    <Container>
      <Title>{t('wallet.title')}</Title>

      <ButtonPrimary
          text={'Request Agryo USD Faucet'}
          onClick={() => requestFaucet()}
        />

      <CardsContainer>
        <UserBalanceCard
          title='User Balance'
          ethereumBalance={etherBalance}
          agryoBalance={agryoStableBalance}
        />

        <PermittedAmountCard
          title='Permitted Amount to Invest'
          contractTypes={contractTypes}
          onSave={autorizeValueWallet}
        />
      </CardsContainer>
    </Container>
  )
}

export default WalletContainer
