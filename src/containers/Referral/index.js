import React from 'react'
import styled from 'styled-components'
import makeEthBlockie from 'ethereum-blockies-base64'
import Container from '../../components/Layout/Container'
import { Title, Subtitle } from '../../components/Typography'
import { ButtonPrimary } from '../../components/Buttons'
import { gt } from 'ramda'

const CopyButtonContainer = styled.div`
  margin: 2rem 0;
`

const CustomSubtitle = styled(Subtitle)`
  width: 80%;
`

const GridList = styled.ul`
  display: grid;
  grid-gap: 1rem;
  list-style: none;
  grid-template-columns: repeat(4, 1fr);

  &:not(:last-of-type) {
    margin-bottom: 2rem;
  }
`

const UserCard = styled.li`
  padding: 1rem;
  display: flex;
  align-items: center;
  background: #fff;
  border-radius: 2px;
  border: 1px solid #ccc;
`

const ProfilePicture = styled.img.attrs({
  alt: 'Ethereum blockie as a rounded picture'
})`
  width: 2.2rem;
  height: 2.2rem;
  border-radius: 50%;
  margin-right: 0.5rem;
  border: 1.5px solid #fff;
`

const ContractCard = styled.li`
  padding: 1rem;
  display: flex;
  align-items: center;
  background: #fff;
  border-radius: 2px;
  border: 1px solid #ccc;
`

const ReferralContainer = ({ referral, t, onCopyLink }) => {
  const url = 'dev.agryo.com/referral/' + referral.refId

  console.log(referral)

  return (
    <Container>
      <Title>{t('referral.title')}</Title>
      <CustomSubtitle>{t('referral.subtitle')}</CustomSubtitle>

      <CopyButtonContainer>
        <ButtonPrimary
          text={t('referral.copy-link')}
          onClick={() => onCopyLink(url)}
        />
      </CopyButtonContainer>

      {gt(referral.referralList.length, 0) && (
        <React.Fragment>
          <Title>People you referred to</Title>
          <GridList>
            {referral.referralList.map(person => (
              <UserCard key={person.walletPublicAddress}>
                <ProfilePicture
                  src={makeEthBlockie(person.walletPublicAddress)}
                />
                <h3>{person.name}</h3>
              </UserCard>
            ))}
          </GridList>
        </React.Fragment>
      )}

      {gt(referral.referralContracts.length, 0) && (
        <React.Fragment>
          <Title>Contracts created</Title>
          <GridList>
            {referral.referralContracts.map(contract => (
              <ContractCard key={contract.contractId}>
                <h3>
                  {contract.contractId} - {contract.status}
                </h3>
              </ContractCard>
            ))}
          </GridList>
        </React.Fragment>
      )}
    </Container>
  )
}

export default ReferralContainer
