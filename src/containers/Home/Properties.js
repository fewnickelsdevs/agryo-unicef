import React from 'react'
import styled from 'styled-components'

import { Title } from '../../components/Typography'
import normalizePropertyCoords from '../../formatters/normalizePropertyCoords'
import PropertyCard from '../../components/PropertyCard'
import { mappedStatus, getPropertyLocation } from '../../services/ceos_utils'

const PropertiesContainer = styled.div`
  ${({ theme }) => `
    margin-top: 3rem;

    display: grid;
    grid-gap: 1rem;
    grid-template-columns: 1fr 1fr 1fr;

    & > * {
      flex: 1;
    }

    ${theme.breakpoints.sm} {
      grid-template-columns: 1fr 1fr;
    }

    ${theme.breakpoints.xs} {
      grid-template-columns: 1fr;
    }
  `}
`

const SeeMoreButton = styled.div`
  height: 254px;
  min-width: 18rem;
  background: #eee;
  cursor: pointer;
  overflow: hidden;
  border-radius: 6px;
  border: 1px dashed #ccc;
  transition: 0.2s all;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  &:hover {
    background: #e1e1e1;
  }
`

const Label = styled.h4`
  color: #a6a6a6;
  text-align: center;
`

const Properties = ({ t, properties, onSeeMore }) => (
  <section>
    <Title>{t('properties.title')}</Title>

    <PropertiesContainer>
      {properties.map(({ geometry, properties }) => {
        const propertyId = properties.f1
        const hectares = properties.f4
        const coords = normalizePropertyCoords(geometry.coordinates)

        return (
          <PropertyCard
            t={t}
            onClick={() => { }}
            key={propertyId}
            active={false}
            status={mappedStatus(t)[properties.f3]}
            label={getPropertyLocation(properties)}
            hectares={hectares}
            locale={coords}
          />
        )
      })}

      <SeeMoreButton onClick={onSeeMore}>
        <Label>{t('properties.seeMore')}</Label>
      </SeeMoreButton>
    </PropertiesContainer>
  </section>
)

export default Properties
