import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled, { css } from 'styled-components'

import Row from '../../components/Layout/Row'
import { useContainerDimensions } from '../../services/getContainerWidth'
import TransparentOnlyChart from '../../components/Charts/TransparentOnlyChart'

const Container = styled.div`
  ${({ theme }) => `
    margin: 1rem;
    height: min-content;
    width: 300px;
    padding: 1rem;
    display: flex;
    max-width: 100%;
    background: white;
    flex-direction: column;
    border-radius: 30px;
    ${theme.breakpoints.sm} {
        width: 100%;
    }
`}
`

const InformationContainer = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;

    ${theme.breakpoints.xs} {
      width: 100%;
      flex-direction: row;
      justify-content: space-between;
    }
  `}
`

const Information = styled.p`
  ${({ theme, spaceLeft }) => css`
    padding: 0;
    color: ${theme.colors.primary};
    margin: 0.25rem 0;
    font-weight: bold;
    margin-left: ${spaceLeft || 0};

    ${theme.breakpoints.xs} {
      margin-left: 0 !important;
      margin-right: 5px;
    }
  `}
`

const InformationList = styled.ul`
  padding: 0;
  list-style: none;
  margin-top: 1rem;
`

const InformationItem = styled.li`
  ${({ theme }) => css`
    color: ${theme.colors.primary};
    display: flex;
    align-items: center;

    ${theme.breakpoints.sm} {
      flex-direction: row-reverse;
    }
  `}
`

const ChartContainer = styled.div`
  ${({ theme }) => css`
    margin-top: auto;
    margin-left: auto;
    border-radius: 8px;
    height: min-content;
  `}
`

const Column = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`

const SubHeading = styled.h1`
  ${({ theme }) => `
    margin: 0;
    color: ${theme.colors.primary};
    font-size: 1.4rem;
  `}
`

const ContainerCharts = styled.div`
  ${({ theme }) => `
  display: flex;
  flex-direction: row;
  align-items: center;
  ${theme.breakpoints.sm} {
    flex-direction: column;
  }
`}
`

function getData(weatherGraph) {
  if (!weatherGraph || !weatherGraph.f3.wspd10m) {
    return false
  }
  const labels = weatherGraph.f2.dates_s.map(({ dates_m, hours }) =>
    parseInt(dates_m / 1000 + hours)
  )
  const velocidadeVento = weatherGraph.f3.wspd10m.map(({ wspd10m }, index) => {
    return { value: wspd10m, time: labels[index] }
  })
  const umidade = weatherGraph.f4.rh2m.map(({ rh2m }, index) => {
    return { value: rh2m, time: labels[index] }
  })
  const probabilidadeChuva = weatherGraph.f5.pwatatm.map(
    ({ pwatatm }, index) => {
      return { value: pwatatm, time: labels[index] }
    }
  )
  const temperatura = weatherGraph.f6.t2m.map(({ t2m }, index) => {
    return { value: t2m, time: labels[index] }
  })

  return [temperatura, velocidadeVento, probabilidadeChuva, umidade]
}

export default function BoxClimate({ t, weatherData }) {
  const { weatherGraph, weatherInfo } = weatherData
  const dataGraph = getData(weatherGraph)
  const [screenWidth, setScreenWidth] = useState(window.innerWidth)
  const ref = React.useRef(null)
  const { width } = useContainerDimensions(ref)

  const updateScreenWidthState = () => {
    setScreenWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', updateScreenWidthState)
    return () => window.removeEventListener('resize', updateScreenWidthState)
  }, [])

  if (!weatherGraph || !weatherGraph.f3.wspd10m) return null
  const { city, country, state } = weatherGraph.f1.address[0]
  return (
    <div>
      {city && country && state && (
        <Row style={{ marginBottom: '1rem', marginTop: '1.75rem' }}>
          <h3>
            {city}/{state} - {country}
          </h3>
        </Row>
      )}
      <ContainerCharts>
        <Container ref={ref}>
          <Column>
            <InformationItem>
              <Information spaceLeft='.5rem'>
                Temperature: {weatherInfo.t2m.toFixed(2)}°C
              </Information>
            </InformationItem>
          </Column>
          <ChartContainer>
            <TransparentOnlyChart
              width={width}
              data={dataGraph[0]}
              containerId={'climateDataUser1'}
            />
          </ChartContainer>
        </Container>

        <Container ref={ref}>
          <Column>
            <InformationItem>
              <FontAwesomeIcon icon='tint' />
              <Information spaceLeft='.5rem'>
                Moisture: {weatherInfo.t2m.toFixed(2)}
              </Information>
            </InformationItem>
          </Column>
          <ChartContainer>
            <TransparentOnlyChart
              width={width}
              data={dataGraph[0]}
              containerId={'climateDataUser2'}
            />
          </ChartContainer>
        </Container>

        <Container ref={ref}>
          <Column>
            <InformationItem>
              <FontAwesomeIcon icon='wind' />
              <Information spaceLeft='.5rem'>
                {t('home.climate.wind')}: {weatherInfo.pwatatm}
              </Information>
            </InformationItem>
          </Column>
          <ChartContainer>
            <TransparentOnlyChart
              width={width}
              data={dataGraph[0]}
              containerId={'climateDataUser3'}
            />
          </ChartContainer>
        </Container>
      </ContainerCharts>
    </div>
  )
}
