import React from 'react'

import { CardsWrapper, SubHeading } from './styles'
import { Title } from '../../components/Typography'
import ContractsCard from '../../components/ContractsCard'
import Container from '../../components/Layout/Container'
import ClimateData from './ClimateData'
import Properties from './Properties'
import styled from 'styled-components'
import BoxClimate from './BoxClimate'

const StyledTitle = styled.h2`
  font-size: 1.5rem;
`

export default function HomeContainer({
  t,
  properties,
  weatherData,
  onSeeMore,
  redirectToContracts,
  userAccount
}) {
  const cards = [
    { icon: 'calculator', label: t('home.cards.costing'), count: 4 },
    {
      icon: 'check-circle',
      label: t('home.cards.secure'),
      count: 0,
      disabled: true
    },
    {
      icon: 'dollar-sign',
      label: t('home.cards.financing'),
      count: 0,
      disabled: true
    },
    { icon: 'clock', label: t('home.cards.sell'), count: 0, disabled: true }
  ]

  return (
    <Container>
      <Title>{t('home.title')}</Title>
      <StyledTitle>Agryo Account: {userAccount}</StyledTitle>

      <CardsWrapper>
        {cards.map(card => (
          <ContractsCard
            t={t}
            onClick={redirectToContracts}
            key={card.label}
            {...card}
          />
        ))}
      </CardsWrapper>

      <SubHeading>{t('home.climate.title')}</SubHeading>

      {/* <ClimateData t={t} weatherData={weatherData} /> */}
      <BoxClimate t={t} weatherData={weatherData} />
      {properties && (
        <Properties t={t} properties={properties} onSeeMore={onSeeMore} />
      )}
    </Container>
  )
}
