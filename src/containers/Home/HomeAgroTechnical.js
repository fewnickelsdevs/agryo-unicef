import React from 'react'

import { CardsWrapper } from './styles'
import { Title } from '../../components/Typography'
import Container from '../../components/Layout/Container'
import styled from 'styled-components'
import HomeDescriptionCard from '../../components/HomeDescriptionCard'

const StyledTitle = styled.h2`
  font-size: 1.5rem;
  margin-bottom: 1.2rem;
`

export default function HomeAgroTechnicalContainer({
  t,
  companyName,
  technicalName
}) {
  const cards = [
    {
      icon: 'clock',
      label: 'Farmers',
      description: 'Register and view your farmers',
      url: '/account/farmers'
    },
    {
      icon: 'dollar-sign',
      label: 'Contracts',
      description: 'View the situation of your farmers contracts',
      url: '/account/farmers/contracts'
    },
    {
      icon: 'clock',
      label: 'Properties',
      description: 'Check the farmers registered properties',
      url: '/account/farmers/properties'
    }
  ]

  return (
    <Container>
      <Title>{t('home.title')}</Title>
      <StyledTitle>Technical: {technicalName}</StyledTitle>
      <StyledTitle>Company: {companyName}</StyledTitle>

      <CardsWrapper>
        {cards.map(card => (
          <HomeDescriptionCard
            t={t}
            onClick={false}
            key={card.label}
            {...card}
          />
        ))}
      </CardsWrapper>
    </Container>
  )
}
