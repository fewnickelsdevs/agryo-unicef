import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'
import { Map, TileLayer, Polygon } from 'react-leaflet'
import styled from 'styled-components'

import Row from '../../components/Layout/Row'

const polygon = [
  [-15.53860333379654, -47.33813524246216],
  [-15.53774761550375, -47.33562416931153],
  [-15.540104379195682, -47.33594603439332]
]
const polygon2 = [
  [-15.535516832550282, -47.336361341608324],
  [-15.536178390135492, -47.33069651616887],
  [-15.540643848287429, -47.33391516698674]
]
const polygon3 = [
  [-15.53605434825002, -47.34189742101506],
  [-15.53506201047866, -47.337605886591234],
  [-15.538499966780147, -47.33802795410156]
]
const polygon4 = [
  [-15.53204202191164, -47.33494513524846],
  [-15.530760226440917, -47.331125669611254],
  [-15.535019064511012, -47.33099692357854]
]

const ColumnWrapper = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`

const InformationColumnWrapper = styled.div`
  margin-left: 1rem;

  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`

const Container = styled.div`
  ${({ theme }) => `
    max-width: 100%;
    height: 26rem;
    padding: 2rem;
    background: #fff;
    margin-bottom: 2rem;
    display: flex;

    ${theme.breakpoints.xs} {
      padding: 1.25rem;
      height: min-content;
      flex-direction: column;
    }
  `}
`

const Information = styled.div`
  display: grid;
  grid-template-columns: 20px 1fr;
  align-items: center;
  margin-bottom: 1rem;
`

const InformationText = styled.p`
  font-size: 1.05rem;
`

const LocationContainer = styled.div`
  ${({ theme }) => `
    margin: 2.4vw 0;

    ${theme.breakpoints.xs} {
      margin: 1.25rem 0;
    }
  `}
`

const MapContainer = styled.div`
  ${({ theme }) => `
    width: 60%;
    overflow: hidden;
    margin-top: auto;
    margin-left: auto;
    border-radius: 8px;
    border: 1px solid #ccc;

    ${theme.breakpoints.xs} {
      width: 100%;
      margin-top: 0.75rem;
    }
  `}
`

const MapSubtitle = styled.div`
  display: grid;
  padding: 1rem;
  grid-gap: 0.5rem;
  align-items: center;
  grid-template-rows: 1fr 1fr;
  grid-template-columns: 1fr 1fr;
`

const SubtitleColor = styled.div`
  width: 1rem;
  height: 1rem;
  border-radius: 4px;
  margin-right: 0.5rem;
  background: ${({ color }) => color};
`

const LeafletMap = styled(Map).attrs({
  zoom: 15
})`
  width: 100%;
  height: 14rem;
  z-index: 2;
`

export default function DataUsage({ t }) {
  return (
    <Container>
      <ColumnWrapper>
        <h3>{t('home.datausage.title')}</h3>

        <LocationContainer>
          <h2>Formosa GO - Brasil</h2>
          <h4>17,3 ha</h4>
        </LocationContainer>

        <Information>
          <FontAwesomeIcon icon='border-style' size='lg' />

          <InformationColumnWrapper>
            <h3>{t('home.datausage.slope')}</h3>

            <InformationText>{t('home.datausage.max')}: 143</InformationText>

            <InformationText>{t('home.datausage.min')}: 0</InformationText>
          </InformationColumnWrapper>
        </Information>

        <Information>
          <FontAwesomeIcon icon='tree' size='lg' />

          <InformationColumnWrapper>
            <h3>{t('home.datausage.groundtype')}</h3>

            <InformationText>Humoso</InformationText>
          </InformationColumnWrapper>
        </Information>

        <Information>
          <FontAwesomeIcon icon='history' size='lg' />

          <InformationColumnWrapper>
            <h3>{t('home.datausage.cultivation')}</h3>

            <Row>
              <Link to=''>2017</Link> &nbsp; - &nbsp; <Link to=''>2018</Link>
              &nbsp; - &nbsp;
              <Link to=''>2019</Link>
            </Row>
          </InformationColumnWrapper>
        </Information>
      </ColumnWrapper>

      <MapContainer>
        <LeafletMap center={polygon[0]}>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          />

          <Polygon color='#F02323' positions={polygon} />
          <Polygon color='#FFB305' positions={polygon2} />
          <Polygon color='#5060B4' positions={polygon3} />
          <Polygon color='#49C16E' positions={polygon4} />
        </LeafletMap>

        <MapSubtitle>
          <Row>
            <SubtitleColor color='#F02323' />
            <span>{t('home.datausage.boundaries')}</span>
          </Row>

          <Row>
            <SubtitleColor color='#FFB305' />
            <span>{t('home.datausage.forest')}</span>
          </Row>

          <Row>
            <SubtitleColor color='#5060B4' />
            <span>{t('home.datausage.water')}</span>
          </Row>

          <Row>
            <SubtitleColor color='#49C16E' />
            <span>{t('home.datausage.preservation')}</span>
          </Row>
        </MapSubtitle>
      </MapContainer>
    </Container>
  )
}
