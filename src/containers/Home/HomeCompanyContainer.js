import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { CardsWrapper } from './styles'
import { Title, Subtitle } from '../../components/Typography'
import Container from '../../components/Layout/Container'
import LoadingContainer from '../../containers/Loading'
import HomeDescriptionCard from '../../components/HomeDescriptionCard'
import { REQUEST_COMPANY_CONTRACTS } from '../../redux/contract/constants'
import MapContractListContainer from '../../containers/ContractsList/MapContractListContainer'
import { useHistory } from 'react-router-dom'


const mapStateToProps = state => ({ contractStore: state.contract })

function HomeCompanyContainer({ contractStore, dispatch, t, companyName }) {
  const history = useHistory()
  useEffect(() => {
    dispatch({ type: REQUEST_COMPANY_CONTRACTS })
  }, [dispatch])

  const cards = [
    {
      icon: 'calculator',
      label: 'Agro Technicals',
      description: '',
      url: '/account/company/technicals'
    },
    {
      icon: 'dollar-sign',
      label: 'Contracts',
      description: '',
      url: '/account/company/contracts'
    },
    {
      icon: 'clock',
      label: 'Farmers',
      description: '',
      url: '/account/company/farmers'
    }
  ]
  const goToContractDetails = (contractType, contractId, contractData) => {
    const url =
      contractData.status === 'PENDING'
        ? '/account/contracts/pending/details'
        : '/account/contracts/details'
    history.push({
      pathname: url,
      state: { contractType, contractId, contractData, noInvest: true }
    })
  }

  if (contractStore.loading) {
    return <LoadingContainer />
  }

  return (
    <Container>
      <Title>{t('home.title')}</Title>
      <Subtitle>
        {t('entities.company')}:{companyName}
      </Subtitle>

      <CardsWrapper>
        {cards.map(card => (
          <HomeDescriptionCard
            t={t}
            onClick={false}
            key={card.label}
            {...card}
          />
        ))}
      </CardsWrapper>
      <MapContractListContainer
        t={t}
        showSideBar={false}
        contracts={contractStore.contracts}
        goToDetails={goToContractDetails}
      />
    </Container>
  )
}
export default connect(mapStateToProps)(HomeCompanyContainer)
