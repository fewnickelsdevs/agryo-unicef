import styled from 'styled-components'

export const SubHeading = styled.h1`
  ${({ light }) => `
    margin: 0;
    color: ${light ? '#fff' : 'auto'};
    font-size: 1.4rem;
  `}
`

export const CardsWrapper = styled.div`
  ${({ theme }) => `
    display: flex;
    align-items: center;
    margin: 15px 0;
    width: 100%;
    height: auto;
    overflow-x: hidden;
    overflow-x: auto;

    & > * {
      margin-right: 1rem;
    }

    ${theme.breakpoints.xs} {
      margin: 2rem 0 3rem;
    }
  `}
`
