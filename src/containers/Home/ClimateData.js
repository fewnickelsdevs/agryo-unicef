import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled, { css } from 'styled-components'

import { SubHeading } from './styles'
import Row from '../../components/Layout/Row'
import ThreeAreaLineScaleChart from '../../components/Charts/ThreeLineScaleChart'
import { useContainerDimensions } from '../../services/getContainerWidth'

const PropertyContainer = styled.div`
  ${({ theme }) => css`
    display: flex;
    align-items: center;
    margin-left: auto;

    ${theme.breakpoints.xs} {
      & h3 {
        display: none;
      }
    }
  `}
`

const Container = styled.div`
  ${({ theme }) => css`
    height: 26rem;
    padding: 2rem;
    display: flex;
    max-width: 100%;
    margin-bottom: 2rem;
    background: ${theme.colors.primary};

    ${theme.breakpoints.xs} {
      height: 35rem;
      padding: 2rem 1.3rem;
      flex-direction: column;
    }
  `}
`

const Temperature = styled.h1`
  ${({ theme }) => css`
    margin: 0;
    color: #fff;
    font-size: 3rem;
    position: relative;

    &:before {
      top: 0.7rem;
      left: 90%;
      content: '°C';
      font-size: 1rem;
      position: absolute;
    }

    ${theme.breakpoints.xs} {
      font-size: 3.5rem;

      &:before {
        top: 0.7rem;
        left: 100%;
      }
    }
  `}
`

const InformationContainer = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;

    ${theme.breakpoints.xs} {
      width: 100%;
      flex-direction: row;
      justify-content: space-between;
    }
  `}
`

const Information = styled.p`
  ${({ theme, spaceLeft }) => css`
    padding: 0;
    color: #fff;
    margin: 0.25rem 0;
    font-weight: bold;
    margin-left: ${spaceLeft || 0};

    ${theme.breakpoints.xs} {
      margin-left: 0 !important;
      margin-right: 5px;
    }
  `}
`

const InformationList = styled.ul`
  padding: 0;
  list-style: none;
  margin-top: 1rem;
`

const InformationItem = styled.li`
  ${({ theme }) => css`
    color: #fff;
    display: flex;
    align-items: center;

    ${theme.breakpoints.xs} {
      flex-direction: row-reverse;
    }
  `}
`

const ChartContainer = styled.div`
  ${({ theme }) => css`
    width: 60%;
    padding: 0.7rem;
    background: #fff;
    margin-top: auto;
    margin-left: auto;
    border-radius: 8px;
    height: min-content;

    ${theme.breakpoints.xs} {
      width: 100%;
    }
  `}
`

const Column = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
`

function getData(weatherGraph) {
  if (!weatherGraph || !weatherGraph.f3.wspd10m) {
    return false
  }
  const labels = weatherGraph.f2.dates_s.map(({ dates_m, hours }) =>
    parseInt(dates_m / 1000 + hours)
  )
  const velocidadeVento = weatherGraph.f3.wspd10m.map(({ wspd10m }, index) => {
    return { value: wspd10m, time: labels[index] }
  })
  const umidade = weatherGraph.f4.rh2m.map(({ rh2m }, index) => {
    return { value: rh2m, time: labels[index] }
  })
  const probabilidadeChuva = weatherGraph.f5.pwatatm.map(
    ({ pwatatm }, index) => {
      return { value: pwatatm, time: labels[index] }
    }
  )
  const temperatura = weatherGraph.f6.t2m.map(({ t2m }, index) => {
    return { value: t2m, time: labels[index] }
  })

  return [temperatura, velocidadeVento, probabilidadeChuva, umidade]
}

export default function ClimateData({ t, weatherData }) {
  const { weatherGraph, weatherInfo } = weatherData
  const dataGraph = getData(weatherGraph)
  const [screenWidth, setScreenWidth] = useState(window.innerWidth)
  const ref = React.useRef(null)
  //const { width, height } = useContainerDimensions(ref)

  const updateScreenWidthState = () => {
    setScreenWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', updateScreenWidthState)
    return () => window.removeEventListener('resize', updateScreenWidthState)
  }, [])

  if (!weatherGraph || !weatherGraph.f3.wspd10m) return null
  const { city, country, state } = weatherGraph.f1.address[0]
  return (
    <div>
      {city && country && state && (
        <Row style={{ marginBottom: '1rem', marginTop: '1.75rem' }}>
          <h3>
            {city}/{state} - {country}
          </h3>
          {/* <PropertyContainer>
          <h3>{t('home.climate.property')}: </h3>

          <Dropdown
            color={colors.primary}
            options={[{ label: `${city} - ${country}` }]}
            title={`${city} - ${country}`}
          />
        </PropertyContainer> */}
        </Row>
      )}

      <Container>
        <Column>
          <SubHeading light>{t('home.climate.title')}</SubHeading>

          <InformationContainer>
            <div>
              <Temperature>{weatherInfo.t2m.toFixed(2)}</Temperature>
              {/* <Information>{t('home.climate.max')}: 24°</Information>
              <Information>{t('home.climate.min')}: 12°</Information> */}
            </div>

            <InformationList>
              <InformationItem>
                <FontAwesomeIcon icon='tint' />
                <Information spaceLeft='.5rem'>
                  {t('home.climate.humidity')}: {weatherInfo.rh2m.toFixed(2)}%
                </Information>
              </InformationItem>

              <InformationItem>
                <FontAwesomeIcon icon='cloud-rain' />
                <Information spaceLeft='.5rem'>
                  {t('home.climate.rain')}: {weatherInfo.pwatatm}%
                </Information>
              </InformationItem>

              {/* <InformationItem>
                <FontAwesomeIcon icon='sun' />
                <Information spaceLeft='.5rem'>
                  {t('home.climate.sun')}: 10h
                </Information>
              </InformationItem> */}

              <InformationItem>
                <FontAwesomeIcon icon='wind' />
                <Information spaceLeft='.5rem'>
                  {t('home.climate.wind')}: {weatherInfo.wspd10m.toFixed(2)}km/h
                </Information>
              </InformationItem>
            </InformationList>
          </InformationContainer>
        </Column>

        <ChartContainer ref={ref}>
          <ThreeAreaLineScaleChart
            data={dataGraph}
            containerId={'climateDataUser'}
          />
          {/* <Line
            key={screenWidth}
            data={dataGraph}
            height={screenWidth >= 768 ? 150 : 260}
          /> */}
        </ChartContainer>
      </Container>
    </div>
  )
}
