import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { subDays } from 'date-fns'
import { TableContainer } from '../../components/Table'
import GraphBox from './GraphBox'
import { useContainerDimensions } from '../../services/getContainerWidth'

const ChartContainer = styled.div`
  margin-top: auto;
  margin-left: auto;
  margin-right: auto;
  border-radius: 8px;
  height: min-content;
  width: 100%;
`

const TabRow = styled.div`
  overflow: hidden;
  background-color: inherit;
`

const TabItem = styled.div`
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  ${({ active }) =>
    active &&
    `{
    border-radius: 5px 5px 0px 0px; 
    background-color: #cae6ca;
  }`}
`

const Charts = ({ data, t }) => {
  const [activeItem, setActiveItem] = useState('landsat')
  const [rootzone, setRootZone] = useState(false)
  const [smprofile, setSmProfile] = useState(false)
  const [landsat, setLandsat] = useState(false)
  const [carbon, setCarbon] = useState(false)
  const [modis, setModis] = useState(false)
  const dateLimit = subDays(new Date(), 200)
  const timestampDateLimit = dateLimit.getTime()

  const ref = React.useRef(null)
  const { width } = useContainerDimensions(ref)

  function isInsideDateLimit(value) {
    return value >= timestampDateLimit
  }

  const getProfileRootZone = () => {
    if (!data[2].data.graph.lines) return false
    try {
      const smap = data[2].data.graph.lines.sort(
        (a, b) => parseFloat(a.dates_m) - parseFloat(b.dates_m)
      )
      const smprofile = smap.map(({ smp, dates_m }) => {
        return {
          value: smp,
          time: new Date(dates_m).toLocaleDateString('en-US')
        }
      })
      const rootzone = smap.map(({ ssm, dates_m }) => {
        return {
          value: ssm,
          time: new Date(dates_m).toLocaleDateString('en-US')
        }
      })
      setRootZone(rootzone)
      setSmProfile(smprofile)
    } catch (err) {
      return false
    }
  }

  const getLandSat = () => {
    if (!data[0].data.graph.lines) return false
    try {
      const landsatOrdenate = data[0].data.graph.lines.sort(
        (a, b) => a.dates_m - b.dates_m
      )
      const landsatLabels = landsatOrdenate.map(({ dates_m, meanval }) => {
        return {
          value: meanval,
          time: new Date(dates_m).toLocaleDateString('en-US')
        }
      })
      setLandsat(landsatLabels)
    } catch (err) {
      return false
    }
  }

  const getModis = () => {
    if (!data[1].data.graph.lines) return false
    try {
      const modisOrdenate = data[1].data.graph.lines.sort(
        (a, b) => parseFloat(a.dates_m) - parseFloat(b.dates_m)
      )
      const modisLabels = modisOrdenate.map(({ dates_m, meanval }) => {
        return {
          value: meanval,
          time: new Date(dates_m).toLocaleDateString('en-US')
        }
      })
      setModis(modisLabels)
    } catch (err) {
      return false
    }
  }


  const getCarbon = () => {
    if (!data[3].data) return false
    try {
      const carbonOrdenate = data[3].data.sort(
        (a, b) => parseFloat(a.date) - parseFloat(b.date)
      )
        console.log(carbonOrdenate)
      const carbonLabels = carbonOrdenate.map(({ date, carbonGrowth }) => {
        return {
          value: carbonGrowth,
          time: new Date(date).toLocaleDateString('en-US')
        }
      })
      console.log(carbonLabels)

      setCarbon(carbonLabels)
    } catch (err) {
      return false
    }
  }


  useEffect(() => {
    getProfileRootZone()
    getLandSat()
    getModis()
    getCarbon()
  }, [])

  const tabItems = [
    { label: 'properties.details.landsat', value: 'landsat' },
    { label: 'properties.details.modis', value: 'modis' },
    { label: 'properties.details.sm-profile', value: 'smprofile' },
    { label: 'properties.details.sm-root', value: 'rootzone' },
    { label: 'properties.details.carbon', value: 'carbon'}
  ]


  return (
    <div>
      <TabRow ref={ref}>
        {tabItems.map(x => (
          <TabItem
            active={activeItem == x.value}
            onClick={() => setActiveItem(x.value)}
          >
            {t(x.label)}
          </TabItem>
        ))}
      </TabRow>
      <TableContainer>
        <ChartContainer>
          {activeItem === 'smprofile' && (
            <GraphBox
              containerId='smprofilegraphproperty'
              data={smprofile}
              width={width}
            />
          )}
          {activeItem === 'rootzone' && (
            <GraphBox
              containerId='rootzonegraphproperty'
              data={rootzone}
              width={width}
            />
          )}
          {activeItem === 'landsat' && (
            <GraphBox
              containerId='landsatgraphproperty'
              data={landsat}
              width={width}
            />
          )}
          {activeItem === 'modis' && (
            <GraphBox
              containerId='modisgraphproperty'
              data={modis}
              width={width}
            />
          )}
          {activeItem === 'carbon' && (
            <GraphBox
              containerId='carbongraphproperty'
              data={carbon}
              width={width}
            />
          )}
        </ChartContainer>
      </TableContainer>
    </div>
  )
}

Charts.propTypes = {
  data: PropTypes.array.isRequired
}

export default Charts
