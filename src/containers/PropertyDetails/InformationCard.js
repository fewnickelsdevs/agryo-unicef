import React from 'react'
import {
  TableContainer,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell
} from '../../components/Table'

const InformationCard = ({ t, title, data }) => (
  <TableContainer>
    <TableHead>
      <TableRow>
        <TableHeadCell>{title}</TableHeadCell>
        <TableHeadCell></TableHeadCell>
      </TableRow>
    </TableHead>

    <TableBody>
      <TableRow striped>
        <TableCell>{t('properties.details.risk-history')}</TableCell>
        <TableCell>{data.croprisklevel}</TableCell>
      </TableRow>
    </TableBody>
  </TableContainer>
)

export default InformationCard
