import React, { useEffect } from 'react'
import { createChart } from 'lightweight-charts'

var darkTheme = {
  chart: {
    layout: {
      backgroundColor: '#f1f2f8',
      lineColor: '#2B2B43',
      textColor: 'black'
    },
    watermark: {
      color: 'rgba(0, 0, 0, 0)'
    },
    crosshair: {
      color: '#758696'
    },
    grid: {
      vertLines: {
        color: '#f1f2f8'
      },
      horzLines: {
        color: '#f1f2f8'
      }
    }
  },
  series: {
    topColor: 'rgba(76, 175, 80, 1)',
    lineColor: '#3F8C48',
    bottomColor: 'rgba(76, 175, 80, 0)',
    lineWidth: 1
  }
}

const GraphBox = ({ width, data, containerId }) => {
  const [chart, setChart] = React.useState(false)
  const [height, setHeight] = React.useState(300)

  const updateWidth = width => {
    if (!chart) {
      return
    }

    const heightX = width > 750 ? 400 : 300

    if (!(height === heightX)) {
      setHeight(heightX + 30)
    }

    chart.resize(width, heightX)
    chart.timeScale().fitContent()
  }

  useEffect(() => {
    updateWidth(width)
  }, [width])

  const generateChart = () => {
    const height = width > 750 ? 400 : 300
    let chart = createChart(containerId, { width, height })

    var areaSeries = chart.addAreaSeries()
    areaSeries.applyOptions(darkTheme.series)
    chart.applyOptions(darkTheme.chart)
    areaSeries.setData(data || [{ value: 0, date: '2019-05-06' }])
    chart.timeScale().fitContent()
    setChart(chart)
  }

  React.useEffect(() => {
    if (!data) {
      return
    }
    generateChart()
  }, [data])

  return (
    <div style={{ height: 400 }}>
      <div id={containerId} className={'LightweightChart'} />
    </div>
  )
}

export default GraphBox
