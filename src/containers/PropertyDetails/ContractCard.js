import React from 'react'
import styled, { css } from 'styled-components'

const Container = styled.li`
  padding: 1rem;
  cursor: pointer;
  background: #fff;
  border-radius: 2px;
  border: 1px solid #ccc;
`

const ContractType = styled.span(
  ({ theme }) => css`
    font-weight: bold;
    color: ${theme.colors.primary};
    font-size: 12px;
  `
)

const ContractName = styled.h3`
  color: #555;
  margin-top: 5px;
  font-weight: normal;
`

const Status = styled.span`
  color: #aaa;
`

const ContractCard = ({ prefix, contract, status, onClick }) => (
  <Container onClick={onClick}>
    <ContractType>{contract.type}</ContractType>

    <ContractName>{prefix + ' ' + contract.contractGeneralID}</ContractName>

    <Status>{status}</Status>
  </Container>
)

export default ContractCard
