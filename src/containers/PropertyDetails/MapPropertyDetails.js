import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import agryoLogo from '../../assets/agryo-sm-white.svg'

import {
  Map,
  Polygon,
  LayersControl,
  TileLayer,
  ZoomControl
} from 'react-leaflet'
import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'
import { Sidebar, Tab } from 'react-leaflet-sidebarv2'
import FullscreenControl from 'react-leaflet-fullscreen'
import 'react-leaflet-fullscreen-control'
const { BaseLayer } = LayersControl

const PropertyMap = styled(Map).attrs({
  zoom: 10,
  zoomControl: false
})`
  width: 100%;
  height: 500px;
  padding: 10px;
  z-index: 2;
`

const BrandLogo = styled.img.attrs({
  alt: 'Agryo logo'
})`
  height: 2rem;
  margin-top: 7px;
`

const mappedStatus = t => ({
  0: t('properties.status.processing'),
  1: t('properties.status.confirmed'),
  2: t('properties.status.invalid'),
  3: t('properties.status.error')
})

export default function MapPropertyDetails({ bounds, onClick, data, t }) {
  const [collapsed, setCollapsed] = useState(false)
  const [selected, setSelected] = useState('home')

  return (
    <React.Fragment>
      <PropertyMap bounds={bounds} onClick={onClick}>
        <LayersControl>
          <BaseLayer name='Satelite' checked>
            <BingLayer bingkey={bingKey} type='Aerial' />
          </BaseLayer>
          <BaseLayer name='Map'>
            <TileLayer
              attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
            />
          </BaseLayer>
        </LayersControl>
        <Polygon color='green' positions={bounds} />

        <Sidebar
          id='sidebar'
          collapsed={collapsed}
          selected={selected}
          autopan={true}
        >
          <Tab
            id='home'
            header={'Property Details'}
            icon={
              <div>
                <BrandLogo src={agryoLogo} />
              </div>
            }
          >
            <div style={{ paddingLeft: 10 }}>
              <p>
                <b>Status</b>
              </p>
              <p style={{ fontSize: 22, marginBottom: 10 }}>
                {mappedStatus(t)[data.f3]}
              </p>

              <p>
                <b>City</b>
              </p>
              <p style={{ fontSize: 22, marginBottom: 10 }}>{data.f7}</p>
              <p>
                <b>Country</b>
              </p>
              <p style={{ fontSize: 22, marginBottom: 10 }}>{data.f5}</p>
              <p>
                <b>Hectares</b>
              </p>
              <p style={{ fontSize: 22, marginBottom: 10 }}>{data.f4}</p>
            </div>
          </Tab>
        </Sidebar>

        <ZoomControl position='bottomright' />
        <FullscreenControl position='bottomright' />
      </PropertyMap>
    </React.Fragment>
  )
}

MapPropertyDetails.propTypes = {
  active: PropTypes.bool.isRequired,
  locale: PropTypes.array.isRequired,
  status: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

MapPropertyDetails.defaultProps = {
  responsive: true,
  useGutter: false
}
