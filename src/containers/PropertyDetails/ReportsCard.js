import React, { useState } from 'react'
import {
  TableContainer,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell
} from '../../components/Table'
import IconButtonDownloadDropdown from '../../components/Dropdown/IconButtonDownloadDropdown'
import ReportProofDialog from '../../components/Dialogs/ReportProofDialog'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

const reportType = {
  rev: 'properties.details.reports.enviromental',
  rg: 'properties.details.reports.enviromental',
  rag: 'properties.details.reports.agronomic',
  rfin: 'properties.details.reports.financial',
  rf: 'properties.details.reports.fraud',
  rinv: 'properties.details.reports.invalid',
  r1: 'properties.details.reports.r1',
  r2: 'properties.details.reports.r2',
  r3: 'properties.details.reports.r3',
  r4: 'properties.details.reports.r4',
  r5: 'properties.details.reports.r5',
  r6: 'properties.details.reports.r6'
}

const Icon = styled(FontAwesomeIcon)`
  color: grey;
  height: 2.65rem;
  padding: 0.6rem;
  border-radius: 50%;
  margin-left: 10px;
  width: 2.65rem !important;
  &:hover {
    background-color: #0080001a;
  }
`

const ReportsCard = ({ t, title, data, propertyId }) => {
  const [detailsModal, setDetailsModal] = useState(false)
  return (
    <TableContainer>
      <TableHead>
        <TableRow>
          <TableHeadCell>{title}</TableHeadCell>
          <TableHeadCell></TableHeadCell>
        </TableRow>
      </TableHead>

      <TableBody>
        {data.map(reports => {
          const { title, s3patheng, s3pathpor } = reports
          const options = [
            {
              label: t('properties.details.reports.download-pt'),
              url: s3pathpor
            },
            {
              label: t('properties.details.reports.download-en'),
              url: s3patheng
            }
          ]
          return (
            <TableRow striped>
              <TableCell>{t(reportType[title])}</TableCell>
              <TableCell style={{ float: 'right', width: 20 }}>
                <IconButtonDownloadDropdown options={options} />
                <Icon
                  style={{ float: 'right' }}
                  onClick={() => setDetailsModal(reports)}
                  icon={faSearch}
                />
              </TableCell>
            </TableRow>
          )
        })}
      </TableBody>
      <ReportProofDialog
        t={t}
        reportData={detailsModal}
        reportType={reportType}
        onCancel={() => setDetailsModal(false)}
        propertyId={propertyId}
      />
    </TableContainer>
  )
}
export default ReportsCard
