import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Map, Polygon, LayersControl, TileLayer } from 'react-leaflet'
import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'
import {
  TableContainer,
  TableHead,
  TableRow,
  TableHeadCell,
  TableBody,
  TableCell
} from '../../components/Table'
import MapPropertyDetails from './MapPropertyDetails'
const { BaseLayer } = LayersControl

const PropertyMap = styled(Map).attrs({
  zoom: 13
})`
  width: 100%;
  height: 240px !important;
  z-index: 2;
  height: 10rem;
`

const PropertyData = styled.div`
  padding: 1rem;
`

const mappedStatus = t => ({
  0: t('properties.status.processing'),
  1: t('properties.status.confirmed'),
  2: t('properties.status.invalid'),
  3: t('properties.status.error')
})

export default function PropertyBasicInfoCard({
  title,
  locale,
  onClick,
  data,
  t
}) {
  return (
    <React.Fragment>
      <MapPropertyDetails t={t} bounds={locale} data={data} onClick={onClick} />

      {/* <TableContainer>
        <TableHead>
        <TableRow>
            <TableHeadCell>{title}</TableHeadCell>
            <TableHeadCell></TableHeadCell>
        </TableRow>
        </TableHead>
      <TableBody>
          <TableRow striped>
            <TableCell>Local</TableCell>
            <TableCell>
            {`${data.f7} - ${data.f5}`}
            </TableCell>
          </TableRow>
          <TableRow striped>
            <TableCell>Hectares</TableCell>
            <TableCell>
            {data.f4}
            </TableCell>
          </TableRow>
          <TableRow striped>
            <TableCell>Status</TableCell>
            <TableCell>
            {mappedStatus(t)[data.f3]}
            </TableCell>
          </TableRow>
    </TableBody>
    </TableContainer> */}
    </React.Fragment>
  )
}

PropertyBasicInfoCard.propTypes = {
  active: PropTypes.bool.isRequired,
  locale: PropTypes.array.isRequired,
  status: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

PropertyBasicInfoCard.defaultProps = {
  responsive: true,
  useGutter: false
}
