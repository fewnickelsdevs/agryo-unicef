import React from 'react'
import styled from 'styled-components'
import Charts from './Charts'
import InformationCard from './InformationCard'
import ReportsCard from './ReportsCard'
import { Title, Subtitle } from '../../components/Typography'
import Container from '../../components/Layout/Container'
import Gallery from '../../components/Gallery'
import formatChartPayload from '../../formatters/formatChartPayload'
import PolygonMapContainer from '../PolygonMap/PolygonMapContainer';

const SectionContainer = styled.section`
  margin-bottom: 2rem;
`

const Heading = styled.div`
  margin-bottom: 1.75rem;
`


class PropertyDetailsContainer extends React.Component {



  renderHeader() {
    const { t, property } = this.props

    const { basicInfo } = property.details
    if (!basicInfo) {
      return null
    }

    return (
      <Heading>
        <Subtitle>{t('properties.details.title')}</Subtitle>
        <Title>
          {t('properties.details.subtitle')} {basicInfo.properties.f1}
        </Title>
      </Heading>
    )
  }

  render() {
    const { t, property, onClickContract } = this.props
    const {
      informations,
      gallery,
      charts,
      reports,
      basicInfo,
      carbon
    } = property.details
    const confirmed = basicInfo ? basicInfo.properties.f3 === '1' : false

    return (
      <Container>
        <SectionContainer>
          {this.renderHeader()}
         <PolygonMapContainer property={property.details} />
        </SectionContainer>

        {!!informations && confirmed && (
          <SectionContainer>
            <InformationCard
              t={t}
              title={t('properties.details.informations')}
              data={property.details.informations}
            />
          </SectionContainer>
        )}

        {reports && reports.initial.length > 0 && (
          <SectionContainer>
            <ReportsCard
              t={t}
              title={'Initial Reports'}
              data={reports.initial}
              propertyId={basicInfo.properties.f1}
            />
          </SectionContainer>
        )}

        {gallery && confirmed && (
          <SectionContainer>
            <Gallery
              title={t('properties.details.gallery')}
              galleryData={gallery}
            />
          </SectionContainer>
        )}

        {!!charts && confirmed && (
          <SectionContainer>
            <Charts t={t} data={formatChartPayload({...charts, carbon})} />
          </SectionContainer>
        )}
      </Container>
    )
  }
}

export default PropertyDetailsContainer
