import React, { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import L from 'leaflet'
import {
  Map,
  Marker,
  TileLayer,
  ZoomControl,
  LayersControl,
  Polygon
} from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import 'react-leaflet-markercluster/dist/styles.min.css'
import FullscreenControl from 'react-leaflet-fullscreen'
import 'react-leaflet-fullscreen-control'
import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'
import agryoLogo from '../../assets/agryo-sm-white.svg'
import shadow from '../../assets/suitcaseIcon.svg'
import DevInvestmenSidebar from './DevInvestmentSidebar'
import { fetchPropertyPolygonApi } from '../../redux/property/utils'
import normalizePropertyCoords from '../../formatters/normalizePropertyCoords'
import { generateContractTypeAndId } from '../../services/contract_utils';

const { BaseLayer } = LayersControl

const PropertyMap = styled(Map)`
  width: 100%;
  z-index: 2;
  height: 80vh;
  margin-top: 1rem;
`
const BrandLogo = styled.img.attrs({
  alt: 'Agryo logo'
})`
  height: 2rem;
  margin-top: 7px;
`

const ContractTypeItem = styled.button`
  cursor: pointer;
  font-size: 1rem;
  font-weight: 600;
  border-radius: 6px;
  padding: 0.6rem 1rem;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  margin-right: 1rem;
  margin-bottom: 0.6rem;
`

const selectedContractIcon = new L.Icon({
  iconUrl: agryoLogo,
  iconRetinaUrl: agryoLogo,
  iconAnchor: [20, 40],
  popupAnchor: [0, -35],
  iconSize: [40, 40],
  shadowUrl: shadow,
  shadowSize: [0, 0],
  shadowAnchor: [0, 0]
})

const DevInvestmentMap = ({
  t,
  onClickContract,
  contracts,
  showSideBar,
  visibleModal,
  contractData,
  hideContractModal,
  onSelectContract,
  contractTypes,
  selectTypeContract,
  goToContractDetails
}) => {
  const [collapsed, setCollapsed] = useState(true)
  const [polygonContract, setPolygonContract] = useState(false)
  const [selected, setSelected] = useState('')
  const [mapZoom, setMapZoom] = useState(2)
  const [center, setCenter] = useState([-15.7801, -47.9292])
  const sidebarRefControl = useRef(null)

  const onClose = () => {
    setSelected('')
    setCollapsed(true)
  }

  const onOpen = id => {
    if (!contractData && id === 'home') return
    if (id == selected && !collapsed) return
    setSelected(id)
    setCollapsed(false)
  }

  useEffect(() => {
    if (contractData && visibleModal) {
      setCollapsed(false)
    } else {
    }
  }, [contractData, visibleModal])

  const onClickOnMarkerContract = async contract => {
    setSelected('home')
    setPolygonContract(false)
    onClickContract(contract)
    setMapZoom(18)
    setCenter([contract.propertyLat, contract.propertyLng])
    const contractInfo = generateContractTypeAndId(contract.contractType, contract.id)
    contract.contractId = contractInfo.contractId
    const polygon = await fetchPropertyPolygonApi(contract.contractId)
    setPolygonContract(polygon.coordinates)
  }

  useEffect(() => {
    if (contractData && visibleModal) {
      setCollapsed(false)
    } else {
    }
  }, [contractData, visibleModal])

  return (
    <PropertyMap
      center={center}
      zoom={mapZoom}
      maxZoom={16}
      zoomControl={false}
      ref={sidebarRefControl}
    >
      {
        showSideBar && <DevInvestmenSidebar
          t={t}
          visibleModal={visibleModal}
          contractData={contractData}
          onSelectContract={onSelectContract}
          contractTypes={contractTypes}
          selectTypeContract={selectTypeContract}
          onClose={onClose}
          onOpen={onOpen}
          collapsed={collapsed}
          selected={selected}
          goToContractDetails={goToContractDetails}
        />
      }
      <LayersControl>
        <BaseLayer name='Satelite' checked>
          <BingLayer bingkey={bingKey} type='Aerial' />
        </BaseLayer>
        <BaseLayer name='Map'>
          <TileLayer
            attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          />
        </BaseLayer>
      </LayersControl>

      <ZoomControl position='bottomright' />
      <FullscreenControl position='bottomright' />

      {contractData && !polygonContract && (
        <Marker
          position={{
            lat: contractData.propertyLat,
            lng: contractData.propertyLng
          }}
          icon={selectedContractIcon}
        />
      )}

      {polygonContract && (
        <Polygon
          color='green'
          positions={normalizePropertyCoords(polygonContract)}
        />
      )}

      <MarkerClusterGroup>
        {contracts.map(contract => {
          if (
            !contract.propertyLat ||
            contractData.propertyLat == contract.propertyLat
          )
            return null

          return (
            <Marker
              key={contract.id}
              onClick={() => onClickOnMarkerContract(contract)}
              position={{
                lat: contract.propertyLat,
                lng: contract.propertyLng
              }}
            />
          )
        })}
      </MarkerClusterGroup>
    </PropertyMap>
  )
}

DevInvestmentMap.propTypes = {
  onClickContract: PropTypes.func.isRequired,
  contracts: PropTypes.array.isRequired
}

DevInvestmentMap.defaultProps = {
  showSideBar: true
}

export default DevInvestmentMap
