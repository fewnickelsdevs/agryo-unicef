import React from 'react'
import styled from 'styled-components'
import { Link, useHistory } from 'react-router-dom'
import currency from 'currency.js'

import { Title } from '../../../components/Typography'
import Container from '../../../components/Layout/Container'
import { CURRENCY_SYMBOL } from '../../../mocks/currency';
import { removeDecimals } from '../../../web3/contracts';

const Table = styled.table`
  width: 100%;
  padding: 1rem;
  background: #fff;
  border-radius: 5px;
`

const TableRow = styled.tr`
  & > * {
    padding: 1px;
  }

  font-size: 1.25rem;
`

const HrefLink = styled.td`
  ${({ theme }) => `
    cursor: pointer;
    text-decoration: underline;
    color: ${theme.colors.primary};
  `}
`

const ContractsListContainer = ({ contracts }) => {
  const history = useHistory()

  const formatedTransactionHash = hash => {
    const reducedHash = hash
      .split('')
      .splice(0, 8)
      .join('')
    return `${reducedHash}...`
  }

  const navigateToBlockchainExplorer = hash => {
    window.open(
      `https://rinkeby.etherscan.io/tx/${hash}`,
      '__blank'
    )
  }

  // const navigateToToContractDetails = contractGeneralID => {
  //   history.push({
  //     pathname: '/account/invest/details',
  //     state: { contractGeneralID, farmerMode: false }
  //   })
  // }

  return (
    <Container>
      <Title>Investments</Title>

      <Table>
        <tr>
          <th align='left'>ID</th>
          <th align='left'>Transaction</th>
          <th align='left'>Invest value</th>
        </tr>

        {contracts.map(contract => (
          <TableRow>
            <td
              // onClick={() =>
              //   navigateToToContractDetails(contract.contractFullID)
              // }
            >
              {contract.contractId}
            </td>
            <td
              onClick={() => navigateToBlockchainExplorer(contract.transactionHash)}
            >
              <HrefLink>
                {formatedTransactionHash(contract.transactionHash)}
              </HrefLink>
            </td>
            <td>
              $ 
              {currency(removeDecimals(contract.amountInvest))
                .divide(100)
                .format()}
            </td>
          </TableRow>
        ))}
      </Table>
    </Container>
  )
}

export default ContractsListContainer
