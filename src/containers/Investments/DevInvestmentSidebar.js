import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Sidebar, Tab } from 'react-leaflet-sidebarv2'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import 'react-leaflet-fullscreen-control'
import {
  StatusContract,
  CONTRACT_SITUATION,
  GET_CONTRACT_TYPE
} from '../../mocks/contractTypes'
import agryoLogo from '../../assets/agryo-sm-white.svg'
import currency from 'currency.js'
import ButtonPrimary from '../../components/Buttons/Primary'
import { crops } from '../../mocks/crops.json'
import { livestocks } from '../../mocks/livestocks.json'

const BrandLogo = styled.img.attrs({
  alt: 'Agryo logo'
})`
  height: 2rem;
  margin-top: 7px;
`

const ContractTypeItem = styled.button`
  cursor: pointer;
  font-size: 1rem;
  font-weight: 600;
  border-radius: 6px;
  padding: 0.6rem 1rem;
  border: 1px solid #ccc;
  color: ${({ active }) => (active ? '#fff' : '#3F8C48')};
  background: ${({ active }) => (active ? '#3F8C48' : '#fff')};

  margin-right: 1rem;
  margin-bottom: 0.6rem;
`
const generateContractTypeAndId = (contractType) => {
  const product = [...crops, ...livestocks].find(product => {
    const productId = contractType
    return product.id === productId
  }) || { name: 'Green Bond', value: 'greenBond' }

  return { name: product.name || "INVALID", value: product.value || "INVALID" }
}


const DevInvestmenSidebar = ({
  t,
  visibleModal,
  contractData,
  onSelectContract,
  contractTypes,
  selectTypeContract,
  onOpen,
  onClose,
  collapsed,
  selected,
  goToContractDetails
}) => {
  const x = contractData.contractId
  let contractType = GET_CONTRACT_TYPE(contractData) || { name: '' }

  const contractInfo = generateContractTypeAndId(contractData.contractType)
  return (
    <Sidebar
      id='sidebar'
      collapsed={collapsed}
      selected={selected}
      onOpen={onOpen}
      onClose={onClose}
      autopan={true}
      closeIcon={<FontAwesomeIcon icon={'window-close'} />}
    >
      <Tab
        id='home'
        header={'Contract Details'}
        icon={
          <div onClick={() => onClose()}>
            <BrandLogo src={agryoLogo} />
          </div>
        }
      >
        {contractData && (
          <div style={{ paddingLeft: 10 }}>
            {contractData.contractId && (
              <p style={{ fontSize: 22 }}>Contract {contractData.contractId}</p>
            )}

            <p style={{ marginTop: 20 }}>
              <b>Status</b>
            </p>
            <p style={{ fontSize: 22, marginBottom: 10 }}>
              {t(StatusContract[contractData.status])}
            </p>

            <p>
              <b>Contract Type</b>
            </p>
            <p style={{ fontSize: 22 }}>{t(contractInfo.name)}</p>

            <p>
              <b>Hectares</b>
            </p>
            <p style={{ fontSize: 22, marginBottom: 10 }}>
              {contractData.fieldHectares / 100}
            </p>

            {contractData.yieldInTonsByHectare && (
              <React.Fragment>
                <p>
                  <b>Expect Production(Tons)</b>
                </p>
                <p style={{ fontSize: 22, marginBottom: 10 }}>
                  {(contractData.yieldInTonsByHectare *
                    contractData.fieldHectares) /
                    10000}
                </p>
              </React.Fragment>
            )}

            <p>
              <b>Date Start</b>{' '}
            </p>
            <p style={{ fontSize: 22, marginBottom: 10 }}>
              {new Date(contractData.startDate * 1000).toLocaleDateString(
                'en-US'
              )}
            </p>

            <p>
              <b>Date End</b>
            </p>
            <p style={{ fontSize: 22, marginBottom: 10 }}>
              {new Date(contractData.endDate * 1000).toLocaleDateString(
                'en-US'
              )}
            </p>

            <p>
              <b>Funded Value</b>
            </p>
            <p style={{ fontSize: 22 }}>
              {contractData.currency} {currency(contractData.fundedValue)
                .divide(100)
                .format()}
            </p>

            <div
              style={{ marginTop: 10, marginLeft: 'auto', marginRight: 'auto' }}
            >
              <ButtonPrimary
                onClick={() => goToContractDetails({...contractData, contractId: x})}
                text={'Details'}
              />
            </div>
          </div>
        )}
      </Tab>
      <Tab
        id='settings'
        header='Settings'
        anchor='bottom'
        icon={
          <div onClick={() => onClose()}>
            <FontAwesomeIcon color={'white'} icon={'tasks'} />
          </div>
        }
      >
        <p>
          <b>Contract Type</b>
        </p>
        {contractTypes.map(option => (
          <ContractTypeItem
            key={option.value || option.slug}
            active={selectTypeContract[option.slug]}
            onClick={() => onSelectContract(option.slug)}
          >
            {option.label}
          </ContractTypeItem>
        ))}

        <p>
          <b>Contract Situation</b>
        </p>
        {CONTRACT_SITUATION.map(option => (
          <ContractTypeItem
            key={option.value || option.slug}
            active={selectTypeContract[option.slug]}
            onClick={() => onSelectContract(option.slug)}
          >
            {option.label}
          </ContractTypeItem>
        ))}
      </Tab>
    </Sidebar>
  )
}

DevInvestmenSidebar.propTypes = {
  onClickContract: PropTypes.func.isRequired,
  contracts: PropTypes.array.isRequired
}

export default DevInvestmenSidebar
