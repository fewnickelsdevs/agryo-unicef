import React from 'react'
import styled from 'styled-components'

import { Title } from '../../components/Typography'
import Container from '../../components/Layout/Container'
import InvestmentOptionCard from './InvestmentOptionCard'

const CardsWrapper = styled.div`
  ${({ theme }) => `
    display: flex;
    align-items: center;
    margin-top: 1.75rem;

    width: 100%;
    height: auto;
    overflow-x: hidden;
    overflow-x: auto;

    text-transform: none;

    & > * {
      margin-right: 1rem;
    }

    ${theme.breakpoints.xs} {
      margin: 2rem 0 3rem;
    }
  `}
`

export default function HomeInvestContainer({ t }) {
  const cards = [
    {
      icon: 'calculator',
      label: t('contracts.form.green-bond'),
      url: '/account/invest/greenbond',
      type: 'greenbonds'
    },
    {
      icon: 'check-circle',
      label: t('contracts.form.crops'),
      url: '/account/invest/crops',
      type: 'crops'
    },
    {
      icon: 'dollar-sign',
      label: t('contracts.form.livestock'),
      url: '/account/invest/livestock',
      type: 'livestocks'
    }
  ]

  return (
    <Container>
      <Title>{t('contracts.title-invest-costing')}</Title>

      <CardsWrapper>
        {cards.map(card => (
          <InvestmentOptionCard key={card.url} {...card} />
        ))}
      </CardsWrapper>
    </Container>
  )
}
