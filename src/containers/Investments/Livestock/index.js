import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

import Container from '../../../components/Layout/Container'
import { Title } from '../../../components/Typography'
import InvestmentMap from '../InvestmentMap'
import LoadingContainer from '../../Loading'
import CostingInfoDialog from '../../../components/Dialogs/DetailsContracts/CostingDialog'
import { ButtonPrimary } from '../../../components/Buttons';
import TableContractListContainer from '../../ContractsList/TableContractListContainer';
import MapContractListContainer from '../../ContractsList/MapContractListContainer';

const LivestockInvestOverviewContainer = ({ t, investContracts, title }) => {
  const [contractData, setContractModal] = useState(false)
  const [mapView, setMapView] = useState(true)

  const history = useHistory()
  const { contracts, loading, error } = investContracts

  function openContractModal(contractData) {
    setContractModal(contractData)
  }

  function closeContractModal() {
    setContractModal(false)
  }

  const goToContractDetails = contractData => {
    history.push({
      pathname: '/account/invest/details',
      state: { contractData }
    })
  }

  if (loading) {
    return <LoadingContainer />
  }

  return (
    <Container>
      <Title>{title}</Title>
      {mapView ?
      <ButtonPrimary text="Table View" onClick={() => setMapView(false)} />
      :
      <ButtonPrimary text="Map View" onClick={() => setMapView(true)} />
      }

      {!mapView ?
        <>
          <TableContractListContainer
            t={t}
            contracts={contracts}
            goToDetails={goToContractDetails}
          />
        </>
        :
        <MapContractListContainer
          t={t}
          contracts={contracts}
          goToDetails={goToContractDetails}
        />
      }

      <InvestmentMap
        onClickContract={openContractModal}
        contracts={contracts}
        loading={loading}
        error={error}
      />
      <CostingInfoDialog
        contractData={contractData}
        closeContractModal={closeContractModal}
        goToContractDetails={goToContractDetails}
      />
    </Container>
  )
}

export default LivestockInvestOverviewContainer
