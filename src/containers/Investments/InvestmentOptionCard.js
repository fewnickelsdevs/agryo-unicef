import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const possibleBackground = {
  greenbonds: require('../../assets/greenbond.jpg'),
  crops: require('../../assets/crops.jpg'),
  livestocks: require('../../assets/livestocks.jpg')
}

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  min-width: 14rem;
  overflow: hidden;
  background: #fff;
  border-radius: 5px;
  padding-bottom: 1rem;
  border: 1px solid #ccc;
`

const Background = styled.div(
  props => `
  width: 100%;
  height: 7rem;
  position: relative;
  margin-bottom: 1rem;
  background-size: cover;
  background-image: linear-gradient(
      rgba(31, 91, 51, 0.5),
      rgba(31, 91, 51, 0.5)
    ),
    url(${possibleBackground[props.type]});
`
)

const StyledLink = styled(Link)`
  color: #555;
  text-decoration: none;
`

const InvestmentOptionCard = ({ type, label, url }) => (
  <StyledLink to={url}>
    <Container>
      <Background type={type} />

      <h3>{label}</h3>
    </Container>
  </StyledLink>
)

InvestmentOptionCard.propTypes = {
  type: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
}

export default InvestmentOptionCard
