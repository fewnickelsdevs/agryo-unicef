import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  Map,
  Marker,
  TileLayer,
  LayersControl,
  ZoomControl
} from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import FullscreenControl from 'react-leaflet-fullscreen'
import 'react-leaflet-fullscreen-control'
import 'react-leaflet-markercluster/dist/styles.min.css'
import { BingLayer } from 'react-leaflet-bing'
import { bingKey } from '../../contracts/api/ceos'
const { BaseLayer } = LayersControl

const PropertyMap = styled(Map).attrs({
  zoom: 2,
  maxZoom: 18,
  center: [0.0, 0.0],
  zoomControl: false
})`
  width: 100%;
  z-index: 2;
  height: 20rem;
  margin: 1rem;
`

const InvestmentMap = ({ onClickContract, contracts }) => (
  <PropertyMap>
    <LayersControl>
      <BaseLayer name='Satelite' checked>
        <BingLayer bingkey={bingKey} type='Aerial' />
      </BaseLayer>
      <BaseLayer name='Map'>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />
      </BaseLayer>
    </LayersControl>

    <ZoomControl position='topright' />
    <FullscreenControl position='bottomright' />

    <MarkerClusterGroup>
      {contracts.map(contract => {
        if (!contract.propertyLat) return null

        return (
          <Marker
            key={contract.id}
            onClick={() => onClickContract(contract)}
            position={{ lat: contract.propertyLat, lng: contract.propertyLng }}
          />
        )
      })}
    </MarkerClusterGroup>
  </PropertyMap>
)

InvestmentMap.propTypes = {
  onClickContract: PropTypes.func.isRequired,
  contracts: PropTypes.array.isRequired
}

export default InvestmentMap
