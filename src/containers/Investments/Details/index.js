import React from 'react'
import styled from 'styled-components'

import ContractInformationCard from './ContractInformationCard'
import InvestmentDetailsCard from './InvestmentDetailsCard'
import Container from '../../../components/Layout/Container'
import { Title, Subtitle } from '../../../components/Typography'
import { crops } from '../../../mocks/crops.json'
import { livestocks } from '../../../mocks/livestocks.json'
import Gallery from '../../../components/Gallery'
import Charts from '../../PropertyDetails/Charts'
import ReportsCard from '../../PropertyDetails/ReportsCard'
import LoadingContainer from '../../../components/Loading/BoxLoading'
import LoadingDialog from '../../../components/Dialogs/LoadingDialog'
import ErrorDialog from '../../../components/Dialogs/Error'
import SuccessDialog from '../../../components/Dialogs/Success'
import GreenBondInfoCard from './GreenBondInfo'
import formatChartPayload from '../../../formatters/formatChartPayload'
import { StatusContract } from '../../../mocks/contractTypes'
import PolygonMapContainer from '../../PolygonMap/PolygonMapContainer';
import ContractBlockchainHistory from './BlockchainHistory';

const SectionContainer = styled.section`
  margin-top: 2rem;
  margin-bottom: 2rem;
`

const Heading = styled.div`
  margin-bottom: 1.75rem;
`

const FullWidthCardContainer = styled.section`
  width: 100%;
  margin-bottom: 1rem;
`

const CardsGridContainer = styled.section`
  display: grid;
  grid-gap: 1rem;
  width: 100%;
  grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
  margin-bottom: 2rem;
`

const generateContractTypeAndId = (contractType, contractId) => {
  const product = [...crops, ...livestocks].find(product => {
    const productId = contractType
    return product.id === productId
  }) || { name: 'Green Bond', value: 'greenBond' }

  let data = {
    "crops": `CETH${contractId}`,
    "livestock": `LETH${contractId}`,
    "greenBond": `GBETH${contractId}`
  }
  return { contractId: data[product.value] || "INVALID", name: product.name || "INVALID", value: product.value || "INVALID" }
}

const InvestmentDetailsContainer = ({
  t,
  user,
  onInvest,
  contractDetails,
  farmer,
  onSuccess,
  onError,
  noInvest = false
}) => {
  const {
    loading,
    contract,
    investLoading,
    investComplete,
    investError
  } = contractDetails
  const { gallery, charts, reports, carbon } = contractDetails.property

  if (loading && !contract) {
    return <LoadingContainer />
  }

  const cityName = contractDetails.property.basicInfo
    ? `${contractDetails.property.basicInfo.properties.f7} - ${contractDetails.property.basicInfo.properties.f5}`
    : ''

  const contractInfo = generateContractTypeAndId(contract.contractType, contract.id)
  

  const productionCardFields = [
    {
      name: 'fieldHectares',
      shouldFormat: true,
      label: t('investments.details.production.fieldHectares'),
      prefix: '',
      postfix: '',
      caption: 'Hectares'
    },
    {
      name: 'technologyLevel',
      shouldFormat: false,
      label: t('investments.details.production.technologyLevel'),
      prefix: '',
      postfix: '',
      caption: ''
    },
    {
      name: 'yieldInTonsByHectare',
      shouldFormat: true,
      label: t('investments.details.production.yieldInTonsByHectare'),
      prefix: '',
      postfix: '',
      caption: 'Tons per hectare'
    },
    {
      name: 'expectedAmountInTons',
      shouldFormat: true,
      label: t('investments.details.production.expectedAmountInTons'),
      prefix: '',
      postfix: '',
      caption: 'Tons'
    }
  ]

  const datesCardFields = [
    {
      name: 'startDate',
      shouldFormat: false,
      label: t('investments.details.dates.start-date'),
      prefix: '',
      postfix: '',
      date: true
    },
    {
      name: 'endDate',
      shouldFormat: false,
      label: t('investments.details.dates.end-date'),
      prefix: '',
      postfix: '',
      date: true
    }
  ]

  const informationCardFields = [
    {
      name: 'fundedValue',
      shouldFormat: true,
      label: t('investments.details.information.funding-value'),
      prefix: contractDetails.contract.currency || "USD",
      postfix: ''
    },
    {
      name: 'interest',
      shouldFormat: false,
      label: t('investments.details.information.interest'),
      prefix: '',
      postfix: '%'
    },
    {
      name: 'paidAmount',
      shouldFormat: true,
      label: t('investments.details.information.current-filled'),
      prefix: contractDetails.contract.currency || "USD",
      postfix: ''
    }
  ]
  
  return (
    <React.Fragment>
      <Container>
        <Heading>
          <Subtitle>{t('investments.details.title')}</Subtitle>
          <Subtitle>Status: {t(StatusContract[contractDetails.contract.status])}</Subtitle>

          <Title>
            {t('investments.details.contract') + ' ' + contract.contractId || contractInfo.contractId}-{' '}
            {t(contractInfo.name)}
          </Title>
        </Heading>
        {!farmer ||
          (!noInvest && (
            <FullWidthCardContainer>
              <InvestmentDetailsCard
                t={t}
                agryoBalance={user.balanceAgryoStable}
                contract={contract}
                onInvest={amountToInvest => onInvest(contract, amountToInvest)}
              />
            </FullWidthCardContainer>
          ))}

        <PolygonMapContainer
          property={contractDetails.property}
        />

        <CardsGridContainer>
          <ContractInformationCard
            title={t('investments.details.information.title')}
            fields={informationCardFields}
            contract={contract}
          />

          <ContractInformationCard
            title={t('investments.details.dates.title')}
            fields={datesCardFields}
            contract={contract}
          />

          {contractInfo.name === 'Green Bond' ? (
            <GreenBondInfoCard
              title={t('investments.details.production.title')}
              fields={productionCardFields}
              hectares={contractDetails.contract.fieldHectares}
              city={cityName || ''}
            />
          ) : (
            <ContractInformationCard
              title={t('investments.details.production.title')}
              fields={productionCardFields}
              contract={contract}
            />
          )}
        </CardsGridContainer>

        <ContractBlockchainHistory t={t} blockchainHistory={contractDetails.contract.blockchainHistory || []} />

        {loading && <LoadingContainer />}

        {reports && (
          <React.Fragment>
            {reports.initial && (
              <SectionContainer>
                <ReportsCard
                  t={t}
                  title={'Property Reports'}
                  data={reports.initial}
                />
              </SectionContainer>
            )}
            {reports.monit && (
              <SectionContainer>
                <ReportsCard
                  t={t}
                  title={'Contract Reports'}
                  data={reports.monit}
                />
              </SectionContainer>
            )}
            {reports.fraud && (
              <SectionContainer>
                <ReportsCard
                  t={t}
                  title={'Fraud Reports'}
                  data={reports.fraud}
                />
              </SectionContainer>
            )}
          </React.Fragment>
        )}

        {gallery && (
          <SectionContainer>
            <Gallery
              title={t('properties.details.gallery')}
              galleryData={gallery}
            />
          </SectionContainer>
        )}

        {charts && (
          <SectionContainer>
            <Charts t={t} data={formatChartPayload({...charts, carbon})} />
          </SectionContainer>
        )}

        <SuccessDialog
          visible={investComplete}
          title={t('investments.dialogs.success')}
          buttonMessage={t('contracts.dialogs.successButton')}
          onClick={onSuccess}
        />

        <ErrorDialog
          visible={investError}
          title={t('investments.dialogs.error')}
          buttonMessage={t('contracts.dialogs.errorButton')}
          onClick={onError}
        />

        <LoadingDialog visible={investLoading} />
      </Container>
    </React.Fragment>
  )
}

export default InvestmentDetailsContainer
