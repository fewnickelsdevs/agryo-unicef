import React from 'react'
import styled from 'styled-components'

import ContractInformationCard from './ContractInformationCard'
import InvestmentDetailsCard from './InvestmentDetailsCard'
import Container from '../../../components/Layout/Container'
import { Title, Subtitle } from '../../../components/Typography'
import { crops } from '../../../mocks/crops.json'
import { livestocks } from '../../../mocks/livestocks.json'
import Gallery from '../../../components/Gallery'
import Charts from '../../PropertyDetails/Charts'
import ReportsCard from '../../PropertyDetails/ReportsCard'
import LoadingContainer from '../../../components/Loading/BoxLoading'
import LoadingDialog from '../../../components/Dialogs/LoadingDialog'
import ErrorDialog from '../../../components/Dialogs/Error'
import SuccessDialog from '../../../components/Dialogs/Success'
import { ButtonPrimary, ButtonSecondary } from '../../../components/Buttons'
import GreenBondInfoCard from './GreenBondInfo'
import { StatusContract } from '../../../mocks/contractTypes'
import formatChartPayload from '../../../formatters/formatChartPayload'
import { generateContractTypeAndId } from '../../../services/contract_utils';
import PolygonMapContainer from '../../PolygonMap/PolygonMapContainer';
import ContractBlockchainHistory from './BlockchainHistory';

const SectionContainer = styled.section`
  margin-top: 2rem;
  margin-bottom: 2rem;
`

const Heading = styled.div`
  margin-bottom: 1.75rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const FullWidthCardContainer = styled.section`
  width: 100%;
  margin-bottom: 1rem;
`

const CardsGridContainer = styled.section`
  display: grid;
  grid-gap: 1rem;
  width: 100%;
  grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
  margin-bottom: 2rem;
`

const PendingContractDetailsContainer = ({
  t,
  user,
  onAuthorize,
  onRevoke,
  contractDetails,
  propertyData,
  farmer,
  onSuccess,
  onError,
  loadingAuth,
  complete,
  hideAprove
}) => {

  const { loading, contract, investError } = contractDetails
  const { gallery, charts, reports, carbon } = propertyData
  if (loading && !contract) {
    return <LoadingContainer />
  }
  const cityName = propertyData.basicInfo
    ? `${propertyData.basicInfo.properties.f7} - ${propertyData.basicInfo.properties.f5}`
    : ''

  const contractInfo = generateContractTypeAndId(contractDetails.contractType, contractDetails.id)

  const productionCardFields = [
    {
      name: 'fieldHectares',
      shouldFormat: true,
      label: t('investments.details.production.fieldHectares'),
      prefix: '',
      postfix: '',
      caption: 'Hectares'
    },
    {
      name: 'technologyLevel',
      shouldFormat: false,
      label: t('investments.details.production.technologyLevel'),
      prefix: '',
      postfix: '',
      caption: ''
    },
    {
      name: 'yieldInTonsByHectare',
      shouldFormat: true,
      label: t('investments.details.production.yieldInTonsByHectare'),
      prefix: '',
      postfix: '',
      caption: 'Tons per hectare'
    },
    {
      name: 'expectedAmountInTons',
      shouldFormat: true,
      label: t('investments.details.production.expectedAmountInTons'),
      prefix: '',
      postfix: '',
      caption: 'Tons'
    }
  ]

  const datesCardFields = [
    {
      name: 'startDate',
      shouldFormat: false,
      label: t('investments.details.dates.start-date'),
      prefix: '',
      postfix: '',
      date: true
    },
    {
      name: 'endDate',
      shouldFormat: false,
      label: t('investments.details.dates.end-date'),
      prefix: '',
      postfix: '',
      date: true
    }
  ]

  const informationCardFields = [
    {
      name: 'fundedValue',
      shouldFormat: true,
      label: t('investments.details.information.funding-value'),
      prefix: '$',
      postfix: ''
    },
    {
      name: 'interest',
      shouldFormat: false,
      label: t('investments.details.information.interest'),
      prefix: '',
      postfix: '%'
    }
  ]


  return (
    <React.Fragment>
      <Container>
        <Heading>
          <div>
            <Subtitle>{t('investments.details.title')}</Subtitle>
            <Subtitle>
              Status: {t(StatusContract[contractDetails.status])}
            </Subtitle>
            <Title>
            {t('investments.details.contract') + ' ' + contractInfo.contractId}-{' '}
            {t(contractInfo.name)}
          </Title>
          </div>
          <div>
            {!hideAprove && (
              <React.Fragment>
                <ButtonPrimary
                  text={'Aprove'}
                  onClick={() => onAuthorize(contractInfo.contractId)}
                />
                <ButtonSecondary onClick={() => onRevoke(contractInfo.contractId)} text={'Cancel'} />
              </React.Fragment>
            )}
          </div>
        </Heading>
        <PolygonMapContainer property={propertyData} />

        <CardsGridContainer>
          <ContractInformationCard
            title={t('investments.details.information.title')}
            fields={informationCardFields}
            contract={contractDetails}
          />

          <ContractInformationCard
            title={t('investments.details.dates.title')}
            fields={datesCardFields}
            contract={contractDetails}
          />

          {contractInfo.value === 'greenBond'  ? (
            <GreenBondInfoCard
              title={t('investments.details.production.title')}
              fields={productionCardFields}
              hectares={contractDetails.fieldHectares}
              city={cityName || ''}
            />
          ) : (
            <ContractInformationCard
              title={t('investments.details.production.title')}
              fields={productionCardFields}
              contract={contractDetails}
            />
          )}
        </CardsGridContainer>

        <ContractBlockchainHistory t={t} blockchainHistory={contractDetails.contract.blockchainHistory || []} />


        {reports && (
          <React.Fragment>
            {reports.initial && (
              <SectionContainer>
                <ReportsCard
                  t={t}
                  title={t('properties.details.reports.property-title')}
                  data={reports.initial}
                />
              </SectionContainer>
            )}
          </React.Fragment>
        )}

        {gallery && (
          <SectionContainer>
            <Gallery
              title={t('properties.details.gallery')}
              galleryData={gallery}
            />
          </SectionContainer>
        )}

        {charts && (
          <SectionContainer>
            <Charts t={t} data={formatChartPayload({ ...charts, carbon })} />
          </SectionContainer>
        )}

        <SuccessDialog
          visible={complete}
          title={'Contract Authorized!'}
          buttonMessage={t('contracts.dialogs.successButton')}
          onClick={onSuccess}
        />

        <ErrorDialog
          visible={investError}
          title={t('investments.dialogs.error')}
          buttonMessage={t('contracts.dialogs.errorButton')}
          onClick={onError}
        />

        <LoadingDialog visible={loadingAuth} />
      </Container>
    </React.Fragment>
  )
}

export default PendingContractDetailsContainer
