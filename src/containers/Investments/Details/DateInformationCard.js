import React from 'react'
import PropTypes from 'prop-types'
import NumberFormat from 'react-number-format'

import {
  TableContainer,
  TableHead,
  TableHeadCell,
  TableRow,
  TableBody,
  TableCell
} from '../../../components/Table'
import { Caption } from '../../../components/Typography'

const InformationCard = ({ title, fields, contract }) => (
  <TableContainer>
    <TableHead>
      <TableRow>
        <TableHeadCell>{title}</TableHeadCell>
        <TableHeadCell />
      </TableRow>
    </TableHead>
    <TableBody>
      {fields.map(field => (
        <TableRow striped key={field.name}>
          <TableCell small={!!field.caption}>
            {field.label}

            <br />
            <Caption>{field.caption}</Caption>
          </TableCell>
          {field.date ? (
            <TableCell align='right'>{contract[field.name]}</TableCell>
          ) : (
            <TableCell align='right'>
              {field.shouldFormat ? (
                <NumberFormat
                  value={contract[field.name] / 100}
                  displayType={'text'}
                  thousandSeparator={true}
                  decimalScale={2}
                  fixedDecimalScale={true}
                  prefix={field.prefix}
                />
              ) : (
                contract[field.name]
              )}

              {field.postfix}
            </TableCell>
          )}
        </TableRow>
      ))}
    </TableBody>
  </TableContainer>
)

InformationCard.propTypes = {
  contract: PropTypes.shape({
    balanceAmount: 0,
    contractGeneralID: PropTypes.string,
    contractID: PropTypes.number,
    contractStatusID: PropTypes.number,
    cropsType: PropTypes.number,
    dueDate: PropTypes.string,
    durationNumberOfMonths: PropTypes.number,
    endDate: PropTypes.string,
    expectedAmountInTons: PropTypes.number,
    farmer: PropTypes.string,
    fieldHectares: PropTypes.number,
    fundedValue: PropTypes.number,
    interest: PropTypes.number,
    investments: PropTypes.array,
    marker: PropTypes.array,
    minimumFillingValue: PropTypes.number,
    minimumValue: PropTypes.number,
    paidAmount: PropTypes.number,
    paymentValue: PropTypes.number,
    propertyId: PropTypes.number,
    startDate: PropTypes.string,
    technologyLevel: PropTypes.number,
    transactionHash: PropTypes.string,
    yieldInTonsByHectare: PropTypes.number
  }).isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      shouldFormat: PropTypes.bool,
      label: PropTypes.string,
      prefix: PropTypes.string,
      postfix: PropTypes.string
    }).isRequired
  ).isRequired
}

export default InformationCard
