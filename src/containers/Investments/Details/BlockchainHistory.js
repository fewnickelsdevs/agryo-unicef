import React from 'react'
import {
    TableContainer,
    TableHead,
    TableHeadCell,
    TableRow,
    TableBody,
    TableCell
  } from '../../../components/Table'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { TypeActionContract } from '../../../mocks/contractTypes';

const historyData = [
    {
        contractId: 'C10',
        blockchainContractId: 'C10',
        typeAction: 'REGISTER',
        transactionHash: '0x2893712d8asopduj',
        transactionId: '0.0.02837',
        timestamp: 1603920209
        
    },
    {
        contractId: 'C10',
        blockchainContractId: 'C10',
        typeAction: 'APROVE',
        transactionHash: '0x293734783FSSS',
        transactionId: '0.0.02847',
        timestamp: 1603920349
        
    },
    {
        contractId: 'C10',
        blockchainContractId: 'C10',
        typeAction: 'PAID',
        transactionHash: '0x293DSOUIAUS',
        transactionId: '0.0.02997',
        timestamp: 1603920949 
    }
]



const ContractBlockchainHistory = ({ t, blockchainHistory }) => {

    const navigateToHederaExplorer = hash => {
        window.open(
          `https://rinkeby.etherscan.io/tx/${hash}`,
          '__blank'
        )
      }
    
    return(
        <TableContainer>
        <TableHead>
          <TableRow>
            <TableHeadCell>Contract History</TableHeadCell>
            <TableHeadCell></TableHeadCell>
            <TableHeadCell></TableHeadCell>
          </TableRow>
        </TableHead>
        <TableBody>
            {blockchainHistory.map((history) => 
                <TableRow striped>
                    <TableCell>{t(TypeActionContract[history.typeAction])}</TableCell>
                    <TableCell>{new Date(history.timestamp * 1000).toUTCString()}</TableCell>
                    <TableCell style={{ float: 'right' }}>
                    {(history.transactionHash.length>6) &&history.transactionHash.substring(0,6)}...
                  <FontAwesomeIcon
                    size='lg'
                    icon='external-link-alt'
                    className='details-icon'
                    color='#3F8C48'
                    onClick={() => navigateToHederaExplorer(history.transactionHash)}
                  />         
                    </TableCell>
                </TableRow>    
            )}
        </TableBody>
        </TableContainer>
    )
}

export default ContractBlockchainHistory
