import React from 'react'
import PropTypes from 'prop-types'
import NumberFormat from 'react-number-format'

import {
  TableContainer,
  TableHead,
  TableHeadCell,
  TableRow,
  TableBody,
  TableCell
} from '../../../components/Table'
import { Caption } from '../../../components/Typography'
import { fromUnixTime } from 'date-fns'

const GreenBondInfoCard = ({ title, fields, hectares, city }) => (
  <TableContainer>
    <TableHead>
      <TableRow>
        <TableHeadCell>{title}</TableHeadCell>
        <TableHeadCell />
      </TableRow>
    </TableHead>
    <TableBody>
      <TableRow>
        <TableCell>Hectares</TableCell>
        <TableCell>{hectares / 100}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell>Local</TableCell>
        <TableCell>{city}</TableCell>
      </TableRow>
    </TableBody>
  </TableContainer>
)

GreenBondInfoCard.propTypes = {
  contract: PropTypes.shape({
    balanceAmount: 0,
    contractGeneralID: PropTypes.string,
    contractID: PropTypes.number,
    contractStatusID: PropTypes.number,
    cropsType: PropTypes.number,
    dueDate: PropTypes.string,
    durationNumberOfMonths: PropTypes.number,
    endDate: PropTypes.string,
    expectedAmountInTons: PropTypes.number,
    farmer: PropTypes.string,
    fieldHectares: PropTypes.number,
    fundedValue: PropTypes.number,
    interest: PropTypes.number,
    investments: PropTypes.array,
    marker: PropTypes.array,
    minimumFillingValue: PropTypes.number,
    minimumValue: PropTypes.number,
    paidAmount: PropTypes.number,
    paymentValue: PropTypes.number,
    propertyId: PropTypes.number,
    startDate: PropTypes.string,
    technologyLevel: PropTypes.number,
    transactionHash: PropTypes.string,
    yieldInTonsByHectare: PropTypes.number
  }).isRequired,
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      shouldFormat: PropTypes.bool,
      label: PropTypes.string,
      prefix: PropTypes.string,
      postfix: PropTypes.string
    }).isRequired
  ).isRequired
}

export default GreenBondInfoCard
