import React, { useState } from 'react'
import PropTypes from 'prop-types'
import currency from 'currency.js'
import CurrencyInput from 'react-currency-input'

import {
  TableContainer,
  TableHead,
  TableHeadCell,
  TableBody,
  TableRow,
  TableCell
} from '../../../components/Table'
import { ButtonPrimary } from '../../../components/Buttons'
import { Caption } from '../../../components/Typography'

const InvestmentDetailsCard = ({ t, agryoBalance, contract, onInvest }) => {
  const [amountToInvest, setAmountToInvest] = useState(100)

  return (
    <TableContainer>
      <TableHead>
        <TableRow>
          <TableHeadCell>{t('investments.details.invest.title')}</TableHeadCell>
          <TableHeadCell />
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow striped>
          <TableCell>
            <CurrencyInput
              style={{ width: '100%', padding: 10 }}
              prefix='$'
              value={amountToInvest}
              onChange={(event, value) => setAmountToInvest(value)}
            />
          </TableCell>

          <TableCell align='right'>
            <ButtonPrimary
              text={t('investments.details.invest.invest-now')}
              onClick={() => onInvest(amountToInvest)}
            />
          </TableCell>
        </TableRow>

        <TableRow striped>
          <TableCell>{t('investments.details.invest.agryo-balance')}</TableCell>

          <TableCell align='right'>
            R$
            {currency(agryoBalance)
              .divide(100)
              .format()}
          </TableCell>
        </TableRow>

        <TableRow striped>
          <TableCell small>
            {t('investments.details.invest.amount-available')}
            <br />
            <Caption>USD</Caption>
          </TableCell>

          <TableCell align='right'>
            $
            {currency(contract.fundedValue)
              .subtract(contract.paidAmount)
              .divide(100)
              .format()}
          </TableCell>
        </TableRow>

        <TableRow striped>
          <TableCell>
            {t('investments.details.invest.value-to-receive')}
          </TableCell>

          <TableCell align='right'>
            $
            {currency(amountToInvest)
              .multiply(contract.interest)
              .divide(100)
              .add(amountToInvest)
              .format()}
          </TableCell>
        </TableRow>
      </TableBody>
    </TableContainer>
  )
}

InvestmentDetailsCard.propTypes = {
  contract: PropTypes.shape({
    balanceAmount: 0,
    contractGeneralID: PropTypes.string,
    contractID: PropTypes.number,
    contractStatusID: PropTypes.number,
    cropsType: PropTypes.number,
    dueDate: PropTypes.string,
    durationNumberOfMonths: PropTypes.number,
    endDate: PropTypes.string,
    expectedAmountInTons: PropTypes.number,
    farmer: PropTypes.string,
    fieldHectares: PropTypes.number,
    fundedValue: PropTypes.number,
    interest: PropTypes.number,
    investments: PropTypes.array,
    marker: PropTypes.array,
    minimumFillingValue: PropTypes.number,
    minimumValue: PropTypes.number,
    paidAmount: PropTypes.number,
    paymentValue: PropTypes.number,
    propertyId: PropTypes.number,
    startDate: PropTypes.string,
    technologyLevel: PropTypes.number,
    transactionHash: PropTypes.string,
    yieldInTonsByHectare: PropTypes.number
  }).isRequired,
  agryoBalance: PropTypes.string.isRequired,
  onInvest: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
}

export default InvestmentDetailsCard
