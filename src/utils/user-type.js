export const USER_TYPES = {
  ROOT: 'ROOT',
  REGIONAL: "REGIONAL",
  ENTERPRISE: 'ENTERPRISE',
  FARMER: 'FARMER',
  TECHNICAL: 'TECHNICAL'
}

const USER_TYPE_ID_MAP = {
  '1': USER_TYPES.ROOT,
  '2': USER_TYPES.ENTERPRISE,
  '3': USER_TYPES.REGIONAL,
  '4': USER_TYPES.TECHNICAL,
  '5': USER_TYPES.FARMER
}

export const USER_TYPES_TITLE_MAP = (t) => {
  return{
  '1': t('user-profile.root'),
  '2': t('user-profile.operator'),
  '3': t('user-profile.regional'),
  '4': t('user-profile.technical') 
  }
}

export const USER_TYPES_ENTERPRISE = (t) => [
  { value: 1, name: t('user-profile.root') },
  { value: 2, name: t('user-profile.operator') },
  { value: 3, name: t('user-profile.regional') },
  { value: 4, name: t('user-profile.technical') }
]

export const P2P_USER_TYPES = (t) => [
  { value: 6, name: "Farmer"},
  { value: 7, name: "Investor"}
]

export const FULL_USER_TYPES = (t) => [
  { value: 1, name: t('user-profile.root') },
  { value: 2, name: t('user-profile.operator') },
  { value: 3, name: t('user-profile.regional') },
  { value: 4, name: t('user-profile.technical') },
  { value: 5, name: t('user-profile.farmer') }
]

export const REGISTRED_ENTERPRISE_USER_TYPES = (t) => [
  { value: 2, name: t('user-profile.operator') },
  { value: 3, name: t('user-profile.regional') },
  { value: 4, name: t('user-profile.technical') }
]




export function getUserType() {
  const userTypeId = localStorage.getItem('agryo-user-type')
  return USER_TYPE_ID_MAP[userTypeId]
}
