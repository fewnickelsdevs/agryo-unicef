import { crops } from '../mocks/crops.json'
import { livestocks } from '../mocks/livestocks.json'

export const StatusContract = {
  PENDING: 'contracts.status.pending',
  CONFIRMED: 'contracts.status.confirmed',
  PAID: 'contracts.status.paid',
  DISPUTE: 'contracts.status.dispute',
  DISPUTE_CLOSED: 'contracts.status.dispute-closed',
  NOT_APROVE: 'contracts.status.not-aprove'
}

export const TypeActionContract = {
  'REGISTER': 'contracts.status.registered', 
  'APROVE':   'contracts.status.confirmed', 
  'PAID':     'contracts.status.paid', 
  'DENY':     'contracts.status.not-aprove', 
}

export const CONTRACT_TYPES = t => {
  return {}
}

export const CONTRACT_SITUATION = [
  { label: 'Pending', slug: 'pending' },
  { label: 'Confirmed', slug: 'confirmed' }
]

export const GET_CONTRACT_TYPE = contract => {
  const product = [...crops, ...livestocks].find(product => {
    const productId = contract.cropsType || contract.livestockType
    return product.id === productId
  }) || { name: 'Green Bond' }
  return product
}
