export const USER_TYPES = (t) => [
    { value: 1, name: t('user-profile.root') },
    { value: 2, name: t('user-profile.operator') },
    { value: 3, name: t('user-profile.regional') },
    { value: 4, name: t('user-profile.technical') }
]

export const FULL_USER_TYPES = (t) => [
    { value: 1, name: t('user-profile.root') },
    { value: 2, name: t('user-profile.operator') },
    { value: 3, name: t('user-profile.regional') },
    { value: 4, name: t('user-profile.technical') },
    { value: 5, name: t('user-profile.farmer') }
]

