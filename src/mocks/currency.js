import currency from 'currency.js'


export const CURRENCY_TYPES = [
    { value: "BRL", name: "Real(BRL)"},
    { value: "USD", name: "Dolar(USD)" },
    { value: "EUR", name: "Euro(EUR)" }
]


export const CURRENCY_SYMBOL =  {
    "USD": "$",
    "BRL": "R$",
    "EUR": "€"
}


export const USD_EUR = 0.842228
export const USD_BRL = 5.30585

export const CURRENCY_CALCULATE_AMOUNT = (value, currencyType, usd_eur, usd_brl) => {
    if(currencyType === 'USD'){
        return value
    }

    if(currencyType === 'BRL'){
        return currency(value).divide(100).multiply(usd_brl).intValue
    }

    if(currencyType === 'EUR'){
        return currency(value).divide(100).multiply(usd_eur).intValue
    }

    return value
}



