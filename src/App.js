import React from 'react'
import { Provider as StateProvider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { ThemeProvider as StyledComponents } from 'styled-components'

import configureStore, { history } from './redux/store'
import Root from './pages/Root'
import AccountRoot from './pages/Account'
import GlobalStyle from './theme/GlobalStyles'
import { breakpoints, colors } from './theme'
import LoadingContainer from './containers/Loading'
import './utils/icon-loader'
import { Route } from 'react-router-dom'

function renderRoutes() {
  return (
    <React.Fragment>
      <Route exact path='/' component={AccountRoot} />
      <Route path='/forget' component={AccountRoot} />
      <Route path='/register' component={AccountRoot} />
      <Route path='/account' component={Root} />
    </React.Fragment>
  )
}


function App({ t }) {
  return (
    <React.Suspense fallback={<LoadingContainer />}>
      <StyledComponents theme={{ breakpoints, colors }}>
        <React.Fragment>
          <GlobalStyle />
          <StateProvider store={configureStore()}>
            <ConnectedRouter history={history}>              
              {renderRoutes()}
            </ConnectedRouter>
          </StateProvider>
        </React.Fragment>
      </StyledComponents>
    </React.Suspense>
  )
}

export default App
