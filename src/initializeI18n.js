import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

let params = new URL(window.location.href).searchParams
const languageFromQuery = params.get('lang')
if (languageFromQuery) {
  localStorage.setItem('i18nextLng', languageFromQuery)
} else if (!localStorage.getItem('i18nextLng')) {
  const curLanguage = navigator.language.split('-')[0]
  localStorage.setItem('i18nextLng', curLanguage)
}

const basePath = process.env.PUBLIC_URL || ''

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    detection: {
      order: ['localStorage'],
      lookupLocalStorage: 'i18nextLng'
    },
    backend: {
      loadPath: `${basePath}/locales/{{lng}}/{{ns}}.json`
    },
    defaultNS: 'translations',
    interpolation: {
      escapeValue: false
    },
    ns: ['translations'],
    fallbackLng: 'en',
    react: {
      useSuspense: true, /* if we use useSuspense:false, it will lead a bad UX*/
      wait: true,
    },
    whitelist: ['pt', 'es', 'en']
  })

export default i18n
