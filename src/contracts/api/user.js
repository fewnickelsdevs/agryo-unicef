import TDRABI from '../abi/TDRABI.json'

const TDRERC20 = '0x2d879f53f83b6ef6961e3bc4bef676dbb66cea27'

const removeDecimalsFromValue = value => {
  if (value === 0 || value === '0') return '0'

  const withoutDecimals = value.slice(0, -16)

  return withoutDecimals
}

const generateAgryoContract = async () => {
  try {
    const { web3 } = window
    const contract = new web3.eth.Contract(TDRABI, TDRERC20)
    return contract
  } catch (err) {
    throw err
  }
}

export const getUserAgryoTokens = async userAddress => {
  const agryoContract = await generateAgryoContract()
  const userBalance = await agryoContract.methods.balanceOf(userAddress).call()
  const normalizedBalance = removeDecimalsFromValue(userBalance)
  return normalizedBalance
}
