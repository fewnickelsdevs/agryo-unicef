import api from '../../services/config'
import { decodeDateHexValues, generateContract } from './utils'
import CropsContractABI from '../abi/CostingCropsABI.json'
import LivestockContractABI from '../abi/CostingLivestockABI.json'
import GreenBondContractABI from '../abi/GreenBondABI.json'
import currency from 'currency.js'
import { removeDecimals } from '../../formatters/currencyDecimal'
import { format, addDays } from 'date-fns'

export const getGreenBondContractList = async () => {
  const response = await api.get('/farmer/v1/investor/contract/greenbond/list')
  return response.data
}

export const getLivestockContractList = async () => {
  const response = await api.get('/farmer/v1/investor/contract/livestock/list')
  return response.data
}

export const getCropsContractList = async () => {
  const response = await api.get('/farmer/v1/investor/contract/crops/list')
  return response.data
}

export const getCropsContractData = async _id => {
  try {
    const cropsContract = await generateContract(
      CropsContractABI,
      process.env.REACT_APP_CROPS_CONTRACT_ADDRESS
    )
    const [basic, info, deal, status] = await Promise.all([
      cropsContract.methods.getBasicInfo(_id).call(),
      cropsContract.methods.getFieldInfo(_id).call(),
      cropsContract.methods.getInfoFunding(_id).call(),
      cropsContract.methods.getStatus(_id).call()
    ])

    const fundedValue = parseInt(removeDecimals(deal[0].toString()))
    const paidAmount = parseInt(removeDecimals(deal[3].toString()))
    const minimumFillingValue = parseInt(deal[1])
    const minimumValue = (fundedValue / 100) * minimumFillingValue
    const { dateStart, dateEnd, durationMonths } = await decodeDateHexValues(
      basic[3]
    )
    const dueDate = '2020/10/10'
    console.log(dueDate)
    const valor = fundedValue / 100
    const currencyHelper = currency(valor).divide(100)
    const interest = 0.75 * parseInt(durationMonths)
    const percent = currency(currencyHelper).multiply(interest)
    const valueToPay = currency(valor).add(percent)
    const paymentValue = currency(valueToPay).value
    const interestContract = interest

    const data = {
      contractGeneralID: `C${_id}`,
      contractID: parseInt(_id),
      farmer: basic[1],
      propertyId: basic[0],
      contractStatusID: parseInt(status),
      cropsType: parseInt(info[0]),
      technologyLevel: parseInt(info[1]),
      fieldHectares: parseInt(info[2]),
      yieldInTonsByHectare: parseInt(info[3]),
      expectedAmountInTons: parseInt(info[2] / 10) * (parseInt(info[3]) / 10),
      durationNumberOfMonths: durationMonths,
      startDate: dateStart,
      endDate: dateEnd,
      dueDate: dueDate,
      fundedValue: fundedValue,
      minimumFillingValue: minimumFillingValue,
      minimumValue: minimumValue,
      interest: parseInt(deal[2]),
      interestContract: interestContract,
      paidAmount: parseInt(paidAmount),
      balanceAmount: parseInt(deal[4]),
      paymentValue: paymentValue
    }
    return data
  } catch (err) {
    console.log(err)
    return err
  }
}

export const getGreenBondContractData = async _id => {
  try {
    const greenBondContract = await generateContract(
      GreenBondContractABI,
      process.env.REACT_APP_GREEN_BOND_CONTRACT_ADDRESS
    )
    const [info, finance] = await Promise.all([
      greenBondContract.methods.getGreenBondInfo(_id).call(),
      greenBondContract.methods.getGreenBondFinance(_id).call()
    ])
    const { dateStart, dateEnd, durationYears } = await decodeDateHexValues(
      info[4]
    )

    const fundedValue = removeDecimals(finance[0])
    const paidAmount = removeDecimals(finance[2])
    const balanceAmount = removeDecimals(finance[3])
    const interest = parseInt(finance[1], 10) / 100
    const data = {
      contractGeneralID: `GB${_id}`,
      contractID: _id,
      contractStatusID: info[0],
      propertyId: info[1],
      farmer: info[2],
      fieldHectares: info[3],
      startDate: dateStart,
      endDate: dateEnd,
      durationYears,
      fundedValue,
      paidAmount,
      interest,
      balanceAmount
    }
    return data
  } catch (err) {
    console.log(err)
    return err
  }
}

export const getLivestockContractData = async _id => {
  const livestockContract = await generateContract(
    LivestockContractABI,
    process.env.REACT_APP_LIVESTOCK_CONTRACT_ADDRESS
  )
  const [basic, info, deal, status] = await Promise.all([
    livestockContract.methods.getBasicInfo(_id).call(),
    livestockContract.methods.getFieldInfo(_id).call(),
    livestockContract.methods.getInfoFunding(_id).call(),
    livestockContract.methods.getStatus(_id).call()
  ])

  const fundedValue = parseInt(removeDecimals(deal[0].toString()))
  const paidAmount = parseInt(removeDecimals(deal[3].toString()))
  const minimumFillingValue = parseInt(deal[1])
  const minimumValue = (fundedValue / 100) * minimumFillingValue

  const {
    dateStart,
    dateEnd,
    durationMonths,
    durationDays
  } = await decodeDateHexValues(basic[3])

  const dueDate = addDays(format(dateEnd, 'YYYY/MM/DD'), 90)
  const valor = fundedValue / 100
  const currencyHelper = currency(valor).divide(100)
  const interest = 0.75 * parseInt(durationMonths)
  const percent = currency(currencyHelper).multiply(interest)
  const valueToPay = currency(valor).add(percent)
  const paymentValue = currency(valueToPay).value

  const data = {
    contractGeneralID: `L${_id}`,
    contractID: parseInt(_id),
    farmer: basic[1],
    propertyId: basic[0],
    contractStatusID: parseInt(status),
    livestockType: parseInt(info[0]),
    technologyLevel: parseInt(info[1]),
    fieldHectares: parseInt(info[2]),
    numberAnimals: parseInt(info[3]),
    yieldInKgByAnimal: parseInt(info[4]),
    expectedAmountInTons: parseInt(info[3]) * parseInt(info[4]),
    durationNumberOfMonths: durationMonths,
    startDate: dateStart,
    endDate: dateEnd,
    dueDate: dueDate,
    fundedValue: parseInt(fundedValue),
    minimumFillingValue: minimumFillingValue,
    minimumValue: minimumValue,
    interest: parseInt(deal[2]),
    interestContract: interest,
    paidAmount: parseInt(paidAmount),
    balanceAmount: parseInt(deal[4]),
    paymentValue: paymentValue
  }
  return data
}

export const getContractByFullId = async _contractFullId => {
  try {
    const isCrops = _contractFullId.includes('C')
    if (isCrops) {
      const contract = await getCropsContractData(
        parseInt(_contractFullId.slice(1))
      )
      return contract
    }
    const isLivestock = _contractFullId.includes('L')
    if (isLivestock) {
      const contract = await getLivestockContractData(
        parseInt(_contractFullId.slice(1))
      )
      return contract
    }
    const isGreenBond = _contractFullId.includes('GB')
    if (isGreenBond) {
      const contract = await getGreenBondContractData(
        parseInt(_contractFullId.slice(2))
      )
      return contract
    }

    return false
  } catch (err) {
    console.log(err)
    return null
  }
}
