import currency from 'currency.js'
import {
  format,
  addDays,
  differenceInMonths,
  differenceInCalendarYears
} from 'date-fns'

export const etherFormatFromValue = value => {
  const intValue = currency(value).intValue
  return `${intValue}0000000000000000`
}

export const decodeDateHexValues = async _data => {
  try {
    const { web3 } = window
    const dateDateConvert = web3.utils.hexToUtf8(_data)
    console.log(dateDateConvert)
    const dateResult = dateDateConvert.split('/')
    console.log(dateResult)
    const dateEnd = format(
      addDays(new Date(), parseInt(dateResult[1])),
      'yyyy-MM-dd'
    )
    console.log(dateEnd)
    const durationMonths = differenceInMonths(dateEnd, dateResult[0])
    console.log(durationMonths)
    const durationYears = differenceInCalendarYears(dateEnd, dateResult[0])
    console.log(durationMonths)

    return {
      dateStart: dateResult[0],
      dateEnd,
      durationMonths,
      durationDays: dateResult[1],
      durationYears
    }
  } catch (err) {
    console.log(err)
    return err
  }
}

export const generateContract = (abi, contractAddress) => {
  const { web3 } = window
  const contract = new web3.eth.Contract(abi, contractAddress)
  return contract
}
