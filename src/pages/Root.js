import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import LoggedArea from './LoggedArea'
import LoadingContainer from '../containers/Loading'
import { REFRESH_USER } from '../redux/user/constants'

const mapStateToProps = state => ({ ...state })
//const authToken = localStorage.getItem('agryo-jwt')

const PrivateRoute = props => {
  const { auth, fetchAndUpdateUserInfo, component: Component, ...rest } = props
  const authToken = localStorage.getItem('agryo-jwt')
  if (authToken && !auth) {
    fetchAndUpdateUserInfo(authToken)
    return <LoadingContainer />
  }
  return (
    <Route
      {...rest}
      render={props => (auth ? <Component {...props} /> : <Redirect to='/' />)}
    />
  )
}

function Root({ user, dispatch }) {
  const fetchAndUpdateUserInfo = async token => {
    dispatch({
      type: REFRESH_USER,
      payload: { token }
    })
  }

  return (
    <PrivateRoute
      path='/account'
      auth={user.user.name}
      fetchAndUpdateUserInfo={fetchAndUpdateUserInfo}
      component={LoggedArea}
    />
  )
}

export default connect(mapStateToProps)(Root)
