import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'

import HomeContainer from '../../containers/Home'
import { FETCH_PROPERTY_REQUEST } from '../../redux/property/actions'
import { useHistory } from 'react-router-dom'

const mapStateToProps = state => ({
  user: state.user,
  property: state.property
})

function Home({ dispatch, property, user: { farmerWeatherInfo, user } }) {
  const history = useHistory()
  const { t } = useTranslation()

  useEffect(() => {
    dispatch({
      type: FETCH_PROPERTY_REQUEST
    })
  }, [])

  return (
    <HomeContainer
      t={t}
      userAccount={user.id}
      properties={property.data}
      weatherData={farmerWeatherInfo}
      redirectToContracts={() => history.push('/account/contracts')}
      onSeeMore={() => history.push('/account/properties')}
    />
  )
}

export default connect(mapStateToProps)(Home)
