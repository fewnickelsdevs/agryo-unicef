import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import HomeCompanyContainer from '../../containers/Home/HomeCompanyContainer'

const mapStateToProps = state => ({
  user: state.user
})

function HomeOperatorCompany({ user }) {
  const { t } = useTranslation()
  return <HomeCompanyContainer t={t} companyName={user.companyName} />
}

export default connect(mapStateToProps)(HomeOperatorCompany)
