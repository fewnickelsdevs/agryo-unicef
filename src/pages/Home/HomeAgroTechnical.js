import React from 'react'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'
import HomeAgroTechnicalContainer from '../../containers/Home/HomeAgroTechnical'

const mapStateToProps = state => ({
  user: state.user
})

function HomeAgroTechnical({ user }) {
  const { t } = useTranslation()

  return (
    <HomeAgroTechnicalContainer
      t={t}
      technicalName={user.technicalName}
      companyName={user.companyName}
    />
  )
}

export default connect(mapStateToProps)(HomeAgroTechnical)
