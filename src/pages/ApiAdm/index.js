import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { replace } from 'connected-react-router'

import {
  GET_API_INFO_REQUEST,
  REQUEST_NEW_API,
  CONFIRM_NEW_API,
  OPEN_MODAL_NEW_API,
  CANCEL_REQUEST_NEW_API,
  CLOSE_NEW_API
} from '../../redux/apiAdmin/constants'
import SuccessDialog from '../../components/Dialogs/Success'
import { useTranslation } from 'react-i18next'
import ErrorDialog from '../../components/Dialogs/Error'
import ConfirmationCode from '../../components/Dialogs/ConfirmationCode'
import ApiAdmContainer from '../../containers/ApiAdm'
import AlertSnackbar from '../../components/AlertSnackbar'
import NewApiKeyDialog from '../../components/Dialogs/NewApiKeyDialog'

const mapStateToProps = state => ({ apiAdmData: state.apiAdm })

const ApiAdm = ({ apiAdmData, dispatch }) => {
  const [code, setCode] = useState('')
  const { t } = useTranslation()

  useEffect(() => {
    dispatch({
      type: GET_API_INFO_REQUEST
    })
  }, [dispatch])

  const {
    apiData,
    requestNewApi,
    waitingNewApiConfirmation,
    newApiData,
    error
  } = apiAdmData

  const handleOpenPopupCreateKeys = () => {
    dispatch({
      type: OPEN_MODAL_NEW_API
    })
  }

  const handleRequestCreateApi = confirmPassword => {
    dispatch({
      type: REQUEST_NEW_API,
      payload: { password: confirmPassword, name: 'api-dev' }
    })
  }

  const handleConfirmCreateApi = code => {
    dispatch({
      type: CONFIRM_NEW_API,
      payload: { token: code }
    })
  }

  const handleCloseNewApiKey = () => dispatch({ type: CLOSE_NEW_API })

  const handleCloseModal = () => {
    dispatch({
      type: CANCEL_REQUEST_NEW_API
    })
  }

  const closeErrorDialog = () => {}

  const closeSuccessDialogAndGoBack = () =>
    dispatch(replace('/account/apiKeys'))

  return (
    <React.Fragment>
      <ApiAdmContainer
        apiData={apiData}
        onCreateKeys={handleOpenPopupCreateKeys}
        t={t}
      />

      <ConfirmationCode
        visible={requestNewApi}
        onConfirmPassword={handleRequestCreateApi}
        waitingNewApiConfirmation={waitingNewApiConfirmation}
        onCodeConfirm={handleConfirmCreateApi}
        onCancel={handleCloseModal}
      />

      <NewApiKeyDialog newApiKey={newApiData} onClick={handleCloseNewApiKey} />

      <AlertSnackbar open={error} message={error} />

      <SuccessDialog
        visible={apiData.success}
        title={t('properties.create.success.title')}
        message={t('properties.create.success.message')}
        buttonMessage={t('properties.create.success.button')}
        onClick={closeSuccessDialogAndGoBack}
      />
    </React.Fragment>
  )
}

export default connect(mapStateToProps)(ApiAdm)
