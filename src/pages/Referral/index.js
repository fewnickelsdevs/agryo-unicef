import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { pipe } from 'ramda'
import { connect } from 'react-redux'
import ReferralContainer from '../../containers/Referral'
import AlertSnackbar from '../../components/AlertSnackbar'

const mapStateToProps = state => ({ referral: state.referral })

const Referral = ({ dispatch, referral }) => {
  const [alertSnackbarProps, setAlertProps] = useState({
    open: false,
    message: 'Copied to clipboard'
  })
  const { t } = useTranslation()

  useEffect(() => {
    dispatch({
      type: 'FETCH_REFERRAL_DATA_REQUEST'
    })
  }, [])

  const copyToClipboard = str => {
    const el = document.createElement('textarea')
    el.value = str
    document.body.appendChild(el)
    el.select()
    document.execCommand('copy')
    document.body.removeChild(el)
  }

  const toggleAlertSnackbar = () => {
    setAlertProps({
      ...alertSnackbarProps,
      open: true
    })

    setTimeout(() => {
      setAlertProps({
        ...alertSnackbarProps,
        open: false
      })
    }, 2000)
  }

  return (
    <React.Fragment>
      <ReferralContainer
        referral={referral}
        t={t}
        onCopyLink={pipe(copyToClipboard, toggleAlertSnackbar)}
      />

      <AlertSnackbar {...alertSnackbarProps} />
    </React.Fragment>
  )
}

export default connect(mapStateToProps)(Referral)
