import React from 'react'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'

import {
  autorizeValueWallet,
  fetchWalletData,
  autorizeValueWalletReset,
  requestFaucetAgryoUsd
} from '../../redux/wallet/actions'
import WalletContainer from '../../containers/Wallet'
import { REQUEST_FAUCET } from '../../redux/wallet/constants';

const mapStateToProps = state => ({
  wallet: state.wallet,
  aproveValueTx: state.aproveValue
})

const mapDispatchToProps = {
  autorizeValueWallet,
  fetchWalletData,
  autorizeValueWalletReset,
  requestFaucetAgryoUsd
}

const Wallet = ({
  wallet,
  aproveValueTx,
  fetchWalletData,
  autorizeValueWallet,
  autorizeValueWalletReset,
  requestFaucetAgryoUsd
}) => {
  const { t } = useTranslation()


  return (
    <WalletContainer
      t={t}
      wallet={wallet}
      fetchWalletData={fetchWalletData}
      autorizeValueWallet={autorizeValueWallet}
      autorizeValueWalletReset={autorizeValueWalletReset}
      aproveValueTx={aproveValueTx}
      requestFaucet={requestFaucetAgryoUsd}
    />
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Wallet)
