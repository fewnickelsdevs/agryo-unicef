import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import LoadingContainer from '../../../containers/Loading'
import {
  REQUEST_COMPANY_TECHNICALS,
  REQUEST_COMPANY_NEW_TECHNICAL,
  REQUEST_CANCEL_TECHNICAL_INVITATE,
  REQUEST_REVOKE_ACCESS_TECHNICAL,
  REQUEST_AUTHORIZE_ACCESS_TECHNICAL
} from '../../../redux/enterprise/actions'
import ListAgroTechnicalsContainer from '../../../containers/AgroTechnicals/ListAgroTechnicals'
import AlertSnackbar from '../../../components/AlertSnackbar'

const mapStateToProps = state => ({ enterprise: state.enterpriseInfo })

const ListCompanyTechnicals = ({ enterprise, dispatch }) => {
  const { t } = useTranslation()

  useEffect(() => {
    fetchTechnicals()
  }, [dispatch])

  if (enterprise.loading) {
    return <LoadingContainer />
  }

  const fetchTechnicals = () => dispatch({ type: REQUEST_COMPANY_TECHNICALS })

  const registerNewAgroTechnical = email => {
    dispatch({ type: REQUEST_COMPANY_NEW_TECHNICAL, payload: { email } })
  }

  const cancelInvitateAgroTechnical = email => {
    dispatch({ type: REQUEST_CANCEL_TECHNICAL_INVITATE, payload: { email } })
  }

  const revokeAccessAgroTechnical = technicalId => {
    dispatch({
      type: REQUEST_REVOKE_ACCESS_TECHNICAL,
      payload: { technicalId }
    })
  }

  const authorizeAccessAgroTechnical = technicalId => {
    dispatch({
      type: REQUEST_AUTHORIZE_ACCESS_TECHNICAL,
      payload: { technicalId }
    })
  }

  return (
    <React.Fragment>
      <ListAgroTechnicalsContainer
        t={t}
        technicals={enterprise.company.technicals}
        pendingTechnicals={enterprise.company.pendingTechnicals}
        onNewTechnical={registerNewAgroTechnical}
        loadingAction={enterprise.loadingAction}
        successAction={enterprise.successAction}
        cancelInvitateAgroTechnical={cancelInvitateAgroTechnical}
        revokeAccessAgroTechnical={revokeAccessAgroTechnical}
        resetState={fetchTechnicals}
        authorizeAccessAgroTechnical={authorizeAccessAgroTechnical}
      />
      <AlertSnackbar
        open={enterprise.errorAction}
        message={enterprise.errorAction}
      />
    </React.Fragment>
  )
}

export default connect(mapStateToProps)(ListCompanyTechnicals)
