import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import LoadingContainer from '../../../containers/Loading'
import { REQUEST_COMPANY_FARMERS } from '../../../redux/enterprise/actions'
import FarmerListContainer from '../../../containers/Farmer/FarmerList'

const mapStateToProps = state => ({ enterprise: state.enterpriseInfo })

const ListCompanyFarmers = ({ enterprise, dispatch }) => {
  const history = useHistory()
  const navigateToFarmerDetails = farmerData =>
    history.push({
      pathname: '/account/farmers/details',
      state: { farmerData, viewMode: true }
    })

  useEffect(() => {
    dispatch({ type: REQUEST_COMPANY_FARMERS })
  }, [dispatch])

  if (enterprise.loading) {
    return <LoadingContainer />
  }

  return (
    <FarmerListContainer
      farmers={enterprise.farmers}
      navigateToFarmerDetails={navigateToFarmerDetails}
      showNewFarmer={false}
    />
  )
}

export default connect(mapStateToProps)(ListCompanyFarmers)
