import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import LoadingContainer from '../../../containers/Loading'
import {
  REQUEST_TECHNICAL_FARMERS,
  REQUEST_COMPANY_NEW_FARMER
} from '../../../redux/enterprise/actions'
import AlertSnackbar from '../../../components/AlertSnackbar'
import SuccessDialog from '../../../components/Dialogs/Success'
import FarmerListContainer from '../../../containers/Farmer/FarmerList'
import { useHistory } from 'react-router-dom'

const mapStateToProps = state => ({ enterprise: state.enterpriseInfo })

const ListCompanyTechnicals = ({ enterprise, dispatch }) => {
  const { t } = useTranslation()
  const history = useHistory()

  const fetchFarmers = () => dispatch({ type: REQUEST_TECHNICAL_FARMERS })

  useEffect(() => {
    fetchFarmers()
  }, [dispatch])

  if (enterprise.loading) {
    return <LoadingContainer />
  }

  const registerNewFarmer = user =>
    dispatch({ type: REQUEST_COMPANY_NEW_FARMER, payload: { user } })

  const navigateToFarmerDetails = farmerData =>
    history.push({
      pathname: '/account/farmers/details',
      state: { farmerData }
    })

  return (
    <React.Fragment>
      <FarmerListContainer
        farmers={enterprise.farmers}
        loading={enterprise.loadingAction}
        onNewFarmer={registerNewFarmer}
        navigateToFarmerDetails={navigateToFarmerDetails}
        showNewFarmer={true}
      />
      <AlertSnackbar
        open={enterprise.errorAction}
        message={enterprise.errorAction}
      />
      <SuccessDialog
        visible={enterprise.successAction}
        title={t('enterprise.create-farmer-sucesss')}
        message={''}
        buttonMessage={t('properties.create.success.button')}
        onClick={fetchFarmers}
      />
    </React.Fragment>
  )
}

export default connect(mapStateToProps)(ListCompanyTechnicals)
