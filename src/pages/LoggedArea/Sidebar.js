import React from 'react'
import { useTranslation } from 'react-i18next'
import SidebarContainer from '../../containers/Sidebar'

const SIDEBAR_ROUTES = {
  "6": getWeb3FarmerItems,
  "7": getWeb3FarmerItems
}

function getWeb3FarmerItems() {
  const { t } = useTranslation()
  const investMode = process.env.REACT_APP_INVEST_MODE === 'true'
  if (investMode) {
    return [
      {
        icon: 'dollar-sign',
        label: "Contracts",
        to: '/account/invest/home'
      },
      {
        icon: 'clipboard-list',
        label: "Investments",
        to: '/account/invest/user'
      },
      {
        icon: 'sliders-h',
        label: t('sidebar.wallet'),
        to: '/account/invest/wallet'
      }
    ]
  }

  return [
    {
      icon: 'chart-bar',
      label: t('sidebar.overview'),
      to: '/account/home'
    },
    {
      icon: 'money-check-alt',
      label: t('sidebar.contracts'),
      to: '/account/contracts'
    },
    {
      icon: 'map-marker-alt',
      label: t('sidebar.my-properties'),
      to: '/account/properties'
    },
    { icon: 'sliders-h', label: t('sidebar.wallet'), to: '/account/wallet' }
  ]
}

export default function Sidebar({
  open,
  onClose,
  userType
}) {
  const { t } = useTranslation()
  const routes = SIDEBAR_ROUTES[userType]
  return <SidebarContainer t={t} items={routes()} open={open} onClose={onClose} />
}
