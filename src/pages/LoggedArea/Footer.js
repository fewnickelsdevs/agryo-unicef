import React from 'react';

function createMarkup() {
  return [
    `<script async src="https://www.googletagmanager.com/gtag/js?id=G-EDDZZJV5LE"></script>`,
    `<script>`,
    `window.dataLayer = window.dataLayer || [];`,
    `  function gtag(){dataLayer.push(arguments);}`,
    `  gtag('js', new Date());`,
    ``,
    `  gtag('config', 'G-EDDZZJV5LE');`,
    `</script>`,
    `<script>`,
    `(function(h,o,t,j,a,r){`,
    `        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};`,
    `        h._hjSettings={hjid:1937377,hjsv:6};`,
    `        a=o.getElementsByTagName('head')[0];`,
    `        r=o.createElement('script');r.async=1;`,
    `        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;`,
    `        a.appendChild(r);`,
    `    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');`,
    `</script>`,
    `<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/7477389.js"></script>`,
  ].join(`\n`)
}
export default function Footer() {
  if (process.env.REACT_APP_PRODUCTION !== 'true') {
    return null
  }

  return (<div dangerouslySetInnerHTML={
    { __html: createMarkup() }
  } />)
}
