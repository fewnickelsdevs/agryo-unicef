import React, { useState, useEffect } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import styled from 'styled-components'
import { connect } from 'react-redux'

import routes from './routes'
import Header from './Header'
import Sidebar from './Sidebar'
import LoadingContainer from '../../containers/Loading'
import { LOGOUT_USER } from '../../redux/user/constants'

const Layout = styled.section`
  ${({ theme }) => `
    padding-top: 4rem;
    padding-left: 14rem;

    ${theme.breakpoints.sm} {
      padding-left: 0;
    }
  `}
`

function LoggedArea({ dispatch }) {
  const [sidebarIsOpen, setSidebarOpen] = useState(false)
  const invest = process.env.REACT_APP_INVEST_MODE === 'true'
  const title = invest ? 'Invest' : 'Farmer'
  const userType = localStorage.getItem('agryo-user-type')

  const logoutUser = () => dispatch({ type: LOGOUT_USER })

  useEffect(() => {
    const listener = () => {
      const breakpoint = 960
      const { innerWidth } = window

      if (innerWidth < breakpoint) return setSidebarOpen(false)

      setSidebarOpen(true)
    }

    listener()

    window.addEventListener('resize', listener)

    return () => {
      window.removeEventListener('resize', listener)
    }
  }, [])

  return (
    <React.Fragment>
      <Header
        title={title}
        onLogout={logoutUser}
        onToggleSidebar={() => setSidebarOpen(!sidebarIsOpen)}
      />
      <Sidebar
        open={sidebarIsOpen}
        userType={userType}
        onClose={() => setSidebarOpen(false)}
      />

      <Layout>
        <React.Suspense fallback={<LoadingContainer />}>
          <Switch>
            {routes.map(({ path, component, exact }) => (
              <Route
                key={path}
                path={path}
                exact={exact}
                component={component}
              />
            ))}
            <Redirect to={invest ? '/account/invest/home' : '/account/home'} />
          </Switch>
        </React.Suspense>
      </Layout>
    </React.Fragment>
  )
}

export default connect()(LoggedArea)
