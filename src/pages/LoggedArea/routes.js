import { lazy } from 'react'

const farmerRoutes = [
  {
    component: lazy(() => import('../Home')),
    exact: true,
    path: '/account/home'
  },
  {
    component: lazy(() => import('../ContractsList/FarmerContractList')),
    exact: true,
    path: '/account/contracts'
  },
  {
    component: lazy(() => import('../Contracts/NewContract')),
    exact: true,
    path: '/account/contracts/new'
  },
  {
    component: lazy(() => import('../Properties')),
    exact: true,
    path: '/account/properties'
  },
  {
    component: lazy(() => import('../Properties/CreateProperty')),
    exact: true,
    path: '/account/properties/create'
  },
  {
    component: lazy(() => import('../Properties/PropertyDetails')),
    exact: true,
    path: '/account/properties/:id/details'
  },
  {
    component: lazy(() => import('../Wallet')),
    exact: true,
    path: '/account/wallet'
  },
  {
    component: lazy(() => import('../Referral')),
    exact: true,
    path: '/account/referral'
  },
  {
    component: lazy(() => import('../Investments/Details')),
    exact: true,
    path: '/account/contracts/details'
  },
  {
    component: lazy(() => import('../Investments/Details/PendingContract')),
    exact: true,
    path: '/account/contracts/pending/details'
  },
  {
    component: lazy(() => import('../ApiAdm')),
    exact: true,
    path: '/account/api'
  }
]

const investRoutes = [
  {
    component: lazy(() => import('../Investments')),
    exact: true,
    path: '/account/invest/home'
  },
  {
    component: lazy(() => import('../Investments/InvestmentsList')),
    exact: true,
    path: '/account/invest/user'
  },
  {
    component: lazy(() => import('../Wallet')),
    exact: true,
    path: '/account/invest/wallet'
  },
  {
    component: lazy(() => import('../Investments/GreenBond')),
    exact: true,
    path: '/account/invest/greenbond'
  },
  {
    component: lazy(() => import('../Investments/Livestock')),
    exact: true,
    path: '/account/invest/livestock'
  },
  {
    component: lazy(() => import('../Investments/Crops')),
    exact: true,
    path: '/account/invest/crops'
  },
  {
    component: lazy(() => import('../Investments/Details/ContractInvest')),
    exact: true,
    path: '/account/invest/details'
  },
  {
    component: lazy(() => import('../Referral')),
    exact: true,
    path: '/account/referral'
  },
  {
    component: lazy(() => import('../Referral')),
    exact: true,
    path: '/account/invest/referral'
  }
]

const routes =
  process.env.REACT_APP_INVEST_MODE === 'true' ? investRoutes : farmerRoutes

export default routes
