import styled from 'styled-components'

export const Layout = styled.section`
  ${({ theme }) => `
    padding-top: 4rem;
    padding-left: 14rem;

    ${theme.breakpoints.sm} {
      padding-left: 0;
    }
  `}
`
