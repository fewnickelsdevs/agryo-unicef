import React from 'react'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'

import HeaderContainer from '../../containers/Header'

const mapStateToProps = state => ({ user: state.user })

function Header({ user, onToggleSidebar, title, onLogout }) {
  const { t } = useTranslation()
  return (
    <HeaderContainer
      title={title}
      onLogout={onLogout}
      onToggleSidebar={onToggleSidebar}
      user={user.user}
      t={t}
    />
  )
}

export default connect(mapStateToProps)(Header)
