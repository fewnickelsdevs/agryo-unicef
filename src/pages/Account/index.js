import React, { lazy } from 'react'
import { Switch, Route } from 'react-router-dom'

import AccountContainer from '../../containers/Account'

export default function AccountRegisterArea() {
  return (
    <AccountContainer
      primaryContent={
        <Switch>
          <Route exact path='/' component={lazy(() => import('./Login'))} />
          <Route
            exact
            path='/forget/password'
            component={lazy(() =>
              import('./ForgetPassword/RequestResetPassword.js')
            )}
          />

          <Route
            exact
            path='/forget/password/confirm/:token'
            component={lazy(() =>
              import('./ForgetPassword/ConfirmNewPassword')
            )}
          />
          <Route
            exact
            path='/register/account'
            component={lazy(() => import('./Register/RegisterWeb3'))}
          />
          <Route
            exact
            path='/register/account/:refId'
            component={lazy(() => import('./Register'))}
          />

          <Route
            exact
            path='/register/invitate/farmer/:invitateFarmerToken'
            component={lazy(() =>
              import('./Register/ConfirmFarmerCompanyInvitate')
            )}
          />

          <Route
            exact
            path='/register/confirm/:token'
            component={lazy(() => import('./ConfirmRegister'))}
          />


          <Route
            exact
            path='/register/confirm/:token'
            component={lazy(() => import('./ConfirmRegister'))}
          />

          <Route
            exact
            path='/register/web3/farmer/confirm/:token'
            component={lazy(() => import('./ConfirmRegister/ConfirmWeb3FarmerRegister'))}
          />


          <Route
            exact
            path='/register/web3/invest/confirm/:token'
            component={lazy(() => import('./ConfirmRegister/ConfirmWeb3InvestorRegister'))}
          />
        </Switch>
      }
    />
  )
}
