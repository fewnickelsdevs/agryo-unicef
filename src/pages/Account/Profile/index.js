import React, { useState } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import ProfileContainer from '../../../containers/Account/Profile'

function Profile() {
  const { t } = useTranslation()
  const [currentPassword, setCurrentPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const handleChange = () => {
    console.log('Validação dos campos')
  }

  return (
    <ProfileContainer
      t={t}
      currentPassword={currentPassword}
      setCurrentPassword={setCurrentPassword}
      newPassword={newPassword}
      setNewPassword={setNewPassword}
      confirmPassword={confirmPassword}
      setConfirmPassword={setConfirmPassword}
      OnChangePassword={handleChange}
    />
  )
}

const mapStateToProps = state => ({ user: state.user })

export default connect(mapStateToProps)(Profile)
