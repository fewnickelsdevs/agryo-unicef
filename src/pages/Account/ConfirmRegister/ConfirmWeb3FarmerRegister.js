import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import RegisterContainer from '../../../containers/Account/ConfirmRegister'
import {
  CONFIRM_REGISTER_WEB3_FARMER_REQUEST,
  RESET_STATE
} from '../../../redux/user/constants'

const mapStateToProps = state => ({ user: state.user })

const ConfirmWeb3FarmerRegister = ({ user: { loading, success, error }, dispatch }) => {
  const { t } = useTranslation()
  const { token } = useParams()
  const history = useHistory()

  useEffect(() => {
    onConfirmUser(token)
  }, [dispatch])

  const resetUserReducerAndGoToLogin = () => {
    dispatch({ type: RESET_STATE })

    history.push('/account/login')
  }

  const onConfirmUser = token => {
    dispatch({ type: CONFIRM_REGISTER_WEB3_FARMER_REQUEST, payload: { token } })
  }

  return (
    <RegisterContainer
      t={t}
      onSuccess={resetUserReducerAndGoToLogin}
      error={error}
      loading={loading}
      success={success}
    />
  )
}

ConfirmWeb3FarmerRegister.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired
}

export default connect(mapStateToProps)(ConfirmWeb3FarmerRegister)
