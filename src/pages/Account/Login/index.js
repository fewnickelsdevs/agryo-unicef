import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import LoginContainer from '../../../containers/Account/Login/LoginWeb3Container'
import {
  AUTH_USER_WEB3_REQUEST,
  AUTH_REQUEST_EMAIL_CONFIRMATION,
  UNLOCK_AFTER_TIME_WAITING,
  RESET_STATE,
  UNLOCK_SEND_EMAIL_AFTER_TIME_WAITING,
  REQUEST_EMAIL_WITH_CODE
} from '../../../redux/user/constants'

function Login({ dispatch, user }) {
  const { t } = useTranslation()
  const history = useHistory()
  const [tokenConfirmation, setTokenConfirmation] = useState('')

  const invest = process.env.REACT_APP_INVEST_MODE === 'true'
  const title = invest ? 'login.heading-investor' : 'login.heading-farmer'
  const userType = invest ? 'investor' : 'farmer'

  const handleRecovery = () => {}

  const handleLogin = (web3Instance, userWeb3Address) => {
    dispatch({ type: AUTH_USER_WEB3_REQUEST, payload: { web3Instance, userWeb3Address, userType } })
  }

  const handleConfirmationCode = () => {
    dispatch({
      type: AUTH_REQUEST_EMAIL_CONFIRMATION,
      payload: { tokenConfirmation }
    })
  }

  const handleSendEmailToken = e => {
    e.preventDefault()
    dispatch({ type: REQUEST_EMAIL_WITH_CODE, payload: {} })
  }

  const redirectToResetPassword = () => {
    history.push('/forget/password')
  }

  const handleUnlockAfterTime = () => {
    dispatch({ type: UNLOCK_AFTER_TIME_WAITING, payload: {} })
  }

  const handleResendEmailConfirmationUnlockAfterTime = () => {
    dispatch({ type: UNLOCK_SEND_EMAIL_AFTER_TIME_WAITING, payload: {} })
  }

  useEffect(() => {
    return () => {
      dispatch({ type: RESET_STATE, payload: {} })
    }
  }, [])

  const navigateToCreateAccountPage = () => {
    history.push('/register/account')
  }

  return (
    <LoginContainer
      t={t}
      title={title}
      loading={user.loading}
      error={user.error}
      onLogin={handleLogin}
      showCreateAccount={true}
      onCreateAccount={navigateToCreateAccountPage}
      requestConfirmationCode={user.requestEmailCodeConfirmation}
      onChangeConfirmationCode={setTokenConfirmation}
      onSubmitConfirmationCode={handleConfirmationCode}
      tokenConfirmation={tokenConfirmation}
      tooManyRequest={user.tooManyRequest}
      timeIsOver={handleUnlockAfterTime}
      handleResetPassword={redirectToResetPassword}
      requestEmailWaitPeriod={user.requestEmailWaitPeriod}
      handleSendEmailToken={handleSendEmailToken}
      handleResendEmailConfirmationUnlockAfterTime={
      handleResendEmailConfirmationUnlockAfterTime
      }
    />
  )
}

const mapStateToProps = state => ({ user: state.user })

export default connect(mapStateToProps)(Login)
