import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import RegisterContainer from '../../../containers/Account/Register'
import {
  REGISTER_USER_REQUEST,
  RESET_STATE
} from '../../../redux/user/constants'
import { isNil, not } from 'ramda'
import validatePassword from '../../../formatters/validatePassword'

const mapStateToProps = state => ({ user: state.user })

const Register = ({ user: { success, loading, error }, dispatch }) => {
  const { t } = useTranslation()
  const { refId } = useParams()
  const history = useHistory()
  const [invalidPass, setInvalidPass] = useState(false)
  const [user, setUser] = useState({
    name: '',
    email: '',
    password: ''
  })

  useEffect(() => {
    if (not(isNil(refId))) setUser(user => ({ ...user, refRegister: refId }))
  }, [refId])

  const changeUserFieldValue = field => value => {
    setUser({ ...user, [field]: value })
    setInvalidPass(false)
  }

  const createNewUser = event => {
    event.preventDefault()
    const validPass = validatePassword(user.password)
    if (validPass) {
      dispatch({ type: REGISTER_USER_REQUEST, payload: user })
    } else {
      setInvalidPass(true)
    }
  }

  const resetUserReducerAndGoToLogin = () => {
    dispatch({ type: RESET_STATE })
    history.push('/')
  }

  return (
    <RegisterContainer
      t={t}
      user={user}
      onChange={changeUserFieldValue}
      onSubmit={createNewUser}
      onSuccess={resetUserReducerAndGoToLogin}
      onGoBack={resetUserReducerAndGoToLogin}
      error={error}
      loading={loading}
      success={success}
      invalidPass={invalidPass}
    />
  )
}

Register.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired
}

export default connect(mapStateToProps)(Register)
