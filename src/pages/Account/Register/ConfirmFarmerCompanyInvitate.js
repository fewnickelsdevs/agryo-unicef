import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import RegisterContainer from '../../../containers/Account/Register'
import {
  REGISTER_USER_REQUEST,
  RESET_STATE
} from '../../../redux/user/constants'

const mapStateToProps = state => ({ user: state.user })

const ConfirmFarmerCompanyInvitate = ({
  user: { success, loading, error },
  dispatch
}) => {
  const { t } = useTranslation()
  const { invitateFarmerToken } = useParams()
  const history = useHistory()
  const [invalidPass, setInvalidPass] = useState(false)
  const [user, setUser] = useState({
    name: '',
    email: '',
    password: ''
  })
  if (!invitateFarmerToken) history.push('/')

  const changeUserFieldValue = field => value => {
    setUser({ ...user, [field]: value })
    setInvalidPass(false)
  }

  const createNewUser = event => {
    event.preventDefault()
    const validPass = checkPassword(user.password)
    if (validPass) {
      dispatch({
        type: REGISTER_USER_REQUEST,
        payload: { ...user, invitateFarmerToken }
      })
    } else {
      setInvalidPass(true)
    }
  }

  const resetUserReducerAndGoToLogin = () => {
    dispatch({ type: RESET_STATE })

    history.push('/')
  }

  const checkPassword = str => {
    // at least one number, one lowercase and one uppercase letter
    // at least 9 characters
    var re = /^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#=+_()/[\?!@$%^&*-]).{8,}$/
    return re.test(str)
  }

  return (
    <RegisterContainer
      t={t}
      user={user}
      onChange={changeUserFieldValue}
      onSubmit={createNewUser}
      onSuccess={resetUserReducerAndGoToLogin}
      onGoBack={resetUserReducerAndGoToLogin}
      error={error}
      loading={loading}
      success={success}
      invalidPass={invalidPass}
    />
  )
}

ConfirmFarmerCompanyInvitate.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired
}

export default connect(mapStateToProps)(ConfirmFarmerCompanyInvitate)
