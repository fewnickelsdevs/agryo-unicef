import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import RegisterContainer from '../../../containers/Account/Register'
import {
    REGISTER_USER_WEB3_REQUEST,
  RESET_STATE
} from '../../../redux/user/constants'
import { isNil, not } from 'ramda'
import validatePassword from '../../../formatters/validatePassword'
import RegisterUserContainer from '../../../containers/Account/Register/RegisterUser';

const mapStateToProps = state => ({ user: state.user })

const Register = ({ user: { success, loading, error }, dispatch }) => {
  const { t } = useTranslation()
  const history = useHistory()
  const invest = process.env.REACT_APP_INVEST_MODE === 'true'
  const title = invest ? 'Register Investor' : 'Register Farmer'
  const userType = invest ? 'investor' : 'farmer'


  const createNewUser = (name, email, web3Address, web3Instance) => {
    dispatch({ type: REGISTER_USER_WEB3_REQUEST, payload: { name, email, web3Address, web3Instance, typeUser: userType } })    
  }

  const resetUserReducerAndGoToLogin = () => {
    dispatch({ type: RESET_STATE })
    history.push('/')
  }

  return (
    <RegisterUserContainer
      t={t}
      title={title}
      onSubmit={createNewUser}
      onSuccess={resetUserReducerAndGoToLogin}
      onGoBack={resetUserReducerAndGoToLogin}
      error={error}
      loading={loading}
      success={success}
    />
  )
}

Register.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired
}

export default connect(mapStateToProps)(Register)
