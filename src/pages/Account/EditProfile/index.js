import React, { useState } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import EditProfileContainer from '../../../containers/Account/EditProfile'

function EditProfile() {
  const { t } = useTranslation()
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [role, setRole] = useState('')
  const [image, setImage] = useState('')

  const handleChange = () => {
    console.log('Validação dos campos')
  }

  return (
    <EditProfileContainer
      t={t}
      name={name}
      setName={setName}
      email={email}
      setEmail={setEmail}
      role={role}
      setRole={setRole}
      image={image}
      setImage={setImage}
      OnChangeProfile={handleChange}
    />
  )
}

const mapStateToProps = state => ({ user: state.user })

export default connect(mapStateToProps)(EditProfile)
