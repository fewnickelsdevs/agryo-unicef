import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  CONFIRM_REGISTER_REQUEST,
  RESET_STATE,
  CONFIRM_RESET_PASSWORD
} from '../../../redux/user/constants'
import ConfirmResetPassword from '../../../containers/Account/ResetPassword/ConfirmResetPassword'

const mapStateToProps = state => ({ user: state.user })

const ConfirmNewPassword = ({
  user: { loading, success, error, resetPasswordSuccess, resetPasswordError },
  dispatch
}) => {
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  const { t } = useTranslation()
  const { token } = useParams()
  const history = useHistory()

  useEffect(() => {
    return () => {
      dispatch({ type: RESET_STATE })
    }
  }, [])

  const onSubmitConfirmationPassword = () => {
    if (password === confirmPassword) {
      dispatch({ type: CONFIRM_RESET_PASSWORD, payload: { password, token } })
    } else {
      alert('Incorret Password')
    }
  }

  const redirectToLogin = () => {
    history.push('/')
  }

  return (
    <ConfirmResetPassword
      t={t}
      password={password}
      onChangePassword={setPassword}
      confirmPassword={confirmPassword}
      onChangeConfirmationPassword={setConfirmPassword}
      loading={loading}
      //tooManyRequest={}
      error={error}
      success={resetPasswordSuccess}
      onSubmitConfirmationPassword={onSubmitConfirmationPassword}
      redirectToLogin={redirectToLogin}
    />
  )
}

ConfirmNewPassword.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired
}

export default connect(mapStateToProps)(ConfirmNewPassword)
