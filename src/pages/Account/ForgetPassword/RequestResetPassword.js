import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { useHistory, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  RESET_STATE,
  REQUEST_RESET_PASSWORD
} from '../../../redux/user/constants'
import RequestResetPasswordContainer from '../../../containers/Account/ResetPassword/RequestResetPassword'

const mapStateToProps = state => ({ user: state.user })

const RequestForgetPassword = ({
  user: { loading, success, error, resetPasswordSuccess, resetPasswordError },
  dispatch
}) => {
  const [email, setEmail] = useState('')
  const [userType, setUserType] = useState(1)

  const { t } = useTranslation()
  const history = useHistory()

  useEffect(() => {
    return () => {
      dispatch({ type: RESET_STATE })
    }
  }, [])

  const onResetPassword = () => {
    dispatch({ type: REQUEST_RESET_PASSWORD, payload: { email, userType } })
  }

  const onRedirectToRegister = () => {
    history.push('/register/account')
  }

  return (
    <RequestResetPasswordContainer
      t={t}
      onResetPassword={onResetPassword}
      error={resetPasswordError}
      loading={loading}
      success={resetPasswordSuccess}
      email={email}
      setEmail={setEmail}
      userType={userType}
      setUserType={setUserType}
      onRedirectToRegister={onRedirectToRegister}
    />
  )
}

RequestForgetPassword.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired
}

export default connect(mapStateToProps)(RequestForgetPassword)
