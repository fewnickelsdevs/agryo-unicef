import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'

import PropertiesContainer from '../../containers/Properties'
import LoadingContainer from '../../containers/Loading'

import { FETCH_PROPERTY_REQUEST } from '../../redux/property/actions'
import { useHistory } from 'react-router-dom'

const mapStateToProps = state => ({ property: state.property })

const Properties = ({ property, dispatch }) => {
  const { t } = useTranslation()
  const history = useHistory()

  useEffect(() => {
    dispatch({
      type: FETCH_PROPERTY_REQUEST
    })
  }, [dispatch])

  const navigateToPropertyDetails = propertyId => {
    history.push(`/account/properties/${propertyId}/details`)
  }

  if (property.loading) {
    return <LoadingContainer />
  }

  if (!property.data) {
    return null
  }

  return (
    <PropertiesContainer
      t={t}
      properties={property.data}
      onClickProperty={navigateToPropertyDetails}
    />
  )
}

export default connect(mapStateToProps)(Properties)
