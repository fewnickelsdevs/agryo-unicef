import React, { useEffect } from 'react'
import { useParams, useHistory, useLocation } from 'react-router-dom'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import LoadingContainer from '../../containers/Loading'
import { FETCH_PROPERTY_DETAILS_REQUEST } from '../../redux/property/actions'
import PropertyDetailsContainer from '../../containers/PropertyDetails'

const mapStateToProps = state => {
  return { property: state.property }
}

const PropertyDetails = ({ dispatch, property }) => {
  const { t } = useTranslation()
  const { id } = useParams()
  const history = useHistory()
  const location = useLocation()

  const navigateToContractDetails = contractData => {
    history.push({
      pathname: '/account/contracts/details',
      state: { contractData, farmerMode: true }
    })
  }

  useEffect(() => {
    dispatch({
      type: FETCH_PROPERTY_DETAILS_REQUEST,
      payload: {
        propertyId: parseInt(id),
        farmerId: location.state ? location.state.farmerId : false
      }
    })
  }, [dispatch])

  if (property.loading) {
    return <LoadingContainer />
  }

  return (
    <PropertyDetailsContainer
      t={t}
      property={property}
      onClickContract={navigateToContractDetails}
    />
  )
}

export default connect(mapStateToProps)(PropertyDetails)
