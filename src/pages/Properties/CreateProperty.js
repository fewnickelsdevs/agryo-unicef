import React from 'react'
import { connect } from 'react-redux'
import { replace } from 'connected-react-router'

import CreatePropertyContainer from '../../containers/CreateProperty'
import {
  CREATE_PROPERTY_REQUEST,
  UPDATE_PROPERTY
} from '../../redux/property/actions'
import SuccessDialog from '../../components/Dialogs/Success'
import { useTranslation } from 'react-i18next'
import ErrorDialog from '../../components/Dialogs/Error'

const mapStateToProps = state => ({ property: state.property })

const CreateProperty = ({ property, dispatch }) => {
  const { t } = useTranslation()

  const handleCreateProperty = propertyData => {
    dispatch({
      type: CREATE_PROPERTY_REQUEST,
      payload: propertyData
    })
  }

  const closeErrorDialog = () =>
    dispatch({
      type: UPDATE_PROPERTY,
      payload: { error: null }
    })

  const closeSuccessDialogAndGoBack = () =>
    dispatch(replace('/account/properties'))

  return (
    <React.Fragment>
      <CreatePropertyContainer
        createPropertyLoading={property.loading}
        onCreateProperty={handleCreateProperty}
        t={t}
        newPropertySuccess={property.success}
        closeSuccessDialog={closeSuccessDialogAndGoBack}
      />

      {/* <SuccessDialog
        visible={property.success}
        title={t('properties.create.success.title')}
        message={t('properties.create.success.message')}
        buttonMessage={t('properties.create.success.button')}
        onClick={closeSuccessDialogAndGoBack}
      /> */}

      <ErrorDialog
        visible={Boolean(property.error)}
        title={t('properties.create.error.title')}
        message={t('properties.create.error.message')}
        buttonMessage={t('properties.create.error.button')}
        onClick={closeErrorDialog}
      />
    </React.Fragment>
  )
}

export default connect(mapStateToProps)(CreateProperty)
