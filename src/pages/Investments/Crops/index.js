import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { FETCH_CROPS_CONTRACTS } from '../../../redux/investContracts/constants'
import CropsInvestmentContainer from '../../../containers/Investments/Crops'

const mapStateToProps = state => ({ investContracts: state.investContracts })

const CropsInvest = ({ dispatch, investContracts }) => {
  const { t } = useTranslation()

  useEffect(() => {
    dispatch({
      type: FETCH_CROPS_CONTRACTS
    })
  }, [dispatch])

  return (
    <CropsInvestmentContainer
      title={t('contracts.title-overview-crops')}
      investContracts={investContracts}
      t={t}
    />
  )
}

export default connect(mapStateToProps)(CropsInvest)
