import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useHistory } from 'react-router-dom'
import { connect } from 'react-redux'

import InvestmentDetailsContainer from '../../../containers/Investments/Details'
import {
  INIT_CONTRACT_REQUEST,
  RESET_VIEW_CONTRACT,
  CONTRACT_INVESTMENT_REQUEST,
  INVESTMENT_RESET
} from '../../../redux/contractDetails/constants'

const mapStateToProps = state => ({
  user: {
    balanceAgryoStable: state.user.balanceAgryoStable
  },
  contractDetails: state.contractDetails
})

const InvestmentDetails = ({ user, dispatch, contractDetails }) => {
  const { t } = useTranslation()
  const [noInvestInfo, setNoInvest] = useState(false)
  const location = useLocation()
  const history = useHistory()
  const dispatchContractInvestment = (contract, amountToInvest) => {
    dispatch({
      type: CONTRACT_INVESTMENT_REQUEST,
      payload: { contractId: contract.contractId, amountToInvest }
    })
  }

  const onSucessClickInvest = () => {
    dispatch({
      type: RESET_VIEW_CONTRACT,
      payload: {}
    })
    history.replace('/account/home')
  }

  const onErrorClickInvest = () => {
    dispatch({
      type: INVESTMENT_RESET,
      payload: {}
    })
  }

  useEffect(() => {
    if (!location.state.contractData && !location.state.contractId) {
      return history.push('/account/home')
    }
    const { contractData, contractId, noInvest } = location.state
    if (noInvest) setNoInvest(true)

    dispatch({
      type: INIT_CONTRACT_REQUEST,
      payload: {
        contractId: contractData.contractId || contractId,
        contractData
      }
    })
  }, [dispatch])

  useEffect(() => {
    return () => {
      dispatch({
        type: RESET_VIEW_CONTRACT
      })
    }
  }, [])

  return (
    <InvestmentDetailsContainer
      t={t}
      contract={{...location.state.contractData, contractId: location.state.contractId}}
      user={user}
      onInvest={dispatchContractInvestment}
      farmer={location.state.farmerMode}
      contractDetails={{...contractDetails, contractId: location.state.contractId}}
      onSuccess={onSucessClickInvest}
      onError={onErrorClickInvest}
      noInvest={noInvestInfo}
    />
  )
}

export default connect(mapStateToProps)(InvestmentDetails)
