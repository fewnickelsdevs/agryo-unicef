import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useHistory } from 'react-router-dom'
import { connect } from 'react-redux'

import InvestmentDetailsContainer from '../../../containers/Investments/Details'
import {
  INIT_WEB3CONTRACT_PENDING_FARMER,
  RESET_VIEW_CONTRACT,
  REQUEST_AUTHORIZE_CONTRACT,
  REQUEST_REPROVE_CONTRACT
} from '../../../redux/contractDetails/constants'
import PendingContractDetailsContainer from '../../../containers/Investments/Details/PendingContract'
import LoadingContainer from '../../../containers/Loading'

const mapStateToProps = state => ({
  user: {
    balanceAgryoStable: state.user.balanceAgryoStable
  },
  contractDetails: state.contractDetails
})

const PendingContractDetails = ({ user, dispatch, contractDetails }) => {
  const { t } = useTranslation()
  const location = useLocation()
  const history = useHistory()
  const [hiddenAprove, setHiddenAprove] = useState(false)

  useEffect(() => {
    if (!location.state.contractId || !location.state.contractType) {
      return history.push('/account/home')
    }

    const {
      contractId,
      contractType,
      hideAprove,
      contractData
    } = location.state
    if (hideAprove) {
      setHiddenAprove(true)
    }
    console.log(      {payload: {
      contractId,
      contractType,
      propertyId: contractData.propertyId
    }})
    dispatch({
      type: INIT_WEB3CONTRACT_PENDING_FARMER,
      payload: {
        contractId: contractData.id,
        contractType
      }
    })
  }, [dispatch])

  useEffect(() => {
    return () => {
      dispatch({
        type: RESET_VIEW_CONTRACT
      })
    }
  }, [])

  const onSucessClick = () => {
    return history.push('/account/home')
  }

  const dispatchAutorizeContract = contractId => {
    dispatch({
      type: REQUEST_AUTHORIZE_CONTRACT,
      payload: { contractType: location.state.contractType, contractId }
    })
  }

  const dispatchRevokeContract = contractId => {
    dispatch({
      type: REQUEST_REPROVE_CONTRACT,
      payload: { contractType: location.state.contractType, contractId }
    })
  }

  if (contractDetails.loading) {
    return <LoadingContainer />
  }

  return (
    <PendingContractDetailsContainer
      t={t}
      contractDetails={{...location.state.contractData, ...contractDetails}}
      propertyData={contractDetails.property}
      onAuthorize={dispatchAutorizeContract}
      onRevoke={dispatchRevokeContract}
      farmer={location.state.farmerMode}
      loadingAuth={contractDetails.investLoading}
      complete={contractDetails.investComplete}
      onSuccess={onSucessClick}
      hideAprove={hiddenAprove}
      // onError={onErrorClickInvest}
    />
  )
}

export default connect(mapStateToProps)(PendingContractDetails)
