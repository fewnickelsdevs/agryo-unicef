import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useHistory } from 'react-router-dom'
import { connect } from 'react-redux'

import InvestmentDetailsContainer from '../../../containers/Investments/Details/DetailsForInvestor'
import {
    INIT_WEB3CONTRACT_FOR_INVEST_REQUEST,
  RESET_VIEW_CONTRACT,
  CONTRACT_INVESTMENT_REQUEST,
  INVESTMENT_RESET
} from '../../../redux/contractDetails/constants'
import { REQUEST_WEB3_INVEST } from '../../../redux/investContracts/constants';
import { FETCH_WALLET } from '../../../redux/wallet/constants';

const mapStateToProps = state => ({
  user: {
    balanceAgryoStable: state.user.balanceAgryoStable
  },
  wallet: state.wallet,
  contractDetails: state.contractDetails
})

const ContractInvestmentDetails = ({ user, dispatch, contractDetails, wallet }) => {
  const { t } = useTranslation()
  const [noInvestInfo, setNoInvest] = useState(false)
  const location = useLocation()
  const history = useHistory()

  const dispatchContractInvestment = (contractType, contractId, amountToInvest ) => {
    console.log("REQUEST_WEB3_INVEST", { contractId, contractType, amountToInvest })
    dispatch({
      type: REQUEST_WEB3_INVEST,
      payload: { contractId, contractType, amountToInvest }
    })
  }

  const onSucessClickInvest = () => {
    dispatch({
      type: RESET_VIEW_CONTRACT,
      payload: {}
    })
    history.replace('/account/home')
  }

  const onErrorClickInvest = () => {
    dispatch({
      type: INVESTMENT_RESET,
      payload: {}
    })
  }

  useEffect(() => {

    if (!location.state.contractData && !location.state.contractId) {
      return history.push('/account/home')
    }
    const { contractData, contractId, noInvest } = location.state
    console.log("location state contract invest")
    if (noInvest) setNoInvest(true)
    dispatch({ type: FETCH_WALLET })
    dispatch({
      type: INIT_WEB3CONTRACT_FOR_INVEST_REQUEST,
      payload: {
        contractId: contractData.contractId,
        contractData
      }
    })

  }, [dispatch])

  useEffect(() => {
    return () => {
      dispatch({
        type: RESET_VIEW_CONTRACT
      })
    }
  }, [])

  return (
    <InvestmentDetailsContainer
      t={t}
      contract={{...location.state.contractData, contractId: location.state.contractId}}
      user={user}
      onInvest={dispatchContractInvestment}
      farmer={false}
      wallet={wallet}
      contractDetails={{...contractDetails, contractId: location.state.contractId}}
      onSuccess={onSucessClickInvest}
      onError={onErrorClickInvest}
      noInvest={false}
    />
  )
}

export default connect(mapStateToProps)(ContractInvestmentDetails)
