import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { FETCH_LIVESTOCK_CONTRACTS } from '../../../redux/investContracts/constants'
import LivestockInvestmentContainer from '../../../containers/Investments/Livestock'

const mapStateToProps = state => ({ investContracts: state.investContracts })

const LivestockInvest = ({ dispatch, investContracts }) => {
  const { t } = useTranslation()

  useEffect(() => {
    dispatch({
      type: FETCH_LIVESTOCK_CONTRACTS
    })
  }, [dispatch])

  return (
    <LivestockInvestmentContainer
      title={t('contracts.title-overview-livestock')}
      investContracts={investContracts}
      t={t}
    />
  )
}

export default connect(mapStateToProps)(LivestockInvest)
