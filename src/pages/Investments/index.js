import React from 'react'
import { useTranslation } from 'react-i18next'

import InvestmentsContainer from '../../containers/Investments'

export default function HomeInvest() {
  const { t } = useTranslation()

  return <InvestmentsContainer t={t} />
}
