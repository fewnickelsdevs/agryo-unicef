import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import UserInvestmentsListContainer from '../../../containers/Investments/InvestmentList/index'
import LoadingContainer from '../../../containers/Loading'
import { FETCH_INVESTOR_CONTRACTS_REQUEST } from '../../../redux/contracts-list/actions'

const mapStateToProps = state => ({ contracts: state.contracts.contracts })

const InvestmentList = ({ dispatch, contracts }) => {
  useEffect(() => {
    dispatch({ type: FETCH_INVESTOR_CONTRACTS_REQUEST })
  }, [])

  if (contracts.loading) {
    return <LoadingContainer />
  }

  return <UserInvestmentsListContainer contracts={contracts} />
}

export default connect(mapStateToProps)(InvestmentList)
