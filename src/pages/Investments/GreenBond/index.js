import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'

import { FETCH_GREENBOND_CONTRACTS } from '../../../redux/investContracts/constants'
import CropsInvestmentContainer from '../../../containers/Investments/Crops'

const mapStateToProps = state => ({ investContracts: state.investContracts })

const GreenBondInvestment = ({ dispatch, investContracts }) => {
  const { t } = useTranslation()
  const title = t('contracts.title-overview-green-bond')

  useEffect(() => {
    dispatch({
      type: FETCH_GREENBOND_CONTRACTS
    })
  }, [dispatch])

  return (
    <CropsInvestmentContainer
      title={title}
      investContracts={investContracts}
      t={t}
    />
  )
}

export default connect(mapStateToProps)(GreenBondInvestment)
