import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'

import ContractsListContainer from '../../containers/ContractsList'
import LoadingContainer from '../../containers/Loading'
import { FETCH_FARMER_CONTRACTS_REQUEST } from '../../redux/contracts-list/actions'

const mapStateToProps = state => ({ contracts: state.contracts })

const ContractsList = ({ dispatch, contracts }) => {
  const { t } = useTranslation()
  const history = useHistory()

  const navigateToNewContract = () => history.push('/account/contracts/new')

  useEffect(() => {
    dispatch({ type: FETCH_FARMER_CONTRACTS_REQUEST })
  }, [])

  if (contracts.loading) {
    return <LoadingContainer />
  }

  return <ContractsListContainer t={t} contracts={contracts.contracts} navigateToNewContract={navigateToNewContract} />
}

export default connect(mapStateToProps)(ContractsList)
