import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { createSelector } from 'reselect'
import { useLocation } from 'react-router-dom'

import ContractContainer from '../../containers/Contracts'
import { updateContract, resetContract } from '../../redux/contract/actions'
import {
  CROPS_CONTRACT_REQUEST,
  LIVESTOCK_CONTRACT_REQUEST,
  GREEN_BOND_REQUEST
} from '../../redux/contract/constants'
import { FETCH_PROPERTY_REQUEST } from '../../redux/property/actions'

const selectProperties = state => state.property

const fetchActiveProperties = createSelector(
  [selectProperties],
  properties => ({
    ...properties,
    data: properties.data.filter(property => property.properties.f3 === '1')
  })
)

const mapStateToProps = state => ({
  contract: state.contract,
  userProperties: fetchActiveProperties(state)
})

const mapDispatchToProps = {
  updateContract,
  resetContract,
  createCropsContract: (contract, farmerId) => ({
    type: CROPS_CONTRACT_REQUEST,
    payload: { contract, farmerId }
  }),
  createLivestockContract: (contract, farmerId) => ({
    type: LIVESTOCK_CONTRACT_REQUEST,
    payload: { contract, farmerId }
  }),
  createGreenBondContract: (contract, farmerId) => ({
    type: GREEN_BOND_REQUEST,
    payload: { contract, farmerId }
  }),
  fetchProperties: () => ({
    type: FETCH_PROPERTY_REQUEST
  })
}

function Contracts({
  userProperties,
  contract,
  resetContract,
  updateContract,
  createCropsContract,
  createLivestockContract,
  createGreenBondContract,
  fetchProperties,
  history
}) {
  const { t } = useTranslation()
  const location = useLocation()
  //const { propertiesData, farmerId } = location.state
  let propertiesData = false
  let farmerId = false
  if (location.state) {
    propertiesData = location.state.propertiesData
    farmerId = location.state.farmerId
  }
  
  const resetContractAndGoBack = () => {
    resetContract()
    history.replace('/account/home')
  }

  const closeErrorDialog = () => {
    updateContract({ error: false })
  }

  const createContract = () => {
    const isLivestockContract = contract.product >= 12
    const { isGreenBond } = contract

    if (isGreenBond) {
      createGreenBondContract(contract, farmerId)
      return
    }

    if (isLivestockContract) {
      createLivestockContract(contract, farmerId)
      return
    }

    createCropsContract(contract, farmerId)
    return
  }

  useEffect(() => {
    if (!propertiesData) {
      fetchProperties()
    }
  }, [])

  return (
    <ContractContainer
      t={t}
      showCheckContracts={propertiesData ? false : true}
      properties={userProperties || propertiesData}
      contract={contract}
      onUpdate={updateContract}
      onSuccess={resetContractAndGoBack}
      onError={closeErrorDialog}
      onCreateContract={createContract}
      pageHistory={history}
      showCheckContracts={false}
    />
  )
}

Contracts.propTypes = {
  contract: PropTypes.shape({
    product: PropTypes.number,
    property: PropTypes.shape({
      id: PropTypes.number
    }),
    technologyLevel: PropTypes.number,
    contractStarts: PropTypes.object,
    contractEnds: PropTypes.object,
    interest: PropTypes.number,
    loanValue: PropTypes.number,
    minimumContractValue: PropTypes.number,
    error: PropTypes.bool,
    success: PropTypes.bool,
    loading: PropTypes.bool
  }).isRequired,
  updateContract: PropTypes.func.isRequired,
  resetContract: PropTypes.func.isRequired,
  createCropsContract: PropTypes.func.isRequired,
  createLivestockContract: PropTypes.func.isRequired,
  history: PropTypes.shape({
    goBack: PropTypes.func
  }).isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(Contracts)
