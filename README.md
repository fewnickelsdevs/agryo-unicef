## Agryo - DAPP 

This repository contains the Agryo Dapp connected on Ethereum.

## Before you get started

We recomment the usage of Metamask Wallet to connect with the Rinkeby Network


## Env Setup


Our different applications init with the follow params on .env

**The is the main url of our API**

REACT_APP_API_URL=


**We have two options, the Farmer(false) and Invest Mode(true)**

REACT_APP_INVEST_MODE=false

**example:**

Init on Farmer Mode:
REACT_APP_INVEST_MODE=false

Init on Invest Mode:
REACT_APP_INVEST_MODE=true

## Folder structure

Our application is mainly splitted in 4 folders inside `/src`

- `/components`: All generic and reusable code goes here, such as buttons, inputs, popups/alerts, typography stuff and so on.
- `/containers`: The layout with JSX and CSS are stored here. The point here is to focus on the top layer on interaction, witch is the user interface, and less business logic such as connect to redux and whatever.
- `/pages`: The business logic of the containers are stored here. It passes the data fetched all the way down with props. **The page is responsible to fetch the data, while the container is responsible to display that data**
- `/redux`: Actions, reducers, constants (types) and sagas are stored here. It has a index file which is `store.js`, this one exports the function to initialize redux.

## Routes

- `/pages/LoggedArea`: You can access the routes of the farmer and investor on routes.js and the enterprise routes on enterpriseRoutes.js


## Useful scripts


### `yarn install || npm install`

Install the project dependencies.


### `yarn start:dev:farmer || npm run-script start:dev:farmer`

Starts the development farmer dApp.


### `yarn start:dev:invest || npm run-script start:dev:invest`

Starts the development invest dApp.

### `yarn build:invest || yarn build:farmer`

Builds the application to production. The bundle is saved on `/build` folder.