import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import { ThemeProvider as StyledComponents } from 'styled-components'
import GlobalStyles from '../src/theme/GlobalStyles'
import { colors, breakpoints } from '../src/theme'

addDecorator(story => (
  <StyledComponents theme={{ colors, breakpoints }}>
    <GlobalStyles />

    {story()}
  </StyledComponents>
))

// automatically import all files ending in *.stories.js
configure(
  require.context(
    '../src',
    true,
    /\.stories\.js$|\/stories\/.*\.js$|\/stories\/.*\.ts$|\/stories\/.*\.tsx$/
  ),
  module
)
