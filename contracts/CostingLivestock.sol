pragma solidity 0.5.13;

//Costing contracts - Financial instruments to support to produce livestock
/*
a) Financial contracts

Type of contracts - Custing/Livestock 
 
- Function: Create new contract
- - Inputs 
- - - Type: Custing/Livestock int
- - -  Farmer ID:  int
- - -  Farm cropfield ID:  int  
- - -  LivestockType: cattle / pork / chicken / goats / lamb / fish int
- - -  Technologic Level: I / II / III int      (I is the lowest value by hectare III is the highest)
- - -  Field hectares: hectares decimal               (3 decimals)
- - -  Number of Animals: int
- - -  Yield in Kg by Animal: tons by animal decimal               (3 decimals)
- - -  Expected amount in tons: tons decimal               (3 decimals)
- - -  Duration: Number of Months int 
- - -  Date Values:  year-month-day - days until end ex:2018/01/01-160  int
- - -  Value of contract: TraderX Stable Coins  int
- - -  Interest in the contract: points base in a year compound int
- - -  Value of contract: Minimum filling to validate the contract   int (between 50% to 100%, it is the choice of the farmer of how much he need to proceed) 
- - Actions
- - - Save new contract 
- - Outputs:
- - - Success: bool
- - - Contract ID: int

- Function: Fund contract with new contract
- - Inputs 
- - - Contract ID to fund: int
- - - Number of stable tokens to fund: int
- - Actions
- - - Validate the amount funded is enough (based in the definition of the farmer of how much he judge is enough to contract)
- - - Save contract has been funded
- - Outputs:
- - - Success: bool
*/

/*
    Overview
    - addLivestockContractInfo (farmer / field)
    - - getBasicInfo
    - - getFieldInfo
    - addLivestockContractDealInfo 
    - - getDealInfoDates
    - - getDealInfoFunding
 */

 /* States 
States:
- Created: By Crop-Field-Owner 
- Invested - Signed by each investor  
- Investment Accepted Signed by Crop-Field-Owner  (to receive the funds the Crop-Field-Owner need to accept the receive funds)
- Custing Payment sent Signed by Crop-Field-Owner (to finish the contract the crop-field-owner need to communicate the assets sent) 
- Custing Payment Signed by Investor (to receive the assets it needs to confirm receive it)
- Contract finished Signed by Crop-Field-Owner (the investors receive their funds only if they communicate the remittance to then) 
- Contract renegotiation Signed by Crop-Field-Owner and Investors (only with 100% aggrement can be renegotiated the contract)
- Raised for dispute? Signed By Crop-Field-Owner or Investor disputes only if the funds was not payed back or if the Investors disagree, Who start dispute is out of other investment rounds or investment opportunities

*/

import "./token-contract/IERC20.sol";

contract CostingLivestock {

    address public owner;
    address public tokenAddress;
    uint32 nextContractID;
    mapping (uint32 => ContractEntry) public contractData;
    mapping (uint32 => ContractStatus) public contractStatus;
    mapping (uint32 => mapping (address => uint256)) public investData;
    mapping (uint32 => address[]) public investorsList;


    enum ContractStatus {
        NOT_EXIST,
        WAITING_INVESTMENT,
        WAITING_PAYMENT,
        INVESTENT_COMPLETED,
        RAISED_FOR_DISPUTE,
        RESOLVED_DISPUTE
    }

    constructor(address _tokenAddress) public {
        owner = msg.sender;
        nextContractID = 0;
        tokenAddress = _tokenAddress;
    }

    modifier onlyOwner(){
        require(msg.sender == owner);
        _;
    }

    struct ContractEntry {
        //investment type is Type: Custing/Investment

        //Contract basic info
        uint32 propertyId;
        address farmer;

        //Field info
        uint8 livestockType;//livestockType: pork / cattle etc...
        uint8 technologyLevel; //Technologic Level: I / II / III int (I is the lowest value by hectare III is the highest)
        uint32 numberAnimals; //Number of animals (int)
        uint32 fieldHectares;//Acreage hectares: hectares decimal (3 decimals)
        uint32 yieldInKgByAnimal;//Yield in kg by Animal: tons by hectare decimal (3 decimals)

        //Date info
        uint256 dateStart;
        uint256 dateEnd;

        //Finance info
        uint256 fundedValue; //Value of contract set by farmer: TraderX Stable Coins  int
        uint256 paidAmount; //actual paid amount by investor : TraderX Stable Coins  int
        uint256 balanceAmount; //actual amount still in the contract: TraderX Stable Coins  int
        uint8 interest;//Interest in the contract: points base in a year compound int (1,5% = 150)
     
    }

    event addLivestockContractInfoEvent(uint32 contractID, address farmer);

    function addLivestockContractInfo(
        uint32 _propertyId,
        uint8 _livestockType,
        uint8 _technologyLevel,
        uint32 _numberAnimals,
        uint32 _fieldHectares,
        uint32 _yieldInKgByAnimal,
        uint256 _dateStart,
        uint256 _dateEnd,
        uint8 _interest,
        uint256 _fundedValue
    ) public returns (uint256) {
        contractData[nextContractID] = ContractEntry({
            propertyId: _propertyId,
            farmer: msg.sender,
            livestockType: _livestockType,
            technologyLevel: _technologyLevel,
            numberAnimals: _numberAnimals,
            fieldHectares: _fieldHectares,
            yieldInKgByAnimal: _yieldInKgByAnimal,
            dateStart: _dateStart,
            dateEnd: _dateEnd,
            fundedValue: _fundedValue,
            paidAmount: 0,
            balanceAmount: 0,
            interest: _interest
        });
        contractStatus[nextContractID] = ContractStatus.WAITING_INVESTMENT;
        emit addLivestockContractInfoEvent(nextContractID, msg.sender);
        nextContractID += 1;
        return nextContractID-1;
    }

    event livestockDepositTokenEvent(uint256 contractID, uint256 amount, address investor);

    modifier availableInvestment(uint32 contractId){
        require(contractStatus[contractId] == ContractStatus.WAITING_INVESTMENT, "Contract not available to invest");
        _;
    }

    modifier validateAmountToken(uint256 _amount, address _userAddress){
        require(_amount <= IERC20(tokenAddress).allowance(msg.sender, address(this)), "Not available balance");
        _;
    }

    modifier validateValueInvest(uint256 _amount, uint32 _contractID){
        require(_amount <= (contractData[_contractID].fundedValue-contractData[_contractID].paidAmount), "Value not available to invest");
        _;
    }

    function depositToken(uint32 _contractID, uint256 _amount) public
        validateAmountToken(_amount, msg.sender) availableInvestment(_contractID) validateValueInvest(_amount, _contractID)
        returns(uint256 _contractActualPaidAmount, uint8 _contractStatus) {
            
        if (investData[_contractID][msg.sender] == 0){
            investorsList[_contractID].push(msg.sender);
        }
        contractData[_contractID].paidAmount += _amount;
        contractData[_contractID].balanceAmount += _amount;
        investData[_contractID][msg.sender] += _amount;

        if(contractData[_contractID].paidAmount == contractData[_contractID].fundedValue){
            contractStatus[_contractID] = ContractStatus.WAITING_PAYMENT;
        }

        emit livestockDepositTokenEvent(_contractID, _amount, msg.sender);

        if (!IERC20(tokenAddress).transferFrom(msg.sender, address(this), _amount)) {
            revert(""); //make sure the transfer went ok
        }
        return ( contractData[_contractID].paidAmount, uint8(contractStatus[_contractID]) );
    }



    function getBasicInfo(uint32 _contractID)
        public view
        returns (
            uint _contractStatus,
            uint256 _dateStart,
            uint256 _dateEnd,
            uint256 fundedValue,
            uint8 interest,
            uint256 paidAmount,
            uint256 balanceAmount
        ) {
        return (
                uint(contractStatus[_contractID]),
                contractData[_contractID].dateStart,
                contractData[_contractID].dateEnd,
                contractData[_contractID].fundedValue,
                contractData[_contractID].interest,
                contractData[_contractID].paidAmount,
                contractData[_contractID].balanceAmount
        );

    }

    function getFieldInfo(uint32 _contractID)
        public view
        returns (
            uint32 _propertyId,
            address _farmer,
            uint8 _livestockType,
            uint8 _technologyLevel,
            uint32 _fieldHectares,
            uint32 _numberAnimals,
            uint32 _yieldInKgByAnimal
        ) {
        return (
            contractData[_contractID].propertyId,
            contractData[_contractID].farmer,
            contractData[_contractID].livestockType,
            contractData[_contractID].technologyLevel,
            contractData[_contractID].fieldHectares,
            contractData[_contractID].numberAnimals,
            contractData[_contractID].yieldInKgByAnimal
        );
    }



    function getInvestorsList(uint32 _contractId) public view returns(address[] memory _investors, uint256[] memory _investValue){
        uint256 size = investorsList[_contractId].length;
        address[] memory addressInvestors = new address[](size);
        uint256[] memory valueInvest = new uint256[](size);
        for(uint i = 0; i < size; i++) {
            addressInvestors[i] = (investorsList[_contractId][i]);
            valueInvest[i] = (investData[_contractId][investorsList[_contractId][i]]);
        }
        return(addressInvestors, valueInvest);
    }

    function getInvestorValue(address _investorAddress, uint32 _contractID) public view returns(uint){
        return investData[_contractID][_investorAddress];
    }

    function getStatus(uint32 _contractID) public view returns (uint) {
        return uint(contractStatus[_contractID]);
    }

}




