pragma solidity 0.5.13;

//Green Bond  - Financial instruments to support to green titles


import "./token-contract/IERC20.sol";


contract GreenBond{
    address public owner;
    address public tokenAddress;
    uint32 nextContractID;

    mapping (uint32 => address) public contractOwner;
    mapping (uint32 => GreenBondEntry) public contractData;
    mapping (uint32 => ContractStatus) public contractStatus;
    mapping (uint32 => mapping (address => uint256)) public investData;
    mapping (uint32 => address[]) public investorsList;


    enum ContractStatus {
        NOT_EXIST,
        WAITING_INVESTMENT,
        WAITING_PAYMENT,
        INVESTENT_COMPLETED,
        RAISED_FOR_DISPUTE,
        RESOLVED_DISPUTE
    }

    struct GreenBondEntry {
        //Contract basic info
        uint32 propertyId;
        address farmer;
        uint32 fieldHectares;//Acreage hectares: hectares decimal (3 decimals)

        //Date info
        uint256 dateStart;
        uint256 dateEnd;

        //Finance info
        uint256 fundedValue; //Value of contract set by farmer: TraderX Stable Coins  int
        uint256 valueToPay;
        uint256 paidAmount; //actual paid amount by investor : TraderX Stable Coins  int
        uint256 balanceAmount; //actual amount still in the contract: TraderX Stable Coins  int ((@@ where withdrawn run))
        uint32 interest;//Interest in the contract: points base in a year compound int (1,5% = 150)
    }

    constructor(address _tokenAddress) public {
        owner = msg.sender;
        nextContractID = 0;
        tokenAddress = _tokenAddress;
    }

    modifier onlyOwner(){
        require(msg.sender == owner);
        _;
    }

    modifier availableInvestment(uint32 contractId){
        require(contractStatus[contractId] == ContractStatus.WAITING_INVESTMENT, "Contract not available to invest");
        _;
    }

    event newGreenBondContract(uint32 contractID, address farmer);

    function newGreenBond(
        uint32 _propertyId,
        uint32 _fieldHectares,
        uint32 _interest,
        uint256 _dateStart,
        uint256 _dateEnd,
        uint256 _fundedValue,
        uint256 _valueToPay
    ) public returns (uint32) {
        contractData[nextContractID] = GreenBondEntry({
            propertyId: _propertyId,
            farmer: msg.sender,
            fieldHectares: _fieldHectares,
            dateStart: _dateStart,
            dateEnd: _dateEnd,
            fundedValue: _fundedValue,
            valueToPay: _valueToPay,
            paidAmount: 0,
            balanceAmount: 0,
            interest: _interest
        });
        contractStatus[nextContractID] = ContractStatus.WAITING_INVESTMENT;
        emit newGreenBondContract(nextContractID, msg.sender);
        nextContractID += 1;
        return nextContractID-1;
    }

    event greenBondDepositTokenEvent(uint256 contractID, uint256 amount, address investor);

    modifier validateAmountToken(uint256 _amount, address _userAddress){
        require(_amount <= IERC20(tokenAddress).allowance(msg.sender, address(this)), "Not available balance");
        _;
    }

    modifier validateValueInvest(uint256 _amount, uint32 _contractID){
        require(_amount <= (contractData[_contractID].fundedValue - contractData[_contractID].paidAmount), "Value not available to invest");
        _;
    }

    function depositToken(uint32 _contractID, uint256 _amount) public
        availableInvestment(_contractID) validateAmountToken(_amount, msg.sender) validateValueInvest(_amount, _contractID) 
        returns(uint256 _contractActualPaidAmount, uint8 _contractStatus) {
        
        if (investData[_contractID][msg.sender] == 0){
            investorsList[_contractID].push(msg.sender);
        }
        contractData[_contractID].paidAmount += _amount;
        contractData[_contractID].balanceAmount += _amount;
        investData[_contractID][msg.sender] += _amount;
       
        if(contractData[_contractID].paidAmount == contractData[_contractID].fundedValue){
            contractStatus[_contractID] = ContractStatus.WAITING_PAYMENT;
        }

        emit greenBondDepositTokenEvent(_contractID, _amount, msg.sender);

        if (!IERC20(tokenAddress).transferFrom(msg.sender, address(this), _amount)) {
            revert(""); //make sure the transfer went ok
        }
        return ( contractData[_contractID].paidAmount, uint8(contractStatus[_contractID]) );
    }

    function getGreenBondInfo(uint32 _contractId) public view
    returns(
        ContractStatus status,
        uint32 propertyId,
        address farmer,
        uint32 fieldHectares,
        uint256 dateStart,
        uint256 dateEnd
        ){
        return(
            contractStatus[_contractId],
            contractData[_contractId].propertyId,
            contractData[_contractId].farmer,
            contractData[_contractId].fieldHectares,
            contractData[_contractId].dateStart,
            contractData[_contractId].dateEnd

        );
    }

    function getGreenBondFinance(uint32 _contractId) public view
    returns(
        uint256 fundedValue,
        uint32 interest,
        uint256 paidAmount,
        uint256 balanceAmount
        ){
        return(
            contractData[_contractId].fundedValue,
            contractData[_contractId].interest,
            contractData[_contractId].paidAmount,
            contractData[_contractId].balanceAmount
        );
    }

    function getInvestorsList(uint32 _contractId) public view returns(address[] memory _investors, uint256[] memory _investValue){
        uint256 size = investorsList[_contractId].length;
        address[] memory addressInvestors = new address[](size);
        uint256[] memory valueInvest = new uint256[](size);
        for(uint i = 0; i < size; i++) {
            addressInvestors[i] = (investorsList[_contractId][i]);
            valueInvest[i] = (investData[_contractId][investorsList[_contractId][i]]);
        }
        return(addressInvestors, valueInvest);
    }

    function getUserInvestment(uint32 _contractId, address _userAddress) public view returns(uint256){
        return investData[_contractId][_userAddress];
    }

}