pragma solidity 0.5.13;

import "./ERC20.sol";
import "./ERC20Detailed.sol";


/**
 * @title SimpleToken
 * @dev Very simple ERC20 Token example, where all tokens are pre-assigned to the creator.
 * Note they can later distribute these tokens as they wish using `transfer` and other
 * `ERC20` functions.
 */
contract Token is ERC20, ERC20Detailed {
    
    uint8 public constant DECIMALS = 18;
    uint256 public constant INITIAL_SUPPLY = 500000000 * (10 ** uint256(DECIMALS));

    /**
     * @dev Constructor that gives msg.sender all of existing tokens.
     */
    constructor () public ERC20Detailed("AGRYO USD TESTNET", "AGRYO", DECIMALS) {
        _mint(msg.sender, INITIAL_SUPPLY);
    }
    
    /** 
     * @dev send to more than one wallet
     */
    // function multiSendToken(address[] memory _beneficiary, uint256 [] memory _value) public  {
    //     require(_beneficiary.length != 0, "Is not possible to send null value");
    //     require(_beneficiary.length == _value.length, "_beneficiary and _value need to have the same length");
    //     uint256 _length = _value.length;
    //     uint256 sumValue = 0;
    //     for(uint256 i = 0; i < _length; i++){
    //         sumValue = sumValue + _value[i];
    //     }
    //     require(balanceOf(msg.sender) >= sumValue, "Insufficient balance");
        
    //     for(uint256 i = 0; i < _length; i++){
    //         transfer(_beneficiary[i],_value[i]);
    //     }
    // }
    
}